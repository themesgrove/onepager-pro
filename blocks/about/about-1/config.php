<?php

return array(
  'slug'    => 'pro-about-1',
  'groups'    => array('about'),

  'contents' => array(
    array(
      'name'=>'title',
      'value'=>'About Us'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.'
    ),
    array('label'=>'Abouts', 'type'=>'divider'), // Divider - Text

    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'value' => 'About Info 1'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
        ),
        array(
          array('name'=>'title', 'value' => 'About Info 2'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
        ),
        array(
          array('name'=>'title', 'value' => 'About Info 2'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'),
        ),
        array(
          array('name'=>'title', 'value' => 'About Info 2'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'),
        ),
      )

    ),
    array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array('name'=>'link_bottom', 'label' => 'Type Button Text', 'type' => 'link', 'placeholder'=> home_url()),
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),
    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),
    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name'     => 'items_columns',
      'label'    => 'Items Columns',
      'type'     => 'select',
      'value'    => '2',
      'options'  => array(
        '1'   => '1',
        '2'   => '2',

      ),
    ),
    array(
      'name' => 'item_desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),
    array(
      'name'     => 'items_alignment',
      'label'    => 'Items Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),
    array(
      'name'     => 'items_animation',
      'label'    => 'Items Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'button_font_size',
      'label' => 'Font Size',
      'append' => 'px',
      'value' => '14'
    ),
    array(
      'name'     => 'button_transformation',
      'label'    => 'Text Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name'     => 'button_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),

  'styles' => array(
    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => 'http://try.getonepager.com/wp-content/uploads/2019/02/about2.jpg'
    ),
    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(255,255,255,0)'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),
    array(
      'name' => 'bg_color',
      'label' => 'Background Color',
      'type' => 'colorpicker',
      'value' => '#1f8fad'
    ),
    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),
    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),
    array('label'=>'About Style', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'item_desc_color',
      'label' => 'Abouts Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),
   array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'button_text_color',
      'label'   => 'Text Color',
      'type'    => 'colorpicker',
      'value'   => '#0089fd'
    ),
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),

    array(
      'name'    => 'button_border_color',
      'label'   => 'Border Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),

    array(
      'name' => 'button_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '50'
    ),
  ),

);
