<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:500"' : '';
?>


<section id="<?php echo $id;?>" class="uk-section uk-position-relative about pro-about-1 uk-background-cover uk-background-norepeat"  <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container-small uk-margin-auto uk-padding">
			<article class="uk-article uk-position-relative">
				<div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">	
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-text-' . $settings['title_transformation'],  
								$title_animation
							); 
						?>
					<?php endif; ?>
					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>
				</div> <!-- section-heading -->

				<div class="uk-grid-medium" uk-grid >
					<?php $i=4; ?>
					<?php foreach($contents['items'] as $about): ?>
						<div class="items uk-width-1-<?php echo $settings['items_columns'];?>@m uk-width-1-1@s">
						    <div class="uk-text-<?php echo $items_alignment;?>" 
							<?php echo ($settings['items_animation'])? $items_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '' ;?>>
								<?php if ($about['description']): ?>
									<!-- Item desc -->
									<p class="uk-text-medium uk-margin-small"><?php echo$about['description'];?></p>
								<?php endif; ?>
							</div><!-- uk-text-center -->
						</div><!-- uk-columns -->
					<?php $i++; endforeach; ?>
				</div> <!-- uk grid medium -->
				<?php if ($contents['link_bottom']): ?>
					<p class="uk-margin-medium-top" <?php echo $button_animation;?>>
						<!-- Link 1 -->
						<?php echo op_link($contents['link_bottom'], 'uk-text-bold uk-button uk-button-default uk-button-large uk-margin-small-right');?>
					</p>
				<?php endif; ?>
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
