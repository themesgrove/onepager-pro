#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}
#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +6 ?>px;
}

#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

#<?php echo $id; ?> .uk-button-primary{
	font-size : <?php echo $settings['button_font_size'];?>px;
	background: <?php echo $styles['button_bg_color'];?>;
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
	border-radius: 5px;
	color : <?php echo $styles['button_text_color'];?>;
	text-transform:<?php echo $settings['button_transformation'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-button-primary:hover{
	background : <?php echo $styles['button_text_color'];?>;
	color : <?php echo $styles['button_bg_color'];?>;
	border-color : <?php echo $styles['button_text_color'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
}