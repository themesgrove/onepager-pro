<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:500"' : '';
?>

<section id="<?php echo $id;?>" class="uk-section uk-position-relative hero pro-hero-8 uk-background-cover uk-background-norepeat" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section-small">
		<div class="uk-container">
			<div class="uk-grid-match uk-flex-middle uk-padding-large" uk-grid>
				<div class="uk-width-1-2@m uk-width-1-1@s">
					<article class="uk-article uk-position-relative uk-margin-auto">
						<div class="section-heading uk-margin-medium-bottom">	
							<?php if($contents['title']):?>
								<!-- Section Title -->
								<?php
									echo op_heading( 
										$contents['title'],
										$settings['heading_type'], 
										'uk-heading-primary  uk-margin-remove', 
										'uk-text-' . $settings['title_transformation'], 
										$title_animation . '"'
									); 
								?>
							<?php endif; ?>

							<?php if($contents['description']):?>
								<!-- Section Sub Title -->
								<p class="uk-text-lead uk-margin-medium" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
									<?php echo $contents['description'];?>
								</p>
							<?php endif; ?>
							<?php if ($contents['link']): ?>
								<p <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : '' ;?>>
										<!-- Link -->
									<?php echo op_link($contents['link'], 'uk-margin-top uk-button uk-button-primary uk-button-large');?>
								</p>
							<?php endif;?>
						</div> <!-- section-heading -->
					</article> <!-- uk-article -->
				</div> <!-- uk-width -->
				<div class="uk-width-1-2@m "></div>

			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
