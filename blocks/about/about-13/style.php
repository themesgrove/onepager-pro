#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-weight:<?php echo $settings['title_font_weight'];?>;
}
#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['section_title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id;?> .op-icon{
	font-size: 13px;
    background: #d6e1fa;
    border-radius: 103px;
    color: #0e4de3;
    height: 25px;
    width: 25px;
    line-height: 25px;
    text-align: center;
}
#<?php echo $id;?> i.op-icon.fa{
    margin-right: 8px;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
}