<?php
	// Animations
	$media_animation = ( $settings['media_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['media_animation'] . ';"' : '';
	$title_animation = ( $settings['title_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['title_animation'] . ';"' : '';
	$items_animation = ( $settings['items_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['items_animation'] . ';"' : '';
	// Alignment
	$title_alignment = ( $settings['title_alignment'] ) ? $settings['title_alignment'] : '';
?>

<section id="<?php echo $id;?>" class="uk-section uk-position-relative about pro-about-13 uk-background-cover uk-background-norepeat" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section-small">
		<div class="uk-container">
			<div class="uk-grid-match uk-flex-middle" uk-grid>
				<div class="uk-width-1-2@m <?php echo $contents['column_reverse'] == true ? 'uk-padding-large uk-padding-remove-right uk-padding-remove-top uk-flex-last' : 'uk-padding-large uk-padding-remove-left uk-padding-remove-top' ?>">
					<?php if ($contents['media']): ?>
						<img <?php echo ($settings['media_animation'])? $media_animation . ';delay:' .'300"' : '' ;?> src="<?php echo $contents['media'];?>">
					<?php endif; ?>
				</div>
				<div class="uk-width-1-2@m">
					<article class="uk-article uk-position-relative uk-margin-auto">
						<div class="section-heading uk-text-<?php echo $title_alignment; ?>">	
							<?php if ( $contents['title'] ) : ?>
							<!-- Section Title -->
							  	<?php 
									echo op_heading(
										$contents['title'],
										$settings['heading_type'],
										'uk-heading-primary uk-margin-medium-bottom uk-text-'.$settings['title_transformation'],
										$title_animation
									); 
								?>
							<?php endif; ?>

							<ul class="uk-list">
								<?php $i=4; ?>
								<?php foreach($contents['items'] as $feature): ?>
									<?php if ($feature['desc']): ?>
											<li class="uk-margin-medium-bottom" <?php echo ($settings['title_animation'])? $title_animation . ';delay:' .'300"' : '' ;?>><?php if($feature['icon']) : ?><i class="op-icon <?php echo $feature['icon']; ?>"></i><?php endif; ?><?php echo $feature['desc'];?></li>
									<?php endif; ?>									    	
								<?php $i++; endforeach; ?>
							</ul>
	
						</div> <!-- section-heading -->
					</article> <!-- uk-article -->
				</div> <!-- uk-width -->
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
