#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-weight:<?php echo $settings['title_font_weight'];?>;
}
#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['section_title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}
#<?php echo $id; ?> .uk-button-text::before{
	border-color : <?php echo $styles['button_text_color']?>;
}

#<?php echo $id; ?> .op-link i::before{
	color : <?php echo $styles['button_text_color']?>;
}

#<?php echo $id; ?> .uk-button{
	color : <?php echo $styles['button_text_color']?>;
}

#<?php echo $id; ?> .uk-button:hover{
	color : <?php echo $styles['button_hover_color']?>;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
}