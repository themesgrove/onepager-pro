<?php
	// Animations
	$media_animation = ( $settings['media_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['media_animation'] . ';"' : '';
	$title_animation = ( $settings['title_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['title_animation'] . ';"' : '';
	// Alignment
	$title_alignment = ( $settings['title_alignment'] ) ? $settings['title_alignment'] : '';
?>

<section id="<?php echo $id;?>" class="uk-section uk-position-relative about pro-about-14 uk-background-cover uk-background-norepeat" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section-small">
		<div class="uk-container">
			<div class="uk-grid-match uk-flex-middle" uk-grid>
				<div class="uk-width-1-2@m <?php echo $contents['column_reverse'] == true ? 'uk-padding-large uk-padding-remove-right uk-padding-remove-top uk-flex-last' : 'uk-padding-large uk-padding-remove-left uk-padding-remove-top' ?>">
					<?php if ($contents['media']): ?>
						<img <?php echo ($settings['media_animation'])? $media_animation . ';delay:' .'300"' : '' ;?> src="<?php echo $contents['media'];?>">
					<?php endif; ?>
				</div>
				<div class="uk-width-1-2@m">
					<article class="uk-article uk-position-relative uk-margin-auto">
						<div class="section-heading uk-text-<?php echo $title_alignment; ?>">	
							<?php if ( $contents['title'] ) : ?>
							<!-- Section Title -->
							  	<?php 
									echo op_heading(
										$contents['title'],
										$settings['heading_type'],
										'uk-heading-primary uk-margin-medium-bottom uk-text-'.$settings['title_transformation'],
										$title_animation
									); 
								?>
							<?php endif; ?>

							<?php if ( $contents['description'] ) : ?>
							  	<?php echo $contents['description'];?>
							<?php endif; ?>

							<?php if ($contents['link']): ?>
							<div class="op-link uk-margin-top" <?php echo ($settings['title_animation'])? $title_animation . ';delay:' .'500"' : '' ;?>>
									<!-- Link -->
								<?php echo op_link($contents['link'], 'uk-button uk-button-text uk-button-large');?> <i class="fa fa-angle-double-right" aria-hidden="true"></i>
							</div>

						<?php endif;?>

	
						</div> <!-- section-heading -->
					</article> <!-- uk-article -->
				</div> <!-- uk-width -->
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
