<?php

return array(
  'slug'    => 'pro-about-15',
  'groups'    => array('about'),

  'contents' => array(
    array('name'=>'column_reverse', 'label'=>'Column Reverse', 'type'=>'switch'),
    array(
      'name'  =>'media',
      'type'  => 'image',
      'label'=>'Image',
      'value' =>'https://demo.wponepager.com/wp-content/uploads/2019/08/about-14-b.png'
    ),
    array(
      'name'=>'sub_title',
      'label'=>'Sub Title',
      'type'=>'text',
      'value' => 'SUPPORT'
    ),
    array(
      'name'=>'title',
      'type'  =>'textarea',
      'value'=>'Manage from one place'
    ),

    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => "StartupOne CRM tracks the client interactions autonomously - whether they are in a call, acroos social media, or an email. <br> Synch with Outlook, Gmail, or with any other of your favorite service and capture every email, call, or meetings as it takes place. "
    ),
    array('name'=>'link', 'text' => 'Learn More', 'type' => 'link'),

  ),

    'settings' => array(
        array('label' => 'Media', 'type' => 'divider'), // Divider - Text
        array(
            'name'    => 'media_animation',
            'label'   => 'Animation',
            'type'    => 'select',
            'value'   => '0',
            'options' => array(
                '0'                   => 'None',
                'fade'                => 'Fade',
                'scale-up'            => 'Scale Up',
                'scale-down'          => 'Scale Down',
                'slide-top-small'     => 'Slide Top Small',
                'slide-bottom-small'  => 'Slide Bottom Small',
                'slide-left-small'    => 'Slide Left Small',
                'slide-right-small'   => 'Slide Right Small',
                'slide-top-medium'    => 'Slide Top Medium',
                'slide-bottom-medium' => 'Slide Bottom Medium',
                'slide-left-medium'   => 'Slide Left Medium',
                'slide-right-medium'  => 'Slide Right Medium',
                'slide-top'           => 'Slide Top 100%',
                'slide-bottom'        => 'Slide Bottom 100%',
                'slide-left'          => 'Slide Left 100%',
                'slide-right'         => 'Slide Right 100%',

            ),

        ),
        array('label' => 'Heading', 'type' => 'divider'), // Divider - Text
        array(
            'name'    => 'heading_type',
            'label'   => 'Heading Type',
            'type'    => 'select',
            'value'   => 'h1',
            'options' => array(
                'h1' => 'h1',
                'h2' => 'h2',
                'h3' => 'h3',
                'h4' => 'h4',
                'h5' => 'h5',
                'h6' => 'h6',
            ),
        ),
        array(
            'name'   => 'section_title_size',
            'label'  => 'Title Size',
            'append' => 'px',
            'value'  => '@section_title_size',
        ),
        array(
            'name'    => 'title_font_weight',
            'label'   => 'Font Weight',
            'type'    => 'select',
            'value'   => '700',
            'options' => array(
                '400' => '400',
                '500' => '500',
                '600' => '600',
                '700' => '700',
            ),
        ),
        array(
            'name'    => 'title_transformation',
            'label'   => 'Title Transformation',
            'type'    => 'select',
            'value'   => 'inherit',
            'options' => array(
                'inherit'    => 'Default',
                'lowercase'  => 'Lowercase',
                'uppercase'  => 'Uppercase',
                'capitalize' => 'Capitalized',
            ),
        ),

        array(
            'name'    => 'title_alignment',
            'label'   => 'Title Alignment',
            'type'    => 'select',
            'value'   => 'left',
            'options' => array(
                'left'    => 'Left',
                'center'  => 'Center',
                'right'   => 'Right',
                'justify' => 'Justify',
            ),
        ),

        array(
            'name'    => 'title_animation',
            'label'   => 'Animation',
            'type'    => 'select',
            'value'   => '0',
            'options' => array(
                '0'                   => 'None',
                'fade'                => 'Fade',
                'scale-up'            => 'Scale Up',
                'scale-down'          => 'Scale Down',
                'slide-top-small'     => 'Slide Top Small',
                'slide-bottom-small'  => 'Slide Bottom Small',
                'slide-left-small'    => 'Slide Left Small',
                'slide-right-small'   => 'Slide Right Small',
                'slide-top-medium'    => 'Slide Top Medium',
                'slide-bottom-medium' => 'Slide Bottom Medium',
                'slide-left-medium'   => 'Slide Left Medium',
                'slide-right-medium'  => 'Slide Right Medium',
                'slide-top'           => 'Slide Top 100%',
                'slide-bottom'        => 'Slide Bottom 100%',
                'slide-left'          => 'Slide Left 100%',
                'slide-right'         => 'Slide Right 100%',

            ),

        ),

    ),

  'styles' => array(
    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => ''
    ),
    array(
      'name' => 'bg_color',
      'label' => 'Background Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(255,255,255,0)'
    ),

    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#212121'

    ),
    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#212121'

    ),

   array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'button_text_color',
      'label'   => 'Text Color',
      'type'    => 'colorpicker',
      'value'   => '#212121'
    ),
    array(
      'name'    => 'button_hover_color',
      'label'   => 'Hover Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Hover Background Color',
      'type'    => 'colorpicker',
      'value'   => '#ff5e49'
    ),
    array(
      'name'    => 'button_shadow_color',
      'label'   => 'Hover Shadow Color',
      'type'    => 'colorpicker',
      'value'   => 'rgba(255,94,73,0.34)'
    ),
  ),

);
