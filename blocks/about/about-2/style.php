#<?php echo $id ?>{
	background-color : <?php echo $styles['bg_color'] ?>;
	
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}
#<?php echo $id ?> .uk-background-secondary{
	background-color:<?php echo $styles['content_bg_color']?>;
}

#<?php echo $id ?> .uk-heading-primary {
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}
#<?php echo $id ?> .uk-heading-primary a,
#<?php echo $id ?> .uk-heading-primary a:hover{
	color : <?php echo $styles['title_link_color']?>;
	text-decoration:none;
}

#<?php echo $id ?> .uk-text-lead {
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['text_color']?>;
}

#<?php echo $id ?> .uk-button-primary{
	background: <?php echo $styles['button_bg_color']?>;
	border:1px solid <?php echo $styles['button_bg_color']?>;
	color : <?php echo $styles['button_text_color']?>;
	border-radius:<?php echo $settings['border_radius'];?>px;
	font-size:<?php echo $settings['button_size'];?>px;
	text-transform:<?php echo $settings['button_transformation'];?>;
}

#<?php echo $id ?> .uk-button-primary:hover{
	background: <?php echo $styles['button_text_color']?>;
	color : <?php echo $styles['button_bg_color']?>;
	border-color : <?php echo $styles['button_text_color']?>;
}

.uk-container-item-padding-large-remove-right{
	margin-right:-100px;
}


@media(max-width:768px){
	#<?php echo $id?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?>  .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}