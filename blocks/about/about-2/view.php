<?php
	// animation repeat
	$animation_repeat = '';
	// media grid
	$media_grid = 'uk-'. $settings['media_grid'] . '@m';

	// Animation media
	$animation_media = ($settings['animation_media']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_media'] .';repeat:' . ($animation_repeat ? 'true' : 'false') . '"' : '';
	// animation content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';

	// Text transformation class
	$heading_class = ($settings['title_transformation']) ? 'uk-text-' . $settings['title_transformation'] : '';

?>

<section id="<?php echo $id; ?>" class="about pro-about-2 uk-cover-container fp-section">
	<div class="uk-container-large">
		<article class="uk-grid-large" uk-grid>
			<!-- Media -->
			<div class="<?php echo $media_grid?> uk-width-1-1@s uk-grid-item-match uk-flex-middle">
				<div class="uk-panel uk-margin-top uk-container-item-padding-large-remove-right" <?php echo $animation_media;?>>
					<img 
						width="<?php echo $settings['media_size']?>" 
						src="<?php echo $contents['image']?>" uk-image>
				</div>
			</div>
			
			<div class="uk-width-expand@m uk-background-secondary uk-grid-item-match uk-flex-middle">
				<div class="uk-panel uk-padding">
					<!-- Title -->
					<?php if($contents['title']): ?>
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$animation_content . '"'
							); 
						?>
					<?php endif;?>
					<!-- Description -->
					<?php if($contents['description']): ?>
						<div class="uk-text-lead" 
							<?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' . ($animation_repeat ? 'true': 'false') . ';delay:' . '300"' : ''); ?>>
							<?php echo $contents['description']?>	
						</div>
					<?php endif; ?>
					<p <?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' . ($animation_repeat ? 'true': 'false') . ';delay:' . '400"' : ''); ?>>
						<!-- Link -->
						<?php echo op_link($contents['link'], 'uk-button-large uk-margin-medium-top uk-button uk-button-primary');?>
					</p>
				</div>	<!-- uk-panel -->
			</div> <!-- uk-grid-item-match -->
		</article> <!-- uk-article --> 
	</div> <!-- uk-section-large -->
</section> <!-- end-section --> 