#<?php echo $id;?> .uk-background-secondary{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
}


<?php if ($styles['item_icon_color']): ?>
	#<?php echo $id;?> .op-media{
		color : <?php echo $styles['item_icon_color']?>;
	}
<?php endif; ?>

#<?php echo $id;?> .uk-text-medium{
	font-size : <?php echo $settings['item_text_size']?>px;
	color : <?php echo $styles['item_text_color']?>;
}

#<?php echo $id; ?> .video-lightbox a{
    border: 3px solid <?php echo $styles['title_color'];?>;
    border-radius: 50px;
    width: 80px;
    height: 80px;
    line-height: 73px;
    font-size: 30px;
    text-align: center;
    color: <?php echo $styles['title_color'];?>;
}
#<?php echo $id; ?> .video-lightbox a:hover i{
	color: <?php echo $styles['icon_hover_color'];?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

#<?php echo $id; ?> .uk-button-default{
	font-size : <?php echo $settings['button_font_size'];?>px;
	background: <?php echo $styles['button_bg_color'];?>;
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
	color : <?php echo $styles['button_text_color'];?>;
	text-transform:<?php echo $settings['button_transformation'];?>;
	border-radius:<?php echo $styles['button_border_radius'];?>px;
}
#<?php echo $id; ?> .uk-button-default:hover{
	background : <?php echo $styles['button_text_color'];?>;
	color : <?php echo $styles['button_bg_color'];?>;
	border-color : <?php echo $styles['button_text_color'];?>;
}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
}