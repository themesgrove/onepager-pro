<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:500"' : '';
?>


<section id="<?php echo $id;?>" class="uk-section uk-position-relative about pro-about-4 uk-background-cover uk-background-norepeat"  <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section-small">
		<div class="uk-container-expand">
			<div class="uk-grid-match uk-flex-middle" uk-grid>
				<div class="uk-width-1-2@m ">
					<div class="uk-margin-medium-top video-lightbox uk-position-relative" uk-lightbox>
					    <div class="uk-card" <?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>
					        <div class="uk-grid-medium uk-flex-center" uk-grid>
					            <div class="uk-text-center">
					            	<a class="uk-button" href="<?php echo $contents['video_link'];?>"><i class="fa fa-play"></i></a>
					            </div>
					        </div> <!-- uk-flex -->
						</div> <!-- uk-card -->
					</div> <!-- uk-lightbox -->
				</div>

				<div class=" uk-width-1-2@m uk-width-1-1@s">
					<article class="uk-article uk-position-relative uk-margin-auto uk-padding uk-background-secondary">
						<div class="section-heading uk-margin-medium-bottom">	
							<?php if($contents['title']):?>
								<!-- Section Title -->
								<?php
									echo op_heading( 
										$contents['title'],
										$settings['heading_type'], 
										'uk-heading-primary uk-margin-remove-top', 
										'uk-text-' . $settings['title_transformation'], 
										$title_animation . '"'
									); 
								?>
							<?php endif; ?>
							<?php if ($contents['title_image']): ?>
								<img src="<?php echo $contents['title_image'];?>">
							<?php endif; ?>
							<?php if($contents['description']):?>
								<!-- Section Sub Title -->
								<p class="uk-text-lead uk-margin-medium" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
									<?php echo $contents['description'];?>
								</p>
							<?php endif; ?>
						</div> <!-- section-heading -->

						<div uk-grid >
							<?php $i=4; ?>
							<?php foreach($contents['items'] as $about): ?>
								<div class="items uk-width-1-2@m uk-width-1-2@s uk-margin-small uk-margin-auto-top">
									<div class="uk-card" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '' ;?>>
										<div class="uk-card-header uk-padding-remove">
											<div class="uk-grid-small uk-flex-middle" uk-grid>
												<?php if ($about['item_text']): ?>
													<div class="uk-width-auto">
														<span class="op-media <?php echo $about['item_icon'];?>"></span>
													</div>
												<?php endif; ?>
												<div class="uk-width-expand">
													<?php if ($about['item_text']): ?>
														<!-- Item desc -->
														<p class="uk-text-medium uk-margin-small"><?php echo$about['item_text'];?></p>
													<?php endif; ?>
												</div>
											</div>
										</div> <!-- uk-card-header -->
									</div>
								</div>
							<?php $i++; endforeach; ?>
							<?php if ($contents['link_bottom']): ?>
								<p class="uk-margin-medium-top" <?php echo $button_animation;?>>
									<!-- Link 1 -->
									<?php echo op_link($contents['link_bottom'], 'uk-text-bold uk-button uk-button-default uk-button-large uk-margin-small-right');?>
								</p>
							<?php endif; ?>
						</div> <!-- uk-grid-medium -->
					</article> <!-- uk-article -->
				</div> <!-- uk-width -->
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
