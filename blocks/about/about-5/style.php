#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-text-large{
	font-size : <?php echo $settings['sub_title_size']?>px;
	color : <?php echo $styles['desc_color']?>;
	line-height : <?php echo ($settings['sub_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?> .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
	line-height : <?php echo ($settings['desc_size']) +12 ?>px;
}

#<?php echo $id;?> .uk-button-primary{
	font-size : <?php echo $settings['button_font_size']?>px;
	color : <?php echo $styles['button_text_color']?>;
	background: <?php echo $styles['button_bg_color']?>;
    border-radius: <?php echo $settings['button_radius']?>px;
    border: 1px solid <?php echo $styles['button_bg_color']?>;
 }

#<?php echo $id;?> .uk-button-primary:hover{
	color : <?php echo $styles['button_bg_color']?>;
	background: <?php echo $styles['button_text_color']?>;
    border: 1px solid <?php echo $styles['button_bg_color']?>;
 }



@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}