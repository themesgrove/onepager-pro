<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// media animation
	$media_animation = ($settings['media_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['media_animation']. ';repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'400"' : '';
?>


<section id="<?php echo $id;?>" class="about pro-about-5">
	<div class="uk-section">
		<div class="uk-container uk-padding">
			<div class="uk-flex-middle uk-flex" uk-grid>
				<article class="uk-article uk-position-relative uk-width-1-2@m">
					<div class="section-heading uk-margin-bottom">	
					<?php if($contents['sub_title']):?>
						<!-- Section Sub Title -->
						<h4 class="uk-text-large uk-margin-small" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
							<?php echo $contents['sub_title'];?>
						</h4>
					<?php endif; ?>
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-remove-top', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>
					</div> <!-- section-heading -->
					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead uk-margin" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>
					<?php if ($contents['link']): ?>
						<p <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : '' ;?>>
								<!-- Link -->
							<?php echo op_link($contents['link'], 'uk-margin-top uk-button uk-button-primary uk-button-large');?>
						</p>
					<?php endif;?>
				</article> <!-- uk-article -->
				<?php if ($contents['image']): ?>
					<div class=" uk-width-1-2@m uk-width-1-1@s" <?php echo $media_animation;?>>
						<img src="<?php echo $contents['image'];?>">
					</div> <!-- uk-width-1-4@m-->
				<?php endif ?>
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
