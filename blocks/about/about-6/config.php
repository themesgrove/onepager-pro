<?php

return array(
  'slug'    => 'pro-about-6',
  'groups'    => array('about'),

  'contents' => array(
    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'A combination of advanced content importers, layouts, header styles, post types and elements gives you creative freedom and control unlike any other theme you’ve seen before.'
    ),
    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '66'),
          array('name'=>'suffix', 'label' => 'Type Suffix','value' => 'k'),
          array('name'=>'title', 'value' => 'Project Completed')
        ),
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '12'),
          array('name'=>'suffix', 'label' => 'Type Suffix','value' => 'm'),
          array('name'=>'title', 'value' => 'Project Plan')
        ),
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '10'),
          array('name'=>'suffix', 'label' => 'Type Suffix','value' => ''),
          array('name'=>'title', 'value' => 'Years Experience')
        )
      )

    )
  ),

  'settings' => array(
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
      array(
        'name' => 'title_size',
        'label' => 'Title Size',
        'append' => 'px',
        'value' => '24'
      ),
      array(
        'name' => 'title_font',
        'type' => 'font', 
        'label' => 'Title Fonts'
      ),
      array(
        'name'     => 'title_font_weight',
        'label'    => 'Font Weight',
        'type'     => 'select',
        'value'    => '500',
        'options'  => array(
          '400'  => '400',
          '500'   => '500',
          '600'   => '600',
          '700'   => '700',
        ),
      ),
      
      array(
        'name'     => 'title_alignment',
        'label'    => 'Alignment',
        'type'     => 'select',
        'value'    => 'center',
        'options'  => array(
          'left'      => 'Left',
          'center'    => 'Center',
          'right'     => 'Right',
          'justify'   => 'Justify',
        )
      ),

      array(
        'name'     => 'title_animation',
        'label'    => 'Animation',
        'type'     => 'select',
        'value'    => '0',
        'options'  => array(        
          '0'                     =>  'None',
          'fade'                  =>  'Fade',
          'scale-up'              =>  'Scale Up',
          'scale-down'            =>  'Scale Down',
          'slide-top-small'       =>  'Slide Top Small',
          'slide-bottom-small'    =>  'Slide Bottom Small',
          'slide-left-small'      =>  'Slide Left Small',
          'slide-right-small'     =>  'Slide Right Small',
          'slide-top-medium'      =>  'Slide Top Medium',
          'slide-bottom-medium'   =>  'Slide Bottom Medium',
          'slide-left-medium'     =>  'Slide Left Medium',
          'slide-right-medium'    =>  'Slide Right Medium',
          'slide-top'             =>  'Slide Top 100%',
          'slide-bottom'          =>  'Slide Bottom 100%',
          'slide-left'            =>  'Slide Left 100%',
          'slide-right'           =>  'Slide Right 100%'
  
        ),
      ),
      array('name'=>'animation_repeat', 'label'=>'Animation Repeat', 'type'=>'switch'),

      array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text
      array(
        'name'     => 'items_columns',
        'label'    => 'Items Columns',
        'type'     => 'select',
        'value'    => '3',
        'options'  => array(
          '2'   => '2',
          '3'   => '3',
          '4'   => '4',
  
        ),
      ),
      array(
        'name' => 'counter_size',
        'label' => 'Counter Size',
        'append' => 'px',
        'value' => '42'
      ),

      array(
        'name' => 'counter_title_size',
        'label' => 'Counter Title Size',
        'append' => 'px',
        'value' => '20'
      ),
      array(
        'name'     => 'counter_title_transformation',
        'label'    => 'Transformation',
        'type'     => 'select',
        'value'    => 'inherit',
        'options'  => array(
          'inherit'   => 'Default',
          'lowercase'   => 'Lowercase',
          'uppercase'   => 'Uppercase',
          'capitalize'  => 'Capitalized'
        ),
      ),
      array(
        'name'     => 'counter_alignment',
        'label'    => 'Alignment',
        'type'     => 'select',
        'value'    => 'center',
        'options'  => array(
          'left'      => 'Left',
          'center'    => 'Center',
          'right'     => 'Right',
          'justify'   => 'Justify',
        )
      ),

      array(
        'name'     => 'animation_item',
        'label'    => 'Animation Items',
        'type'     => 'select',
        'value'    => '0',
        'options'  => array(        
          '0'                     =>  'None',
          'fade'                  =>  'Fade',
          'scale-up'              =>  'Scale Up',
          'scale-down'            =>  'Scale Down',
          'slide-top-small'       =>  'Slide Top Small',
          'slide-bottom-small'    =>  'Slide Bottom Small',
          'slide-left-small'      =>  'Slide Left Small',
          'slide-right-small'     =>  'Slide Right Small',
          'slide-top-medium'      =>  'Slide Top Medium',
          'slide-bottom-medium'   =>  'Slide Bottom Medium',
          'slide-left-medium'     =>  'Slide Left Medium',
          'slide-right-medium'    =>  'Slide Right Medium',
          'slide-top'             =>  'Slide Top 100%',
          'slide-bottom'          =>  'Slide Bottom 100%',
          'slide-left'            =>  'Slide Left 100%',
          'slide-right'           =>  'Slide Right 100%'
  
        ),
      ),
  ),

  'styles' => array(
    array('label'=> 'Background' , 'type'=> 'divider'),
    array(
      'name'  => 'bg_color',
      'label' => 'Background Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),
    array('label'=> 'Title' , 'type'=> 'divider'),
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#192431'

    ),

    array('label'=> 'Counters' , 'type'=> 'divider'),

    array(
      'name' => 'counter_color',
      'label' => 'Counter Color',
      'type' => 'colorpicker',
      'value' => '@color.primary'

    ),

    array(
      'name' => 'counter_title_color',
      'label' => 'Counter Title Color',
      'type' => 'colorpicker',
      'value' => '@color.primary'

    ),

  ),
);
