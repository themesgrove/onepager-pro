#<?php echo $id?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']+10);?>px;
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id?> .section-heading:before {
    position: absolute;
    content: "";
    border: 1px solid <?php echo $styles['title_color']?>;
    border-bottom: 1px solid transparent;
    border-right: 1px solid transparent;
	width: 10%;
    height: 50%;
    left: 0;
    right: 0;
	margin: -20px;
	opacity: 0.1;
}

#<?php echo $id?> .section-heading:after {
	position: absolute;
    content: "";
    border: 1px solid <?php echo $styles['title_color']?>;
    border-top: 1px solid transparent;
    border-left: 1px solid transparent;
    width: 10%;
    height:50%;
    bottom: 0;
    right: 0;
	margin: -20px;
	opacity: 0.1;
}
#<?php echo $id?> .counter-item .counter{
	font-size : <?php echo $settings['counter_size']?>px;
	color : <?php echo $styles['counter_color']?>;
	line-height : <?php echo ($settings['counter_size']+8);?>px;
}

#<?php echo $id?> .counter-item .count-title{
	font-size : <?php echo $settings['counter_title_size']?>px;
	color : <?php echo $styles['counter_title_color']?>;
	line-height : <?php echo ($settings['counter_title_size']+8);?>px;
	
}


@media (max-width: 768px){
	#<?php echo $id?> .counter-item .counter{
	font-size : <?php echo ($settings['counter_size']-6);?>px;
	line-height : <?php echo ($settings['counter_size']+3);?>px;
	}

	#<?php echo $id?> .counter-item .count-title{
		font-size : <?php echo ($settings['counter_title_size']-6)?>px;
		line-height : <?php echo ($settings['counter_title_size']+5);?>px;
		
	}
	#<?php echo $id?> .section-heading .uk-text-lead{
		font-size : <?php echo ($settings['title_size']-2);?>px;
		line-height : <?php echo ($settings['title_size']+8);?>px;
	}

}