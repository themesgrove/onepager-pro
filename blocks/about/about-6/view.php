<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// Items animation
	$animation_item = ($settings['animation_item']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_item'].';repeat:true;' : '';
	
	// wp_enqueue_script('jquery-inview', ONEPAGER_PRO_URL . 'assets/js/jquery.inview.min.js', ['jquery']);
?>
<script src="//cdn.jsdelivr.net/jquery.inview/0.2/jquery.inview.min.js"></script>
    <!-- Count Down section start -->
	<section id="<?php echo $id?>" class="uk-section-large counter about pro-about-6">
	    <div class="uk-container-small uk-margin-auto">
			<div class="section-heading uk-padding-small uk-width-1-1@m uk-width-1-1@l uk-width-1-1@s uk-position-relative  uk-margin-large-bottom uk-margin-auto uk-text-<?php echo $title_alignment;?>">
				<?php if($contents['description']):?>
					<!-- Section Sub Title -->
					<div class="uk-text-lead"
						<?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>
						<?php echo $contents['description'];?>
					</div>
				<?php endif; ?>
			</div> <!-- section-heading -->
			<div class="uk-text-center" uk-grid>
				<?php $i=4;?>	
				<?php foreach($contents['items'] as $counter): ?>
					<div class="uk-width-1-<?php echo $settings['items_columns'];?>@m uk-width-1-1@s">
						<div class="counter-item uk-text-<?php echo $settings['counter_alignment'];;?>" <?php echo ($settings['animation_item'] ? $animation_item. ';repeat:' .($animation_repeat ? 'true' : 'false') . ';delay:' . $i . '00"' : ''); ?>> 
							<h2 class="counter uk-margin-small"><span class="count"><?php echo $counter['count']; ?></span><?php echo $counter['suffix'];?></span></h2> 
							<h4 class="count-title uk-margin-remove uk-text-<?php echo $settings['counter_title_transformation']?>"><?php echo $counter['title']?></h4> 
							                        
						</div>
					</div>
				<?php $i++;endforeach; ?>  
			</div> <!-- uk-width -->                                                 
		</div> <!-- uk-container-small -->
	</section> <!-- section-id -->
	
	<script>
		jQuery(document).ready(function() {
			var counters = jQuery("#<?php echo $id?> .count");
			var countersQuantity = counters.length;
			var counter = [];

			for (i = 0; i < countersQuantity; i++) {
				counter[i] = parseInt(counters[i].innerHTML);
			}

			var count = function(start, value, id) {
				var localStart = start;
				setInterval(function() {
				if (localStart < value) {
					localStart++;
					counters[id].innerHTML = localStart;
				}
				}, 40);
			};

			for (j = 0; j < countersQuantity; j++) {
				count(0, counter[j], j);
			}
		});

		jQuery('#<?php echo $id?>').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				jQuery(this).find('.count').each(function () {
					var $this = jQuery(this);
					jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
						duration: 1000,
						easing: 'swing',
						step: function () {
							$this.text(Math.ceil(this.Counter));
						}
					});
				});
				jQuery(this).unbind('inview');
			}
		});

		</script>


