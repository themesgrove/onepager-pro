#<?php echo $id ?>{
	<?php if($styles['bg_color']):?>
		background-color : <?php echo $styles['bg_color'] ?>;
	<?php endif;?>
	
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id ?> .uk-heading-primary {
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}
#<?php echo $id ?> .uk-text-lead {
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['text_color']?>;
	line-height : <?php echo ($settings['desc_size']) +15?>px;
}
#<?php echo $id ?> .uk-button{
	background: <?php echo $styles['button_bg_color']?>;
	color : <?php echo $styles['button_text_color']?>;
	border-radius:<?php echo $settings['border_radius']?>px;
	text-transform:<?php echo $settings['button_transformation']?>;
}

#<?php echo $id ?> .uk-button:hover{
	background: <?php echo $styles[ 'button_text_color']?>;
	color : <?php echo $styles['button_bg_color']?>;
	border-color: <?php echo $styles['button_bg_color']?>;
}


@media(max-width:768px){
	#<?php echo $id?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}