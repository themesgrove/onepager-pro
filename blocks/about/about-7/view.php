<?php
	// animation repeat
	$animation_repeat = '';
	// media grid
	$media_grid = 'uk-'. $settings['media_grid'] . '@m';
	// Animation media
	$animation_media = ($settings['animation_media']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_media'] .';repeat:' . ($animation_repeat ? 'true' : 'false') . '"' : '';
	// animation contant
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';

	// Alignment
	$content_position = '';
	// media padding
	$media_padding = 'uk-container-item-padding-remove-left';
	
	if($settings['media_alignment'] == 'right'){
		$content_position = 'uk-flex-first@m uk-first-column';
		$media_padding = 'uk-container-item-padding-remove-right';
	}
	// Text transformation class
	$heading_class = ($settings['title_transformation']) ? 'uk-text-' . $settings['title_transformation'] : '';

?>
<section id="<?php echo $id; ?>" class="fp-section about pro-about-7">
	<div class="uk-section <?php echo $styles['padding_bottom_off'] ? 'uk-padding-remove': '';?>">
		<div class="uk-container">
			<article class="uk-grid-large " uk-grid>
				<!-- Media -->
				<div class="<?php echo $media_grid?> <?php echo ($settings['media_alignment'] == 'right') ? 'uk-padding-remove-left' :'';?> uk-width-1-1@s uk-grid-item-match uk-flex-middle">
					<div class="uk-panel <?php echo $media_padding?>" <?php echo $animation_media;?>>
						<img 
							width="<?php echo $settings['media_size']?>" 
							src="<?php echo $contents['image']?>" 
							alt="<?php echo $contents['title']?>" 
							class="op-max-width-none <?php echo ($settings['media_alignment'] == 'left') ? 'uk-float-right' :''; ?>" uk-image>
					</div>
				</div>
				<div class="uk-width-expand@m uk-grid-item-match uk-flex-middle <?php echo $content_position;?>">
					<div class="uk-panel uk-margin-medium-right">
						<!-- Title -->
						<?php if($contents['title']): ?>
							<?php
									echo op_heading( 
										$contents['title'],
										$settings['heading_type'], 
										'uk-heading-primary ', 
										'uk-text-' . $settings['title_transformation'], 
										$animation_content . '"'
									); 
								?>
						<?php endif;?>
						<!-- Description -->
						<?php if($contents['description']): ?>
							<div class="uk-text-lead" 
								<?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' . '300"' : ''); ?>>
								<?php echo $contents['description']?>	
							</div>
						<?php endif; ?>
						<p <?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' . ($animation_repeat ? 'true': 'false') . ';delay:' . '400"' : ''); ?>>
								<!-- Link -->
								<?php echo op_link($contents['link'], 'uk-button-large uk-margin-medium-top uk-button uk-button-primary');?>
						</p>
					</div>	<!-- uk-panel -->
				</div> <!-- uk-grid-match -->
			</article> <!-- uk-article --> 
		</div> <!-- uk-container -->
	</div> <!-- uk-section --> 
</section> <!-- end-section --> 