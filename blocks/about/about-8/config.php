<?php

return array(
  'slug'    => 'pro-about-8',
  'groups'    => array('about'),

  'contents' => array(

    array('label'=>'Counter Items', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '230'),
          array('name'=>'title', 'value' => 'Projects Done'),
          array('name'=>'icon', 'label'  => 'Icon', 'type' => 'icon', 'value' => 'fa fa-database fa-2x')
        ),
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '467'),
          array('name'=>'title', 'value' => 'Satisfaction Clients'),
          array('name'=>'icon', 'label' => 'Icon', 'type' => 'icon', 'value' => 'fa fa-smile-o fa-2x')
        ),
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '789'),
          array('name'=>'title', 'value' => 'Awards Win'),
          array('name'=>'icon', 'label' => 'Icon', 'type' => 'icon', 'value' => 'fa fa-trophy fa-2x')
        ),
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '580'),
          array('name'=>'title', 'value' => 'Cup Of Coffee'),
          array('name'=>'icon', 'label' => 'Icon', 'type' => 'icon', 'value' => 'fa fa-thumbs-o-up fa-2x')
        )
      )

    ),
  ),

  'settings' => array(

    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'item_counter_size',
      'label' => 'Counter Size',
      'append' => 'px',
      'value' => '36'
    ),

    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '16'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),

    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'items_animation',
      'label'    => 'Items Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('name'=>'animation_repeat', 'label'=>'Animation Repeat', 'type'=>'switch'),
  ),


  'styles' => array(

    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => 'http://try.getonepager.com/wp-content/uploads/2019/02/cta-1-bg.jpg'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(0,0,0,0.7)'
    ),
    array(
      'name'   => 'bg_color',
      'label'  => 'Background Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),


    array('label'=>'Items Style', 'type'=>'divider'), // Divider - Text

    array(
      'name'   => 'counter_color',
      'label'  => 'Counter Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),

    array(
      'name'   => 'item_title_color',
      'label'  => 'Title Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),

    array(
      'name'   => 'icon_color',
      'label'  => 'Icon Color',
      'type'   => 'colorpicker',
      'value'  => '@color.primary'
    ),
    array(
      'name'   => 'icon_bg_color',
      'label'  => 'Icon Bg Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),
  ),
);
