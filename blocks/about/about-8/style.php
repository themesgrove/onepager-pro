#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


<?php if ($styles['icon_color']): ?>
	
	#<?php echo $id;?> .op-media{
		color : <?php echo $styles['icon_color']?>;
	    border: 1px solid #e5e5e5;
	    width: 80px;
	    height: 80px;
	    text-align: center;
	    line-height: 80px;
    	border-radius: 100px;
    	background-color:<?php echo $styles['icon_bg_color']?>;
	}
<?php endif; ?>



#<?php echo $id;?> .items .uk-card .counter{
	font-size : <?php echo $settings['item_counter_size']?>px;
	color : <?php echo $styles['counter_color']?>;
}


#<?php echo $id;?> .items .uk-card .uk-text-medium{
	font-size : <?php echo $settings['item_title_size']?>px;
	color : <?php echo $styles['item_title_color']?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color'];?>;
}


@media(max-width:768px){
	#<?php echo $id; ?> .items{
		padding: 25px 40px;
	}
}