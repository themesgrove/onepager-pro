<?php
	// animation repeat
	$animation_repeat = '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';target:>.items; delay:200;repeat:' .($animation_repeat ? 'true' :'false' ) . ';"' : '';

	// wp_enqueue_script('jquery-inview', ONEPAGER_PRO_URL . 'assets/js/jquery.inview.min.js', ['jquery']);
?>
<script src="//cdn.jsdelivr.net/jquery.inview/0.2/jquery.inview.min.js"></script>

<section id="<?php echo $id;?>" class="about pro-about-8 uk-position-relative"
<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container">
			<article class="uk-article uk-position-relative">
				<div class="uk-grid-medium" <?php echo $items_animation;?>uk-grid>
					<?php foreach($contents['items'] as $counter): ?>
						<div class="items uk-width-1-4@m uk-width-1-2@s uk-margin-auto-top">
							<div class="uk-card">
								<div class="uk-card-header uk-padding-remove">
									<div uk-grid>
										<?php if ($counter['icon']): ?>
											<div class="uk-width-auto">
												<span class="op-media <?php echo $counter['icon'];?>"></span>
											</div>
										<?php endif; ?>
										<div class="uk-width-expand">
											<?php if ($counter['count']): ?>
												<h2 class="counter uk-margin-remove">
													<span class="count-number">	
														<?php echo $counter['count']; ?>
													</span>
												</h2> 
											<?php endif; ?>

											<?php if ($counter['title']): ?>
												<!-- Item desc -->
												<p class="uk-text-medium uk-margin-remove"><?php echo $counter['title']?></p>
											<?php endif; ?>
										</div> <!-- uk-width-expand -->
									</div> <!-- uk-grid-small -->
								</div> <!-- uk-card-header -->
							</div> <!-- uk-card -->
						</div> <!-- items -->
					<?php endforeach; ?> 
				</div> <!-- uk grid medium -->
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->

<script>

    jQuery(document).ready(function() {
			var counters = jQuery("<?php echo $id;?> .count-number");
			var countersQuantity = counters.length;
			var counter = [];

			for (i = 0; i < countersQuantity; i++) {
				counter[i] = parseInt(counters[i].innerHTML);
			}

			var count = function(start, value, id) {
				var localStart = start;
				setInterval(function() {
				if (localStart < value) {
					localStart++;
					counters[id].innerHTML = localStart;
				}
				}, 40);
			};

			for (j = 0; j < countersQuantity; j++) {
				count(0, counter[j], j);
			}
		});

		jQuery('#<?php echo $id;?>').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				jQuery(this).find('.count-number').each(function () {
					var $this = jQuery(this);
					jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
						duration: 1000,
						easing: 'swing',
						step: function () {
							$this.text(Math.ceil(this.Counter));
						}
					});
				});
				jQuery(this).unbind('inview');
			}
		});

</script>