<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:500"' : '';
?>

<section id="<?php echo $id;?>" class="uk-section uk-position-relative about pro-about-9 uk-background-cover uk-background-norepeat" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section-small">
		<div class="uk-container">
			<div class="uk-grid-match uk-flex-middle" uk-grid>
				<div class="uk-width-1-2@m ">
					<div class="video-lightbox uk-position-relative" uk-lightbox>
					    <div class="uk-card" <?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>

							<div class="uk-height-large uk-flex uk-flex-middle uk-flex-center uk-background-caption uk-background-norepeat uk-background-center-center" style="background-image: url(<?php echo $contents['video_bg_img'];?>);">
							    <div class="uk-text-center">
					            	<a class="uk-button" href="<?php echo $contents['video_link'];?>"><i class="fa fa-play"></i></a>
					            </div>
					        </div>
						</div> <!-- uk-card -->
					</div> <!-- uk-lightbox -->
				</div>

				<div class=" uk-width-1-2@m uk-width-1-1@s">
					<article class="uk-article uk-position-relative uk-margin-auto uk-padding">
						<div class="section-heading uk-margin-medium-bottom">	
							<?php if($contents['title']):?>
								<!-- Section Title -->
								<?php
									echo op_heading( 
										$contents['title'],
										$settings['heading_type'], 
										'uk-heading-primary  uk-margin-remove', 
										'uk-text-' . $settings['title_transformation'], 
										$title_animation . '"'
									); 
								?>
							<?php endif; ?>

							<?php if($contents['description']):?>
								<!-- Section Sub Title -->
								<p class="uk-text-lead uk-margin-medium uk-margin-remove-bottom" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
									<?php echo $contents['description'];?>
								</p>
							<?php endif; ?>
							<?php if ($contents['link']): ?>
								<p class="uk-margin-remove-bottom" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : '' ;?>>
										<!-- Link -->
									<?php echo op_link($contents['link'], 'uk-margin-top uk-button uk-button-primary uk-button-large uk-margin-remove-bottom');?>
								</p>
							<?php endif;?>
						</div> <!-- section-heading -->
					</article> <!-- uk-article -->
				</div> <!-- uk-width -->
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
