#<?php echo $id; ?>{
	background-color : <?php echo $styles['bg_color'];?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;

}

#<?php echo $id;?> .section-heading .uk-heading-primary{ 
	font-size : <?php echo $settings['section_title_size'];?>px; 
	color : <?php echo $styles['section_title_color'];?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px; 
	color : <?php echo $styles['desc_color'];?>;
}

#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a{
	font-size : <?php echo $settings['item_title_size'];?>px;
	color : <?php echo $styles['item_title_color'];?>;
}

#<?php echo $id;?> .uk-card .uk-card-body .uk-text-small{
	font-size : <?php echo $settings['item_desc_size'];?>px;
	color : <?php echo $styles['item_desc_color'];?>;
}

#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a:hover{
	color : <?php echo $styles['link_color'];?>;
	text-decoration:none;
}

#<?php echo $id;?> .uk-card .uk-card-body a,
#<?php echo $id;?> .uk-card .uk-card-body a:hover{
	color : <?php echo $styles['link_color'];?>;
	text-decoration:none;
}

#<?php echo $id;?> .uk-button-text::before{
    bottom: -5px;
	border-bottom : 1px solid <?php echo $styles['link_color'];?>;
}


#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
	#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a{
		font-size : <?php echo ($settings['item_title_size']/2) +8;?>px;
	}
}
