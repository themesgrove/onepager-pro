<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';

	// title animation
	$item_animation = ($settings['item_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['item_animation'].';target:div>.uk-card; delay:200;repeat:' .($animation_repeat ? 'true' :'false' ) . ';"' : '';

	// Arguments
	$args = array(
		'posts_per_page'   => $contents['total_posts'],
		'cat'         => $contents['category'],
	);
	// Build query
	$query = new WP_Query( $args );
?>

<section id="<?php echo $id;?>" class="uk-position-relative blog pro-blog-1 uk-padding-small <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" 	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container uk-position-relative">
		    <div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">
		        <?php if($contents['title']):?>
	              <!-- Section Title -->
					  	<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
	            <?php endif; ?>

	            <?php if($contents['description']):?>
	                <div class="uk-text-lead" <?php echo ($settings['title_animation']) ? $title_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : ''?>><?php echo $contents['description']?></div>
		        <?php endif; ?>
		    </div> <!-- Section heading -->

			<!-- WP Posts -->
			<div class="" <?php echo $item_animation;?> uk-grid>
				<?php if( $query->have_posts() ) : ?>
					<?php while( $query->have_posts() ) : $query->the_post(); ?>
						<div class="uk-width-1-<?php echo $settings['items_columns'];?>@m uk-width-1-1@s">
					        <div class="uk-card uk-card-default" <?php echo ($settings['item_animation']) ? $item_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'400"' : ''?>>
						        <?php if ($contents['thumbnail_enable']): ?>
						            <div class="uk-card-media-top">
						                <a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail('', array('', '')); ?>
										</a>
						            </div>
						        <?php endif; ?>
					            <div class="uk-card-body uk-text-<?php echo $settings['item_alignment'];?>">
				               		<h3 class="uk-card-title uk-margin-remove-bottom uk-text-<?php echo $settings['item_title_transformation']?>">
										<a href="<?php the_permalink(); ?>">
											<?php echo wp_trim_words( get_the_title(), $contents['title_limit'], '' );?>
										</a>
									</h3>
					              	<p class="uk-text-small uk-margin-remove-top">
										<?php op_the_excerpt($contents['text_limit']); ?>
									</p>
									<?php if ($contents['readmore_text']): ?>	
										<a class="uk-button-text" href="<?php the_permalink();?>"><?php echo $contents['readmore_text'];?></a>
									<?php endif; ?>
					            </div> <!-- uk-card-body -->
					        </div> <!-- uk-card -->
				        </div> <!-- uk-width-1-1 -->
					<?php endwhile; ?>
					<?php endif; ?>
				<?php wp_reset_query(); ?>
			</div> <!-- uk-grid -->
		</div><!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
