<?php

return array(

  'slug'      => 'pro-blog-2', // Must be unique and singular
  'groups'    => array('blogs'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Title', 'type'=>'divider'),
    array(
      'name'=>'sub_title',
      'label'=>'Sub Title',
      'value' => 'Our Works'
    ),
    array(
      'name'=>'title',
      'value' => 'Latest Projects'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value'=> ''
    ),

    array('label'=>'Blog', 'type'=>'divider'),
    array(
      'name'=>'category',
      'type'=>'category'
    ),

    array(
      'name'  => 'total_posts',
      'label' => 'Show Posts',
      'value' => '5'
    ),
    array(
      'name'  => 'title_limit',
      'label' => 'Title Words',
      'value' => '3'
    ),

    array('name'=> 'dot_enable', 
      'label' =>'Dot Enable', 
      'type'  =>'switch',
      'value' => 'yes'
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Heading', 'type'=>'divider'),
    array(
      'name' => 'sub_title_size',
      'label' => 'Sub Title Size',
      'append' => 'px',
      'value' => '18'
    ),
    array(
      'name'   => 'section_title_size',
      'label'  => 'Title Size',
      'append' => 'px',
      'value'  => '@section_title_size'
    ),

    array(
      'name'  => 'title_font',
      'type'  => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit' => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),


    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),

    array(
      'name'   => 'desc_size',
      'label'  => 'Desc Size',
      'append' => 'px',
      'value'  => '18'
    ),


    array(
      'name'     => 'title_animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Items', 'type'=>'divider'),
    array(
      'name'     => 'items_columns',
      'label'    => 'Columns',
      'type'     => 'select',
      'value'    => '3',
      'options'  => array(
        '2'   => '2',
        '3'   => '3',
        '4'   => '4',

      ),
    ),
    array(
      'name'     => 'item_alignment',
      'label'    => 'Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'    => 'Left',
        'center'  => 'Center',
        'right'   => 'Right',
      ),
    ),
    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
       'value' => '22'
    ),



    array(
      'name'     => 'item_title_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit' => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),


    array(
      'name'   => 'item_desc_size',
      'label'  => 'Author Size',
      'append' => 'px',
      'value'  => '16'
    ),

  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array(
      'name'    => 'bg_color',
      'label'   => 'Background Color',
      'type'    => 'colorpicker',
      'value'   => '#f5f5f5'
    ),

    array('label'=>'Title', 'type'=>'divider'),
    array(
      'name'  => 'section_title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#222'
    ),

    array(
      'name'  => 'desc_color',
      'label' => 'Desc Color',
      'type'  => 'colorpicker',
      'value' => '#666'
    ),

    array('label'=>'Items', 'type'=>'divider'),
    array(
      'name'  => 'item_title_color',
      'label' => 'Item Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),

    array(
      'name'    => 'link_color',
      'label'   => 'Hover Color',
      'type'    => 'colorpicker',
      'value'   => '@color.primary'
    ),
  ),
);
