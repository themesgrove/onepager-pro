#<?php echo $id; ?>{
	background-color : <?php echo $styles['bg_color'];?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;

}

#<?php echo $id;?> .section-heading .uk-text-large{ 
	font-size : <?php echo $settings['sub_title_size'];?>px; 
	color : <?php echo $styles['desc_color'];?>;
	line-height : <?php echo ($settings['sub_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{ 
	font-size : <?php echo $settings['section_title_size'];?>px; 
	color : <?php echo $styles['section_title_color'];?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px; 
	color : <?php echo $styles['desc_color'];?>;
	line-height : <?php echo ($settings['desc_size']) +10 ?>px;
}

#<?php echo $id;?> .uk-slider-items  .uk-card-title a{
	font-size : <?php echo $settings['item_title_size'];?>px;
	color : <?php echo $styles['item_title_color'];?>;
	line-height : <?php echo ($settings['item_title_size']) +10 ?>px;
}
#<?php echo $id;?> .uk-slider-items  .uk-card-title a:hover,
#<?php echo $id;?> .uk-slider-items .uk-panel span:hover{
	color: <?php echo $styles['link_color'];?>;
	text-decoration:none;
}

#<?php echo $id;?> .uk-slider-items .uk-text-small{
	font-size : <?php echo $settings['item_desc_size'];?>px;
	color : <?php echo $styles['item_title_color'];?>;
}
#<?php echo $id;?> .uk-slider-items .uk-panel span{
	color : <?php echo $styles['item_title_color'];?>;
}

#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a:hover{
	color : <?php echo $styles['link_color'];?>;
	text-decoration:none;
}

#<?php echo $id;?> .uk-dotnav li a {
    width: 40px;
    height: 15px;
    border-radius: 20px;
    border: 2px solid #ddd;
}
#<?php echo $id;?> .uk-dotnav li.uk-active a {
	 border-color:<?php echo $styles['link_color'];?>;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
	#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a{
		font-size : <?php echo ($settings['item_title_size']/2) +8;?>px;
	}
}
