<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';

	// items alignment
	$item_alignment = ($settings['item_alignment']) ? $settings['item_alignment'] : '';
	// Arguments
	$args = array(
		'posts_per_page'   => $contents['total_posts'],
		'cat'         => $contents['category'],
	);
	// Build query
	$query = new WP_Query( $args );
?>

<section id="<?php echo $id;?>" class="uk-position-relative blog pro-blog-2">
	<div class="uk-section">
		<div class="uk-container-expand">
		    <div class="section-heading uk-width-1-2@m uk-margin-auto uk-position-relative uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">
		    	<?php if($contents['sub_title']):?>
					<!-- Section Sub Title -->
					<h4 class="uk-text-large uk-margin-small" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
						<?php echo $contents['sub_title'];?>
					</h4>
				<?php endif; ?>
		        <?php if($contents['title']):?>
	              <!-- Section Title -->
	              	<h1 class="uk-heading-primary uk-margin-remove-top uk-text-<?php echo $settings['title_transformation'];?>" <?php echo ($settings['title_animation']) ? $title_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : ''?>>
	                	<?php echo $contents['title'];?>
					  </h1>
					  	<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
	            <?php endif; ?>

	            <?php if($contents['description']):?>
	                <div class="uk-text-lead" <?php echo ($settings['title_animation']) ? $title_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : ''?>><?php echo $contents['description']?></div>
		        <?php endif; ?>
		    </div> <!-- Section heading -->

			<div class="uk-light" tabindex="-1" uk-slider="autoplay:true;sets: true;">
				<ul class="uk-slider-items uk-child-width-1-<?php echo $settings['items_columns'];?>@l uk-child-width-1-<?php echo $settings['items_columns'];?>@m uk-child-width-1-1@s"
				>
					<!-- WP Posts -->
					<?php if( $query->have_posts() ) : ?>
					<?php while( $query->have_posts() ) : $query->the_post(); ?>  
						<?php if ( has_post_thumbnail() ):?> 
				        <li class="uk-position-relative">
							<div class="uk-visible-toggle uk-transition-toggle">
					            <a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('', array('', '')); ?>
								</a>
								<div class="uk-overlay-primary uk-position-cover uk-hidden-hover uk-transition-fade">
						            <div class="uk-position-center uk-panel uk-position-z-index" uk-lightbox>
							         	 <?php  $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
									        /* link thumbnail to full size image for use with lightbox*/
									        echo '<a href="'.esc_url($featured_img_url).'">'; 
									        echo '<span class="fa fa-search fa-2x"></span></a>'; 
									    ?>
						            </div>
						            <div class="uk-position-bottom-<?php echo $item_alignment;?> uk-panel uk-padding uk-text-<?php echo $item_alignment;?> ">
							            <h3 class="uk-transition-slide-bottom-small uk-card-title uk-margin-remove-bottom uk-text-<?php echo $settings['item_title_transformation']?>">
											<a href="<?php the_permalink(); ?>">
												<?php echo wp_trim_words( get_the_title(), $contents['title_limit'], '' );?>
											</a>
										</h3>
										<h5 class="uk-text-small uk-text-capitalize uk-transition-slide-bottom-small uk-margin-remove">
											By : <?php the_author(); ?>
										</h5>
						            </div>
					            </div>
				            </div>
				        </li>
					<?php endif;endwhile; endif;?>
					<?php wp_reset_query(); ?>
				</ul><!-- uk-slider-items -->
				 <?php if ($contents['dot_enable']): ?>
 						<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin-medium"></ul>
 				<?php endif; ?>
			</div> <!-- uk-light -->
		</div><!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
