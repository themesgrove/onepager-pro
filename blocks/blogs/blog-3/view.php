<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';

	// title animation
	$item_animation = ($settings['item_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['item_animation'].';' : '';

	$featured = 2;
    $list = 3;
    $post_num_to_show = $featured + $list;
    $count = 1;
	// Arguments
	$args = array(
		'posts_per_page'   => $post_num_to_show,
		'cat'         => $contents['category'],
	);
	// Build query
	$query = new WP_Query( $args );


?>


<section id="<?php echo $id;?>" class="blog pro-blog-3 uk-padding-small">
	<div class="uk-section">
		<div class="uk-container uk-position-relative">
		    <div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">
		    	<?php if($contents['sub_title']):?>
					<!-- Section Sub Title -->
					<h4 class="uk-text-large uk-margin-small" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
						<?php echo $contents['sub_title'];?>
					</h4>
				<?php endif; ?>
		        <?php if($contents['title']):?>
	              <!-- Section Title -->
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary', 
							'uk-text-' . $settings['title_transformation'], 
							$title_animation . '"'
						); 
					?>
	            <?php endif; ?>

	            <?php if($contents['description']):?>
	                <div class="uk-text-lead" <?php echo ($settings['title_animation']) ? $title_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : ''?>><?php echo $contents['description']?></div>
		        <?php endif; ?>
		    </div> <!-- Section heading -->

			<!-- WP Posts -->
			<div class="uk-child-width-1-3@m"  uk-grid>
				<?php $content=0; if($query->have_posts()): while($query->have_posts()): $query->the_post();$content++; if($content > $list) break;?>
						<?php if (has_post_thumbnail()): ?>
						<div <?php echo ($settings['item_animation']) ? $item_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$content .'00"' : ''?>>
					        <div class="uk-card uk-card-default">
					            <div class="uk-card-media-top">
					             	<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('', array('', '')); ?>
									</a>
					            </div> <!-- uk-card-media-top -->
					             <div class="uk-grid-medium uk-margin-remove uk-padding-remove" uk-grid>
						            <div class="uk-width-1-4@m uk-width-1-4@s border-right">
						            	<?php 
											$archive_month = get_the_time('F'); 
											$archive_day   = get_the_time('d');  ?>

						                <h2 class="uk-text-large uk-margin-remove-bottom"> <?php echo $archive_day;?></h2>
						                <p class="uk-text-emphasis uk-margin-remove-top"><?php echo $archive_month;?> </p>
						            </div> <!-- uk-width-auto -->
						            <div class="uk-width-3-4@m uk-width-3-4@s border-bottom">
								        <div class="uk-card-body uk-padding-remove">
							               <h3 class="uk-card-title uk-margin-remove uk-text-<?php echo $settings['item_title_transformation']?>">
												<a href="<?php the_permalink(); ?>">
													<?php echo wp_trim_words( get_the_title(), $contents['title_limit'], '' );?>
												</a>
											</h3>
							                <p class="uk-margin-remove">By : <?php the_author(); ?></p>
							            </div> <!-- uk-card-body -->
						            </div> <!-- uk-width-expand -->
						        </div> <!-- uk-grid-small -->
					        </div> <!-- uk-card -->
					    </div>
					<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
				<?php wp_reset_query(); ?>
				<?php $query->rewind_posts(); ?>

				<div <?php echo ($settings['item_animation']) ? $item_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : ''?>>
					<div class="uk-background-default uk-card-default">
				        <?php $title = 0;
				            if($query->have_posts()): while($query->have_posts()): $query->the_post();
				            $title++; if($title >  $list) break;
		            	?>
					      	<div class="uk-grid-medium uk-margin-remove uk-padding-remove" uk-grid>
					            <div class="uk-width-1-4@m uk-width-1-4@s border-right">
					            	<?php $archive_month = get_the_time('F'); 
										$archive_day   = get_the_time('d');  ?>
					                <h2 class="uk-text-large uk-margin-remove-bottom"> <?php echo $archive_day;?></h2>
					                <p class="uk-text-emphasis uk-margin-remove-top"><?php echo $archive_month;?> </p>
					            </div> <!-- uk-width-auto -->
					            <div class="uk-width-3-4@m uk-width-3-4@s uk-padding-small">
							        <div class="uk-card-body uk-padding-remove">
						               <h3 class="uk-card-title uk-margin-remove uk-text-<?php echo $settings['item_title_transformation']?>">
											<a href="<?php the_permalink(); ?>">
												<?php echo wp_trim_words( get_the_title(), $contents['title_limit'], '' );?>
											</a>
										</h3>
						                <p class="uk-margin-remove">By : <?php the_author(); ?></p>
						            </div> <!-- uk-card-body -->
					            </div> <!-- uk-width-expand -->
					        </div> <!-- uk-grid-small -->
						<?php endwhile; ?>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div> <!-- uk-background-default -->
				</div> <!-- uk-width-child -->
			</div> <!-- uk-width-1-3 -->
		</div><!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
