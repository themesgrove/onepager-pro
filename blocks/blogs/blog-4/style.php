#<?php echo $id; ?>{
	background-color : <?php echo $styles['bg_color'];?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;

}

#<?php echo $id;?> .section-heading .uk-text-large{
	font-size : <?php echo $settings['sub_title_size']?>px;
	color : <?php echo $styles['desc_color']?>;
	line-height : <?php echo ($settings['sub_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{ 
	font-size : <?php echo $settings['section_title_size'];?>px; 
	color : <?php echo $styles['section_title_color'];?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px; 
	color : <?php echo $styles['desc_color'];?>;
}
#<?php echo $id;?> .uk-card-default .uk-text-large{
    font-size: 30px;
    color : <?php echo $styles['hover_color'];?>;
}

#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a{
	font-size : <?php echo $settings['item_title_size'];?>px;
	color : <?php echo $styles['item_title_color'];?>;
	line-height : <?php echo ($settings['item_title_size']) +8 ?>px;
}

#<?php echo $id;?> .uk-card{
   border:1px solid <?php echo $styles['item_border_color'];?>;
   border-radius:5px;
}
#<?php echo $id;?> .border-top{
	border-top:1px solid <?php echo $styles['item_border_color'];?>;
}
#<?php echo $id;?> .border-left{
	border-left:1px solid <?php echo $styles['item_border_color'];?>;
}
#<?php echo $id;?> .uk-card-media-top{
	height: 250px;
    overflow: hidden;
    border-radius: 5px 5px 0 0;
}
#<?php echo $id;?>  .border-right {
    border-right: 1px solid <?php echo $styles['item_border_color'];?>;
    padding: 20px;
}
#<?php echo $id;?>  .border-bottom {
    padding: 20px;
}

#<?php echo $id;?> .uk-card .uk-text-meta-date{
	  color: <?php echo $styles['hover_color'];?>;
}

#<?php echo $id;?> .uk-card i{
	  color: <?php echo $styles['hover_color'];?>;
}

#<?php echo $id;?> .uk-card .padding-small {
    padding: 5px 15px;
}

#<?php echo $id;?> .uk-card .uk-text-lead{
	font-size : <?php echo $settings['item_desc_size'];?>px;
	color : <?php echo $styles['item_desc_color'];?>;
	line-height : <?php echo ($settings['item_desc_size']) +10 ?>px;
}

#<?php echo $id;?> .uk-card .uk-text-meta{
	font-size : 16px;
	color : <?php echo $styles['item_desc_color'];?>;
}

#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a:hover,
#<?php echo $id;?> .uk-card-default .uk-card-title a:hover{
	color : <?php echo $styles['hover_color'];?>;
	text-decoration:none;
}
#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a:foucs{
	outline:none;
}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
	#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a{
		font-size : <?php echo ($settings['item_title_size']/2) +8;?>px;
		line-height : <?php echo ($settings['item_title_size']/2) +8 ?>px;
	}
	#<?php echo $id;?>  .uk-card .first-child{
		margin:10px 15px 0;
	}
}
