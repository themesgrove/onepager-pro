<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';

	// title animation
	$item_animation = ($settings['item_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['item_animation'].';target:>div; delay:200;repeat:' .($animation_repeat ? 'true' :'false' ) . ';"' : '';

	// Arguments
	$args = array(
		'posts_per_page'   => $contents['total_posts'],
		'cat'         => $contents['category'],
	);
	// Build query
	$query = new WP_Query( $args );


?>


<section id="<?php echo $id;?>" class="blog pro-blog-4">
	<div class="uk-section">
		<div class="uk-container uk-position-relative uk-padding-small">
		    <div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">
		    	<?php if($contents['sub_title']):?>
					<!-- Section Sub Title -->
					<h4 class="uk-text-large uk-margin-small" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
						<?php echo $contents['sub_title'];?>
					</h4>
				<?php endif; ?>
		        <?php if($contents['title']):?>
	              <!-- Section Title -->
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary uk-margin-remove-top', 
							'uk-text-' . $settings['title_transformation'], 
							$title_animation . '"'
						); 
					?>
	            <?php endif; ?>

	            <?php if($contents['description']):?>
	                <div class="uk-text-lead" <?php echo ($settings['title_animation']) ? $title_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : ''?>><?php echo $contents['description']?></div>
		        <?php endif; ?>
		    </div> <!-- Section heading -->

			<!-- WP Posts -->
			<div class="uk-child-width-1-<?php echo $settings['items_columns'];?>@m uk-child-width-1-1@s"  <?php echo $item_animation;?>uk-grid>
				<?php if($query->have_posts()): while($query->have_posts()): $query->the_post();?>
					<?php if (has_post_thumbnail()): ?>
						<div>
					        <div class="uk-card">
					            <div class="uk-card-media-top">
					             	<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('', array('', '')); ?>
									</a>
					            </div> <!-- uk-card-media-top -->
								 <div class="uk-card-body uk-padding-small">
					                <p class="uk-text-bold uk-text-meta-date uk-margin-remove"><?php echo the_date();?> </p>
					               <h3 class="uk-card-title uk-margin-remove uk-text-<?php echo $settings['item_title_transformation']?>">
										<a href="<?php the_permalink(); ?>">
											<?php echo wp_trim_words( get_the_title(), $contents['title_limit'], '' );?>
										</a>
									</h3>
									<p class="uk-text-lead"><?php op_the_excerpt($contents['text_limit']); ?></p>
							    </div> <!-- uk-card-body -->
							    <div class="uk-margin-remove-left uk-margin-small-top border-top" uk-grid>
					                <div class="uk-width-1-4@m uk-padding-remove-left first-child">
				                       	<div class="uk-grid-small uk-flex-middle padding-small" uk-grid>
								            <div class="uk-width-auto uk-margin-remove">
								                <i class="fa fa-user-o"></i>
								            </div>
								            <div class="uk-width-expand">
								                <p class="uk-text-meta uk-margin-remove-top"><?php the_author(); ?></p>
								            </div>
								        </div>
								    </div>
								    <div class="uk-width-1-2@m">
				                       	<div class="uk-grid-small padding-small uk-padding-remove-left uk-flex-middle" uk-grid>
								            <div class="uk-width-auto uk-margin-remove">
								               <i class="fa fa-comment-o"></i>
								            </div>
								            <div class="uk-width-expand">
								                <p class="uk-text-meta uk-margin-remove-top"><?php echo get_comments_number(); ?> Comments</p>
								            </div>
								        </div>
								    </div>
								    <div class="uk-width-1-4@m uk-padding-remove border-left">
				                       	<div class="uk-grid-small padding-small uk-flex-middle" uk-grid>
								            <div class="uk-width-auto uk-align-right uk-margin-remove-bottom uk-padding-remove">
								            <a href="<?php the_permalink();?>"> <i class="fa fa-repeat"></i></a>
								            </div>
								        </div>
								    </div>
						        </div>
					        </div> <!-- uk-card -->
					    </div>
					<?php endif; endwhile; endif; ?>
				<?php wp_reset_query(); ?>
			</div> <!-- uk-width-1-3 -->
		</div><!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
