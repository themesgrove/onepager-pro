<?php

return array(

  'slug'      => 'pro-blog-5', // Must be unique and singular
  'groups'    => array('blogs'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array(
      'name'=>'title',
      'value' => 'Highlights'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value'=> 'Lorem ipsum dolor sit amet, sed dicunt oporteat cu, laboramus definiebas cum et. Duo id omnis persequeris neglegentur, facete commodo ea usu, possit lucilius sed ei. Esse efficiendi scripserit eos ex. Sea utamur iisque salutatus id.Mel autem animal.'
    ),
    array(
      'name'=>'category',
      'type'=>'category'
    ),
    array('name'=> 'thumbnail_enable', 
      'label'=>'Thumbnail', 
      'type'=>'switch',
      'value' => 'yes'
    ),

    array(
      'name' => 'title_limit',
      'label' => 'Title Words',
      'value' => '8'
    ),
    array(
      'name' => 'total_posts',
      'label' => 'Show Posts',
      'value' => '3'
    ),
    array(
      'name' => 'text_limit',
      'label' => 'Excerpt Length',
      'value' => '20'
    ),

    array(
      'name'  => 'readmore_text',
      'label' => 'Read More',
      'value' => 'Read More',
    ),

  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Heading', 'type'=>'divider'),
    array(
      'name'   => 'section_title_size',
      'label'  => 'Title Size',
      'append' => 'px',
      'value'  => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit' => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),


    array(
      'name'     => 'title_animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Items', 'type'=>'divider'),
    array(
      'name'     => 'items_columns',
      'label'    => 'Items Columns',
      'type'     => 'select',
      'value'    => '3',
      'options'  => array(
        '2'   => '2',
        '3'   => '3',
        '4'   => '4',

      ),
    ),
    array(
      'name'     => 'item_alignment',
      'label'    => 'Item Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'    => 'Left',
        'center'  => 'Center',
        'right'   => 'Right',
      ),
    ),
    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
       'value' => '20'
    ),

    array(
      'name'     => 'item_title_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit' => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'item_desc_size',
      'label' => 'Item Desc Size',
      'append' => 'px',
      'value' => '16'
    ),

   array(
    'name'     => 'item_animation',
    'label'    => 'Animation Item',
    'type'     => 'select',
    'value'    => 'fadeInUp',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => '#fff'
    ),

    array(
      'name'    => 'bg_color',
      'label'   => 'Background Color',
      'type'    => 'colorpicker',
      'value'   => 'rgba(50,55,66,0.77)'
    ),

    array('label'=>'Title', 'type'=>'divider'),
    array(
      'name'  => 'section_title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#212121'
    ),

    array(
      'name'  => 'desc_color',
      'label' => 'Desc Color',
      'type'  => 'colorpicker',
      'value' => '#212121'
    ),

    array('label'=>'Items', 'type'=>'divider'),
    array(
      'name'  => 'item_title_color',
      'label' => 'Item Title Color',
      'type'  => 'colorpicker',
      'value' => '#212121'
    ),
    array(
      'name'  => 'item_desc_color',
      'label' => 'Item Desc Color',
      'type'  => 'colorpicker',
      'value' => '#212121'
    ),
    array(
      'name'    => 'link_hover_color',
      'label'   => 'Link Hover Color',
      'type'    => 'colorpicker',
      'value'   => '#e74c3c'
    ),
  ),
);
