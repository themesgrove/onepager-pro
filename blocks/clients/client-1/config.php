<?php

return array(

  'slug'      => 'pro-client-1', // Must be unique
  'groups'    => array('clients'), // Blocks group for filter

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'title',
      'value'=>'Our Happy Clients'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Marketers at the world’s fastest-growing companies use Onepager to transform <br>strangers into customers. Here’s what they have to say.'
    ),
    array('label'=>'Clients', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'clients',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'name', 'value' => 'Client 1'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/mcqueen-copy.png' ),
          array('name' => 'link', 'type' => 'text', 'placeholder'=> home_url() ),
        ),

        array(
          array('name'=>'name', 'value' => 'Client 2'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/logo6-750x526.png' ),
           array('name' => 'link', 'type' => 'text', 'placeholder'=> home_url() ),
        ),
        array(
          array('name'=>'name', 'value' => 'Client 3'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/pompadous-copy.png' ),
           array('name' => 'link', 'type' => 'text', 'placeholder'=> home_url() ),
        ),
        array(
          array('name'=>'name', 'value' => 'Client 4'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/pacific-coast-copy.png' ),
           array('name' => 'link', 'type' => 'text', 'placeholder'=> home_url() ),
        )
      )
    )
  ),

  'styles' => array(
   
    array(
      'name'  => 'bg_image',
      'label' => 'Background Image',
      'type'  => 'image',
      'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/partner-bg.png'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(34,36,64,0.85)'
    ),
    array('label'=>'Title Styles', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),
    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(
        '0'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'animation_content',
      'label'    => 'Content Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array('label'=>'Client Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name'     => 'items_columns',
      'label'    => 'Items Columns',
      'type'     => 'select',
      'value'    => '4',
      'options'  => array(
        '2'   => '2',
        '3'   => '3',
        '4'   => '4',

      ),
    ),
    array(
      'name'     => 'animation_items',
      'label'    => 'Items Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array(
      'name' => 'image_width',
      'label' => 'Image Width',
      'append' => 'px',
      'value' => '120'
    ),
  ),

);
