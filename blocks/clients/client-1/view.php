<?php
	// animation repeat
	$animation_repeat = '';
	// content animation
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';

	// item animation
	$animation_items = ($settings['animation_items']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_items'].'' : '';
?>

<div id="<?php echo $id; ?>" 
	class="uk-background-cover uk-background-norepeat uk-position-relative client pro-client-1 fp-section"   
	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>
	data-src="<?php echo $styles['bg_image'];?>" uk-img>

	<div class="uk-overlay-primary uk-position-cover"></div>
		<div class="uk-section-large">
			<div class="uk-container uk-position-relative">
				<div class="section-heading uk-width-1-1 uk-margin-auto uk-text-center">
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$animation_content . '"'
							); 
						?>
					<?php endif; ?>
					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<div class="uk-text-lead uk-padding-small"
							<?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' .($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>
							<?php echo $contents['description'];?>
						</div>
					<?php endif; ?>
				</div> <!-- section-heading -->

			    <div class="uk-child-width-1-<?php echo $settings['items_columns'];?>@m uk-child-width-1-2@s uk-margin-xlarge-top uk-text-center uk-flex-middle" uk-grid>
			    <?php $i=4; ?>
					<?php foreach($contents['clients'] as $index => $client): ?>
						<div class="uk-margin-bottom" 
						<?php echo ($settings['animation_items'] ? $animation_items . ';repeat:' .($animation_repeat ? 'true' : 'false') . ';delay:' . $i . '00"' : ''); ?>>
							<?php if ($client['link']): ?>
							   <a href="<?php echo $client['link'];?>" target="_blank">
					            	<img width="<?php echo $settings['image_width'];?>" src="<?php echo $client['image']?>" alt="<?php echo $client['name']?>">
					            </a>
					        <?php else: ?>
					        	<img width="<?php echo $settings['image_width'];?>" src="<?php echo $client['image']?>" alt="<?php echo $client['name']?>">
							<?php endif; ?>
						</div> <!-- uk-margin-bottom -->
					<?php $i++; endforeach; ?>
			    </div> <!-- uk-child-width -->
			</div> <!-- uk-container -->
		</div> <!-- uk-section -->
	</div> <!-- section-id -->