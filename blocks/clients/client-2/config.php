<?php

return array(

  'slug'      => 'pro-client-2', // Must be unique
  'groups'    => array('clients'), // Blocks group for filter

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Clients', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'clients',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'name', 'value' => 'Client 1'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/03/client2.png' ),
          array('name' => 'link', 'type' => 'text', 'placeholder'=> home_url() ),
        ),

        array(
          array('name'=>'name', 'value' => 'Client 2'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/03/client4.png' ),
           array('name' => 'link', 'type' => 'text', 'placeholder'=> home_url() ),
        ),
        array(
          array('name'=>'name', 'value' => 'Client 3'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/03/client5.png' ),
           array('name' => 'link', 'type' => 'text', 'placeholder'=> home_url() ),
        ),
        array(
          array('name'=>'name', 'value' => 'Client 4'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/03/client5.png' ),
           array('name' => 'link', 'type' => 'text', 'placeholder'=> home_url() ),
        ),
        array(
          array('name'=>'name', 'value' => 'Client 5'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/03/client9.png' ),
           array('name' => 'link', 'type' => 'text', 'placeholder'=> home_url() ),
        )
      )
    )
  ),

  'styles' => array(
    array(
      'name'    => 'bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    
    array('label'=>'Client Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name'     => 'items_columns',
      'label'    => 'Items Columns',
      'type'     => 'select',
      'value'    => '4',
      'options'  => array(
        '2'   => '2',
        '3'   => '3',
        '4'   => '4',
        '5'   => '5',
        '6'   => '6',

      ),
    ),
    array(
      'name'     => 'animation_items',
      'label'    => 'Items Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array('name'=>'animation_repeat', 'label'=>'Animation Repeat', 'type'=>'switch'),

    array(
      'name' => 'image_width',
      'label' => 'Image Width',
      'append' => 'px',
      'value' => '150'
    ),
  ),

);
