#<?php echo $id;?> {
	background-color:<?php echo $styles['bg_color'];?>
}

#<?php echo $id;?> .uk-panel img{
    filter: gray; /* IE6-9 */
    -webkit-filter: grayscale(1);
    filter: grayscale(1);
    transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-panel:hover img{
  -webkit-filter: grayscale(0);
  filter: none;
  transition: all 0.5s ease;
}

#<?= $id ?> .uk-section-small{
	border-bottom: 1px solid #ddd;
}