<?php
	// item animation
	$animation_items = ($settings['animation_items']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_items'].';target:>li; delay:200' : '';
?>

<div id="<?php echo $id; ?>" class="client pro-client-4">
	<div class="uk-section-small">
		<div class="uk-container">
		    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="autoplay:true;">
			    <ul class="uk-slider-items uk-child-width-1-<?php echo $settings['items_columns'];?>@m uk-child-width-1-2@s uk-grid uk-text-center" <?php echo  $animation_items;?>>
					<?php foreach($contents['clients'] as $index => $client): ?>
				        <li>
				            <div class="uk-panel">
				               	<?php if ($client['link']): ?>
								   <a href="<?php echo $client['link'];?>" target="_blank">
						            	<img width="<?php echo $settings['image_width'];?>" src="<?php echo $client['image']?>" alt="<?php echo $client['name']?>">
						            </a>
						        <?php else: ?>
						        	<img width="<?php echo $settings['image_width'];?>" src="<?php echo $client['image']?>" alt="<?php echo $client['name']?>">
								<?php endif; ?>
				            </div> <!-- uk-panel -->
				        </li> 
				    <?php endforeach; ?>
			   </ul> <!-- uk-slider-items -->
			</div> <!-- uk-slider -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</div> <!-- section-id -->