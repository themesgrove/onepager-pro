#<?= $id ?>{
	<?php if($styles['bg_image']):?>
	background-image: url(<?= $styles['bg_image']?>);
    background-repeat: no-repeat;
    background-size: cover;
	<?php endif;?>	
}
#<?= $id; ?> .uk-overlay-primary{
	background: <?= $styles['overlay_color']?>;
}
#<?= $id;?> .uk-heading-primary{
	font-size : <?= $settings['title_size']?>px;
	color : <?= $styles['title_color']?>;
}
#<?= $id ?> .uk-text-lead {
	font-size : <?= $settings['desc_size']?>px;
	color : <?= $styles['desc_color']?>;
}
#<?= $id ?> .countdown {
	color : <?= $styles['countdown_color']?>;
}
#<?= $id ?> .uk-countdown-number{
	font-size : <?= $settings['countdown_number_size']?>px;
}
#<?= $id ?> .uk-countdown-label{
	font-size : <?= $settings['countdown_label']?>px;
}
#<?= $id ?> .social-links a {
	color : <?= $styles['icon_color']?>;
}

#<?= $id ?> .countdown-number {
    margin: 15px;
    width: 140px;
    height: 140px;
    border: 2px solid <?= $styles['countdown_color']?>;
    border-radius: 5px;
    padding: 22px 0 0 0;
}

#<?= $id ?> .social-links > a {
    font-size: 18px;
    color: <?= $styles['countdown_color']?>;
    border: 2px solid <?= $styles['countdown_color']?>;
    width: 38px;
    height: 38px;
    border-radius: 5px;
    line-height: 38px;
}