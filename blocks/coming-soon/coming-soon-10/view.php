<?php
	//Animation 
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'] .';repeat:' . 'true"' : '';	
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].';repeat:' . 'true"' : '';
	$countdown_animation = ($settings['countdown_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['countdown_animation'].';repeat:' . 'true"' : '';
	$social_animation = ($settings['social_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['social_animation'].';repeat:' . 'true"' : '';
?>
<section id="<?php echo $id; ?>" class="fp-section coming-soon coming-soon-10 uk-height-viewport uk-flex uk-flex-right uk-flex-middle uk-position-relative uk-background-cover">
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container">
			<div class="uk-grid-large uk-position-relative" uk-grid>
				<div class="uk-width-expand@m">
					<!-- Title -->
					<?php if ($contents['title']): ?>
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary uk-text-center', 
							$settings['title_transformation'], 
							$title_animation  
						); 
					?>
					<?php endif;?>
					<!-- Description -->
					<?php if ($contents['description']): ?>
						<p class="uk-text-lead uk-text-center uk-align-center uk-width-1-2@s" <?php echo $content_animation;?> >
							<?php echo $contents['description']; ?>
						</p>
					<?php endif;?>
					<!-- countdown -->
					<?php if ($contents['date']): ?>
					<div class="countdown uk-flex uk-flex-center uk-text-center uk-flex-middle uk-container uk-container-center" <?php echo $countdown_animation;?> >
						<div class="uk-grid-small uk-child-width-auto" uk-grid uk-countdown="date: <?php echo $contents['date']; ?>">
						    <div>
						         <div class="uk-countdown-number uk-countdown-days"></div>
						         <div class="uk-countdown-label uk-margin-small uk-text-center uk-visible@s"><?php _e( 'Days', 'onepager-pro' ) ?></div>
						    </div>
						    <div class="uk-countdown-separator">:</div>
						    <div>
						         <div class="uk-countdown-number uk-countdown-hours"></div>
						         <div class="uk-countdown-label uk-margin-small uk-text-center uk-visible@s"><?php _e( 'Hours', 'onepager-pro' ) ?></div>
						    </div>
						    <div class="uk-countdown-separator">:</div>
						    <div>
						        <div class="uk-countdown-number uk-countdown-minutes"></div>
						        <div class="uk-countdown-label uk-margin-small uk-text-center uk-visible@s"><?php _e( 'Minutes', 'onepager-pro' ) ?></div>
						    </div>
						    <div class="uk-countdown-separator">:</div>
						    <div>
						        <div class="uk-countdown-number uk-countdown-seconds"></div>
						        <div class="uk-countdown-label uk-margin-small uk-text-center uk-visible@s"><?php _e( 'Seconds', 'onepager-pro' ) ?></div>
						    </div>
						</div>
					</div> <!-- end-countdown -->
					<?php endif;?>
					<div class="social-links uk-margin uk-text-center" <?php echo $social_animation;?> >
						<?php foreach ( $contents['social'] as $social ): ?>
							<a class="icon" href="<?php echo $social ?>" target="_blank"></a>
						<?php endforeach; ?>
					</div><!-- social-links -->

				</div> <!-- width-expand -->
			</div> <!-- uk-grid-large -->
		</div> <!-- uk-container -->
	</div>
</section> <!-- end-section -->