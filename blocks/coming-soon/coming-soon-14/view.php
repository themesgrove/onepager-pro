<?php
	//Animation 
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'] .';repeat:' . 'true"' : '';	
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].';repeat:' . 'true"' : '';
	$countdown_animation = ($settings['countdown_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['countdown_animation'].';repeat:' . 'true"' : '';
?>
<section id="<?= $id; ?>" class="fp-section uk-section coming-soon coming-soon-14 uk-height-viewport uk-position-relative">
	<div class="uk-overlay-primary uk-position-cover"></div>
	
	<article class="uk-position-center">
		<!-- Title -->
		<?php if ($contents['title']): ?>
		<?php
			echo op_heading( 
				$contents['title'],
				$settings['heading_type'], 
				'uk-heading-primary uk-text-center', 
				$settings['title_transformation'], 
				$title_animation  
			); 
		?>
		<?php endif;?>
		<!-- Description -->
		<?php if ($contents['description']): ?>
			<div class="uk-text-lead uk-text-center uk-align-center " <?= $content_animation;?> >
				<?= $contents['description']?>
			</div>
		<?php endif;?>
		<?php if ($contents['date']): ?>
		<div class="countdown uk-flex uk-flex-center uk-text-center uk-flex-middle uk-container uk-container-center" <?php echo $countdown_animation;?> >
			<div class="uk-grid-small uk-child-width-auto" uk-grid uk-countdown="date: <?php echo $contents['date']; ?>">
			    <div>
			        <div class="uk-countdown-number uk-countdown-days"></div>
			        <div class="uk-countdown-label uk-margin-small uk-text-center uk-visible@s"><?php _e( 'Days', 'onepager-pro' ) ?></div>
			    </div>
			    <div class="uk-countdown-separator">:</div>
			    <div>
			        <div class="uk-countdown-number uk-countdown-hours"></div>
			        <div class="uk-countdown-label uk-margin-small uk-text-center uk-visible@s"><?php _e( 'Hours', 'onepager-pro' ) ?></div>
			    </div>
			    <div class="uk-countdown-separator">:</div>
			    <div>
			        <div class="uk-countdown-number uk-countdown-minutes"></div>
			        <div class="uk-countdown-label uk-margin-small uk-text-center uk-visible@s"><?php _e( 'Minutes', 'onepager-pro' ) ?></div>
			    </div>
			    <div class="uk-countdown-separator">:</div>
			    <div>
			        <div class="uk-countdown-number uk-countdown-seconds"></div>
			        <div class="uk-countdown-label uk-margin-small uk-text-center uk-visible@s"><?php _e( 'Seconds', 'onepager-pro' ) ?></div>
			    </div>
			</div>
		</div> <!-- end-countdown -->
		<?php endif;?>
	</article>
</section> <!-- end-section -->