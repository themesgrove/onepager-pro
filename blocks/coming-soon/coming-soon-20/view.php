<?php
	//Animation 
	$logo_animation = ($settings['logo_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['logo_animation'].';repeat:' . 'true"' : '';
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'] .';repeat:' . 'true"' : '';	
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].';repeat:' . 'true"' : '';
	$form_animation = ($settings['form_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['form_animation'].';repeat:' . 'true"' : '';
?>
<section id="<?= $id; ?>" class="fp-section uk-section coming-soon coming-soon-20 uk-height-viewport uk-position-relative">
	<div class="uk-overlay-primary uk-position-cover"></div>

	<article class="uk-position-center">
		<?php if ($contents['logo']): ?>
		<!-- Logo -->
		<div class="logo">
			<img class="op-logo uk-align-center uk-text-center" src="<?= $contents['logo']?>" alt="logo" <?= $logo_animation;?>>
		</div>
		<?php endif;?>
		<!-- Title -->
		<?php if ($contents['title']): ?>
		<?php
			echo op_heading( 
				$contents['title'],
				$settings['heading_type'], 
				'uk-heading-primary', 
				$settings['title_transformation'], 
				$title_animation  
			); 
		?>
		<?php endif;?>
		<!-- Description -->
		<?php if ($contents['description']): ?>
			<div class="uk-text-lead uk-text-center uk-align-center uk-width-1-2@s" <?= $content_animation;?> >
				<?= $contents['description']?>
			</div>
		<?php endif;?>
		<!-- Contact form -->
		<div class="uk-grid-item-match uk-flex-middle uk-text-center uk-align-center" <?php echo $form_animation;?>>
			<?php echo do_shortcode($contents['form']);?>
		</div>
	</article> 
	
</section> <!-- end-section -->