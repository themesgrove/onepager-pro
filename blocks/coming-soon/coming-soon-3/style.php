#<?= $id ?>{
	<?php if($styles['bg_image']):?>
	background-image: url(<?= $styles['bg_image']?>);
    background-repeat: no-repeat;
    background-size: cover;
	<?php endif;?>	
}
#<?= $id; ?> .uk-overlay-primary{
	background: <?= $styles['overlay_color']?>;
}
#<?= $id;?> .uk-heading-primary{
	font-size : <?= $settings['title_size']?>px;
	color : <?= $styles['title_color']?>;
}
#<?= $id ?> .uk-text-lead {
	font-size : <?= $settings['desc_size']?>px;
	color : <?= $styles['desc_color']?>;
}
#<?= $id ?> .countdown {
	color : <?= $styles['countdown_color']?>;
}
#<?= $id ?> .uk-countdown-number{
	font-size : <?= $settings['countdown_number_size']?>px;
}

@media(max-width:768px){
        #<?= $id ?> .uk-countdown-number{
        font-size : 40px;
    }
}
@media(max-width:640px){
        #<?= $id ?> .uk-countdown-number{
        font-size : 30px;
    }
}
#<?= $id ?> .uk-countdown-label{
	font-size : <?= $settings['countdown_label']?>px;
}
#<?= $id ?> .social-links a {
	color : <?= $styles['icon_color']?>;
}

#<?= $id ?> .countdown-number {
    background: <?= $styles['countdown_bg_color']?>;
    margin: 15px;
    width: 145px;
    height: 145px;
    border: 5px solid <?= $styles['countdown_bg_color']?>;
    padding: 20px 0 0 0;
    border-radius: 100%;
}
#<?= $id ?> .social-links > a {
    font-size: 18px;
    background: <?= $styles['countdown_bg_color']?>;
    width: 38px;
    height: 38px;
    line-height: 38px;
    border-radius: 100%;
}
