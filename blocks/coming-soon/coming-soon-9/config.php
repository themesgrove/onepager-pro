<?php

return array(

  'slug'      => 'pro-coming-soon-09', // Must be unique and singular
  'groups'    => array('Coming Soon'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array(
      'name'=>'title',
      'value' => "I'LL BE BACK SOON"
    ),
    array(
      'name'=>'date',
      'type'=>'date',
      'value'=>'08/15/2020',
    ),
    array(
      'name'=>'description',
      'type'=>'editor',
      'value'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
    ),
    array(
      'name'=> 'social',
      'label' => 'Social Links',
      'value' => array('http://facebook.com/themesgrove', 'http://twitter.com/themesgrove', 'http://linkedin.com/themesgrove', 'http://www.instagram.com/themesgrove')
    ),
    array(
      'name' => 'form',
      'label' => 'Contact Form Shortcode',
      'type' => 'textarea'
    )
  ),
  // Settings - $settings available on view file to access the option
  'settings' => array(

    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),

    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'uppercase',
      'options'  => array(
        '0'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
      ),
    ),

    array('label'=>'Description', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),    
    array(
      'name'     => 'content_animation',
      'label'    => 'Content Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
      ),
    ),

    array('label'=>'Countdown', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'countdown_number_size',
      'label' => 'Countdown Number Size',
      'append' => 'px',
      'value' => '40'
    ),

    array(
      'name' => 'countdown_label',
      'label' => 'Countdown Label Size',
      'append' => 'px',
      'value' => '16'
    ),
    array(
      'name'     => 'countdown_animation',
      'label'    => 'Countdown Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Social', 'type'=>'divider'), // Divider - Text
    array(
      'name'     => 'social_animation',
      'label'    => 'Social Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array('label'=>'Form', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'label_height',
      'label' => 'Label Height',
      'append' => 'px',
      'value' => '50'
    ),    
    array(
      'name' => 'label_width',
      'label' => 'Label Width',
      'append' => 'px',
      'value' => '340'
    ), 
    array(
      'name'     => 'form_animation',
      'label'    => 'Form Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),


  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => 'https://images.pexels.com/photos/695799/pexels-photo-695799.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260'
    ),
    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(20,11,5,0.36)'
    ),
    array('label'=>'Content', 'type'=>'divider'), // Divider - Text

    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#ffffff'
    ),
    array(
      'name'  => 'desc_color',
      'label' => 'description Color',
      'type'  => 'colorpicker',
      'value' => '#ffffff'
    ),
    array('label'=>'Countdown', 'type'=>'divider'), // Divider - Text
    array(
      'name'  => 'countdown_color',
      'label' => 'Countdown Color',
      'type'  => 'colorpicker',
      'value' => '#4d1f00'
    ),
    array(
      'name'  => 'countdown_bg_color',
      'label' => 'Countdown Background Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),
    array('label'=>'Social', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'icon_color',
      'label'   => 'Icon Color',
      'type'    => 'colorpicker',
      'value' => '#4d1f00'
    ),
    array('label'=>'Form', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'label_bg_color',
      'label'   => 'Label Background Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(255,255,255,0.45)'
    ),     
    array(
      'name'    => 'submit_color',
      'label'   => 'Submit Color',
      'type'    => 'colorpicker',
      'value' => '#ffffff'
    ),  
    array(
      'name'    => 'submit_bg_color',
      'label'   => 'Submit Background Color',
      'type'    => 'colorpicker',
      'value' => '#4d1f00'
    ),
    array(
      'name'    => 'placeholder_color',
      'label'   => 'placeholder Color',
      'type'    => 'colorpicker',
      'value' => '#4d1f00'
    ), 
  ),

);
