#<?= $id ?>{
	<?php if($styles['bg_image']):?>
	background-image: url(<?= $styles['bg_image']?>);
    background-repeat: no-repeat;
    background-size: cover;
	<?php endif;?>	
}
#<?= $id; ?> .uk-overlay-primary{
	background: <?= $styles['overlay_color']?>;
}
#<?= $id;?> .uk-heading-primary{
	font-size : <?= $settings['title_size']?>px;
	color : <?= $styles['title_color']?>;
}
#<?= $id ?> .uk-text-lead {
	font-size : <?= $settings['desc_size']?>px;
	color : <?= $styles['desc_color']?>;
}
#<?= $id ?> .countdown {
	color : <?= $styles['countdown_color']?>;
}
#<?= $id ?> .uk-countdown-number{
	font-size : <?= $settings['countdown_number_size']?>px;
}

@media(max-width:768px){
		#<?= $id ?> .uk-countdown-number{
		font-size : 40px;
	}
}
@media(max-width:640px){
		#<?= $id ?> .uk-countdown-number{
		font-size : 30px;
	}
}
#<?= $id ?> .uk-countdown-label{
	font-size : <?= $settings['countdown_label']?>px;
}
#<?= $id ?> .countdown-number {
    background: <?= $styles['countdown_bg_color']?>;
    margin: 15px;
    width: 135px;
    height: 135px;
    border: 1px solid <?= $styles['countdown_bg_color']?>;
    border-radius: 100%;
    padding: 15px 0 0 0;
}
#<?= $id ?> .social-links > a {
	color : <?= $styles['icon_color']?>;
    font-size: 18px;
    background: <?= $styles['countdown_bg_color']?>;
    width: 40px;
    height: 40px;
    border-radius: 100%;
    line-height: 40px;
}
#<?= $id ?> .wpforms-form {
    display: flex;
    justify-content: center;
}
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=date], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=datetime], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=datetime-local], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=email], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=month], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=number], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=password], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=range], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=search], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=tel], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=text], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=time], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=url], 
#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=week], 
#<?= $id ?> div.wpforms-container-full .wpforms-form select, 
#<?= $id ?> div.wpforms-container-full .wpforms-form textarea{
	height : <?= $settings['label_height']?>px;
	width: <?= $settings['label_width']?>px;
	background: <?= $styles['label_bg_color']?>;
	border-color: <?= $styles['submit_bg_color']?>;
}
#<?= $id ?> ::placeholder {
	color: <?= $styles['placeholder_color']?>;
	opacity: 0.5;
}

#<?= $id ?> div.wpforms-container-full .wpforms-form input[type=submit], 
#<?= $id ?> div.wpforms-container-full .wpforms-form button[type=submit], 
#<?= $id ?> div.wpforms-container-full .wpforms-form .wpforms-page-button{
	height : <?= $settings['label_height']?>px;
	color: <?= $styles['submit_color']?>;
	background: <?= $styles['submit_bg_color']?>;
	border-color: <?= $styles['submit_bg_color']?>;
}
