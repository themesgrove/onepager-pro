#<?php echo $id; ?>{
	<?php if($styles['bg_image']):?>
		background-image: url(<?php echo $styles['bg_image'];?>);
		background-size:cover;
	<?php endif;?>
	<?php if($styles['bg_color']):?>
		background-color : <?php echo $styles['bg_color']; ?>;
	<?php endif;?>
	
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;

}

#<?php echo $id;?>  .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px;
	color : <?php echo $styles['desc_color'];?>;
}

#<?php echo $id; ?> .uk-text-meta,
#<?php echo $id; ?> .heading-info .uk-text-lead,
#<?php echo $id; ?> .uk-card .uk-card-header .fa{
	color : <?php echo $styles['accent_color'];?>;
	font-size : <?php echo $settings['info_size'];?>px;
}

#<?php echo $id; ?> .contact-wrapper input[type="text"],
#<?php echo $id; ?> .contact-wrapper input[type="email"],
#<?php echo $id; ?> .contact-wrapper input[type="subject"],
#<?php echo $id; ?> .contact-wrapper textarea{
	background: transparent;
    border: 1px solid #fff;
	padding:10px;
	width:95%;
	color:#fff;
	font-size:14px;
}

#<?php echo $id; ?> ::placeholder {
  color:#fff;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color'];?>;
}

#<?php echo $id; ?> input[type="submit"]{
	background: <?php echo $styles['button_bg_color'];?>;
	color : <?php echo $styles['button_text_color'];?>;
	border: 1px solid <?php echo $styles['button_text_color'];?>;
	text-transform: uppercase;
    font-size: 16px;
	padding: 15px;
    width: 100%;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> input[type="submit"]:hover{
	background: <?php echo $styles['button_hover_color'];?>;
	color : <?php echo $styles['button_hover_text_color']?>;
	border: 1px solid <?php echo $styles['button_hover_color'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}


#<?php echo $id; ?> .social-links a,
#<?php echo $id; ?> .uk-card .uk-card-header .fa{
	color: <?php echo $styles['accent_color'];?>;
}
#<?php echo $id; ?> input[type="text"]:focus,
#<?php echo $id; ?> input[type="email"]:focus,
#<?php echo $id; ?> textarea:focus{
	border-color : <?php echo $styles['accent_color'];?>;
	outline : 0;
}

#<?php echo $id; ?> div.wpcf7-validation-errors, 
#<?php echo $id; ?> div.wpcf7-acceptance-missing {
    color: #fff;
}
@media(max-width:768px){
	#<?php echo $id; ?> input[type="submit"]{
		width:420px;
	}
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
}
