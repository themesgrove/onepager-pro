<?php 
	// title animation
	$animation_title = ($settings['animation_title']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_title'].'' : '';
	// info animation
	$info_animation = ($settings['info_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['info_animation'] : '';
	// form animation
	$form_animation = ($settings['form_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['form_animation'] : '';
 ?>


<section id="<?php echo $id; ?>" 
	class="uk-background-cover uk-background-norepeat uk-position-relative fp-section contact pro-contact-1"   
	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>
	data-src="<?php echo $styles['bg_image'];?>" uk-img>

	<div class="uk-overlay-primary uk-position-cover"></div>
		<div class="uk-section">
			<div class="uk-container-small uk-margin-auto uk-position-relative uk-padding">
				<div class="section-heading uk-text-center">
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$animation_title . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<div class="uk-text-lead uk-padding-small">
							<?php echo $contents['description'];?>
						</div>
					<?php endif; ?>
				</div> <!-- section-heading -->
			<div class="uk-child-width-1-2@s uk-grid uk-margin-large-top" uk-grid>
				<div class="uk-card uk-margin-medium-top" <?php echo $info_animation;?>>
					<div class="heading-info uk-margin-medium-bottom">	
						<?php if($contents['info_text']):?>
							<p class="uk-text-lead">
								<?php echo $contents['info_text']; ?> 	
							</p>
						<?php endif; ?>
					</div>

					<?php if($contents['address']):?>
						<div class="uk-card-header uk-padding-remove uk-margin">
							<div class="uk-grid-small uk-flex-middle" uk-grid>
								<div class="uk-width-auto">
									<span class="fa fa-home"></span>
								</div>
								<div class="uk-width-expand">
									<p class="uk-text-meta uk-margin-remove-top">
										<?php echo $contents['address']; ?>
									</p>
								</div>
							</div>
						</div> <!-- uk-card-header -->
					<?php endif; ?>

					<?php if($contents['phone']):?>
						<div class="uk-card-header uk-padding-remove uk-margin">
							<div class="uk-grid-small uk-flex-middle" uk-grid>
								<div class="uk-width-auto">
									<span class="fa fa-phone"></span>
								</div>
								<div class="uk-width-expand">
									<p class="uk-text-meta uk-margin-remove-top">
										<?php echo $contents['phone']; ?>
									</p>
								</div>
							</div>
						</div> <!-- uk-card-header -->
					<?php endif; ?>


					<?php if($contents['email']):?>
						<div class="uk-card-header uk-padding-remove uk-margin">
							<div class="uk-grid-small uk-flex-middle" uk-grid>
								<div class="uk-width-auto">
									<span class="fa fa-envelope-o"></span>
								</div>
								<div class="uk-width-expand">
									<p class="uk-text-meta uk-margin-remove-top">
										<?php echo $contents['email']; ?>
									</p>
								</div>
							</div>
						</div> <!-- uk-card-header -->
					<?php endif; ?>

					<div class="social-links uk-margin">
						<?php foreach ( $contents['social'] as $social ): ?>
							<a class="icon" href="<?php echo $social ?>" target="_blank"></a>
						<?php endforeach; ?>
					</div><!-- social-links -->

				</div><!-- uk-card -->

				<div class="contact-wrapper uk-position-relative" <?php echo $form_animation;?>>
						<?php echo do_shortcode($contents['form']);?>
				</div><!-- uk-grid-item-match -->

			</div><!-- uk-child-width -->
		</div> <!-- uk-container -->
	</div>
</section> <!-- end-section -->
