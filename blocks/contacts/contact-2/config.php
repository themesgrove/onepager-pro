<?php

return array(

  'slug'      => 'pro-contact-2', // Must be unique and singular
  'groups'    => array('contact'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Top Image', 'type'=>'divider'), // Divider - Text
    array(
      'name'  => 'top_image',
      'label' => 'Image',
      'type'  => 'image',
      'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/contact-2-top.png'
    ),
    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'title',
      'value'=>''
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Marketers at the world’s fastest-growing companies use Onepager to transform <br>strangers into customers. Here’s what they have to say.'
    ),

    array('label'=>'Contact Form', 'type'=>'divider'), // Divider - Background
    array(
      'name' => 'form',
      'label' => 'Contact Form Shortcode',
      'type' => 'textarea',
      'value' => '[contact-form-7 id="1706" title="Contact form 1"]'
    ),
    array('label'=>'Contact Info', 'type'=>'divider'), // Divider - Background
    array(
      'name'=>'address',
      'type' => 'textarea',
      'value' => '23 Salient Road, London, United Kingdom, PO-LDN 123'
    ),
    array(
      'name'=>'phone',
      'value'=> '+1(555)666.777.8888'
    ),
    array(
      'name'=>'email',
      'type'=>'text',
      'value'=> 'example@demo.com'
    ),
    array(
      'name'=> 'social',
      'label' => 'Social Links',
      'value' => array('http://facebook.com/themexpert', 'http://twitter.com/themexpert', 'http://linkedin.com/themexpert')
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(

    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title', 'type'=>'divider'), // Divider - Background
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit' => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'animation_title',
      'label'    => 'Title Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('name'=>'animation_repeat', 'label'=>'Animation Repeat', 'type'=>'switch'),

    array('label'=>'Contat Form', 'type'=>'divider'), // Divider - Background
    array(
      'name'     => 'form_animation',
      'label'    => 'Form Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    
   array('label'=>'Contact Info', 'type'=>'divider'), // Divider - Background
    array(
      'name' => 'info_size',
      'label' => 'Info Size',
      'append' => 'px',
      'value' => '16'
    ),
    array(
      'name'     => 'info_animation',
      'label'    => 'Info Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    ),


  // Fields - $styles available on view file to access the option
  
  'styles' => array(
    array('label'=>'Background', 'type'=>'divider'), // Divider - Background
    array(
      'name'  => 'bg_image',
      'label' => 'Image',
      'type'  => 'image',
      'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/banner-3-bg.png'
    ),

    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(17,38,43,0.82)'
    ),
    array(
      'name'    => 'bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#252c3a'
    ),
    array(
      'name'  => 'desc_color',
      'label' => 'Desc Color',
      'type'  => 'colorpicker',
      'value' => '#252c3a'
    ),

    array('label'=>'Contact Form', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'button_text_color',
      'label'   => 'Button Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Button Bg Color',
      'type'    => 'colorpicker',
      'value'   => 'rgba(0,0,0,0.68)'
    ),
    array('label'=>'Contact Info', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'accent_color',
      'label'   => 'Icon/Text Color',
      'type'    => 'colorpicker',
      'value'   => '#252c3a'
    ),


  ),

);
