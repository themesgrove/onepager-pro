#<?php echo $id; ?>{
	<?php if($styles['bg_image']):?>
		background-image: url(<?php echo $styles['bg_image'];?>);
		background-size:cover;
	<?php endif;?>	
}
#<?php echo $id; ?> .uk-background-default{
	<?php if($styles['bg_color']):?>
		background-color : <?php echo $styles['bg_color']; ?>;
	<?php endif;?>
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;

}


#<?php echo $id;?>  .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px;
	color : <?php echo $styles['desc_color'];?>;
}

#<?php echo $id; ?> .uk-text-meta,
#<?php echo $id; ?> .heading-info .uk-text-lead,
#<?php echo $id; ?> .uk-card .uk-card-header .fa{
	color : <?php echo $styles['accent_color'];?>;
	font-size : <?php echo $settings['info_size'];?>px;
}

#<?php echo $id; ?> .contact-wrapper input[type="text"],
#<?php echo $id; ?> .contact-wrapper input[type="email"],
#<?php echo $id; ?> .contact-wrapper input[type="subject"],
#<?php echo $id; ?> .contact-wrapper textarea{
	background: transparent;
    border: 1px solid <?php echo $styles['accent_color'];?>;
	padding:10px;
	width:100%;
	font-size: 14px;
	color:<?php echo $styles['accent_color'];?>;
}

#<?php echo $id; ?> ::placeholder,
#<?php echo $id; ?> form {
  color:<?php echo $styles['accent_color'];?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color'];?>;
}

#<?php echo $id; ?> input[type="submit"]{
	background: <?php echo $styles['button_bg_color'];?>;
	color : <?php echo $styles['button_text_color'];?>;
	border: 1px solid <?php echo $styles['button_text_color'];?>;
	text-transform: uppercase;
    font-size: 16px;
	padding: 15px;
    width: 100%;
}
#<?php echo $id; ?> input[type="submit"]:hover{
	background: <?php echo $styles['button_text_color'];?>;
	color : <?php echo $styles['button_bg_color']?>;
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
}

#<?php echo $id; ?> .social-links a,
#<?php echo $id; ?> .uk-card .uk-card-header .fa{
	color: <?php echo $styles['accent_color'];?>;
}
#<?php echo $id; ?> input[type="text"]:focus,
#<?php echo $id; ?> input[type="email"]:focus,
#<?php echo $id; ?> textarea:focus{
	border-color : <?php echo $styles['accent_color'];?>;
	outline : 0;
}


@media(width:768px){
	#<?php echo $id; ?> input[type="submit"]{
		width:420px;
	}
}