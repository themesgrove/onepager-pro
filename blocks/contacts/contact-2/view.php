<?php 

	// title animation
	$animation_title = ($settings['animation_title']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_title'].'' : '';
	// form animation
	$form_animation = ($settings['form_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['form_animation'] . ';delay:400"' : '';
	// info animation
	$info_animation = ($settings['info_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['info_animation'] . ';delay:500"' : '';
 ?>


<section id="<?php echo $id; ?>" 
	class="uk-background-cover uk-background-norepeat uk-position-relative contact pro-contact-2"   
	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>
	data-src="<?php echo $styles['bg_image'];?>" uk-img>

	<div class="uk-overlay-primary uk-position-cover"></div>
		<div class="uk-padding uk-width-1-2@m uk-width-1-1@s uk-margin-auto uk-position-relative uk-background-default">
			<div class="section-heading uk-text-center ">
				<?php if ($contents['top_image']): ?>
					<div <?php echo ($settings['animation_title'] ? $animation_title . ';delay:' . '100"' : ''); ?>>
						
						<img width="300" src="<?php echo $contents['top_image'];?>">
					</div>
				<?php endif; ?>
				<?php if($contents['title']):?>
					<!-- Section Title -->
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary', 
							'uk-text-' . $settings['title_transformation'], 
							$animation_title . '"'
						); 
					?>
				<?php endif; ?>

				<?php if($contents['description']):?>
					<!-- Section Sub Title -->
					<div class="uk-text-lead uk-padding-small"
						<?php echo ($settings['animation_title'] ? $animation_title . ';delay:' . '300"' : ''); ?>>
						<?php echo $contents['description'];?>
					</div>
				<?php endif; ?>
			</div> <!-- section-heading -->
			<div class="contact-wrapper uk-position-relative" <?php echo $form_animation;?>>
					<?php echo do_shortcode($contents['form']);?>
			</div><!-- uk-grid-item-match -->
			<div class="uk-card uk-margin-medium-top" <?php echo $info_animation;?>>
				<?php if($contents['address']):?>
					<div class="uk-card-header uk-padding-remove uk-margin">
						<div class="uk-grid-small uk-flex-middle" uk-grid>
							<div class="uk-width-auto">
								<span class="fa fa-home"></span>
							</div>
							<div class="uk-width-expand">
								<p class="uk-text-meta uk-margin-remove-top">
									<?php echo $contents['address']; ?>
								</p>
							</div>
						</div>
					</div> <!-- uk-card-header -->
				<?php endif; ?>
				<?php if($contents['phone']):?>
					<div class="uk-card-header uk-padding-remove uk-margin">
						<div class="uk-grid-small uk-flex-middle" uk-grid>
							<div class="uk-width-auto">
								<span class="fa fa-phone"></span>
							</div>
							<div class="uk-width-expand">
								<p class="uk-text-meta uk-margin-remove-top">
									<?php echo $contents['phone']; ?>
								</p>
							</div>
						</div>
					</div> <!-- uk-card-header -->
				<?php endif; ?>
				<?php if($contents['email']):?>
					<div class="uk-card-header uk-padding-remove uk-margin">
						<div class="uk-grid-small uk-flex-middle" uk-grid>
							<div class="uk-width-auto">
								<span class="fa fa-envelope-o"></span>
							</div>
							<div class="uk-width-expand">
								<p class="uk-text-meta uk-margin-remove-top">
									<?php echo $contents['email']; ?>
								</p>
							</div>
						</div>
					</div> <!-- uk-card-header -->
				<?php endif; ?>
				<div class="social-links uk-margin">
					<?php foreach ( $contents['social'] as $social ): ?>
						<a class="icon" href="<?php echo $social ?>" target="_blank"></a>
					<?php endforeach; ?>
				</div><!-- social-links -->
			</div><!-- uk-card -->
		</div><!-- uk-child-width -->
	</div> <!-- uk-container -->
</section> <!-- end-section -->
