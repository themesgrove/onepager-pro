<?php

return array(

  'slug'      => 'pro-contact-3', // Must be unique and singular
  'groups'    => array('contact'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array(
      'name'=>'title',
      'value' => 'Discover the tools that will help you plan.'
    ),
    array(
      'name'=>'description',
      'type'=>'editor',
      'value'=> '-Special Edition'
    ),
    array('label'=>'Contact Form', 'type'=>'divider'), // Divider - Background
    array(
      'name'=>'title_form',
      'value' => 'Contact Us'
    ),
    array(
      'name' => 'form',
      'label' => 'Contact Form Shortcode',
      'type' => 'textarea',
      'value' => '[contact-form-7 id="1706" title="Contact form 1"]'
    ),

  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array(
      'name' => 'note_form',
      'label' => 'Form',
      'type' => 'divider'
    ),
    array(
      'name'     => 'form_alignment',
      'label'    => 'Form Alignment',
      'type'     => 'select',
      'value'    => 'right',
      'options'  => array(
        'left'    => 'Left',
        'right'   => 'Right'
      ),
    ),
    array(
      'name'     => 'form_grid',
      'label'    => 'Form Grid',
      'type'     => 'select',
      'value'    => 'width-1-2',
      'options'  => array(
        'width-1-2'   => 'Half',
        'width-1-3'   => 'One Thrids',
        'width-1-4'   => 'One Fourth',
        'width-2-3'   => 'Two Thirds',
      ),
    ),
    array(
      'name'     => 'animation_form',
      'label'    => 'Animation Form',
      'type'     => 'select',
      'value'    => 'slide-right-medium',
      'options'  => array(
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
        ),
      ),
    array( 'label' => 'Title', 'type' => 'divider'),
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0           => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '20'
    ),

    array(
      'name'     => 'animation_content',
      'label'    => 'Animation Content',
      'type'     => 'select',
      'value'    => 'slide-left-medium',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array( 'label' => 'Button', 'type' => 'divider'),
      array(
      'name'     => 'button_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
       'inherit'      => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array(
      'name'    => 'bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#fff2f1'
    ),
    array('name'=>'padding_bottom_off', 'label'=>'Padding Off', 'type'=>'switch'),
    
    array('label'=>'Content', 'type'=>'divider'),
    array(
      'name'    => 'content_bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),
    array(
      'name'  => 'text_color',
      'label' => 'Text Color',
      'type'  => 'colorpicker',
      'value' => '#ff8777'
    ),
    array('label'=>'Button Styles', 'type'=>'divider'),
    array(
      'name'    => 'button_text_color',
      'label'   => 'Button Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Button Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#ff8777'
    ),
  ),
);
