#<?php echo $id ?>{
	<?php if($styles['bg_color']):?>
		background-color : <?php echo $styles['bg_color'] ?>;
	<?php endif;?>
	
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id ?> .uk-heading-primary,
#<?php echo $id ?> .uk-text-large{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}
#<?php echo $id ?> .uk-text-lead {
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['text_color']?>;
	letter-spacing:2px;
}
#<?php echo $id; ?> .contact-wrapper input[type="text"],
#<?php echo $id; ?> .contact-wrapper input[type="email"],
#<?php echo $id; ?> .contact-wrapper input[type="subject"],
#<?php echo $id; ?> .contact-wrapper textarea{
    padding: 10px;
    width: 96%;
    font-size: 14px;
    border: 1px solid #ddd;
    max-width: 750px;
    min-width: 750px;
}


#<?php echo $id; ?> input[type="submit"]{
	background: <?php echo $styles['button_bg_color'];?>;
	color : <?php echo $styles['button_text_color'];?>;
	border: 1px solid <?php echo $styles['button_text_color'];?>;
	text-transform: <?php echo ($settings['button_transformation']) ?>;
    font-size: 16px;
	padding: 15px;
	width: 100%;
	max-width: 750px;
    min-width: 750px;
	letter-spacing:2px;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> input[type="submit"]:hover{
	background: <?php echo $styles['button_text_color'];?>;
	color : <?php echo $styles['button_bg_color']?>;
	border: 1px solid <?php echo $styles['button_bg_color']?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}

#<?php echo $id ?> .uk-background-default{
	background-color:<?php echo $styles['content_bg_color'];?>
}
#<?php echo $id; ?> .contact-wrapper input:focus{
	 border: 1px solid <?php echo $styles['button_bg_color'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}

#<?php echo $id; ?> input:focus {
	outline:none;
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> input[type="submit"]:hover{
	outline:none; 
}
#<?php echo $id; ?> select:focus { 
	outline:none; 
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}

#<?php echo $id; ?> textarea:focus { 
	outline:none; 
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
}

@media(max-width:768px){
	#<?php echo $id?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}