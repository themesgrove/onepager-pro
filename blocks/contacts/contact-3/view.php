<?php

	// media grid
	$form_grid = 'uk-'. $settings['form_grid'] . '@m';
	// Animation media
	$animation_form = ($settings['animation_form']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_form'] . '"' : '';
	// animation content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';

	// Alignment
	$content_position = '';
	// media padding
	$media_padding = 'uk-container-item-padding-remove-left';
	
	if($settings['form_alignment'] == 'right'){
		$content_position = 'uk-flex-first@m uk-first-column';
		$media_padding = 'uk-container-item-padding-remove-right';
	}
	// Text transformation class
	$heading_class = ($settings['title_transformation']) ? 'uk-text-' . $settings['title_transformation'] : '';

?>
<section id="<?php echo $id; ?>" class="fp-section contact pro-contact-3">
	<div class="uk-section <?php echo $styles['padding_bottom_off'] ? 'uk-padding-remove': '';?>">
		<div class="uk-padding-large uk-padding-remove-top uk-padding-remove-bottom">
			<article class="uk-grid-large " uk-grid>
				<!-- Media -->
				<div class="<?php echo $form_grid?> uk-width-1-1@s uk-grid-item-match uk-flex-middle">
					<div class="uk-panel uk-padding-small" <?php echo $animation_form;?>>
						<div class="contact-wrapper uk-position-relative">
							<?php if($contents['title_form']): ?>
								<h2 class="uk-text-large uk-margin-medium-top uk-margin-medium-bottom uk-text-center <?php echo $heading_class; ?>" >
									<?php echo $contents['title_form']?>
								</h2>
							<?php endif;?>
							<?php echo do_shortcode($contents['form']);?>
						</div><!-- uk-grid-item-match -->
					</div>
				</div>
				<div class="uk-width-expand@m uk-background-default uk-grid-item-match uk-flex-middle <?php echo $content_position;?>">
					<div class="uk-panel">
						<!-- Title -->
						<?php if($contents['title']): ?>
						<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary', 
							'uk-text-' . $settings['title_transformation'], 
							$animation_content . '"'
						); 
						?>
						<?php endif;?>
						<!-- Description -->
						<?php if($contents['description']): ?>
							<div class="uk-text-lead" 
								<?php echo ($settings['animation_content'] ? $animation_content : ''); ?>>
								<?php echo $contents['description']?>	
							</div>
						<?php endif; ?>
					</div>	<!-- uk-panel -->
				</div> <!-- uk-grid-match -->
			</article> <!-- uk-article --> 
		</div> <!-- uk-container -->
	</div> <!-- uk-section --> 
</section> <!-- end-section --> 