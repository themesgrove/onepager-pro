<?php 

	// title animation
	$animation_title = ($settings['animation_title']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_title'].'' : '';
	// form animation
	$form_animation = ($settings['form_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['form_animation'] : '';

 ?>


<section id="<?php echo $id; ?>" class="contact pro-contact-4">
	<div class="uk-section">
		<div class="uk-container">
			<div class="uk-width-1-2@m uk-width-1-1@s uk-margin-auto">
				<div class="section-heading uk-text-center uk-margin-large-bottom">
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$animation_title . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<div class="uk-text-lead uk-padding-small"
							<?php echo ($settings['animation_title'] ? $animation_title : ''); ?>>
							<?php echo $contents['description'];?>
						</div>
					<?php endif; ?>
				</div> <!-- section-heading -->

				<div class="contact-wrapper uk-padding-small" <?php echo $form_animation;?>>
					<?php echo do_shortcode($contents['form']);?>
				</div><!-- contact-wrapper -->
			</div><!-- uk-padding -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
