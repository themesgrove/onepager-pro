#<?php echo $id;?> .uk-background-secondary{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;

}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
}



#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}
#<?php echo $id; ?> form.wpcf7-form {
    text-align: center;
}

#<?php echo $id; ?> .op-form-wrapper input[type=email], 
#<?php echo $id; ?> .op-form-wrapper input[type=number], 
#<?php echo $id; ?> .op-form-wrapper input[type=password], 
#<?php echo $id; ?> .op-form-wrapper input[type=reset], 
#<?php echo $id; ?> .op-form-wrapper input[type=search], 
#<?php echo $id; ?> .op-form-wrapper input[type=tel], 
#<?php echo $id; ?> .op-form-wrapper input[type=text], 
#<?php echo $id; ?> .op-form-wrapper input[type=url], 
#<?php echo $id; ?> .op-form-wrapper textarea{
    background: transparent;
    max-width: 87%;
    padding: 12px;
    border: 1px solid #ddd;
    border-radius: 5px;
    font-size: 15px;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .op-form-wrapper .select-control {
    display: block;
    width: 100%;
    color: #787878;;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ddd;
    border-radius: 5px;
    height: 43px;
    margin-top: 25px;
    font-size: 15px;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .op-form-wrapper input:focus {
	outline:none;
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?>  input[type="submit"]:hover{
	outline:none; 
}
#<?php echo $id; ?> .op-form-wrapper select:focus { 
	outline:none; 
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> input[type="submit"]{
	background: <?php echo $styles['button_bg_color'];?>;
	color : <?php echo $styles['button_text_color'];?>;
	border: 1px solid <?php echo $styles['button_text_color'];?>;
	text-transform: <?php echo $settings['button_transformation'];?>;
    font-size: <?php echo $settings['button_font_size'];?>px;
    border-radius: <?php echo $styles['button_border_radius'];?>px;
	padding:14px 45px;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> input[type="submit"]:hover{
	background: <?php echo $styles['button_text_color'];?>;
	color : <?php echo $styles['button_bg_color']?>;
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
}