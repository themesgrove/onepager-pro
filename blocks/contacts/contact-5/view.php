<?php

	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
?>


<section id="<?php echo $id;?>" class="uk-section uk-position-relative contact pro-contact-5 uk-background-cover uk-background-norepeat uk-background-top-center"  <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container uk-margin-auto-left" uk-grid>
			<article class="uk-article uk-position-relative uk-width-1-2@m uk-width-1-1@s uk-margin-auto uk-padding-large uk-border-rounded uk-background-secondary">
				<div class="section-heading uk-margin-medium-bottom uk-text-<?php echo $settings['title_alignment'];?>">	
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<h1 class="uk-heading-primary uk-margin-remove uk-text-<?php echo $settings['title_transformation'];?>" <?php echo ($settings['title_animation'])? $title_animation  : '' ;?>>
							<?php echo $contents['title'];?>
						</h1>
					<?php endif; ?>
					<?php if ($contents['title_image']): ?>
						<img src="<?php echo $contents['title_image'];?>">
					<?php endif; ?>
					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead uk-margin-medium" <?php echo ($settings['title_animation'])? $title_animation  : '' ;?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>
				</div> <!-- section-heading -->
				<?php if ($contents['form']): ?>
					<div <?php echo ($settings['title_animation'])? $title_animation  : '' ;?>>
						<?php echo do_shortcode($contents['form']);?>
					</div>
				<?php endif; ?>
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
