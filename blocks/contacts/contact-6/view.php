<?php 
	
	// title animation
	$animation_title = ($settings['animation_title']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_title'].'' : '';
	// form animation
	$form_animation = ($settings['form_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['form_animation']  : '';

 ?>


<section id="<?php echo $id; ?>" class="uk-background-cover uk-background-norepeat uk-position-relative contact pro-contact-6" data-src="<?php echo $styles['bg_image'];?>" uk-image>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container">
			<div class="uk-width-1-2@m uk-width-1-1@s uk-margin-auto">
			<div class="uk-overlay-primary uk-position-cover"></div>
				<div class="section-heading uk-text-center uk-margin-large-bottom  uk-position-relative">
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<h1 class="uk-heading-primary uk-position-relative uk-text-<?php echo $settings['title_transformation'];?>"
							<?php echo ($settings['animation_title'] ? $animation_title  : ''); ?>>
							<?php echo $contents['title'];?>
						</h1>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<div class="uk-text-lead uk-padding-small"
							<?php echo ($settings['animation_title'] ? $animation_title  : ''); ?>>
							<?php echo $contents['description'];?>
						</div>
					<?php endif; ?>
				</div> <!-- section-heading -->

				<div class="contact-wrapper uk-padding-small  uk-position-relative" <?php echo $form_animation;?>>
					<?php echo do_shortcode($contents['form']);?>
				</div><!-- contact-wrapper -->
			</div><!-- uk-padding -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
