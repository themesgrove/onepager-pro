<?php
   
	// Content Animation
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].';' : '';
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';' : '';
	// Content Alignment
	//$content_alignment = ($settings['content_alignment']) ? $settings['content_alignment'] : '';
?>


<section id="<?php echo $id;?>" class="uk-position-relative uk-background-norepeat uk-background-cover cta pro-cta-2 " <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container">
			<div class="uk-grid-large uk-position-relative uk-padding-small uk-grid-match uk-flex-middle" uk-grid>
				<div class="uk-width-expand@m">
					<div class="uk-text-left">
						<!-- Title -->
						<?php if($contents['title']): ?>
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-remove', 
								'uk-text-' . $settings['title_transformation'], 
								$content_animation . '"'
							); 
						?>
						<?php endif; ?>

						<!-- Description -->
						<?php if($contents['description']): ?>
							<div class="uk-text-lead" <?php echo ($settings['content_animation']) ? $content_animation . ';delay:' .'300"' : ''?>>
							<?php echo $contents['description'];?></div>
						<?php endif; ?>
					</div> <!-- text-alignment -->
				</div> <!-- uk-width-expand -->

				<!-- Link -->
				<div class="uk-width-1-4@m">
					<?php if ($contents['link']): ?>
						<p <?php echo ($settings['button_animation']) ? $button_animation . ';delay:' .'500"' : ''?>>	
							<!-- Link -->
							<?php echo op_link($contents['link'], 'uk-inline-blcok uk-align-right@m uk-button uk-button-primary uk-button-large uk-margin-remove');?>
						</p>
					<?php endif; ?>
				</div> <!--uk-width-1-4@m -->
			</div><!-- uk-grid-large -->
		</div><!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
