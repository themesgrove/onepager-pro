#<?php echo $id; ?> .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .wpforms-form{
	display: flex;
}

#<?php echo $id; ?> .wpforms-form input[type=date], 
#<?php echo $id; ?> .wpforms-form input[type=datetime], 
#<?php echo $id; ?> .wpforms-form input[type=datetime-local], 
#<?php echo $id; ?> .wpforms-form input[type=email],
#<?php echo $id; ?> .wpforms-form input[type=month], 
#<?php echo $id; ?> .wpforms-form input[type=number], 
#<?php echo $id; ?> .wpforms-form input[type=password], 
#<?php echo $id; ?> .wpforms-form input[type=range], 
#<?php echo $id; ?> .wpforms-form input[type=search], 
#<?php echo $id; ?> .wpforms-form input[type=tel], 
#<?php echo $id; ?> .wpforms-form input[type=text], 
#<?php echo $id; ?> .wpforms-form input[type=time],
#<?php echo $id; ?> .wpforms-form input[type=url],
#<?php echo $id; ?> .wpforms-form input[type=week], 
#<?php echo $id; ?> .wpforms-form select, 
#<?php echo $id; ?> .wpforms-form textarea {
    background-color: transparent;
    border: none;
    border-bottom: 1px solid <?php echo $styles['placeholder_color'];?>;
}
#<?php echo $id; ?> .wpforms-form input[type=submit], 
#<?php echo $id; ?> .wpforms-form button[type=submit], 
#<?php echo $id; ?> .wpforms-form .wpforms-page-button {
    border: none;
    background: <?php echo $styles['button_bg_color'];?>;
    color: <?php echo $styles['button_text_color'];?>;
	font-size : <?php echo $settings['button_font_size'];?>px; 
    padding: 10px 28px;
    border-radius: 50px;
    -webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .wpforms-form input[type=submit]:hover, 
#<?php echo $id; ?> .wpforms-form button[type=submit]:hover, 
#<?php echo $id; ?> .wpforms-form .wpforms-page-button:hover{
	background: <?php echo $styles['button_text_color'];?>;
	color: <?php echo $styles['button_bg_color'];?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s eaase;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .wpforms-form input:focus, 
#<?php echo $id; ?> .wpforms-form textarea:focus, 
#<?php echo $id; ?> .wpforms-form select:focus {
    border: none;
    border-bottom: 1px solid <?php echo $styles['button_bg_color'];?>;
}
#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['bg_color'];?>;
}

#<?php echo $id; ?> div.wpforms-container-full, #<?php echo $id; ?> div.wpforms-container-full .wpforms-form *{
	width: 90%;
}

#<?php echo $id; ?> ::placeholder {
  color: <?php echo $styles['placeholder_color'];?>;
  opacity: 1; /* Firefox */
}
#<?php echo $id; ?> :-ms-input-placeholder { 
  color: <?php echo $styles['placeholder_color'];?>;
}
#<?php echo $id; ?> ::-ms-input-placeholder {
  color: <?php echo $styles['placeholder_color'];?>;
}

@media(max-width:768px){
	#<?php echo $id?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +20 ?>px;
	}
}