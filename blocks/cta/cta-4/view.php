<?php
	// Content Animation
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].';' : '';
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';' : '';
?>


<section id="<?php echo $id;?>" class="uk-position-relative uk-background-norepeat uk-background-cover cta pro-cta-4 ">
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container">
			<div class="uk-position-relative uk-background-cover uk-background-norepeat uk-background-center-center uk-padding-large"data-src="<?php echo $styles['bg_image']; ?>" uk-img>
			<div class="uk-background-cover" uk-grid>
			    <div class="uk-width-expand@m">
			        <div class="uk-card">
						<div class="cta-content">

							<div class="op-icon uk-display-inline-block uk-float-left" <?php echo ($settings['content_animation']) ? $content_animation . ';delay:' .'100"' : ''?>>
								<i class="fa fa-phone"></i>
							</div>
							<div class="content uk-float-left">
									<!-- Title -->
								<?php if($contents['title']): ?>
									<?php
										echo op_heading( 
											$contents['title'],
											$settings['heading_type'], 
											'uk-heading-primary uk-margin-remove', 
											'uk-text-' . $settings['title_transformation'], 
											$content_animation . '"'
										); 
									?>
								<?php endif; ?>

								<!-- Description -->
								<?php if($contents['description']): ?>
									<div class="uk-text-lead" <?php echo ($settings['content_animation']) ? $content_animation  . ';delay:' .'300"' : ''?>>
									<?php echo $contents['description'];?></div>
								<?php endif; ?>
							</div>

						</div>

			        </div>
			    </div>
			    <div class="uk-width-1-3@m">
			        <div class="uk-card">					
						<?php if ($contents['link']): ?>
							<div <?php echo ($settings['button_animation']) ? $button_animation . ';delay:' .'500"' : ''?>>	
								<!-- Link -->
								<?php echo op_link($contents['link'], 'uk-inline-blcok uk-align-right@m uk-button uk-button-primary uk-button-large uk-margin-remove');?>
							</div>
						<?php endif; ?>
					</div>
			    </div>
			</div>
			</div>
		</div><!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
