#<?php echo $id; ?>{
	background-color : <?php echo $styles['bg_color']; ?>;
}
#<?php echo $id; ?> .uk-heading-primary{
	font-size : <?php echo $settings['title_size']; ?>px;
	color : <?php echo $styles['title_color']; ?>;
}

#<?php echo $id; ?> .uk-text-lead{
	font-size : <?php echo $settings['desc_size']; ?>px;
	color : <?php echo $styles['desc_color']; ?>;
}

#<?php echo $id; ?> .uk-button-primary{
	font-size : <?php echo $settings['button_font_size']; ?>px;
	background: <?php echo $styles['button_bg_color']; ?>;
	color : <?php echo $styles['button_text_color']; ?>;
	text-transform:<?php echo $settings['button_transformation']; ?>;
	border-radius: 5px;
}

#<?php echo $id; ?> .uk-button-primary.op-button-two{
	margin-left: 20px;
	color : <?php echo $styles['bg_color']; ?>;
	background: <?php echo $styles['button_text_color']; ?>;
}

#<?php echo $id; ?> .uk-button-primary.op-button-two:hover{
	margin-left: 20px;
	color : <?php echo $styles['button_text_color']; ?>;
	background: <?php echo $styles['button_bg_color']; ?>;
}

#<?php echo $id; ?> .uk-button-primary:hover{
	background : <?php echo $styles['button_text_color']; ?>;
	color : <?php echo $styles['button_bg_color']; ?>;
}


@media(max-width:768px){
	#<?php echo $id; ?> .uk-heading-primary{
		font-size : <?php echo ( $settings['title_size'] / 1.5 ); ?>px;
	}
}
