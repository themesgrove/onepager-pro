<?php
	// Content Animation
	$content_animation = ( $settings['content_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['content_animation'] . ';"' : '';

	// Button Animation
	$button_animation = ( $settings['button_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['button_animation'] . '"' : '';
?>
<section id="<?php echo $id; ?>" class="fp-section cta pro-cta-5">
	<div class="uk-section">
		<div class="uk-container">
			<div class="uk-grid-large" uk-grid>
				<div class="uk-width-expand@m">
					<div class="" <?php echo $content_animation; ?>>
						<!-- Title -->
						<?php if ( $contents['title'] ) : ?>
							<?php 
								echo op_heading(
									$contents['title'],
									$settings['heading_type'],
									'uk-heading-primary  uk-text-'.$settings['title_transformation'],
									$content_animation
								); 
							?>
						<?php endif; ?>

						<!-- Description -->
						<?php if ( $contents['description'] ) : ?>
							<p class="uk-text-lead">
								<?php echo $contents['description']; ?>	
							</p>
						<?php endif; ?>
					</div>

				</div> <!-- width-expand -->
				<!-- Link -->
				<div class="uk-width-1-2@m uk-flex uk-flex-middle uk-flex-right" <?php echo $button_animation; ?>>
					<?php echo op_link( $contents['link'], 'uk-button uk-button-primary uk-button-large' ); ?>
					<?php echo op_link( $contents['link_2'], 'op-button-two uk-button uk-button-primary uk-button-large' ); ?>
				</div> <!-- end-link -->
			</div> <!-- uk-grid-large -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
