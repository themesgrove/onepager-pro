<?php
	// animation repeat
	$animation_repeat = '';
	// media grid
	$media_grid = 'uk-'. $settings['media_grid'] . '@m';
	// Animation media
	$animation_media = ($settings['animation_media']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_media'] .';repeat:' . ($animation_repeat ? 'true' : 'false') . '"' : '';
	// animation contant
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';
	// animation slider items
	$animation_slider_item = ($settings['slider_item_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['slider_item_animation'] .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:400"' : '';
	// Alignment
	$content_position = '';
	// media padding
	$media_padding = 'uk-container-item-padding-remove-left';
	
	if($settings['media_alignment'] == 'right'){
		$content_position = 'uk-flex-first@m uk-first-column';
		$media_padding = 'uk-container-item-padding-remove-right';
	}
	// Text transformation class
	$heading_class = ($settings['title_transformation']) ? 'uk-text-' . $settings['title_transformation'] : '';

?>
<section id="<?php echo $id; ?>" class="fp-section features pro-features-1 uk-cover-container uk-padding-small <?php echo $styles['padding_bottom_off'] ? 'uk-padding-remove-bottom': '';?>">
<div class="uk-section">
	<div class="uk-container">
		<article class="uk-grid-large" uk-grid>
			<!-- Media -->
			<div class="<?php echo $media_grid?> uk-grid-item-match uk-flex-middle">
				<div class="uk-panel <?php echo $media_padding?>" <?php echo $animation_media;?>>
					<img 
						width="<?php echo $settings['media_size']?>" 
						src="<?php echo $contents['image']?>" 
						alt="<?php echo $contents['title']?>" 
						class="op-max-width-none <?php echo ($settings['media_alignment'] == 'left') ? 'uk-float-right' :''; ?>" uk-image>
				</div>
			</div>
			<div class="uk-width-expand@m uk-grid-item-match uk-flex-middle <?php echo $content_position;?>">
				<div class="uk-panel">
					<!-- Title -->
					<?php if($contents['title']): ?>
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$animation_content . '"'
							); 
						?>
					<?php endif;?>
					<!-- Description -->
					<?php if($contents['description']): ?>
						<div class="uk-text-lead" 
							<?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' . '300"' : ''); ?>>
							<?php echo $contents['description']?>	
						</div>
					<?php endif; ?>
					<p <?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' . ($animation_repeat ? 'true': 'false') . ';delay:' . '400"' : ''); ?>>
							<!-- Link -->
							<?php echo op_link($contents['link'], 'uk-button-medium uk-margin-medium-top uk-button uk-button-primary');?>
					</p>
					<!-- Item slider -->
					<div class="uk-margin-medium-top" uk-slider="sets: true;" <?php echo $animation_slider_item;?>>
						<ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-2@m uk-grid">
							<?php foreach($contents['items'] as $item): ?>
							<li>
								<div class="uk-card">
									<div class="uk-grid-small" uk-grid>
										<div class="uk-width-auto">
											<!-- Item image -->
											<?php if ($item['media']): ?>
												<?php if( op_is_image($item['media'])):?>
													<img class="op-media uk-margin-right" src="<?php echo $item['media']; ?>" alt="<?php echo $item['title'];?>" />
												<?php else :?>
													<span class="op-media uk-margin-right <?php echo $item['media']; ?>"></span>
												<?php endif;?>
											<?php endif ;?>
										</div>
										<div class="uk-width-expand uk-padding-remove">
											<h3 class="uk-card-title uk-margin-small-bottom"><?php echo $item['title']?></h3>
											<p class="uk-card-desc uk-margin-remove"><?php echo $item['description']?></p>
										</div>
									</div>
								</div>
							</li>
							<?php endforeach; ?>
						</ul>
						<ul class="uk-slider-nav uk-dotnav op-barnav uk-dark uk-flex-left uk-margin"></ul>
					</div> <!-- uk-margin-medium-top -->
				</div>	<!-- uk-panel -->
			</div> <!-- uk-grid-match -->
		</article> <!-- uk-article --> 
	</div> <!-- uk-section --> 
	</div>
</section> <!-- end-section --> 