#<?php echo $id ?>{
	background-color : <?php echo $styles['bg_color'] ?>;
	
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id ?> .uk-heading-primary {
	font-size : <?php echo $settings['top_title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['top_title_size']) +10 ?>px;
}

#<?php echo $id ?> .uk-text-large {
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id ?> .uk-text-lead {
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['text_color']?>;
	line-height: 1.8;
}


#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color'];?>;
}

@media(max-width:768px){
	#<?php echo $id?> .uk-text-large{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?>  .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}