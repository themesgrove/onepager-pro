<?php
	// animation repeat
	$animation_repeat = '';
	// media grid
	$media_grid = 'uk-'. $settings['media_grid'] . '@m';
	// Animation media
	$animation_media = ($settings['animation_media']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_media'] .';repeat:' . ($animation_repeat ? 'true' : 'false') . '"' : '';
	// animation content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';
	// Alignment
	$content_position = '';
	// media padding
	$media_padding = 'uk-container-item-padding-remove-left';
	
	if($settings['media_alignment'] == 'right'){
		$content_position = 'uk-flex-first@m uk-first-column';
		$media_padding = 'uk-container-item-padding-remove-right';
	}
	// Text transformation class
	$heading_class = ($settings['title_transformation']) ? 'uk-text-' . $settings['title_transformation'] : '';

?>
<section id="<?php echo $id; ?>"
	class="uk-background-cover uk-background-norepeat uk-position-relative features pro-features-10 uk-cover-container"  
	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>
	data-src="<?php echo $styles['bg_image'];?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>

	<div class="uk-section">
		<div class="uk-padding-large">
			<article class="uk-grid-large" uk-grid>
				<!-- Media -->
				<div class="<?php echo $media_grid?> uk-grid-item-match uk-flex-middle">
					<div class="uk-panel <?php echo $media_padding?>" <?php echo $animation_media;?>>
						<img 
							width="<?php echo $settings['media_size']?>" 
							src="<?php echo $contents['image']?>" 
							class="<?php echo ($settings['media_alignment'] == 'left') ? 'uk-float-right' :''; ?>" uk-image>
					</div>
				</div>	
				<div class="uk-width-expand@m uk-grid-item-match uk-flex-middle <?php echo $content_position;?>">
					<div class="uk-panel">
						<?php if($contents['top_title']): ?>
							<!-- Description -->
							<h3 class="uk-heading-primary uk-margin-remove-bottom <?php echo $heading_class; ?>" 
								<?php echo ($settings['animation_content'] ? $animation_content .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '100"' : ''); ?>> 
								<?php echo $contents['top_title'];?></h3>
						<?php endif; ?>
						<!-- Title -->
						<?php if($contents['title']): ?>
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-small', 
								'uk-text-' . $settings['title_transformation'], 
								$animation_content . '"'
							); 
						?>
						<?php endif;?>
						<!-- Description -->
						<?php if($contents['description']): ?>
							<div class="uk-text-lead" 
								<?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' .($animation_repeat ? 'true' :'false') .';delay:' . '300"' : ''); ?>>
								<?php echo $contents['description']?>	
							</div>
						<?php endif; ?>
						<div class="uk-margin-medium-top"  <?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' .($animation_repeat ? 'true' :'false') .';delay:' . '400"' : ''); ?>>
							<img width="150" src="<?php echo $contents['signature']?>"  uk-image>
						</div>
					</div> <!-- uk-panel -->
				</div> <!-- uk-grid-match -->
			</article> <!-- uk-article --> 
		</div> <!-- uk-section --> 
	</div> <!-- uk-section-large -->
</section> <!-- end-section --> 