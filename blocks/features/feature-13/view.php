<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';' : '';
?>


<section id="<?php echo $id;?>" class="features pro-features-13">
	<div class="uk-section">
		<div class="uk-container">
			<article class="uk-article uk-padding-small">
				<?php if($contents['title'] || $contents['description']):?>
					<div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>" <?php echo $title_animation;?>>	
						<?php if($contents['title']):?>
							<!-- Section Title -->
							<?php
								echo op_heading( 
									$contents['title'],
									$settings['heading_type'], 
									'uk-heading-primary uk-margin-small', 
									'uk-text-' . $settings['title_transformation'], 
									$animation_content . '"'
								); 
							?>
						<?php endif; ?>

						<?php if($contents['description']):?>
							<!-- Section Sub Title -->
							<p class="uk-text-lead" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
								<?php echo $contents['description'];?>
							</p>
						<?php endif; ?>
					</div> <!-- section-heading -->
				<?php endif; ?>

				<div class="uk-grid-large" uk-grid >
				<?php $i=4; ?>
					<?php foreach($contents['items'] as $feature): ?>
						<div class="uk-width-1-<?php echo $settings['items_columns'];?>@m uk-width-1-1@s">
							<div class="uk-text-<?php echo $items_alignment;?>" <?php echo ($settings['items_animation'])? $items_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '' ;?>>
								<?php if( $feature['media']):?>
									<!-- Item image -->
									<?php if( op_is_image($feature['media'])):?>
										<img class="op-media" width="80" src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>" />
									<?php else :?>
										<span class="op-media <?php echo $feature['media']; ?>"></span>
									<?php endif;?>
								<?php endif;?>
								<!-- Item title -->
								<?php if($feature['title']): ?>
									<h3 class="item-title uk-margin uk-margin-remove-bottom uk-text-<?php echo $settings['title_transformation'];?>">
										<?php echo $feature['title'];?>
									</h3>
								<?php endif; ?>
								<?php if($feature['description']): ?>
								<!-- Item desc -->
									<p class="uk-text-medium uk-margin-small"><?php echo $feature['description'];?></p>
								<?php endif; ?>
							</div><!-- text-center -->
						</div><!-- uk-columns -->
					<?php $i++;endforeach; ?>
				</div> <!-- uk grid medium -->
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
