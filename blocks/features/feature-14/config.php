<?php

return array(
  'slug'    => 'pro-features-14',
  'groups'    => array('features'),

  'contents' => array(
    array(
      'name'=>'sub_title',
      'label'=>'Sub Title',
      'value' => 'Our Goal'
    ),
    array(
      'name'=>'title',
      'type'=>'textarea',
      'value'=>'Company Mission'
    ),

    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat.'
    ),
    array('label'=>'Image', 'type'=>'divider'), // Divider - Text
    array(
      'name'  => 'image',
      'type'  => 'image',
      'value' => 'http://demo.themefisher.com/themefisher/biztrox/images/chart.png',
      'label' => 'Image Left'
    ),

    array('label'=>'Accordion Items', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'value' => 'Who should use WPOnepager?'),
          array('name'=>'text', 'label'  => 'Text', 'type' => 'textarea', 'value' => 'WPOnepager is perfect for business owners, agency, bloggers, marketer, developers, and basically everyone else. If you want to create a beautiful WordPress landing page, then you need to use the OnePager drag & drop onepage builder.')
        ),
        array(
          array('name'=>'title', 'value' => 'Do i nedd to have coding skills?'),
          array('name'=>'text', 'label'  => 'Text', 'type' => 'textarea', 'value' => 'Onepage Builder – Easiest Landing Page Builder For WordPress” is open source software. The following people have contributed to this plugin.')
        ),
        array(
          array('name'=>'title', 'value' => 'Compatilbe with all of the themes?'),
          array('name'=>'text', 'label'  => 'Text', 'type' => 'textarea', 'value' => 'WPOnepager is perfect for business owners, agency, bloggers, marketer, developers, and basically everyone else. If you want to create a beautiful WordPress landing page, then you need to use the OnePager drag & drop onepage builder.')
        )
      )

    ),
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'sub_title_size',
      'label' => 'Sub Title Size',
      'append' => 'px',
      'value' => '22'
    ),

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),


    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),
    //array('name'=>'titile_bottom_image', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-1.jpg'),

    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Accordion Settings', 'type'=>'divider'), // Divider - Text


    array(
      'name' => 'accordion_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name' => 'accordion_desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),

    array('label'=>'Image', 'type'=>'divider'), // Divider - Text
      array(
      'name'     => 'media_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),


  'styles' => array(
    array(
      'name'   => 'bg_color',
      'label'  => 'Background Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),

    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name'   => 'title_color',
      'label'  => 'Title Color',
      'type'   => 'colorpicker',
      'value'  => '#222'

    ),

    array(
      'name'   => 'desc_color',
      'label'  => 'Desc Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),

    array('label'=>'Accordion Style', 'type'=>'divider'), // Divider - Text
    array(
      'name'   => 'accordion_bg_color',
      'label'  => 'Bg Color',
      'type'   => 'colorpicker',
      'value'  => '#f5f5f5'
    ),

    array(
      'name'   => 'accordion_title_color',
      'label'  => 'Title Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),

    array(
      'name'   => 'accordion_active_color',
      'label'  => 'Active Color',
      'type'   => 'colorpicker',
      'value'  => '@color.primary'
    ),

    array(
      'name'   => 'accordion_desc_color',
      'label'  => 'Desc Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),
  ),
);
