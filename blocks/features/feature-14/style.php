#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-text-large{
	font-size : <?php echo $settings['sub_title_size']?>px;
	color : <?php echo $styles['desc_color']?>;
	line-height : <?php echo ($settings['sub_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?> .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
	line-height : <?php echo ($settings['desc_size']) +12 ?>px;
}

#<?php echo $id;?> .uk-accordion .accordion-item .uk-accordion-title{
	font-size : <?php echo $settings['accordion_title_size']?>px;
	color : <?php echo $styles['accordion_title_color']?>;
	background: <?php echo $styles['accordion_bg_color']?>;
    padding: 10px 20px;
    border-radius: 50px;
    border: 1px solid #ddd;
 }
#<?php echo $id;?> .uk-accordion .accordion-item .uk-accordion-content{
	font-size : <?php echo $settings['accordion_desc_size']?>px;
	color : <?php echo $styles['accordion_desc_color']?>;
 }

#<?php echo $id;?> .uk-accordion-title::before{
	 margin-left: 0;
    float: left;
    margin-right: 3px;
}
#<?php echo $id;?> .uk-accordion .uk-accordion-title::before{
    margin-left: 0;
    float: left;
    margin-right: 3px;
}

#<?php echo $id;?>  .uk-accordion .uk-open .uk-accordion-title,
#<?php echo $id;?>  .uk-accordion .accordion-item:hover a.uk-accordion-title{
	color:<?php echo $styles['accordion_active_color']?>;
}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}