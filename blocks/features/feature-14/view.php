<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// media animation
	$media_animation = ($settings['media_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['media_animation']. ';repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'400"' : '';
?>


<section id="<?php echo $id;?>" class="features pro-features-14 uk-padding-small">
	<div class="uk-section-large">
		<div class="uk-container">
			<div class="section-heading uk-margin-bottom">	
				<?php if($contents['sub_title']):?>
					<!-- Section Sub Title -->
					<h4 class="uk-text-large uk-margin-small" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
						<?php echo $contents['sub_title'];?>
					</h4>
				<?php endif; ?>
				<?php if($contents['title']):?>
					<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-small', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
				<?php endif; ?>
			</div> <!-- section-heading -->

			<div uk-grid>
				<article class="uk-article uk-position-relative uk-width-1-2@m">
					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead uk-margin-medium" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>
					<div class="uk-margin-large-top">
						<?php $i=4; ?>
						<div class="uk-margin-small uk-margin-auto-top">
							<ul uk-accordion>
								<?php foreach($contents['items'] as $accordion): ?>
									<?php $open = $i == 4 ? 'uk-open' : ''; ?>
								    <li class="<?php echo $open;?> accordion-item uk-margin" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '' ;?>>
								        <a class="uk-accordion-title" href="#"><?php echo $accordion['title']; ?></a>
								        <div class="uk-accordion-content">
								            <p><?php echo $accordion['text']?></p>
								        </div>
								    </li>
						   		 <?php $i++; endforeach; ?> 
							</ul> <!-- uk-accordion -->
						</div> <!-- items -->
					</div> <!-- uk grid medium -->
				</article> <!-- uk-article -->
				<?php if ($contents['image']): ?>
					<div class=" uk-width-1-2@m uk-width-1-1@s" <?php echo $media_animation;?>>
						<img src="<?php echo $contents['image'];?>">
					</div> <!-- uk-width-1-4@m-->
				<?php endif ?>
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
