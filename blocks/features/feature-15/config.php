<?php

return array(
  'slug'    => 'pro-features-15',
  'groups'    => array('features'),

  'contents' => array(
    array(
      'name'=>'sub_title',
      'label'=>'Sub Title',
      'value' => 'About Us'
    ),
    array(
      'name'=>'title',
      'value'=>'Who We Are?'
    ),

    array(
      'name'  =>'title_image',
      'type'  => 'image',
      'label'=>'Title Bottom Image',
      'value' =>''
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Excepteur sint occaecat cupidatat non proident sunt culpa qui officia deserunt mollit anim id est laborum.'
    ),
    array('label'=>'Abouts', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'item_text',  'label' => 'Text', 'value'=>'Business Services'),
          array('name'=>'item_icon', 'label' => 'Icon', 'type' => 'icon', 'value'=>'fa fa-angle-right fa-lg'),
        ),
        array(
         array('name'=>'item_text', 'label' => 'Text', 'value'=>'Audit & AssuranceB'),
          array('name'=>'item_icon','label' => 'Icon',  'type' => 'icon', 'value'=>'fa fa-angle-right fa-lg'),
        ),
        array(
         array('name'=>'item_text', 'label' => 'Text', 'value'=>'Business Services'),
          array('name'=>'item_icon',  'label' => 'Icon', 'type' => 'icon','value'=>'fa fa-angle-right fa-lg'),
        ),
        array(
           array('name'=>'item_text','label' => 'Text',  'value'=>'Audit & Assurance'),
          array('name'=>'item_icon', 'label' => 'Icon', 'type' => 'icon','value'=>'fa fa-angle-right fa-lg'),
        ),
      )

    ),
    array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array('name'=>'link_bottom', 'text' => 'Explore More','label' => 'Type Button Text', 'type' => 'link', 'placeholder'=> home_url()),

    array('label'=>'Images', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'images',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'item_title',  'label' => 'Title', 'value'=>'Image 1'),
          array('name'=>'item_image', 'label' => 'Image', 'type' => 'image', 'value'=>'http://try.getonepager.com/wp-content/uploads/2019/04/about-1.jpg'),
        ),
        array(
          array('name'=>'item_title',  'label' => 'Title', 'value'=>'Image 2'),
          array('name'=>'item_image','label' => 'Image',  'type' => 'image', 'value'=>'http://try.getonepager.com/wp-content/uploads/2019/04/about-2.jpg'),
        ),
        array(
          array('name'=>'item_title',  'label' => 'Title', 'value'=>'Image 3'),
          array('name'=>'item_image',  'label' => 'Image', 'type' => 'image','value'=>'http://try.getonepager.com/wp-content/uploads/2019/04/about-3.jpg'),
        ),
      )

    ),
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'sub_title_size',
      'label' => 'Sub Title Size',
      'append' => 'px',
      'value' => '22'
    ),
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '32'
    ),
    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),

    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),
    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'item_text_size',
      'label' => 'Text Size',
      'append' => 'px',
      'value' => '16'
    ),
    array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'button_font_size',
      'label' => 'Font Size',
      'append' => 'px',
      'value' => '16'
    ),
    array(
      'name'     => 'button_transformation',
      'label'    => 'Text Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name'     => 'button_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),

  'styles' => array(
    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => ''
    ),
    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),
    array(
      'name' => 'bg_color',
      'label' => 'Background Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),
    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#222'

    ),
    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#343a40'

    ),
    array('label'=>'About Style', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'item_text_color',
      'label' => 'Text Color',
      'type' => 'colorpicker',
      'value' => '#343a40'
    ),

    array(
      'name' => 'item_icon_color',
      'label' => 'Icon Color',
      'type' => 'colorpicker',
      'value' => '@color.primary'
    ),
   array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'button_text_color',
      'label'   => 'Text Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#0089fd'
    ),
    array(
      'name' => 'button_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '50'
    ),

    array('label'=>'Images', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'nav_text_color',
      'label'   => 'Text Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'nav_bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#000'
    ),
  ),

);
