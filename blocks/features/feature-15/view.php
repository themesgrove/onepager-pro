<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:500"' : '';
?>


<section id="<?php echo $id;?>" class="uk-section uk-position-relative features pro-features-15 uk-background-cover uk-background-norepeat"  <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container">
			<div uk-grid>
				<article class="uk-article uk-position-relative uk-width-1-2@m uk-width-1-1@s uk-padding uk-background-secondary">
					<div class="section-heading uk-margin-medium-bottom">
						<?php if($contents['sub_title']):?>	
							<!-- Section Sub Title -->
							<h4 class="uk-text-large uk-margin-small">
								<?php echo $contents['sub_title'];?>
							</h4>
						<?php endif; ?>
						<?php if($contents['title']):?>
							<!-- Section Title -->
							<?php
								echo op_heading( 
									$contents['title'],
									$settings['heading_type'], 
									'uk-heading-primary uk-margin-small', 
									'uk-text-' . $settings['title_transformation'], 
									$title_animation . '"'
								); 
							?>
						<?php endif; ?>
						<?php if ($contents['title_image']): ?>
							<img src="<?php echo $contents['title_image'];?>">
						<?php endif; ?>
						<?php if($contents['description']):?>
							<!-- Section Sub Title -->
							<p class="uk-text-lead uk-margin-medium" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
								<?php echo $contents['description'];?>
							</p>
						<?php endif; ?>
					</div> <!-- section-heading -->

					<div uk-grid >
						<?php $i=4; ?>
						<?php foreach($contents['items'] as $about): ?>
							<div class="items uk-width-1-2@m uk-width-1-2@s uk-margin-small uk-margin-auto-top">
								<div class="uk-card" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '' ;?>>
									<div class="uk-card-header uk-padding-remove">
										<div class="uk-grid-small uk-flex-middle" uk-grid>
											<?php if ($about['item_text']): ?>
												<div class="uk-width-auto">
													<span class="op-media <?php echo $about['item_icon'];?>"></span>
												</div>
											<?php endif; ?>
											<div class="uk-width-expand">
												<?php if ($about['item_text']): ?>
													<!-- Item desc -->
													<p class="uk-text-medium uk-margin-small"><?php echo$about['item_text'];?></p>
												<?php endif; ?>
											</div>
										</div>
									</div> <!-- uk-card-header -->
								</div>
							</div>
						<?php $i++; endforeach; ?>
						<?php if ($contents['link_bottom']): ?>
							<p class="uk-margin-medium-top" <?php echo $button_animation;?>>
								<!-- Link 1 -->
								<?php echo op_link($contents['link_bottom'], 'uk-text-bold uk-button uk-button-default uk-button-large uk-margin-small-right');?>
							</p>
						<?php endif; ?>
					</div> <!-- uk-grid-medium -->
				</article> <!-- uk-article -->
				<div class="uk-width-1-2@m">
					<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="ratio: false">
					    <ul class="uk-slideshow-items uk-border-rounded" uk-height-viewport="offset-top: true; offset-bottom: 30">
						    <?php foreach ($contents['images'] as $image): ?>
						        <li>
						            <img src="<?php echo $image['item_image']; ?>" alt="<?php echo $image['item_title']; ?>" uk-cover>
						        </li>
						    <?php endforeach; ?>
					    </ul>

					    <a class="nav-button uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
					    <a class="nav-button uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

					</div>
				</div>
			</div>
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
