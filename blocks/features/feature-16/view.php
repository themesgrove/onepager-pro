<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';' : '';
?>


<section id="<?php echo $id;?>" class="uk-position-relative features pro-features-16"  
<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container-expand uk-padding uk-padding-remove-top">
			<article class="uk-article uk-position-relative">
				<div class="section-heading uk-width-3-4@m uk-width-1-1@s uk-margin-auto uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">	
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-small', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>
					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>
				</div>

				<div class="uk-grid-medium" uk-grid >
					<?php $i=4; ?>
					<?php foreach($contents['items'] as $feature): ?>
						<div class="items uk-width-1-<?php echo $settings['items_columns'];?>@m uk-width-1-1@s">
							<div class="uk-text-<?php echo $items_alignment;?>" 
							<?php echo ($settings['items_animation'])? $items_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '' ;?>>
								<div class="item-img">
									<!-- Item image -->
									<?php if(trim($feature['link']) && $feature['media']): ?>
										<a href="<?php echo $feature['link']; ?>" target="<?php echo $feature['target'] ? '_blank' : ''?>">
											<img class="op-media" src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>" /></a>
									<?php else: ?>
									<?php if($feature['media']):?>
										<img class="op-media" src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>" />

									<?php endif;endif;?>	
								</div>
								<?php if ($feature['title']): ?>
									<!-- Item title -->
									<h3 class="item-title uk-margin uk-text-<?php echo $settings['title_transformation'];?>">
										<?php if(trim($feature['link'])): ?>
											<a href="<?php echo $feature['link']; ?>" target="<?php echo $feature['target'] ? '_blank' : ''?>"><?php echo $feature['title'];?></a>
										<?php else: ?>
											<?php echo $feature['title'];?>
										<?php endif; ?>
									</h3>
								<?php endif; ?>
								<?php if ($feature['description']): ?>
									<!-- Item desc -->
									<p class="uk-text-medium uk-margin-remove"><?php echo $feature['description'];?></p>
								<?php endif; ?>
							</div><!-- blurb -->
						</div><!-- uk-columns -->
					<?php $i++; endforeach; ?>
				</div> <!-- uk grid medium -->
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
