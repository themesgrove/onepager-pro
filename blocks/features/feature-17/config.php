<?php

return array(
  'slug'    => 'pro-features-17',
  'groups'    => array('features'),

  'contents' => array(
    array(
      'name'=>'sub_title',
      'value' => 'Best Service',
      'label'=>'Sub Title'
    ),
    array(
      'name'=>'title',
      'type'=>'textarea',
      'value'=>'Service We Provide'
    ),

    array('label'=>'Items', 'type'=>'divider'), // Divider - Text


    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'value' => 'Business Consulting'),
          array('name'=>'media', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-1.jpg'),
          array('name'=>'icon', 'type'=>'icon', 'size'=>'fa-2x', 'value'=> 'fa fa-bar-chart fa-2x'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since .'),
          array('name'=>'link', 'type' => 'link', 'text' => 'Read More', 'placeholder'=> home_url()),
        ),
        array(
          array('name'=>'title', 'value' => 'Valuable Idea'),
          array('name'=>'media', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-2.jpg'),
          array('name'=>'icon', 'type'=>'icon', 'size'=>'fa-2x', 'value'=> 'fa fa-skyatlas fa-2x'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since .'),
          array('name'=>'link', 'type' => 'link', 'text' => 'Read More','placeholder'=> home_url()),
        ),
        array(
          array('name'=>'title', 'value' => 'Market Strategy'),
          array('name'=>'media', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-3.jpg'),
          array('name'=>'icon', 'type'=>'icon', 'size'=>'fa-2x', 'value'=> 'fa fa-database fa-2x'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since .'),
          array('name'=>'link', 'type' => 'link', 'text' => 'Read More', 'placeholder'=> home_url()),
        )
      )

    )
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'sub_title_size',
      'label' => 'Sub Title Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),


    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),
    array(
      'name'     => 'title_position',
      'label'    => 'Title Position',
      'type'     => 'select',
      'value'    => 'top',
      'options'  => array(
        'top'   => 'Top',
        'bottom'   => 'Bottom',
      ),
    ),

    //array('name'=>'titile_bottom_image', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-1.jpg'),

    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name'     => 'items_columns',
      'label'    => 'Items Columns',
      'type'     => 'select',
      'value'    => '3',
      'options'  => array(
        '2'   => '2',
        '3'   => '3',
        '4'   => '4',

      ),
    ),

    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '22'
    ),

    array(
      'name' => 'item_desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),

    array(
      'name'     => 'items_alignment',
      'label'    => 'Items Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
      )
    ),
    array(
      'name' => 'button_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '50'
    ),

    array(
      'name'     => 'items_animation',
      'label'    => 'Items Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),


  'styles' => array(
    array(
      'name'   => 'bg_color',
      'label'  => 'Background Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),

    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name'   => 'title_color',
      'label'  => 'Title Color',
      'type'   => 'colorpicker',
      'value'  => '#222'

    ),

    array(
      'name'   => 'desc_color',
      'label'  => 'Desc Color',
      'type'   => 'colorpicker',
      'value'  => '#222'

    ),

    array('label'=>'Items Style', 'type'=>'divider'), // Divider - Text

    array(
      'name'   => 'overlay_color',
      'label'  => 'Overlay Hover Color',
      'type'   => 'colorpicker',
      'value'  => 'rgba(34, 34, 34, 0.31)'
    ),

    array(
      'name'   => 'item_title_color',
      'label'  => 'Item Title Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),

    array(
      'name'   => 'item_desc_color',
      'label'  => 'Item Desc Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),

    array(
      'name'   => 'icon_color',
      'label'  => 'Icon Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),

    array('label'=> 'Button Style' , 'type'=> 'divider'),
    array(
      'name'    => 'cta_color',
      'label'   => 'Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'cta_bg',
      'label'   => 'Background',
      'type'    => 'colorpicker',
      'value'   => '#222'
    ),
    array(
      'name'    => 'cta_hover_color',
      'label'   => 'Hover Color',
      'type'    => 'colorpicker',
      'value'   => '#e84444'
    ),
  ),
);
