<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';target:>div> .uk-card; delay:200;repeat:' .($animation_repeat ? 'true' :'false' ) . ';"' : '';
?>


<section id="<?php echo $id;?>" class="features pro-features-17 uk-padding-small">
	<div class="uk-section">
		<div class="uk-container">
			<article class="uk-article uk-position-relative">
				<div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">	
					<?php if($contents['sub_title']):?>
						<!-- Section Sub Title -->
						<h4 class="uk-text-lead uk-margin-small" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
							<?php echo $contents['sub_title'];?>
						</h4>
					<?php endif; ?>
					<?php if($contents['title']):?>
						<!-- Section Title -->
							<?php
								echo op_heading( 
									$contents['title'],
									$settings['heading_type'], 
									'uk-heading-primary uk-margin-remove-top', 
									'uk-text-' . $settings['title_transformation'], 
									$title_animation . '"'
								); 
							?>
					<?php endif; ?>
				</div> <!-- section-heading -->

				<div class="uk-grid-medium" <?php echo $items_animation;?> uk-grid>
					<?php $i=4; ?>
					<?php foreach($contents['items'] as $service): ?>
						<div class="uk-width-1-<?php echo $settings['items_columns'];?>@m uk-width-1-1@s uk-grid-match">
							<div class="uk-card uk-border-rounded uk-text-<?php echo $items_alignment;?>">
								<div class="uk-card-media-top uk-position-relative">
									<?php if ($settings['title_position'] == 'top'): ?>
										<?php if ($service['title']): ?>
											<!-- Item title -->
											<h3 class="uk-card-title uk-padding-small uk-margin uk-margin-remove-bottom uk-text-<?php echo $settings['title_transformation'];?>">
												<?php echo $service['title'];?>
											</h3>
										<?php endif; ?>
									<?php endif; ?>
									<?php if($service['media']):?>
										<div class="service-image uk-position-relative">
											<div class="uk-overlay-primary uk-position-cover"></div>
												<img class="op-media " src="<?php echo $service['media']; ?>" alt="<?php echo $service['title'];?>" />
									
										</div>
									<?php endif;?>
									<?php if($service['icon']):?>
										<p class="service-icon uk-border-rounded uk-position-bottom-center uk-background-primary uk-padding-small">
											<span class="op-media <?php echo $service['icon']; ?>"></span></p>
									<?php endif;?>
								</div>
								<div class="uk-card-body uk-position-relative <?php echo $settings['title_position'] == 'top' && $service['icon']  ? 'uk-padding' : 'uk-padding-small';?> uk-padding-remove-left uk-padding-remove-right">
									<?php if ($settings['title_position'] == 'bottom'): ?>
										<?php if ($service['title']): ?>
											<!-- Item title -->
											<h3 class="uk-card-title uk-margin uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-margin-remove-bottom <?php echo $service['icon'] ? 'uk-margin-medium-top' : ''; ?> uk-text-<?php echo $settings['title_transformation'];?>">
												<?php echo $service['title'];?>
											</h3>
										<?php endif; ?>
									<?php endif; ?>
								
									<?php if ($service['description']): ?>
										<!-- Item desc -->
										<p class="uk-text-medium <?php echo $settings['title_position'] == 'top' && $service['icon']  ? 'uk-margin-remove' : 'uk-margin-remove-top';?> uk-padding-small"><?php echo $service['description'];?></p>
									<?php endif; ?>
									<?php if($service['link']): ?>
										<p class="service-button uk-position-bottom-<?php echo $items_alignment;?>">
											<?php echo op_link($service['link'], 'uk-button uk-button-secondary uk-button-large');?>
										</p>
									<?php endif;?>			
								</div>
							</div>
						</div>
					<?php $i++; endforeach; ?>
				</div> <!-- uk grid medium -->
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
