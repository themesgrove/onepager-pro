<?php

return array(
  'slug'    => 'pro-features-18',
  'groups'    => array('features'),

  'contents' => array(
    array(
      'name'=>'sub_title',
      'label'=>'Sub Title',
      'value' => 'Best Service'
    ),
    array(
      'name'=>'title',
      'type'=>'textarea',
      'value'=>'Why Choose Us'
    ),

    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat.

    Duis aute irure dolor in reprehenderit voluptate velit esse cillum dolore fugiat nulla pariatur.Excepteur sint ocaecat cupidatat non proident sunt culpa qui officia deserunt mollit anim id est laborum. sed perspiciatis unde omnisiste natus error sit voluptatem accusantium.doloremque ladantium totam rem aperieaque ipsa quae ab illo inventore.veritatis. et quasi architecto beatae vitae dicta sunt explicabo.'
    ),

    array('label'=>'Counter Items', 'type'=>'divider'), // Divider - Text


    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '230'),
          array('name'=>'title', 'value' => 'Projects Done'),
          array('name'=>'icon', 'label'  => 'Icon', 'type' => 'icon', 'value' => 'fa fa-database fa-2x')
        ),
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '789'),
          array('name'=>'title', 'value' => 'Satisfaction Clients'),
          array('name'=>'icon', 'label' => 'Icon', 'type' => 'icon', 'value' => 'fa fa-smile-o fa-2x')
        ),
        array(
          array('name'=>'count', 'label' => 'Type Your Number','value' => '580'),
          array('name'=>'title', 'value' => 'Cup Of Coffee'),
          array('name'=>'icon', 'label' => 'Icon', 'type' => 'icon', 'value' => 'fa fa-thumbs-o-up fa-2x')
        )
      )

    ),

    array('label'=>'Progress Bar Items', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'progress_items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'label' => 'Branding','value' => 'Branding'),
          array('name'=>'number', 'value' => '85'),
        ),
        array(
          array('name'=>'title', 'label' => 'Consulting','value' => 'Consulting'),
          array('name'=>'number', 'value' => '90'),
        ),
        array(
          array('name'=>'title', 'label' => 'Promotion','value' => 'Promotion'),
          array('name'=>'number', 'value' => '60'),
        ),
        array(
          array('name'=>'title', 'label' => 'Branding','value' => 'Business'),
          array('name'=>'number', 'value' => '75'),
        )
      )

    )
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'sub_title_size',
      'label' => 'Sub Title Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),


    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),
    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),
    //array('name'=>'titile_bottom_image', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-1.jpg'),

    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text


    array(
      'name' => 'item_counter_size',
      'label' => 'Counter Size',
      'append' => 'px',
      'value' => '36'
    ),

    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '16'
    ),


    array(
      'name'     => 'items_animation',
      'label'    => 'Items Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array('label'=>'Progress Bar Settings', 'type'=>'divider'), // Divider - Text
      array(
      'name'     => 'progressbar_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),


  'styles' => array(
    array(
      'name'   => 'bg_color',
      'label'  => 'Background Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),

    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name'   => 'title_color',
      'label'  => 'Title Color',
      'type'   => 'colorpicker',
      'value'  => '#222'

    ),

    array(
      'name'   => 'desc_color',
      'label'  => 'Desc Color',
      'type'   => 'colorpicker',
      'value'  => '#222'

    ),

    array('label'=>'Items Style', 'type'=>'divider'), // Divider - Text

    array(
      'name'   => 'counter_color',
      'label'  => 'Counter Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),

    array(
      'name'   => 'item_title_color',
      'label'  => 'Title Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),

    array(
      'name'   => 'icon_color',
      'label'  => 'Icon Color',
      'type'   => 'colorpicker',
      'value'  => '@color.primary'
    ),

    array('label'=> 'Progress Bar Style' , 'type'=> 'divider'),
    // array(
    //   'name'    => 'progress_bg',
    //   'label'   => 'Background',
    //   'type'    => 'colorpicker',
    //   'value'   => '#222'
    // ),
    array(
      'name'    => 'progress_animate_color',
      'label'   => 'Animate Color',
      'type'    => 'colorpicker',
      'value'   => '@color.primary'
    ),
  ),
);
