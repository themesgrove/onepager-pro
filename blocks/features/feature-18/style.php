#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-text-large{
	font-size : <?php echo $settings['sub_title_size']?>px;
	color : <?php echo $styles['desc_color']?>;
	line-height : <?php echo ($settings['sub_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?> .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
	line-height : <?php echo ($settings['desc_size']) +12 ?>px;
}


<?php if ($styles['icon_color']): ?>
	
	#<?php echo $id;?> .op-media{
		color : <?php echo $styles['icon_color']?>;
	    border: 1px solid #e5e5e5;
	    padding: 16px 19px;
    	border-radius: 100px;
	}
<?php endif; ?>



#<?php echo $id;?> .items .uk-card .counter{
	font-size : <?php echo $settings['item_counter_size']?>px;
	color : <?php echo $styles['counter_color']?>;
}


#<?php echo $id;?> .items .uk-card .uk-text-medium,
#<?php echo $id;?> .service-counter label{
	font-size : <?php echo $settings['item_title_size']?>px;
	color : <?php echo $styles['item_title_color']?>;
}

#<?php echo $id; ?> .service-counter .uk-progress{
	border-radius:50px;
}



@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}