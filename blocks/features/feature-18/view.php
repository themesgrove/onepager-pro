<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';' : '';
	// progessbar animation
	$progressbar_animation = ($settings['progressbar_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['progressbar_animation'].';' : '';

	$items = $contents['progress_items'];
?>

<script src="//cdn.jsdelivr.net/jquery.inview/0.2/jquery.inview.min.js"></script>
<section id="<?php echo $id;?>" class="features pro-features-18 uk-padding-small">
	<div class="uk-section-large">
		<div class="uk-container">
			<div class="section-heading uk-margin-bottom uk-text-<?php echo $title_alignment;?>">	
				<?php if($contents['sub_title']):?>
					<!-- Section Sub Title -->
					<h4 class="uk-text-large uk-margin-small" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
						<?php echo $contents['sub_title'];?>
					</h4>
				<?php endif; ?>
				<?php if($contents['title']):?>
					<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-remove-top', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
				<?php endif; ?>
			</div> <!-- section-heading -->

			<div uk-grid>
				<article class="uk-article uk-position-relative uk-width-3-4@m">
					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead uk-margin-medium" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>
					<div class="uk-grid-medium " uk-grid>
						<?php $i=4; ?>
						<?php foreach($contents['items'] as $service): ?>
							<div class="items uk-width-1-3@m uk-width-1-2@s uk-margin-small uk-margin-auto-top">
								<div class="uk-card" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '' ;?>>
									<div class="uk-card-header uk-padding-remove">
										<div class="uk-flex-middle" uk-grid>
											<?php if ($service['icon']): ?>
												<div class="uk-width-auto">
													<span class="op-media <?php echo $service['icon'];?>"></span>
												</div>
											<?php endif; ?>
											<div class="uk-width-expand">
												<?php if ($service['count']): ?>
													<h2 class="counter uk-margin-remove">
														<span class="count-number">	
															<?php echo $service['count']; ?>
														</span>
													</h2> 
												<?php endif; ?>

												<?php if ($service['title']): ?>
													<!-- Item desc -->
													<p class="uk-text-medium uk-margin-remove"><?php echo $service['title']?></p>
												<?php endif; ?>
											</div> <!-- uk-width-expand -->
										</div> <!-- uk-grid-small -->
									</div> <!-- uk-card-header -->
								</div> <!-- uk-card -->
							</div> <!-- items -->
						<?php $i++; endforeach; ?> 
					</div> <!-- uk grid medium -->
				</article> <!-- uk-article -->
				<div class="service-counter uk-width-1-4@m uk-width-1-1@s" <?php echo $progressbar_animation ;?>>
				<?php $i=0; ?>
					<?php foreach($contents['progress_items'] as $progressbar): ?>
						<div>
							<label><?php echo $progressbar['title'];?></label>
							<progress id="progressbar-<?php echo $i;?>" class="uk-progress uk-margin-remove-top" data-value="<?php echo $progressbar['number'];?>" max="100"><?php echo $progressbar['number'];?>%</progress>
						</div>
					<?php $i++; endforeach; ?>
				</div> <!-- uk-width-1-4@m-->
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->

<script>

   animate = [];
	 UIkit.util.ready(function () {
	     var bar = document.getElementsByTagName('progress');
	     for(var i = 0; i < bar.length; i++)
	    {
	       UikitIntervalProgress(bar.item(i), i);
	    }
	});
	function UikitIntervalProgress(item, index) {
		let Max = item.dataset.value;
	  	animate[index] = setInterval(function () {
	        item.value += 10;
	        if (item.value > Max) {
	        	item.value = Max;
	            clearInterval(animate[index]);
	        }
	    }, Math.floor(Math.random()*0.5*1000));
	}

    jQuery(document).ready(function() {
			var counters = jQuery(".count-number");
			var countersQuantity = counters.length;
			var counter = [];

			for (i = 0; i < countersQuantity; i++) {
				counter[i] = parseInt(counters[i].innerHTML);
			}

			var count = function(start, value, id) {
				var localStart = start;
				setInterval(function() {
				if (localStart < value) {
					localStart++;
					counters[id].innerHTML = localStart;
				}
				}, 40);
			};

			for (j = 0; j < countersQuantity; j++) {
				count(0, counter[j], j);
			}
		});

		jQuery('#<?php echo $id;?>').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				jQuery(this).find('.count-number').each(function () {
					var $this = jQuery(this);
					jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
						duration: 1000,
						easing: 'swing',
						step: function () {
							$this.text(Math.ceil(this.Counter));
						}
					});
				});
				jQuery(this).unbind('inview');
			}
		});

</script>