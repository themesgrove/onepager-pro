<?php

return array(
  'slug'    => 'pro-features-19',
  'groups'    => array('features'),

  'contents' => array(
    array(
      'name'=>'title',
      'type'=>'textarea',
      'value'=>'Recent Achievements'
    ),

    array('label'=>'Items', 'type'=>'divider'), // Divider - Text


    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'media', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-3-1.png'),
          array('name'=>'title', 'value' => 'Udhiya Project'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since .'),
          array('name'=>'link', 'type' => 'link', 'text' => 'More Info', 'placeholder'=> home_url()),
        ),
        array(
          array('name'=>'media', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-3-2.png'),
          array('name'=>'title', 'value' => 'Safe Water For All'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since .'),
          array('name'=>'link', 'type' => 'link', 'text' => 'More Info','placeholder'=> home_url()),
        ),
        array(
          array('name'=>'media', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-3-3.png'),
          array('name'=>'title', 'value' => 'Ramadan Food Package'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since .'),
          array('name'=>'link', 'type' => 'link', 'text' => 'More Info', 'placeholder'=> home_url()),
        )
      )

    )
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),


    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'  => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),

    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('name'=>'animation_repeat', 'label'=>'Animation Repeat', 'type'=>'switch'),
    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name'     => 'items_columns',
      'label'    => 'Items Columns',
      'type'     => 'select',
      'value'    => '3',
      'options'  => array(
        '2'   => '2',
        '3'   => '3',
        '4'   => '4',

      ),
    ),

    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '20'
    ),

    array(
      'name' => 'item_desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),

    array(
      'name'     => 'items_alignment',
      'label'    => 'Items Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
      )
    ),
    array(
      'name' => 'button_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '0'
    ),

    array(
      'name'     => 'items_animation',
      'label'    => 'Items Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),


  'styles' => array(
    array(
      'name'   => 'bg_color',
      'label'  => 'Background Color',
      'type'   => 'colorpicker',
      'value'  => '#0e2741'
    ),

    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name'   => 'title_color',
      'label'  => 'Title Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'

    ),

    array('label'=>'Items Style', 'type'=>'divider'), // Divider - Text
    array(
      'name'   => 'item_bg_color',
      'label'  => 'Background Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),

    array(
      'name'   => 'overlay_color',
      'label'  => 'Overlay Hover Color',
      'type'   => 'colorpicker',
      'value'  => 'rgba(34, 34, 34, 0.31)'
    ),

    array(
      'name'   => 'item_title_color',
      'label'  => 'Item Title Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),

    array(
      'name'   => 'item_desc_color',
      'label'  => 'Item Desc Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),

    array('label'=> 'Button Style' , 'type'=> 'divider'),
    array(
      'name'    => 'cta_color',
      'label'   => 'Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'cta_bg',
      'label'   => 'Background',
      'type'    => 'colorpicker',
      'value'   => '#32bce2'
    ),
    array(
      'name'    => 'cta_hover_color',
      'label'   => 'Hover Color',
      'type'    => 'colorpicker',
      'value'   => '#32bce2'
    ),
  ),
);
