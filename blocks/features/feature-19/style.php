#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}

#<?php echo $id;?> .uk-card.uk-card-default{
	background: <?php echo $styles['item_bg_color']?>;
}

#<?php echo $id;?> .uk-card .service-button{
	margin: -26px 0px;
}
<?php if ($settings['items_alignment'] =='left' || $settings['items_alignment'] =='right'): ?>
	#<?php echo $id;?> .uk-card .service-button{
		margin:-26px 38px;
	}
<?php endif; ?>

#<?php echo $id;?> .uk-card .op-media {
	width:100%;
}
#<?php echo $id;?> .uk-card .service-image {
	margin-top:-40px;
    margin-right: -40px;
    margin-left: 40px;
}

#<?php echo $id;?> .uk-card .uk-card-title,
#<?php echo $id;?> .uk-card  .uk-card-title a{
	font-size : <?php echo $settings['item_title_size']?>px;
	color : <?php echo $styles['item_title_color']?>;
}


#<?php echo $id;?> .uk-card .uk-card-body .uk-text-medium{
	font-size : <?php echo $settings['item_desc_size']?>px;
	color : <?php echo $styles['item_desc_color']?>;
}

#<?php echo $id; ?> .uk-card .uk-background-primary{
	background-color:<?php echo $styles['cta_hover_color']; ?>;
}

#<?php echo $id; ?> .uk-card .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
	transition: all 0.3s ease;
	opacity:0;
}

#<?php echo $id; ?> .uk-card:hover .uk-overlay-primary{
	transition: all 0.3s ease;
	opacity:1;
}


#<?php echo $id; ?> .uk-button-secondary{
	<?php if($styles['cta_bg']):?>
		background : <?php echo $styles['cta_bg']; ?>;
	<?php endif;?>
	color : <?php echo $styles['cta_color']; ?>;
	border : 1px solid <?php echo $styles['cta_bg']; ?>;
	border-radius:<?php echo $settings['button_border_radius'];?>px;
	font-size: 16px;
	letter-spacing: 2px;
}

#<?php echo $id; ?> .uk-button-secondary:hover{
	background : <?php echo $styles['cta_color']; ?>;
	color : <?php echo $styles['cta_hover_color']; ?>;
	border-color : <?php echo $styles['cta_hover_color']; ?>;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +20 ?>px;
	}
	#<?php echo $id;?> .uk-card .uk-card-title,
	#<?php echo $id;?> .uk-card  .uk-card-title a{
		font-size : <?php echo ($settings['item_title_size']/2)+7?>px;
		line-height : <?php echo ($settings['item_title_size']/2) +17 ?>px;
	}
	<?php if ($settings['items_alignment'] =='left' || $settings['items_alignment'] =='right'): ?>
		#<?php echo $id;?> .uk-card .service-button{
			margin:-26px 30px;
		}
	<?php endif; ?>
	#<?php echo $id?> .uk-card {
	    margin:50px 0;
	}
	#<?php echo $id;?> .uk-card .service-image {
	    margin-right: 0;
	    margin-left: 30px;
	}
}