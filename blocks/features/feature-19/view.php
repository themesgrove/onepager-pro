<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';' : '';
?>


<section id="<?php echo $id;?>" class="features pro-features-3 uk-padding-small">
	<div class="uk-section">
		<div class="uk-container">
			<article class="uk-article uk-position-relative">
				<div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">	
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-remove-bottom', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>
				</div> <!-- section-heading -->

				<div class="uk-grid-large uk-margin-large-bottom" uk-grid>
					<?php $i=4; ?>
					<?php foreach($contents['items'] as $service): ?>
						<div class="uk-width-1-<?php echo $settings['items_columns'];?>@m uk-width-1-1@s uk-grid-match"
						<?php echo ($settings['items_animation'])? $items_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '' ;?>
						>
							<div class="uk-card uk-margin-medium-top uk-card-default uk-text-<?php echo $items_alignment;?>">
								<div class="uk-card-media-top">
									<?php if($service['media']):?>
										<div class="service-image uk-position-relative">
											<div class="uk-overlay-primary uk-position-cover"></div>
											<img class="op-media " src="<?php echo $service['media']; ?>" alt="<?php echo $service['title'];?>" />
										</div>
									<?php endif;?>
								</div>
								<div class="uk-card-body uk-position-relative uk-padding uk-padding-remove-top">
									<?php if ($service['title']): ?>
										<!-- Item title -->
										<h3 class="uk-card-title uk-margin-top uk-margin-remove-bottom uk-text-<?php echo $settings['title_transformation'];?>">
											<?php echo $service['title'];?>
										</h3>
									<?php endif; ?>
								
									<?php if ($service['description']): ?>
										<!-- Item desc -->
										<p class="uk-text-medium uk-margin-remove uk-padding-small uk-padding-remove-right uk-padding-remove-left"><?php echo $service['description'];?></p>
									<?php endif; ?>
									<?php if($service['link']): ?>
										<p class="service-button uk-position-bottom-<?php echo $items_alignment;?>">
											<?php echo op_link($service['link'], 'uk-button uk-button-secondary uk-button-large');?>
										</p>
									<?php endif;?>			
								</div>
							</div>
						</div>
					<?php $i++; endforeach; ?>
				</div> <!-- uk grid medium -->
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
