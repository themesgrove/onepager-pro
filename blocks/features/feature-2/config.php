<?php

return array(

  'slug'      => 'pro-features-2', // Must be unique and singular
  'groups'    => array('features'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array(
      'name'=>'title',
      'value' => 'Prototype your project'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value'=> 'Import our free design resources into your favorite prototyping app.'
    ),

    array(
      'name'=>'content_image',
      'type'=>'image',
      'label' => 'Content Image',
      'value'=> 'http://try.getonepager.com/fullscreen/wp-content/uploads/sites/2/2019/02/Layer-13.png'
    ),

    array( 'name'=>'link', 'type' => 'link'),
    array(
      'name'=>'image',
      'label' => 'Media Image',
      'type'=>'image',
      'value'=> 'http://try.getonepager.com/fullscreen/wp-content/uploads/sites/2/2019/02/Layout-7.png'
    ),

  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array(
      'name' => 'note_image',
      'label' => 'Image',
      'type' => 'divider'
    ),
    array(
      'name'     => 'media_alignment',
      'label'    => 'Image Alignment',
      'type'     => 'select',
      'value'    => 'right',
      'options'  => array(
        'left'    => 'Left',
        'right'   => 'Right'
      ),
    ),
    array(
      'name'     => 'media_grid',
      'label'    => 'Image Grid',
      'type'     => 'select',
      'value'    => 'width-1-2',
      'options'  => array(
        'width-1-2'   => 'Half',
        'width-1-3'   => 'One Thirds',
        'width-1-4'   => 'One Fourth',
        'width-2-3'   => 'Two Thirds',
      ),
    ),
    array(
      'name'     => 'animation_media',
      'label'    => 'Animation Media',
      'type'     => 'select',
      'value'    => 'slide-bottom-medium',
      'options'  => array(
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
        ),
      ),

    array( 'label' => 'Contents', 'type' => 'divider'),
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0           => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Description Size',
      'append' => 'px',
      'value' => 22
    ),


    array(
      'name'     => 'animation_content',
      'label'    => 'Animation Content',
      'type'     => 'select',
      'value'    => 'slide-bottom-medium',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array( 'label' => 'Bottom Image', 'type' => 'divider'),
    array(
      'name' => 'media_size',
      'label' => 'Image Size',
      'append' => 'px',
      'value' => ''
    ),
    array(
      'name'     => 'animation_media_bottom',
      'label'    => 'Animation Bottom Media',
      'type'     => 'select',
      'value'    => 'slide-bottom-medium',
      'options'  => array(
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
        ),
      ),
    
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array('label'=>'Background Image', 'type'=>'divider'), // Divider - Background
    array(
      'name'  => 'bg_image',
      'label' => 'Image',
      'type'  => 'image'
    ),
    array(
      'name'     => 'bg_repeat',
      'label'    => 'Repeat',
      'type'     => 'select',
      'options'  => array(
        'no-repeat'     => 'No Repeat',
        'repeat'      => 'Repeat All',
        'repeat-x'      => 'Repeat X',
        'repeat-y'      => 'Repeat Y',
      )
    ),
    array(
      'name'    => 'bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#272d4d'
    ),
    array('label'=>'Content', 'type'=>'divider'),
    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name'  => 'text_color',
      'label' => 'Text Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),

    array('label'=>'Button', 'type'=>'divider'),
    array(
      'name'    => 'button_text_color',
      'label'   => 'Text Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Background',
      'type'    => 'colorpicker',
      'value'   => '@color.primary'
    ),

    array(
      'name' => 'border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '4'
    ),

  ),
);
