<?php
	// animation repeat
	$animation_repeat = '';
	// media grid
	$media_grid = 'uk-'. $settings['media_grid'] . '@m';
	// Animation Media
	$animation_media = ($settings['animation_media']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_media'] .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:400"' : '';
	// Animation Content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].';repeat:' .($animation_repeat ? 'true' : 'false') . '"' : '';
	// Animation bottom image
	$animation_media_bottom = ($settings['animation_media_bottom']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_media_bottom'].';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:400"' : '';

	// Alignment
	$content_position = '';
	// media padding
	$media_padding = 'uk-container-item-padding-remove-left';
	
	if($settings['media_alignment'] == 'right'){
		$content_position = 'uk-flex-first@m uk-first-column';
		$media_padding = 'uk-container-item-padding-remove-right';
	}
	// Text transformation class
	$heading_class = ($settings['title_transformation']) ? 'uk-text-' . $settings['title_transformation'] : '';

?>
<section id="<?php echo $id; ?>" class="fp-section features pro-features-2 uk-cover-container uk-padding-remove-bottom uk-padding-small">
<div class="uk-section">
	<div class="uk-container">
		<article class="uk-grid-large" uk-grid>
			<!-- Media -->
			<div class="<?php echo $media_grid?>">
				<!-- Description -->
				<?php if($contents['description']): ?>
					<div class="uk-text-lead" <?php echo $animation_content;?>>
						<?php echo $contents['description']?>
					</div>
				<?php endif; ?>

				<div class="uk-panel uk-margin-medium-top <?php echo $media_padding?>" <?php echo $animation_media;?>>
					<!-- content image -->
					<img
						width="400"   
						src="<?php echo $contents['content_image']?>" 
						alt="<?php echo $contents['title']?>" 
						class="op-max-width-none <?php echo ($settings['media_alignment'] == 'left') ? 'uk-float-right' :''; ?>" uk-image>
				</div><!--uk-panel -->
			</div><!-- uk-item-grid-match -->
			
			<div class="uk-width-expand@m <?php echo $content_position?>">
				<div class="uk-panel">
					<!-- Title -->
					<?php if($contents['title']): ?>
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$animation_content . '"'
							); 
						?>
					<?php endif;?>

					<?php if ($contents['link']): ?>
						<div <?php echo $animation_media;?>>
							<!-- Link -->
							<?php echo op_link($contents['link'], 'uk-margin-medium-top uk-button uk-button-primary');?>
						</div>
					<?php endif;?>

				</div>	<!-- uk-panel -->
			</div> <!-- uk-width-expand -->
			<div class="uk-align-center bottom-image" <?php echo $animation_media_bottom;?>>
				<!-- media image -->
				<img 
					width="<?php echo $settings['media_size']?>"
					src="<?php echo $contents['image']?>" 
					alt="<?php echo $contents['title']?>" uk-image>
			</div> <!-- aligen-center -->
		</article> <!-- article -->
	</div> <!-- uk-container -->
	</div>
</section> <!-- section -->