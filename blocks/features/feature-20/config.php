<?php

return array(
  'slug'    => 'pro-features-20',
  'groups'    => array('features'),

  'contents' => array(
    array(
      'name'=>'sub_title',
      'label'=>'Sub Title',
      'value' => 'Our Services'
    ),
    array(
      'name'=>'title',
      'type'=>'textarea',
      'value'=>'We Are Expert In Marketing & Investment'
    ),

    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit sed eiusmod tempor didunt laboris nisi ut aliquip ex commodo consequat. duis aute irure dolor in reprehenderit voluptate velit esse cillum dolore fugiat nulla pariatur.Excep teur sint ocaecat cupidatat non proident sunt culpa qui officia deserunt mollit anim id est laborum.'
    ),
    array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array('name'=>'link', 'text' => 'Explore Now', 'type' => 'link'),

    array('label'=>'Items', 'type'=>'divider'), // Divider - Text


    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'value' => 'Business Consulting'),
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-2x', 'value'=> 'fa fa-bar-chart fa-2x'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem ipsum dolor amet consecte tur adipisicing elit sed done eius mod tempor enim ad minim'),
        ),
        array(
          array('name'=>'title', 'value' => 'Valuable Idea'),
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-2x', 'value'=> 'fa fa-skyatlas fa-2x'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem ipsum dolor amet consecte tur adipisicing elit sed done eius mod tempor enim ad minim'),
        ),
        array(
          array('name'=>'title', 'value' => 'Business Consulting'),
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-2x', 'value'=> 'fa fa-skyatlas fa-2x'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem ipsum dolor amet consecte tur adipisicing elit sed done eius mod tempor enim ad minim'),
        ),
        array(
          array('name'=>'title', 'value' => 'Market Strategy'),
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-2x', 'value'=> 'fa fa-database fa-2x'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Lorem ipsum dolor amet consecte tur adipisicing elit sed done eius mod tempor enim ad minim'),
        )
      )

    )
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'sub_title_size',
      'label' => 'Sub Title Size',
      'append' => 'px',
      'value' => '22'
    ),

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),

    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),
    //array('name'=>'titile_bottom_image', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/service-1.jpg'),

    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array('label'=>'Button Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'button_font_size',
      'label' => 'Font Size',
      'append' => 'px',
      'value' => '16'
    ),

    array(
      'name' => 'button_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '50'
    ),

    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '20'
    ),

    array(
      'name' => 'item_desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),
      array(
      'name'     => 'items_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),


  'styles' => array(
    array(
      'name'   => 'bg_color',
      'label'  => 'Background Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),

    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name'   => 'title_color',
      'label'  => 'Title Color',
      'type'   => 'colorpicker',
      'value'  => '#222'

    ),

    array(
      'name'   => 'desc_color',
      'label'  => 'Desc Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),

    array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array(
      'name'   => 'button_text_color',
      'label'  => 'Text Color',
      'type'   => 'colorpicker',
      'value'  => '#fff'
    ),
    array(
      'name'   => 'button_bg_color',
      'label'  => 'Bg Color',
      'type'   => 'colorpicker',
      'value'  => '@color.primary'
    ),

    array('label'=>'Items Style', 'type'=>'divider'), // Divider - Text
    array(
      'name'   => 'item_icon_color',
      'label'  => 'Icon Color',
      'type'   => 'colorpicker',
      'value'  => '@color.primary'

    ),
    array(
      'name'   => 'item_title_color',
      'label'  => 'Title Color',
      'type'   => 'colorpicker',
      'value'  => '#222'

    ),

    array(
      'name'   => 'item_desc_color',
      'label'  => 'Desc Color',
      'type'   => 'colorpicker',
      'value'  => '#222'
    ),
  ),
);
