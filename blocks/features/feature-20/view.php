<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';target:>div> .uk-card; delay:200;repeat:' .($animation_repeat ? 'true' :'false' ) . ';"' : '';
?>


<section id="<?php echo $id;?>" class="features pro-features-5">
	<div class="uk-section">
		<div class="uk-container uk-padding">
			<div class="uk-flex uk-flex-middle" uk-grid>
				<article class="uk-article uk-position-relative uk-width-1-2@m">
					<div class="section-heading uk-margin-bottom">	
					<?php if($contents['sub_title']):?>
						<!-- Section Sub Title -->
						<h4 class="uk-text-large uk-margin-small" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
							<?php echo $contents['sub_title'];?>
						</h4>
					<?php endif; ?>
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-remove-top', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>
						<?php if($contents['description']):?>
							<!-- Section Sub Title -->
							<p class="uk-text-lead uk-margin" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : '' ;?>>
								<?php echo $contents['description'];?>
							</p>
						<?php endif; ?>
						<?php if ($contents['link']): ?>
							<p <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : '' ;?>>
									<!-- Link -->
								<?php echo op_link($contents['link'], 'uk-margin-top uk-button uk-button-primary uk-button-large');?>
							</p>
						<?php endif;?>
					</div> <!-- section-heading -->
				</article> <!-- uk-article -->
				<div class="uk-width-1-2@m">
					<div uk-grid <?php echo $items_animation;?>>
			        <?php foreach($contents['items'] as $feature): ?>
						<div class="uk-width-1-2@m uk-width-1-1@s">
							<div class="uk-card uk-text-center uk-border-rounded uk-padding-small">
								<div class="uk-card-media-top uk-position-relative uk-margin-top">
									<?php if($feature['media']):?>
										<?php if( op_is_image($feature['media'])):?>
											<img class="op-media" src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>" />
										<?php else :?>
											<span class="op-media <?php echo $feature['media']; ?>"></span>
										<?php endif;?>
									<?php endif ;?>
								</div>
								<div class="uk-card-body uk-padding-remove uk-margin-bottom">
									<?php if ($feature['title']): ?>
										<!-- Item title -->
										<h3 class="uk-card-title uk-margin-top uk-text-<?php echo $settings['title_transformation'];?>">
											<?php echo $feature['title'];?>
										</h3>
									<?php endif; ?>
								
									<?php if ($feature['description']): ?>
										<!-- Item desc -->
										<p class="uk-text-medium uk-padding-remove"><?php echo $feature['description'];?></p>
									<?php endif; ?>		
								</div>
							</div>
						</div>
					<?php endforeach; ?>
					</div>
				</div>
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
