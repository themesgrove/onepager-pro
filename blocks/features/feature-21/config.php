<?php

return array(

  'slug'      => 'pro-features-21', // Must be unique
  'groups'    => array('features'), // Blocks group for filter

  // Fields - $contents available on view file to access the option
  'contents' => array(
  array('name'=>'video_link', 'label' => 'Upload mp4 video', 'type' => 'image', 'value'=>'https://quirksmode.org/html5/videos/big_buck_bunny.mp4'),

    array('name'=>'title', 'value'=> 'SHARE THIS VIDEO'),

  array('name'=> 'social', 'label' => 'Social Share Links', 'value' => array('https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fplugin.demo%2Fbeauty-is-perfect-in-its-imperfections-so-you-just-have-to-go-with-the-imperfections%2F', 'https://twitter.com/intent/tweet?text=Beauty+is+perfect+in+its+imperfections%2C+so+you+just+have+to+go+with+the+imperfections.&url=http%3A%2F%2Fplugin.demo%2Fbeauty-is-perfect-in-its-imperfections-so-you-just-have-to-go-with-the-imperfections%2F', 'https://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fplugin.demo%2Fbeauty-is-perfect-in-its-imperfections-so-you-just-have-to-go-with-the-imperfections%2F&title=Beauty+is+perfect+in+its+imperfections%2C+so+you+just+have+to+go+with+the+imperfections.&summary=The+model+is+talking+about+booking+her+latest+gig%2C+modeling+WordPress+underwear+in+the+brand+latest+Perfectly+Fit+campaign%2C+which+was+shot+by+Lachian+Bailey.+It+was+such+a+surreal+moment+cried+she+admitted.+It%E2%80%99s+kind+of+confusing+because+I%E2%80%99m+a+bigger+girl%2C+Dalbesio+says.+I%E2%80%99m+not+the+biggest+girl+on+the+market+but+I%E2%80%99m+%E2%80%A6%3Cp+class%3D%22read-more%22%3E+%3Ca+class%3D%22%22+href%3D%22http%3A%2F%2Fplugin.demo%2Fbeauty-is-perfect-in-its-imperfections-so-you-just-have-to-go-with-the-imperfections%2F%22%3E+%3Cspan+class%3D%22screen-reader-text%22%3EBeauty+is+perfect+in+its+imperfections%2C+so+you+just+have+to+go+with+the+imperfections.%3C%2Fspan%3E+Read+More+%C2%BB%3C%2Fa%3E%3C%2Fp%3E') ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
   
    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '22'
    ),
    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name'     => 'title_alignment',
      'label'    => 'Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),

    array(
      'name'     => 'title_animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Video', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'video_position',
      'label' => 'Video Position',
      'append' => 'px',
      'value' => '-200'
    ),

    //array('name'=>'target', 'label'=>'Auto Play Off/On', 'type'=>'switch'),

    array('label'=>'Social Links', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'icon_size',
      'label' => 'Icon Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name' => 'icon_border-width',
      'label' => 'Border Width',
      'append' => 'px',
      'value' => '2'
    ),

    array(
      'name' => 'icon_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '100'
    ),

    array(
      'name'     => 'icon_animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),

  'styles' => array(

    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => 'http://try.getonepager.com/wp-content/uploads/2019/02/video-1-bg.jpg'
    ),

    array(
      'name'     => 'bg_image_size',
      'label'    => 'Size',
      'type'     => 'select',
      'value'    => 'uk-background-cover',
      'options'  => array(
        'uk-background-contain'   => 'Contain',
        'uk-background-cover'     => 'Cover',
      ),
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(255,255,255,0)'
    ),


    array('label'=>'Video', 'type'=>'divider'), // Divider - Text

    array(
      'name'    => 'placeholder_image',
      'label'   => 'Place Holder Image',
      'type'    => 'image',
      'value'   => 'http://try.getonepager.com/wp-content/uploads/2019/02/video-1-placeholder.png'
    ),

    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '@color.primary'
    ),


  array('label'=>'Social Links', 'type'=>'divider'), // Divider - Text

    array(
      'name'=>'icon_color',
      'label' => 'Icon Color',
      'type'  => 'colorpicker',
      'value' => '@color.primary'
    ),

    array(
      'name'=>'icon_hover_color',
      'label' => 'Icon Hover Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),
  ),
);
