#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id; ?> .uk-card .uk-card-title{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color'];?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
background:<?php echo $styles['overlay_color'];?>;
	
}

#<?php echo $id; ?> .uk-card .social-links a{
	font-size : <?php echo $settings['icon_size']?>px;
	color : <?php echo $styles['icon_color'];?>;
	border: <?php echo $settings['icon_border-width']?>px solid <?php echo $styles['icon_color'];?>;
    border-radius:  <?php echo $settings['icon_border_radius']?>px;
	width: 45px;
    height: 45px;
    line-height: 45px;
}
#<?php echo $id; ?> .uk-card  .social-links a:hover{
	background-color : <?php echo $styles['icon_color'];?>;
}

#<?php echo $id; ?> .uk-card  .social-links a:hover:before{
	color : <?php echo $styles['icon_hover_color'];?>;
}

#<?php echo $id; ?> .video{
	margin-top:<?php echo $settings['video_position'];?>px;
	border-radius:5px;
}

@media(max-width:768px){
	#<?php echo $id; ?> .video{
		margin-top:-120px !important;
	}
}


