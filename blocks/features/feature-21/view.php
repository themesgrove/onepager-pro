<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// item animation
	$item_animation = ($settings['icon_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['icon_animation'].';repeat:true;' : '';
	// video placeholder
	$video_placeholder = ($styles['placeholder_image']) ? 'poster="'.$styles['placeholder_image'].'"' : '';
?>

	<section id="<?php echo $id; ?>" class="uk-section uk-position-relative features pro-features-1 uk-padding <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>

		<div class="uk-overlay-primary uk-position-cover"></div>
		<div class="uk-container">
			<div class="uk-position-relative uk-text-center" 
				<?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '100"' : ''); ?>>
				<video class="video uk-width-1-1@xl uk-width-3-4@m uk-width-1-1@s" <?php echo $video_placeholder; ?> uk-video="autoplay: false"loop uk-video>
				    <source src="<?php echo $contents['video_link'];?>" type="video/mp4">
				    </video>
			</div>
			<div class="uk-card uk-text-<?php echo $title_alignment;?>">
			    <div class="uk-card-body">
					<h3 class="uk-card-title uk-text-bold uk-margin-remove-bottom uk-text-<?php echo $settings['title_transformation'];?>" <?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>
						<?php echo $contents['title'];?>
					</h3>
					<div class="social-links uk-margin-medium-top">
					    <?php $i=3; ?>
			            <?php foreach ( $contents['social'] as $social ): ?>
			              	<a class="icon" href="<?php echo $social; ?>" target="_blank" 
			              		<?php echo ($settings['icon_animation'] ? $item_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . $i . '00"' : ''); ?>>
			              	</a>
			            <?php $i++; endforeach; ?>
		            </div>
		        </div><!-- social-links -->
			</div> <!-- uk-card -->
		</div> <!-- uk-container -->
	</section> <!-- uk-section -->


	<script type="text/javascript">
		jQuery('.video').click(function(){this.paused?this.play():this.pause();});
	</script>