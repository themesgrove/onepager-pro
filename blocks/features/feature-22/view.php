<?php
	// animation repeat
	$animation_repeat ='';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// video placeholder
	$video_placeholder = ($styles['placeholder_image']) ? 'poster="'.$styles['placeholder_image'].'"' : '';
	// video animation
	$video_animation = ($settings['video_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['video_animation'].';repeat:true;' : '';
?>
	<section id="<?php echo $id; ?>" class="uk-section uk-position-relative features pro-features-22 <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>

		<div class="uk-overlay-primary uk-position-cover"></div>
		<div class="uk-container uk-position-relative">
			<div class="section-heading uk-width-1-1@m uk-padding-small uk-margin-auto uk-text-<?php echo $title_alignment;?>">
				<?php if($contents['title']):?>
					<!-- Section Title -->
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary', 
							'uk-text-' . $settings['title_transformation'], 
							$title_animation . '"'
						); 
					?>
				<?php endif; ?>

				<?php if($contents['description']):?>
					<!-- Section Sub Title -->
					<div class="uk-text-lead"
						<?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>
						<?php echo $contents['description'];?>
					</div>
				<?php endif; ?>
			</div> <!-- section-heading -->
			<div class="uk-position-relative uk-text-center uk-padding-small" 
				<?php echo ($settings['video_animation'] ? $video_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '400"' : ''); ?>>
				<video controls class="video-2" <?php echo $video_placeholder; ?> uk-video="autoplay: false"loop uk-video>
				    <source src="<?php echo $contents['video_link'];?>" type="video/mp4">
				</video>
			</div>
		</div> <!-- uk-container -->
	</section> <!-- uk-section -->


<script type="text/javascript">
	jQuery('.video-2').click(function(){this.paused?this.play():this.pause();});
</script>