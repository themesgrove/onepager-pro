#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}
#<?php echo $id; ?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}

#<?php echo $id; ?> .uk-overlay-primary{
background:<?php echo $styles['overlay_color'];?>;
	
}
#<?php echo $id; ?> .video-lightbox .uk-text-meta{
   font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
   color:<?php echo $styles['title_color'];?>;
   font-size : <?php echo ($settings['title_size']/2)-10;?>px;
   line-height : <?php echo ($settings['title_size']/2);?>px;
}
#<?php echo $id; ?> .video-lightbox a{
    border: 3px solid <?php echo $styles['title_color'];?>;
    border-radius: 50px;
    width: 80px;
    height: 80px;
    line-height: 73px;
    font-size: 30px;
    text-align: center;
    color: <?php echo $styles['title_color'];?>;
}
#<?php echo $id; ?> .video-lightbox a:hover i{
	color: <?php echo $styles['icon_hover_color'];?>;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-meta {
		   font-size : <?php echo ($settings['title_size']/2)-10;?>px;
		   line-height : <?php echo ($settings['title_size']/2)-3;?>px;
	}
}