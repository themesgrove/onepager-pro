<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
?>
	<section id="<?php echo $id; ?>" class="uk-section uk-position-relative features pro-features-23 <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>

		<div class="uk-overlay-primary uk-position-cover"></div>
		<div class="uk-container uk-position-relative">
			<div class="section-heading uk-width-1-2@m uk-margin-right-auto uk-padding ">
				<?php if($contents['title']):?>
					<!-- Section Title -->
					
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary', 
							'uk-text-' . $settings['title_transformation'], 
							$title_animation . '"'
						); 
					?>
				<?php endif; ?>

				<div class="uk-margin-medium-top feature-lightbox" uk-lightbox>
				    <div class="uk-card" <?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>
				        <div class="uk-grid-medium uk-flex-middle" uk-grid>
				            <div class="uk-width-auto">
				            	<a class="uk-button" href="<?php echo $contents['video_link'];?>"><i class="fa fa-play"></i></a>
				            </div>
				            <div class="uk-width-expand">
				                <p class="uk-text-meta uk-margin-remove-top">
				                <?php echo $contents['video_title'];?></p>
				            </div> <!-- uk-width-expand -->
				        </div> <!-- uk-flex -->
					</div> <!-- uk-card -->
				</div> <!-- uk-lightbox -->
			</div> <!-- section-heading -->
		</div> <!-- uk-container -->
	</section> <!-- uk-section -->
