<?php

return array(
  'slug'    => 'pro-features-24',
  'groups'    => array('features'),

  'contents' => array(
    array(
      'name'=>'title',
      'value'=>'Our <i style="color: #004cfb;">Featured</i> Mosque'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Lorem ipsum dolor sit amet, sed dicunt oporteat cu, laboramus definiebas cum et. Duo id omnis <br>persequeris neglegentur, facete commodo ea usu, possit lucilius sed ei. Esse efficiendi scripserit eos ex. <br>Sea utamur iisque salutatus id.Mel autem animal.'
    ),
    array('label'=>'Items', 'type'=>'divider'), // Divider - Text


    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> 'https://images.unsplash.com/photo-1512970648279-ff3398568f77?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=325'),
          array('name'=>'title', 'value' => 'A Game Plan For Ramadan'),
          array('name'=>'raised', 'value' => '$18,000'),
          array('name'=>'goal', 'value' => '$25,000'),
          array('name'=>'item_desc', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunt oporteat cuum Tuo iomnis persequeris neglegentur, facete commodo ea use possit lucilius sed ei.'),
          array(
            'name'  => 'readmore_text',
            'label' => 'Donate',
            'value' => 'Donate Now',
          ),
           array('name'=>'link', 'placeholder'=> home_url()),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> 'https://images.unsplash.com/photo-1536253253742-6c8195fd0c1e?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=325'),
          array('name'=>'title', 'value' => 'Sheikh Zayed Grand Mosque'),
          array('name'=>'raised', 'value' => '$18,000'),
          array('name'=>'goal', 'value' => '$25,000'),
          array('name'=>'item_desc', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunt oporteat cuum Tuo iomnis persequeris neglegentur, facete commodo ea use possit lucilius sed ei.'),
          array(
            'name'  => 'readmore_text',
            'label' => 'Donate',
            'value' => 'Donate Now',
          ),
           array('name'=>'link', 'placeholder'=> home_url()),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> 'https://images.unsplash.com/photo-1512971064777-efe44a486ae0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=335'),
          array('name'=>'title', 'value' => 'Mosque-Madrassa of Sultan H.'),
          array('name'=>'raised', 'value' => '$18,000'),
          array('name'=>'goal', 'value' => '$25,000'),
          array('name'=>'item_desc', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunt oporteat cuum Tuo iomnis persequeris neglegentur, facete commodo ea use possit lucilius sed ei.'),
          array(
            'name'  => 'readmore_text',
            'label' => 'Donate',
            'value' => 'Donate Now',
          ),
           array('name'=>'link', 'placeholder'=> home_url()),
        ),

      )

    )
  ),

  'settings' => array(
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),


    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),
    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name'     => 'items_columns',
      'label'    => 'Items Columns',
      'type'     => 'select',
      'value'    => '3',
      'options'  => array(
        '2'   => '2',
        '3'   => '3',
        '4'   => '4',

      ),
    ),

    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '22'
    ),

    array(
      'name' => 'item_desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),

    array(
      'name'     => 'items_alignment',
      'label'    => 'Items Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),


    array(
      'name'     => 'items_animation',
      'label'    => 'Items Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),


  'styles' => array(
    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => '#ffffff'
    ),
    array(
      'name' => 'bg_color',
      'label' => 'Background Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),

    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#212121'

    ),

    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#212121'

    ),

    array('label'=>'Items Style', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'item_title_color',
      'label' => 'Item Title Color',
      'type' => 'colorpicker',
      'value' => '#212121'
    ),

    array(
      'name' => 'item_desc_color',
      'label' => 'Item Desc Color',
      'type' => 'colorpicker',
      'value' => '#212121'
    ),

    array(
      'name' => 'icon_color',
      'label' => 'Icon Color',
      'type' => 'colorpicker',
      'value' => '#004cfb'
    ),
    array(
      'name' => 'raised_color',
      'label' => 'Raised Color',
      'type' => 'colorpicker',
      'value' => '#212121'
    ),
    array(
      'name' => 'goal_color',
      'label' => 'Goal Color',
      'type' => 'colorpicker',
      'value' => '#004cfb'
    ),
    array(
      'name' => 'btn_hover_color',
      'label' => 'Button Hover Color',
      'type' => 'colorpicker',
      'value' => '#004cfb'
    ),
  ),
);
