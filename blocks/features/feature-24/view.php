<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';' : '';
?>


<section id="<?php echo $id;?>" class="uk-position-relative features pro-features-24 uk-padding-small" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container">
			<article class="uk-article uk-position-relative">
				<div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">	
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary  uk-margin-medium-bottom', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>
					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>
				</div>

				<div class="uk-grid-medium" uk-grid >
					<?php $i=4; ?>
					<?php foreach($contents['items'] as $feature): ?>
						<div class="items uk-width-1-<?php echo $settings['items_columns'];?>@m uk-width-1-1@s">
							<div class="uk-text-<?php echo $items_alignment;?>" 
							<?php echo ($settings['items_animation'])? $items_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '' ;?>>
								<div class="item-img">
									<!-- Item image -->
									<?php if( op_is_image($feature['media'])):?>
										<img class="op-media" src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>" />
									<?php else :?>
										<span class="op-media <?php echo $feature['media']; ?>"></span>
									<?php endif;?>
									
								</div>
								<?php if ($feature['title']): ?>
									<!-- Item title -->
									<h3 class="item-title uk-margin uk-text-<?php echo $settings['title_transformation'];?>">
										<?php if(trim($feature['link'])): ?>
											<a href="<?php echo $feature['link']; ?>"><?php echo $feature['title'];?></a>
										<?php else: ?>
											<?php echo $feature['title'];?>
										<?php endif; ?>
									</h3>
								<?php endif; ?>
								<p class="op-meta"> <?php echo esc_html__( 'Raised:', 'onepager-pro' )?> <?php echo $feature['raised'];?> <span><?php echo esc_html__( 'Goal:', 'onepager-pro' )?> <?php echo $feature['goal'];?></span></p>
								<?php if ($feature['item_desc']): ?>
									<!-- Item desc -->
									<p class="uk-text-medium"><?php echo $feature['item_desc'];?></p>
								<?php endif; ?>
									<?php if ($feature['readmore_text']): ?>	
										<a class="uk-button-text" href="<?php echo $feature['link']; ?>"><?php echo $feature['readmore_text'];?></a>
									<?php endif; ?>
							</div><!-- blurb -->
						</div><!-- uk-columns -->
					<?php $i++; endforeach; ?>
				</div> <!-- uk grid medium -->
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
