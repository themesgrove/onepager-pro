<?php

return array(

  'slug'      => 'pro-features-25', // Must be unique and singular
  'groups'    => array('features'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Title', 'type'=>'divider'),
    array(
      'name'=>'title',
      'value' => 'Our <i style="color: #b1d3a5;">Upcoming</i> Event'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value'=> 'Lorem ipsum dolor sit amet, sed dicunt oporteat cu, laboramus definiebas cum et. Duo id omnis <br>persequeris neglegentur, facete commodo ea usu, possit lucilius sed ei. Esse efficiendi scripserit eos ex. <br>Sea utamur iisque salutatus id.Mel autem animal.'
    ),
    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> ''),
          array('name'=>'title', 'value' => 'MOIC ..Model Organization Of Islamic Conference'),
          array('name'=>'date', 'value' => '22'),
          array('name'=>'month', 'value' => 'May'),
          array(
            'name'  => 'date_time',
            'label' => 'Date Time',
            'value' => 'Sat May 20 2019 at 11:00 am',
          ),
          array('name'=>'link', 'placeholder'=> home_url()),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> ''),
          array('name'=>'title', 'value' => 'DFW Islamic Youth Competition'),
          array('name'=>'date', 'value' => '15'),
          array('name'=>'month', 'value' => 'Jul'),
          array(
            'name'  => 'date_time',
            'label' => 'Date Time',
            'value' => 'Sat 15 July 2020 at 11:00 am',
          ),
          array('name'=>'link', 'placeholder'=> home_url()),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> ''),
          array('name'=>'title', 'value' => 'Lifestyle Connoisseur - 3rd & 4th October 2022'),
          array('name'=>'date', 'value' => '03'),
          array('name'=>'month', 'value' => 'Oct'),
          array(
            'name'  => 'date_time',
            'label' => 'Date Time',
            'value' => 'Sat October 20 2020 at 11:00 am',
          ),
          array('name'=>'link', 'placeholder'=> home_url()),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> 'https://images.unsplash.com/photo-1529070538774-1843cb3265df?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=325'),
          array('name'=>'title', 'value' => 'Islamic Finance Awards | The Asset'),
          array('name'=>'date', 'value' => '02'),
          array('name'=>'month', 'value' => 'Jan'),
          array(
            'name'  => 'date_time',
            'label' => 'Date Time',
            'value' => 'Sat January 02 2021 at 11:00 am',
          ),
          array('name'=>'link', 'placeholder'=> home_url()),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> 'https://images.unsplash.com/photo-1552481280-97a6383d9f06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=320'),
          array('name'=>'title', 'value' => 'Look into Your Heart - Sheikh Hussain Yee'),
          array('name'=>'date', 'value' => '12'),
          array('name'=>'month', 'value' => 'Dec'),
          array(
            'name'  => 'date_time',
            'label' => 'Date Time',
            'value' => 'Sat December 12 2022 at 11:00 am',
          ),
          array('name'=>'link', 'placeholder'=> home_url()),
        ),
      )
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title', 'type'=>'divider'),
    array(
      'name'   => 'section_title_size',
      'label'  => 'Title Size',
      'append' => 'px',
      'value'  => '20'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit' => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),


    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),


    array(
      'name'     => 'title_animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Items', 'type'=>'divider'),

    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '22'
    ),

    array(
      'name'     => 'item_title_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit' => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),


    array(
      'name' => 'item_desc_size',
      'label' => 'Item Desc Size',
      'append' => 'px',
      'value' => '16'
    ),

   array(
    'name'     => 'item_animation',
    'label'    => 'Animation Item',
    'type'     => 'select',
    'value'    => 'fadeInUp',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(

    array(
      'name'    => 'bg_color',
      'label'   => 'Background Color',
      'type'    => 'colorpicker',
      'value'   => '#f5f5f5'
    ),

    array('label'=>'Title', 'type'=>'divider'),
    array(
      'name'  => 'section_title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#222'
    ),

    array(
      'name'  => 'desc_color',
      'label' => 'Desc Color',
      'type'  => 'colorpicker',
      'value' => '#666'
    ),

    array('label'=>'Items', 'type'=>'divider'),
    array(
      'name'  => 'item_bg_color',
      'label' => 'Item Bg Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name'  => 'item_title_color',
      'label' => 'Item Title Color',
      'type'  => 'colorpicker',
      'value' => '#343a40'
    ),
    array(
      'name'  => 'item_desc_color',
      'label' => 'Item Desc Color',
      'type'  => 'colorpicker',
      'value' => '#666'
    ),
    array(
      'name'    => 'hover_color',
      'label'   => 'Hover Color',
      'type'    => 'colorpicker',
      'value'   => '@color.primary'
    ),
  ),

);
