#<?php echo $id; ?>{
	background-color : <?php echo $styles['bg_color'];?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;

}

#<?php echo $id;?> .section-heading .uk-heading-primary{ 
	font-size : <?php echo $settings['section_title_size'];?>px; 
	color : <?php echo $styles['section_title_color'];?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px; 
	color : <?php echo $styles['desc_color'];?>;
}
#<?php echo $id;?> .uk-card-default .uk-text-large{
    font-size: 30px;
    color : <?php echo $styles['hover_color'];?>;
}

#<?php echo $id;?> .op-meta {
    height: 90px;
    width: 90px;
    border: 1px solid #b1d3a5;
    background: #b1d3a5;
    font-size: 20px;
    text-align: center;
}
#<?php echo $id;?> .op-date{
    line-height: 26px;
}
#<?php echo $id;?> .uk-card-title {
	font-size : <?php echo $settings['item_title_size'];?>px; 
}
#<?php echo $id;?>  .op-card-big .op-meta {
    position: absolute;
    right: 100px;
    top: 170px;
}
@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
	#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a,
	#<?php echo $id;?> .uk-card-default .uk-card-title a{
		font-size : <?php echo ($settings['item_title_size']/2) +8;?>px;
	}
}
