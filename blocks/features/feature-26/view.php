<?php

	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';

	// title animation
	$item_animation = ($settings['item_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['item_animation'].';' : '';

?>


<section id="<?php echo $id;?>" class="features pro-features-26 uk-padding-small">
	<div class="uk-section">
		<div class="uk-container uk-position-relative">

		    <div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">
		        <?php if($contents['title']):?>
	              <!-- Section Title -->
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary uk-margin-remove-top uk-margin-medium-bottom', 
							'uk-text-' . $settings['title_transformation'], 
							$title_animation . '"'
						); 
					?>
	            <?php endif; ?>

	            <?php if($contents['description']):?>
	                <div class="uk-text-lead" <?php echo ( $settings['title_animation'] ? $title_animation . 'delay:500"' : '' ); ?>><?php echo $contents['description']?></div>
		        <?php endif; ?>
		    </div> <!-- Section heading -->

			<div class="uk-child-width-1-2@m"  uk-grid>
				<div class="uk-card">
				<?php $i = 0; ?>
				<?php foreach($contents['items'] as $feature): ?>
					<div class="uk-text-center" uk-grid <?php echo ( $settings['item_animation'] ? $item_animation . 'delay:300"' : '' ); ?>>
					    <div class="uk-width-auto">
							<div class="op-meta uk-flex uk-flex-center uk-flex-middle">
								<h3 class="uk-margin-remove op-date"><?php echo $feature['date'];?> <br><?php echo $feature['month'];?></h3>
							</div>
					    </div>
					    <div class="uk-width-expand uk-text-left uk-padding-small uk-padding-remove-vertical">
					        <div class="uk-card uk-card-body uk-padding-remove">
								<h3 class="uk-card-title uk-margin-small uk-link-heading uk-margin-remove">
									<a href="<?php echo $feature['link'];?>">
										<?php echo $feature['title'];?>					
									</a>
								</h3>
					        	<p class="uk-margin-remove"><?php echo $feature['date_time'];?></p>
					        </div>
					    </div>
					</div>
				<?php  if ($i == 2) { break; } ?>
				<?php $i++; endforeach; ?>
				</div>

				<!-- Start post with image -->
				<?php $count = 0; ?>
				<?php foreach($contents['items'] as $feature): ?>
				<?php
				    $count++; // Note that first iteration is $count = 1 not 0 here.
				    if($count <= 3) continue; // Skip the iteration unless 4th or above.
				?>
		        <div class="uk-card" <?php echo ( $settings['item_animation'] ? $item_animation . 'delay:600"' : '' ); ?>>
		            <div class="uk-card-media-top op-card-big">
		            	<div class="op-meta uk-flex uk-flex-center uk-flex-middle">
							<h3 class="uk-margin-remove op-date"><?php echo $feature['date'];?> <br><?php echo $feature['month'];?></h3>
						</div>
		                <img src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>">
		            </div>
		            <div class="uk-card-body uk-padding-remove-horizontal">
		                <h3 class="uk-card-title uk-heading-primary uk-link-heading"><a href="<?php echo $feature['link'];?>"><?php echo $feature['title'];?></a></h3>
		                <p><?php echo $feature['date_time'];?></p>
		            </div>
		        </div>
				<?php $i++; endforeach; ?>

			</div><!-- uk-container -->
		</div>
	</div> <!-- uk-section -->
</section> <!-- end-section -->
