<?php

return array(

  'slug'      => 'pro-features-27', // Must be unique and singular
  'groups'    => array('features'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Title', 'type'=>'divider'),
    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> 'https://demo.wponepager.com/wp-content/uploads/2019/05/banner-1.jpg'),
          array('name'=>'link', 'placeholder'=> home_url()),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> 'https://demo.wponepager.com/wp-content/uploads/2019/05/banner-2.jpg'),
          array('name'=>'link', 'placeholder'=> home_url()),
        ),
      )
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(

   array('label'=>'Items', 'type'=>'divider'),
   array(
    'name'     => 'item_animation',
    'label'    => 'Animation Item',
    'type'     => 'select',
    'value'    => 'fadeInUp',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(

    array(
      'name'    => 'bg_color',
      'label'   => 'Background Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),

  ),

);
