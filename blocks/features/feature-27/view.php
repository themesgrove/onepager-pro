<?php
	// items animation
	$item_animation = ($settings['item_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['item_animation'].';' : '';
?>
<section id="<?php echo $id;?>" class="uk-section uk-position-relative features pro-features-27 uk-background-cover uk-background-norepeat">
	<div class="uk-section-small">
		<div class="uk-container">
			<div class="uk-grid-match uk-flex-middle" uk-grid>
				<?php $i = 0; ?>
				<?php foreach($contents['items'] as $feature): ?>
					<div class="uk-width-1-2@m" <?php echo ( $settings['item_animation'] ? $item_animation . 'delay:300"' : '' ); ?>>
						<?php if ($feature['media']): ?>
							<a href="<?php echo $feature['link'];?>"><img src="<?php echo $feature['media'];?>" alt="Banner"></a>
						<?php endif; ?>
					</div>
				<?php $i++; endforeach; ?>
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->

