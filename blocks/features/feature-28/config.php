<?php

return array(
  'slug'    => 'pro-features-28',
  'groups'    => array('features'),

  'contents' => array(
    array(
      'name'=>'title',
      'value'=>'Help Us Our Causes'
    ),

    array('label'=>'Items', 'type'=>'divider'), // Divider - Text

    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'media', 'type'=>'media', 'value'=> 'https://demo.wponepager.com/wp-content/uploads/2019/05/d6.jpg'),
          array('name'=>'title', 'value' => 'Give Life to Yemen'),
          array('name'=>'description', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunto oporteat cuum.'),
          array(
            'name'  => 'readmore_text',
            'label' => 'Donate',
            'value' => 'Donate Now',
          ),
          array('name'=>'link', 'placeholder'=> home_url()),
          array(
            'name'  => 'form_shortcode',
            'label' => 'Button Shortcode',
            'value' => '',
          ),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'value'=> 'https://demo.wponepager.com/wp-content/uploads/2019/05/d5.jpg'),
          array('name'=>'title', 'value' => 'Water for Gaza'),
          array('name'=>'description', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunto oporteat cuum.'),
          array(
            'name'  => 'readmore_text',
            'label' => 'Donate',
            'value' => 'Donate Now',
          ),
           array('name'=>'link', 'placeholder'=> home_url()),
           array(
            'name'  => 'form_shortcode',
            'label' => 'Button Shortcode',
            'value' => '',
          ),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'value'=> 'https://demo.wponepager.com/wp-content/uploads/2019/05/d4.jpg'),
          array('name'=>'title', 'value' => 'Child Sponsorship'),
          array('name'=>'description', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunto oporteat cuum.'),
          array(
            'name'  => 'readmore_text',
            'label' => 'Donate',
            'value' => 'Donate Now',
          ),
           array('name'=>'link', 'placeholder'=> home_url()),
           array(
            'name'  => 'form_shortcode',
            'label' => 'Button Shortcode',
            'value' => '',
          ),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'value'=> 'https://demo.wponepager.com/wp-content/uploads/2019/05/d3.jpg'),
          array('name'=>'title', 'value' => 'Syrian Mother & Child'),
          array('name'=>'description', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunto oporteat cuum.'),
          array(
            'name'  => 'readmore_text',
            'label' => 'Donate',
            'value' => 'Donate Now',
          ),
           array('name'=>'link', 'placeholder'=> home_url()),
           array(
            'name'  => 'form_shortcode',
            'label' => 'Button Shortcode',
            'value' => '',
          ),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'value'=> 'https://demo.wponepager.com/wp-content/uploads/2019/05/d2.jpg'),
          array('name'=>'title', 'value' => 'Feed the Fasting'),
          array('name'=>'description', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunto oporteat cuum.'),
          array(
            'name'  => 'readmore_text',
            'label' => 'Donate',
            'value' => 'Donate Now',
          ),
           array('name'=>'link', 'placeholder'=> home_url()),
           array(
            'name'  => 'form_shortcode',
            'label' => 'Button Shortcode',
            'value' => '',
          ),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'value'=> 'https://demo.wponepager.com/wp-content/uploads/2019/05/d1.jpg'),
          array('name'=>'title', 'value' => 'Clean Water for All'),
          array('name'=>'description', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunto oporteat cuum.'),
          array(
            'name'  => 'readmore_text',
            'label' => 'Donate',
            'value' => 'Donate Now',
          ),
          array('name'=>'link', 'placeholder'=> home_url()),
          array(
            'name'  => 'form_shortcode',
            'label' => 'Button Shortcode',
            'value' => '',
          ),
        ),
      )

    )
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),


    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name'     => 'items_columns',
      'label'    => 'Items Columns',
      'type'     => 'select',
      'value'    => '3',
      'options'  => array(
        '2'   => '2',
        '3'   => '3',
        '4'   => '4',

      ),
    ),

    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '22'
    ),

    array(
      'name' => 'item_desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),

    array(
      'name'     => 'items_animation',
      'label'    => 'Items Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),


  'styles' => array(
    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => '#ffffff'
    ),
    array(
      'name' => 'bg_color',
      'label' => 'Background Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),

    array('label'=>'Title Style', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#212121'
    ),

    array('label'=>'Items Style', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'item_title_color',
      'label' => 'Item Title Color',
      'type' => 'colorpicker',
      'value' => '#212121'
    ),

    array(
      'name' => 'item_desc_color',
      'label' => 'Item Desc Color',
      'type' => 'colorpicker',
      'value' => '#212121'
    ),

    array(
      'name' => 'btn_color',
      'label' => 'Button Color',
      'type' => 'colorpicker',
      'value' => '#8ec63d'
    ),

    array(
      'name' => 'btn_hover_color',
      'label' => 'Button Hover Color',
      'type' => 'colorpicker',
      'value' => '#7ab02c'
    ),
  ),
);
