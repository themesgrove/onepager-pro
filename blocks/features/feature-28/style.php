#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}

#<?php echo $id;?> a.uk-button-text{
	text-decoration: none;
}
#<?php echo $id;?> a.uk-button-text:hover{
	color : <?php echo $styles['btn_hover_color']?>;
}
#<?php echo $id;?> .uk-button-text::before{
	border-color : <?php echo $styles['btn_hover_color']?>;
}
#<?php echo $id;?> .item-title,
#<?php echo $id;?> .item-title a{
	font-size : <?php echo $settings['item_title_size']?>px;
	color : <?php echo $styles['item_title_color']?>;
}

#<?php echo $id;?> .item-desc{
	font-size : <?php echo $settings['item_desc_size']?>px;
	color : <?php echo $styles['item_desc_color']?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

#<?php echo $id; ?> .items .item-img{
	overflow:hidden;
}
#<?php echo $id; ?> .items img{
    transition: all 0.3s ease;
    overflow: hidden;
    transform: scale(1);
}
#<?php echo $id; ?> .items:hover img{
	transition: all 0.3s ease;
    transform: scale(1.1);
}
#<?php echo $id; ?> .op-shortcode-button button{
	width: 100%;
    height: 40px;
    border: none;
    text-transform: uppercase;
    color: #fff;
    font-size: 14px;
}
#<?php echo $id; ?> .uk-button-primary, 
#<?php echo $id; ?> .op-shortcode-button button{
	background: <?php echo $styles['btn_color']?>;
	transition: all 0.3s ease;
}
#<?php echo $id; ?> .uk-button-primary:hover, 
#<?php echo $id; ?> .op-shortcode-button button:hover{
	background: <?php echo $styles['btn_hover_color']?>;
	transition: all 0.3s ease;
}
@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}