<?php

	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].';' : '';
?>

<section id="<?php echo $id;?>" class="uk-position-relative features pro-features-28 uk-padding-small" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container">
			<article class="uk-article uk-position-relative">
				<div class="section-heading uk-margin-large-bottom uk-text-center">	
					<?php if($contents['title']):?>
						<!-- Section Title -->
							<?php
								echo op_heading( 
									$contents['title'],
									$settings['heading_type'], 
									'uk-heading-primary uk-margin-medium-bottom', 
									'uk-text-' . $settings['title_transformation'], 
									$title_animation . '"'
								); 
							?>
					<?php endif; ?>
				</div>

				<div class="uk-child-width-1-<?php echo $settings['items_columns'];?>@m" uk-grid>
				<?php $i=4; ?>
				<?php foreach($contents['items'] as $feature): ?>
				    <div class="item" <?php echo ($settings['items_animation'])? $items_animation .';delay:' .'300"' : '' ;?>>
				        <div class="uk-card uk-card-default uk-text-center">
				            <div class="uk-card-media-top">
								<!-- Item image -->
								<?php if($feature['media']):?>
									<img class="op-media" src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>" />
								<?php endif;?>
				            </div>
				            <div class="uk-card-body">
								<?php if ($feature['title']): ?>
									<!-- Item title -->
									<h3 class="item-title">
										<?php if(trim($feature['link'])): ?>
											<a href="<?php echo $feature['link']; ?>"><?php echo $feature['title'];?></a>
										<?php else: ?>
											<?php echo $feature['title'];?>
										<?php endif; ?>
									</h3>
								<?php endif; ?>
								<?php if ($feature['description']): ?>
									<!-- Item desc -->
									<p class="item-desc"><?php echo $feature['description'];?></p>
								<?php endif; ?>

								<?php if ($feature['form_shortcode']): ?>	
									<a class="op-shortcode-button uk-width-1-1 uk-margin-small-bottom" href="<?php echo $feature['link']; ?>"><?php echo $feature['form_shortcode'];?></a>
									<?php elseif ($feature['readmore_text']) : ?>
									<a class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom" href="<?php echo $feature['link']; ?>"><?php echo $feature['readmore_text'];?></a>
								<?php endif; ?>
				            </div>
				        </div>
				    </div>
				<?php $i++; endforeach; ?>
				</div>

			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
