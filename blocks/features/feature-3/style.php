#<?php echo $id;?>{
	<?php if ($styles['bg_color']): ?>
		background-color : <?php echo $styles['bg_color']?>;
	<?php endif; ?>
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;x;
}


#<?php echo $id;?> .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?> .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
}



#<?php echo $id;?> .op-media{
	color : <?php echo $styles['icon_color']?>;
}


#<?php echo $id;?> .item-title,
#<?php echo $id;?> .item-title a{
	color : <?php echo $styles['item_title_color']?>;
}


#<?php echo $id;?> .uk-text-medium{
	color : <?php echo $styles['item_desc_color']?>;
}

#feature-item-easing{
	<?php if ($styles['bg_image']): ?>
		background-image:url(<?php echo $styles['bg_image']; ?>);
		background-repeat: no-repeat;
	    background-size: auto;
	    background-position: center;
	<?php endif; ?>
}

@media(max-width:768px){
	#<?php echo $id?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}