<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].'' : '';
?>


<section id="<?php echo $id;?>" class="fp-section uk-cover-container features pro-features-3 uk-padding-small">
	<div class="uk-section">
		<div class="uk-container">
			<article class="uk-article">
				<div class="uk-panel uk-text-<?php echo $title_alignment;?>">	
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-medium-bottom', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead uk-width-3-4@m uk-width-1-1@s uk-margin-auto" 
							<?php echo ($settings['title_animation'] ? $title_animation . ';repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' . '300"' : ''); ?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>
				</div>

				<div id="feature-item-easing" class="feature-item uk-text-center uk-margin-medium-top">
				    <div class="uk-grid uk-margin-auto" uk-grid>
				    	<?php $i=3; ?>
				     	<?php foreach($contents['items'] as $feature): ?>
					        <div class="uk-card uk-width-1-3@m uk-width-1-1@s  uk-margin-small" 
					        	<?php echo ($settings['items_animation'] ? $items_animation . ';repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' . $i . '00"' : ''); ?>>
					        	<!-- item image -->
								<?php if( op_is_image($feature['image'])):?>
									<img width="" src="<?php echo $feature['image']; ?>" alt="<?php echo $feature['title'];?>" />
								<?php endif;?>
					        </div>
				        <?php $i++; endforeach; ?>
				    </div>
				</div>
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- op-section -->
