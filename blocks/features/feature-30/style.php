#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['section_title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
}

#<?php echo $id;?> .item-title,
#<?php echo $id;?> .item-title a{
	font-size : <?php echo $settings['item_title_size']?>px;
	color : <?php echo $styles['item_title_color']?>;
}


#<?php echo $id;?> .uk-text-medium{
	font-size : <?php echo $settings['item_desc_size']?>px;
	color : <?php echo $styles['item_desc_color']?>;
}


#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

#<?php echo $id; ?> .items .item-img{
	overflow:hidden;
}
#<?php echo $id; ?> .items img{
    transition: all 0.3s ease;
    overflow: hidden;
    transform: scale(1);
    width: 100%;
}
#<?php echo $id; ?> .items:hover img{
	transition: all 0.3s ease;
  transform: scale(1.1);
}

#<?php echo $id; ?> .card-single:nth-child(4n+1) .uk-link-heading:hover{
  color: #f9be4d;
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(4n+1) .uk-card{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 2px 5px 0px rgba(255, 194, 77, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(4n+1):hover .uk-card{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 2px 10px 0px rgba(255, 194, 77, 0.5);
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .card-single:nth-child(4n+2) .uk-link-heading:hover{
  color: #ff647a;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .card-single:nth-child(4n+2) .uk-card{
  border-radius: 10px;
  background-color: #fff;
  box-shadow: 0px 2px 5px 0px rgba(255, 90, 113, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(4n+2):hover .uk-card{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 2px 10px 0px rgba(255, 90, 113, 0.5);
  transition: all 0.5s ease;
}
}
#<?php echo $id; ?> .card-single:nth-child(4n+3) .uk-link-heading:hover{
  color: #4d75f9;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .card-single:nth-child(4n+3) .uk-card{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 2px 5px 0px rgba(0, 126, 246, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(4n+3):hover .uk-card{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow:  0px 2px 10px 0px rgba(0, 126, 246, 0.5);
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .card-single:nth-child(4n+4) .uk-link-heading:hover{
  color: #68dd72;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .card-single:nth-child(4n+4) .uk-card{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 2px 5px 0px rgba(51, 208, 65, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(4n+4):hover .uk-card{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 2px 10px 0px rgba(51, 208, 65, 0.5);
  transition: all 0.5s ease;
}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}