#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['section_title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
}

#<?php echo $id;?> .uk-card-title{
	font-size : <?php echo $settings['item_title_size']?>px;
	color : <?php echo $styles['item_title_color']?>;
}

#<?php echo $id;?> .uk-card-title:hover a{
  color : <?php echo $styles['item_title_color']?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

#<?php echo $id; ?> .items .item-img{
	overflow:hidden;
}
#<?php echo $id; ?> .items img{
    transition: all 0.3s ease;
    overflow: hidden;
    transform: scale(1);
    width: 100%;
}
#<?php echo $id; ?> .items:hover img{
	transition: all 0.3s ease;
    transform: scale(1.1);
}

#<?php echo $id; ?> .card-single:nth-child(5n+1) .uk-card{
  border-radius: 10px;
  background-color: #b45ef4;
  box-shadow: 0px 4px 5px 0px rgba(189, 114, 245, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+1):hover .uk-card{
  border-radius: 10px;
  background-color: #b45ef4;
  box-shadow: 0px 4px 25px 0px rgba(189, 114, 245, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+1) .op-media{
  background-color: #bd72f5;
}

#<?php echo $id; ?> .card-single:nth-child(5n+2) .uk-card{
  border-radius: 10px;
  background-color: #f45e5e;
  box-shadow: 0px 4px 5px 0px rgba(244, 94, 94, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+2):hover .uk-card{
  border-radius: 10px;
  background-color: #f45e5e;
  box-shadow: 0px 4px 25px 0px rgba(244, 94, 94, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+2) .op-media{
  background-color: #f57272;
}

#<?php echo $id; ?> .card-single:nth-child(5n+3) .uk-card{
  border-radius: 10px;
  background-color: #5e6cf4;
  box-shadow: 0px 4px 5px 0px rgba(94, 108, 244, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+3):hover .uk-card{
  border-radius: 10px;
  background-color: #5e6cf4;
  box-shadow: 0px 4px 25px 0px rgba(94, 108, 244, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+3) .op-media{
  background-color: #727ef5;
}

#<?php echo $id; ?> .card-single:nth-child(5n+4) .uk-card{
  border-radius: 10px;
  background-color: #f4ac5e;
  box-shadow: 0px 4px 5px 0px rgba(244, 172, 94, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+4):hover .uk-card{
  border-radius: 10px;
  background-color: #f4ac5e;
  box-shadow: 0px 4px 25px 0px rgba(244, 172, 94, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+4) .op-media{
  background-color: #f5b672;
}

#<?php echo $id; ?> .card-single:nth-child(5n+5) .uk-card{
  border-radius: 10px;
  background-color: #66a5fe;
  box-shadow: 0px 4px 5px 0px rgba(102, 165, 254, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+5):hover .uk-card{
  border-radius: 10px;
  background-color: #66a5fe;
  box-shadow: 0px 4px 25px 0px rgba(102, 165, 254, 0.35);
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:nth-child(5n+5) .op-media{
  background-color: #66a5fe;
}

#<?php echo $id; ?> .op-media{
    padding: 20px;
    border-radius: 100px;
    background: #efeff3;
    height: 95px;
    width: 95px;
    transition: all 0.5s ease;
}

#<?php echo $id; ?> .card-single:hover .op-media{

}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}