#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['section_title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}
#<?php echo $id; ?> .uk-card:nth-child(4n+1){
	border-top : 5px solid #ff5e49;
	border-radius: 5px;
}

#<?php echo $id; ?> .uk-card:nth-child(4n+1) a.uk-button-text{
	color: #ff5e49;
}

 #<?php echo $id; ?> .uk-card:nth-child(4n+1) .uk-button-text::before{
 	border-color: #ff5e49;
}

#<?php echo $id; ?> .op-card-single:nth-child(4n+2) > .uk-card{
	border-top : 5px solid #0079ff;
	border-radius: 5px;
}

#<?php echo $id; ?> .op-card-single:nth-child(4n+2) a.uk-button-text{
	color: #0079ff;
}

 #<?php echo $id; ?> .op-card-single:nth-child(4n+2) .uk-button-text::before{
 	border-color: #0079ff;
}
#<?php echo $id; ?> .op-card-single:nth-child(4n+3) > .uk-card{
	border-top : 5px solid #28ce37;
	border-radius: 5px;
}

#<?php echo $id; ?> .op-card-single:nth-child(4n+3) a.uk-button-text{
	color: #28ce37;
}

 #<?php echo $id; ?> .op-card-single:nth-child(4n+3) .uk-button-text::before{
 	border-color: #28ce37;
}
#<?php echo $id; ?> .op-card-single:nth-child(4n+4) > .uk-card{
	border-top : 5px solid #faaf7c;
	border-radius: 5px;
}

#<?php echo $id; ?> .op-card-single:nth-child(4n+4) a.uk-button-text{
	color: #faaf7c;
}

 #<?php echo $id; ?> .op-card-single:nth-child(4n+4) .uk-button-text::before{
 	border-color: #faaf7c;
}