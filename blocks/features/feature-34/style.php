#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['section_title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id; ?> .uk-card{
    border-top: 5px solid rgba(251, 136, 159, 0);
    border-radius: 5px;
    transition: all 0.3s ease;
    box-shadow: 0px 4px 5px 0px rgba(251, 136, 159, 0);
    padding-top: 40px;
    padding-bottom: 40px;
}
#<?php echo $id; ?> .uk-card:hover{
	border-top : 5px solid #ff5e49;
	border-radius: 5px;
	transition: all 0.3s ease;
	box-shadow: 0px 4px 5px 0px rgba(251, 136, 159, 0.35);
}

#<?php echo $id; ?> .uk-card:hover .uk-card-title{
	color: #ff5e49;
	transition: all 0.3s ease;
}

 #<?php echo $id; ?> .uk-card:hover .uk-button-text::before{
 	border-color: #ff5e49;
 	transition: all 0.3s ease;
}
