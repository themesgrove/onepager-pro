<?php
	// title animation
	$title_animation = ( $settings['title_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['title_animation'] . ';"' : '';
	$items_animation = ( $settings['items_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['items_animation'] . ';"' : '';
	// title alignment
	$title_alignment = ( $settings['title_alignment'] ) ? $settings['title_alignment'] : '';
?>
<section id="<?php echo $id;?>" class="uk-section uk-position-relative features pro-features-34 uk-background-cover uk-background-norepeat">
	<div class="uk-section-small">
		<div class="uk-container">

			<div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment; ?>">	
				<?php if ( $contents['title'] ) : ?>
				<!-- Section Title -->
				  	<?php 
						echo op_heading(
							$contents['title'],
							$settings['heading_type'],
							'uk-heading-primary uk-text-'.$settings['title_transformation'],
							$title_animation
						); 
					?>
				<?php endif; ?>
				<?php if($contents['desc']):?>
					<!-- Section Sub Title -->
					<p class="uk-text-lead" <?php echo ($settings['title_animation'])? $title_animation . ';delay:' .'300"' : '' ;?>>
						<?php echo $contents['desc'];?>
					</p>
				<?php endif; ?>
			</div>

			<div class="uk-grid-match uk-flex-middle" uk-grid>
				<?php $i = 0; ?>
				<?php foreach($contents['items'] as $feature): ?>
					<div class="op-card-single uk-width-1-3@m" <?php echo ( $settings['items_animation'] ? $items_animation . 'delay:300"' : '' ); ?>>
				        <div class="uk-card uk-card-small uk-card-body uk-text-center">
							<?php if( op_is_image($feature['media'])):?>
								<img class="op-media uk-display-inline-block" src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>" />
							<?php else :?>
								<span class="op-media uk-display-inline-block <?php echo $feature['media']; ?>"></span>
							<?php endif;?>
				            <h3 class="uk-card-title"><?php echo $feature['title']; ?></h3>
				            <p><?php echo $feature['description']; ?></p>
		            		<?php if ($feature['link']['text']): ?>	
								<a class="uk-button-text uk-link-heading" href="<?php echo $feature['link']['url']; ?>"><?php echo $feature['link']['text'];?></a>
							<?php endif; ?>
				        </div>
					</div>
				<?php $i++; endforeach; ?>
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->

