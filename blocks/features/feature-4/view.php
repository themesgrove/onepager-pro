<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].'' : '';
	// array chunk
	$items = array_chunk($contents['items'], 2);

?>

<section id="<?php echo $id;?>" class="fp-section features pro-features-4">
	<div class="uk-section-large">
		<div class="uk-container">
			<article class="uk-article">
				<div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">	
					<?php if ($contents['media_top']): ?>
							<?php if( op_is_image($contents['media_top'])):?>
								<img src="<?php echo $contents['media_top']; ?>" 
								<?php echo ($settings['title_animation'] ? $title_animation .'repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '100"' : ''); ?>>
							<?php else :?>
								<span class="op-media uk-padding <?php echo $contents['media_top']; ?>" 
									<?php echo ($settings['title_animation'] ? $title_animation .'repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '100"' : ''); ?>>	
								</span>
							<?php endif;?>
						<?php endif; ?>

					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-text-bold', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead" 
							<?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '400"' : ''); ?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>

					<?php if ($contents['link']['text']): ?>		
						<p 
							<?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '500"' : ''); ?>>		
							<a class="uk-button-primary uk-text-bold uk-button-large uk-margin-medium-top uk-button" target="<?php echo $contents['link']['target'] ? '_blank' : ''?>" href="<?php echo $contents['link']['url']; ?>"><?php echo $contents['link']['text']; ?>

							<i class="btn-icon <?php echo $contents['button_icon'];?> <?php echo ($contents['button_icon']) ? 'uk-margin-left' : '' ?>"></i>
							</a>
						</p>
					<?php endif; ?>
				</div> <!-- section-heading -->

				<div class="uk-position-relative">
					<div class="uk-width-1-3@m uk-width-width-1-1@s uk-visible@s uk-position-center uk-margin-medium-top">
						<img class="op-media center-image" width="520" src="<?php echo $contents['image']; ?>" />
					</div>
					<?php foreach($items as $item):?>
						<div class="uk-flex uk-flex-wrap uk-margin">
						<?php $i=4; ?>
							<?php foreach($item as $key=>$feature):?>
								<?php if($key%2): ?>
									<div class="uk-width-1-3@m uk-width-width-1-1@s"></div>
								<?php endif; ?>
								<div class="uk-width-1-3@m uk-width-width-1-1@s uk-float-<?= ($key%2) ? 'right' : 'left' ?>">
									<div class="uk-card uk-padding uk-text-<?php echo $items_alignment;?>" 
										<?php echo ($settings['items_animation'] ? $items_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . $i . '00"' : ''); ?>>
										<!-- Item image -->
										<?php if( op_is_image($feature['media'])):?>
											<img class="op-media" src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>" />
										<?php else :?>
											<span class="op-media <?php echo $feature['media']; ?>"></span>
										<?php endif;?>
										<!-- Item title -->
										<h3 class="uk-card-title uk-text-bold uk-margin-top uk-margin-remove-bottom uk-text-<?php echo $settings['item_title_transformation'];?>">
												<?php echo $feature['title'];?>
										</h3>
										<!-- Item desc -->
										<p class="uk-text-medium uk-margin-small"><?php echo $feature['description'];?></p>
									</div><!-- blurb -->
								</div> <!-- uk-width -->
							<?php $i++; endforeach;?>
						</div> <!-- uk-flex -->
					<?php endforeach;?>
				</div> <!-- uk-position-relative -->
			</article> <!-- uk-article -->
		</div> <!-- uk-section -->
	</div> <!-- uk-container -->
</section> <!-- op-section -->
