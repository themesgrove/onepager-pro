<?php

return array(
  'slug'    => 'pro-features-5',
  'groups'    => array('features'),

  'contents' => array(
    array('name'=>'media_top', 'label'=>'Top Icon/Image','type'=>'media', 'size'=>'fa-4x', 'value'=> ''),
    array(
      'name'=>'title',
      'value'=>'Freehand'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Build hi-fi prototypes with your real design files in Sketch.'
    ),
    array('label'=>'Button  ', 'type'=>'divider'), // Divider - Text
    array('name'=>'link', 'type' => 'link', 'text' => 'Read More', 'placeholder'=> home_url()),
    array('name'=>'button_icon', 'label'=>'Icon','type'=>'icon', 'size'=>'fa-lg', 'value'=> ''),

    array('label'=>'Media Image', 'type'=>'divider'), // Divider - Text
    array('name'=>'image', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/02/Interface.png', 'label'=>'Upload Your Image'),

    array('label'=>'Items  ', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-2x', 'value'=> 'fa fa-refresh fa-2x'),
          array('name'=>'title', 'value' => 'Infinite'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Build hi-fi prototypes with your real design files in Sketch.'),
        ),
        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-2x', 'value'=> 'fa fa-tags fa-2x'),
          array('name'=>'title', 'value' => 'Agile'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Build hi-fi prototypes with your real design files in Sketch.'),
        ),

        array(
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-2x', 'value'=> 'fa fa-flag-o fa-2x'),
          array('name'=>'title', 'value' => 'Inclusive'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Build hi-fi prototypes with your real design files in Sketch.'),
        )
      )

    ),
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),

    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'     => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),
    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'title_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Button Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'button_size',
      'label' => 'Text Size',
      'append' => 'px',
      'value' => '16'
    ),


    array(
      'name'     => 'button_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'button_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '50'
    ),

    array('label'=>'Image Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'media_size',
      'label' => 'Image Size',
      'append' => 'px',
      'value' => '810'
    ),

    array(
      'name' => 'image_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '10'
    ),

    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '20'
    ),

    array(
      'name'     => 'item_title_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'item_desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '14'
    ),

    array(
      'name'     => 'items_alignment',
      'label'    => 'Items Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),


    array(
      'name'     => 'items_animation',
      'label'    => 'Items Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),




  'styles' => array(
    array(
      'name'  => 'bg_image',
      'label' => 'Image',
      'type'  => 'image',
      'value' => ''
    ),
    array(
      'name' => 'bg_color',
      'label' => 'Background Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),

    array('label'=>'Top Icon', 'type'=>'divider'), // Divider - Text
      array(
      'name' => 'top_image_color',
      'label' => 'Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),

    array(
      'name' => 'top_image_bg_color',
      'label' => 'Bg Color',
      'type' => 'colorpicker',
      'value' => '@color.primary'

    ),

    array('label'=>'Title Styles', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '@color.primary'

    ),

    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#323232'

    ),

    array('label'=>'Button Styles', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'button_color',
      'label' => 'Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),

    array(
      'name' => 'button_bg_color',
      'label' => 'Bg Color',
      'type' => 'colorpicker',
      'value' => '@color.primary'

    ),


    array('label'=>'Items Styles', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'item_title_color',
      'label' => 'Item Title Color',
      'type' => 'colorpicker',
      'value' => '#48525d'
    ),

    array(
      'name' => 'item_desc_color',
      'label' => 'Item Desc Color',
      'type' => 'colorpicker',
      'value' => '#48525d'
    ),

    array(
      'name' => 'icon_color',
      'label' => 'Icon Color',
      'type' => 'colorpicker',
      'value' => '#48525d'
    ),
  ),

);
