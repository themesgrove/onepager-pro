#<?php echo $id;?>{
	<?php if ($styles['bg_image']): ?>
		background-image:url(<?php echo $styles['bg_image'];?>);
		background-repeat:no-repeat;
		background-repeat: no-repeat;
	    background-size: contain;
	    background-position: center;
	<?php endif; ?>
	<?php if ($styles['bg_color']): ?>
		background-color : <?php echo $styles['bg_color']?>;
	<?php endif; ?>
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
}


<?php if ($contents['media_top']): ?>
	
	#<?php echo $id;?> .section-heading .op-media{
		color : <?php echo $styles['top_image_color']?>;
		background-color:<?php echo $styles['top_image_bg_color']?>;
	}
<?php endif; ?>

#<?php echo $id;?> .section-heading .uk-button-primary{
	font-size : <?php echo $settings['button_size']?>px;
	color : <?php echo $styles['button_color']?>;
	background-color:<?php echo $styles['button_bg_color']?>;
	text-transform:<?php echo $settings['button_transformation']?>;
	border-radius:<?php echo $settings['button_border_radius']?>px;
}

#<?php echo $id;?> .section-heading .uk-button-primary:hover{
	color : <?php echo $styles['button_bg_color']?>;
	background-color:<?php echo $styles['button_color']?>;
	border-color:<?php echo $styles['button_bg_color']?>;
}

#<?php echo $id;?> .uk-card-title,
#<?php echo $id;?> .uk-card-title a{
	font-size : <?php echo $settings['item_title_size']?>px;
	color : <?php echo $styles['item_title_color']?>;
}


#<?php echo $id;?> .uk-text-medium{
	font-size : <?php echo $settings['item_desc_size']?>px;
	color : <?php echo $styles['item_desc_color']?>;
}
	
#<?php echo $id;?> .section-heading .op-image{
	border-radius: <?php echo $settings['image_border_radius'];?>px;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}