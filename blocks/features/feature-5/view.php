<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].'' : '';
?>


<section id="<?php echo $id;?>" class="fp-section features pro-features-5 uk-padding-small">
	<div class="uk-section-large">
		<div class="uk-container-small uk-margin-auto">
			<article class="uk-article">
				<div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">	

					<?php if ($contents['media_top']): ?>
						<?php if( op_is_image($contents['media_top'])):?>
							<img src="<?php echo $contents['media_top']; ?>" 
						<?php echo ($settings['title_animation'] ? $title_animation . ';repeat:' .($animation_repeat ? 'true' :'false') .';delay:' . '100"' : ''); ?>>
						<?php else :?>
							<span class="op-media uk-padding <?php echo $contents['media_top']; ?>" 
								<?php echo ($settings['title_animation'] ? $title_animation . ';repeat:' .($animation_repeat ? 'true' :'false') .';delay:' . '100"' : ''); ?>>
							</span>
						<?php endif;?>
					<?php endif; ?>
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-text-bold', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead" 
							<?php echo ($settings['title_animation'] ? $title_animation . ';repeat:' .($animation_repeat ? 'true' :'false') .';delay:' . '400"' : ''); ?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>

					<?php if ($contents['link']['text']): ?>		
						<p 	
							<?php echo ($settings['title_animation'] ? $title_animation . ';repeat:' .($animation_repeat ? 'true' :'false') .';delay:' . '500"' : ''); ?>>
							<a class="uk-button-primary uk-text-bold uk-button-large uk-margin-medium-top uk-button" target="<?php echo $contents['link']['target'] ? '_blank' : ''?>" href="<?php echo $contents['link']['url']; ?>"><?php echo $contents['link']['text']; ?>

							<i class="<?php echo $contents['button_icon'];?> <?php echo ($contents['button_icon']) ? 'uk-margin-left' : '' ?>"></i>
							</a>
						</p>
					<?php endif; ?>
					<?php if ($contents['image']): ?>
						<div class="uk-panel uk-margin-large-top uk-text-center" 
							<?php echo ($settings['title_animation'] ? $title_animation . ';repeat:' .($animation_repeat ? 'true' :'false') .';delay:' . '600"' : ''); ?>>
							<img 
								width="<?php echo $settings['media_size'];?>" 
								src="<?php echo $contents['image'];?>" 
								alt="<?php echo $contents['title'];?>" 
								class="op-image" uk-image>
						</div>
					<?php endif; ?>
				</div> <!-- section-heading -->

				 <div class="uk-grid uk-margin-auto  uk-text-<?php echo $items_alignment;?>" uk-grid>
				 <?php $i=3; ?>
					<?php foreach($contents['items'] as $feature): ?>
					<div class="uk-width-1-3@m uk-width-1-1@s"  
						<?php echo ($settings['items_animation'] ? $items_animation . ';repeat:' .($animation_repeat ? 'true' :'false') .';delay:' . $i . '00"' : ''); ?>>
						<div class="uk-card uk-margin-large">
							<!-- Item image -->
							<?php if( op_is_image($feature['media'])):?>
								<img class="op-media" src="<?php echo $feature['media']; ?>" alt="<?php echo $feature['title'];?>" />
							<?php else :?>
								<span class="op-media <?php echo $feature['media']; ?>"></span>
							<?php endif;?>

							<!-- Item title -->
							<h3 class="uk-card-title uk-text-bold uk-margin-top uk-margin-remove-bottom uk-text-<?php echo $settings['item_title_transformation'];?>">
									<?php echo $feature['title'];?>
							</h3>

							<!-- Item desc -->
							<p class="uk-text-medium uk-margin-small"><?php echo $feature['description'];?></p>
						</div><!-- uk-panel -->
					</div><!-- uk-columns -->
					<?php $i++; endforeach; ?>
				</div>
			</article> <!-- uk-article -->
		</div> <!-- uk-section -->
	</div> <!-- uk-container -->
</section> <!-- op-section -->
