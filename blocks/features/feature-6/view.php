<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// items animation
	$media_animation = ($settings['media_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['media_animation'] . ';repeat:' .($animation_repeat ? 'true' :'false') .'"' : '';
?>


<section id="<?php echo $id;?>" class="fp-section features pro-features-6 uk-padding-small">
	<div class="uk-section-large">
		<div class="uk-container-small uk-margin-auto">
			<article class="uk-article">
				<div class="section-heading uk-text-<?php echo $title_alignment;?>">	

					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-text-bold', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead" 
							<?php echo ($settings['title_animation'] ? $title_animation . ';delay:' . '300"' : ''); ?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>

					<?php if ($contents['image']): ?>
						<div class="uk-panel uk-margin-xlarge-top uk-text-center" <?php echo $media_animation;?>>
							<img 
								width="<?php echo $settings['media_size'];?>" 
								src="<?php echo $contents['image'];?>" 
								alt="<?php echo $contents['title'];?>" 
								class="op-image" uk-image>
						</div>
					<?php endif; ?>
				</div> <!-- section-heading -->
			</article> <!-- uk-article -->
		</div> <!-- uk-section -->
	</div> <!-- uk-container -->
</section> <!-- op-section -->
