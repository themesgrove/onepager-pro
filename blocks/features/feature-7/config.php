<?php

return array(

  'slug'      => 'pro-features-7', // Must be unique and singular
  'groups'    => array('features'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array(
      'name'=>'title',
      'type'=>'textarea',
      'value' => 'We Will Take Care Of Your Ranking, Traffic & Revenue!You Better Focus On Your Core Businesses!'
    ),
    array(
      'name'=>'description',
      'type'=>'editor',
      'value'=> 'The world is a dangerous place to live; not because of the people who are evil, but because of the people who dont do anything about it.'
    ),
    array(
      'name'=>'image',
      'type'=>'image',
      'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/02/content-img.png'
    ),
    array( 'name'=>'link', 'text' => 'Read More','type' => 'link'),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array(
      'name' => 'note_image',
      'label' => 'Image',
      'type' => 'divider'
    ),
    array(
      'name'     => 'media_alignment',
      'label'    => 'Image Alignment',
      'type'     => 'select',
      'value'    => 'right',
      'options'  => array(
        'left'    => 'Left',
        'right'   => 'Right'
      ),
    ),
    array(
      'name'     => 'media_grid',
      'label'    => 'Image Grid',
      'type'     => 'select',
      'value'    => 'width-1-2',
      'options'  => array(
        'width-1-2'   => 'Half',
        'width-1-3'   => 'One Thirds',
        'width-1-4'   => 'One Fourth',
        'width-2-3'   => 'Two Thirds',
      ),
    ),
    array(
      'name' => 'media_size',
      'label' => 'Image Size',
      'append' => 'px',
      'value' => '810'
    ),
    array(
      'name'     => 'animation_media',
      'label'    => 'Animation Media',
      'type'     => 'select',
      'value'    => 'slide-right-medium',
      'options'  => array(
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
        ),
      ),
    array( 'label' => 'Title', 'type' => 'divider'),
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0           => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '22'
    ),

    array(
      'name'     => 'animation_content',
      'label'    => 'Animation Content',
      'type'     => 'select',
      'value'    => 'slide-left-medium',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Button Settings', 'type'=>'divider'), // Divider - Background
    array(
      'name' => 'button_size',
      'label' => 'Button Size',
      'append' => 'px',
      'value' => '14'
    ),
    array(
      'name'     => 'button_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'uppercase',
      'options'  => array(
        'inherit'     => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '50'
    ),
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array('label'=>'Background Image', 'type'=>'divider'), // Divider - Background
    array(
      'name'  => 'bg_image',
      'label' => 'Image',
      'type'  => 'image'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(239,0,0,0)'
    ),
    array(
      'name'    => 'bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#BABABB'
    ),
    array('label'=>'Content', 'type'=>'divider'),
    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),

    array(
      'name'  => 'title_link_color',
      'label' => 'Title link Color',
      'type'  => 'colorpicker',
      'value' => '#36deff'
    ),
    array(
      'name'  => 'text_color',
      'label' => 'Desc Color',
      'type'  => 'colorpicker',
      'value' => '#3d3d42'
    ),
    array('label'=>'Button', 'type'=>'divider'),
    array(
      'name'    => 'button_text_color',
      'label'   => 'Button Text',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Button Background',
      'type'    => 'colorpicker',
      'value'   => '@color.primary'
    ),
  ),
);
