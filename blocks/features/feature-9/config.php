<?php

return array(
  'slug'    => 'pro-features-9',
  'groups'    => array('features'),

  'contents' => array(
    array(
      'name'=>'title',
      'value'=>'We are Open, Solid, Honest & Always Follow Our Soul & Work Ethics'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Dont limit yourself. Many people limit themselves to what they think they can do. You can go as far as your mind lets you. What you believe, remember, you can achieve'
    ),
    array(
      'name' => 'image',
      'type' => 'image',
      'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/Mockup.png'
    ),

    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'value' => 'International SEO'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart'),
          array('name'=>'media', 'type'=>'icon', 'size'=>'fa-2x', 'value'=> 'fa fa-mobile fa-2x'),
          array('name'=>'link', 'placeholder'=> home_url()),
          array('name'=>'target', 'label'=>'open in new window', 'type'=>'switch'),
        ),
        array(
          array('name'=>'title', 'value' => 'Online Reputation Management'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Design is not how it looks like of feels lie, design is how its works'),
          array('name'=>'media', 'type'=>'icon', 'size'=>'fa-2x', 'value'=> 'fa fa-mobile fa-2x'),
          array('name'=>'link', 'placeholder'=> home_url()),
          array('name'=>'target', 'label'=>'open in new window', 'type'=>'switch'),
        ),
        array(
          array('name'=>'title', 'value' => 'Social Media'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Start by doing whats necessary; then do whats possible; and suddenly you are doing the impossible.'),
          array('name'=>'media', 'type'=>'icon', 'size'=>'fa-2x', 'value'=> 'fa fa-mobile fa-2x'),
          array('name'=>'link', 'placeholder'=> home_url()),
          array('name'=>'target', 'label'=>'open in new window', 'type'=>'switch'),
        ),
        array(
          array('name'=>'title', 'value' => 'Content Marketing'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Start by doing whats necessary; then do whats possible; and suddenly you are doing the impossible.'),
          array('name'=>'media', 'type'=>'icon', 'size'=>'fa-2x', 'value'=> 'fa fa-mobile fa-2x'),
          array('name'=>'link', 'placeholder'=> home_url()),
          array('name'=>'target', 'label'=>'open in new window', 'type'=>'switch'),
        )
      )
    )
  ),

  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Media', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'media_size',
      'label' => 'Image Size',
      'append' => 'px',
      'value' => '1200'
    ),
    array(
    'name'     => 'animation_media',
    'label'    => 'Media Animation',
    'type'     => 'select',
    'value'    => '0',
    'options'  => array(
      '0'                     =>  'None',
      'fade'                  =>  'Fade',
      'scale-up'              =>  'Scale Up',
      'scale-down'            =>  'Scale Down',
      'slide-top-small'       =>  'Slide Top Small',
      'slide-bottom-small'    =>  'Slide Bottom Small',
      'slide-left-small'      =>  'Slide Left Small',
      'slide-right-small'     =>  'Slide Right Small',
      'slide-top-medium'      =>  'Slide Top Medium',
      'slide-bottom-medium'   =>  'Slide Bottom Medium',
      'slide-left-medium'     =>  'Slide Left Medium',
      'slide-right-medium'    =>  'Slide Right Medium',
      'slide-top'             =>  'Slide Top 100%',
      'slide-bottom'          =>  'Slide Bottom 100%',
      'slide-left'            =>  'Slide Left 100%',
      'slide-right'           =>  'Slide Right 100%'
      ),
    ),

    array('label'=>'Content', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(
        '0'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'animation_content',
      'label'    => 'Content Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('label'=>'Items', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'item_title_size',
      'label' => 'Item Title Size',
      'append' => 'px',
      'value' => '22'
    ),
    array(
      'name'     => 'item_title_transformation',
      'label'    => 'Item Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'item_desc_size',
      'label' => 'Item Desc Size',
      'append' => 'px',
      'value' => '14'
    ),

  ),

  'styles' => array(
    array(
      'name'  => 'bg_image',
      'label' => 'Image',
      'type'  => 'image'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(255,150,102,0.72)'
    ),
    array(
      'name' => 'bg_color',
      'label' => 'Background Color',
      'type' => 'colorpicker',
      'value' => 'rgba(255,95,98,0)'
    ),

    array('label'=>'Title', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),
    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),

    array('label'=>'Items', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'icon_color',
      'label' => 'Icon Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),

    array(
      'name' => 'item_title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),
    array(
      'name' => 'item_desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),


  ),

);
