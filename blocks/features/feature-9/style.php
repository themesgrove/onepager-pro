#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color'];?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: transparent;
	background-image: linear-gradient(#ff5f62, <?php echo $styles['overlay_color'];?>);
}


#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id;?> .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?>  .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px;
	color : <?php echo $styles['desc_color'];?>;
}

#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title,
#<?php echo $id;?> .uk-card .uk-card-body .uk-card-title a{
	font-size : <?php echo $settings['item_title_size'];?>px;
	color:<?php echo $styles['item_title_color'];?>;
}

#<?php echo $id;?> .uk-card .uk-card-body .uk-text-medium{
	font-size : <?php echo $settings['item_desc_size'];?>px;
	color:<?php echo $styles['item_desc_color'];?>;
}

<?php if ($styles['icon_color']): ?>	
	#<?php echo $id;?> .op-icon{
		color : <?php echo $styles['icon_color'];?>;
		border: 1px solid <?php echo $styles['icon_color'];?>;
	    border-radius: 3px;
	    padding: 10px 10px;
	}
<?php endif; ?>

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}