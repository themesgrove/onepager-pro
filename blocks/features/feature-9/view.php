<?php
	// animation repeat
	$animation_repeat = '';
	// Animation Media
	$animation_media = ($settings['animation_media']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_media'] .';repeat:' . ($animation_repeat ? 'true' : 'false') . '"' : '';
	// Animation Content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';

?>

<section id="<?php echo $id;?>"
	class="uk-background-contain uk-background-norepeat uk-position-relative feature pro-feature-9 uk-cover-container fp-section"   
	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>
	data-src="<?php echo $styles['bg_image'];?>" uk-img>

	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container-large uk-padding-large uk-position-relative">
			<div uk-grid>
			    <div class="uk-text-left uk-width-3-5@m">
			    	<div class="section-heading uk-margin-large-bottom">
			    		<?php if($contents['title']):?>
							<!-- Section Title -->
							<?php
								echo op_heading( 
									$contents['title'],
									$settings['heading_type'], 
									'uk-heading-primary', 
									'uk-text-' . $settings['title_transformation'], 
									$animation_content . '"'
								); 
							?>
						<?php endif; ?>

						<?php if($contents['description']):?>
							<!-- Section Sub Title -->
							<div class="uk-text-lead"
								<?php echo ($settings['animation_content'] ? $animation_content . ';repeat:'  .($animation_repeat ? 'true' : 'false') .';delay:' . '300"' : ''); ?>>
								<?php echo $contents['description'];?>
							</div>
						<?php endif; ?>
					</div>  <!-- section-heading -->

				    <?php $i=4; ?>
				    <div class="uk-child-width-1-2@m" uk-grid>
				    <?php foreach($contents['items'] as $feature): ?>
					    <div
					    <?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' . $i . '00"' : ''); ?>>
					       	<div class="uk-card" uk-grid>
								<div class=" uk-card-media-left uk-width-auto uk-margin-small-top">
									<?php if($feature['media']):?>
										<span class="op-icon <?php echo $feature['media']; ?>"></span>
									<?php endif;?>
								</div>
								<div class=" uk-width-expand uk-padding-small uk-padding-remove-top">
									<div class="uk-card-body uk-padding-remove">
										<h3 class="uk-card-title uk-margin-remove uk-text-<?php echo $settings['item_title_transformation'];?>">
											<?php if(trim($feature['link'])): ?>
												<a href="<?php echo $feature['link']; ?>" target="<?php echo $feature['target'] ? '_blank' : ''?>"><?php echo $feature['title'];?></a>
											<?php else: ?>
												<?php echo $feature['title'];?>
											<?php endif; ?>
										</h3>
										<p class="uk-text-medium  uk-margin-small"><?php echo $feature['description'];?></p>
									</div><!-- uk-card-body -->
								</div>
							</div>
					    </div>
					<?php $i++; endforeach; ?>
					</div>
			    </div> <!-- uk-width-35 -->
			    <div class="uk-width-1-5@m uk-grid-item-match uk-flex-middle">
			        <div class="uk-card uk-container-item-padding-remove-right"  <?php echo $animation_media;?>>
			        	<img class="op-max-width-none" src="<?php echo $contents['image'];?>" alt="<?php echo $contents['title'];?>" width="<?php echo $settings['media_size'];?>" uk-image>
			        </div> <!-- uk-card -->
			    </div> <!-- uk-width-1-5 -->
			</div> <!--uk-grid -->
		</div><!-- uk-container-large -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->



