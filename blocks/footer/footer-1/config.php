<?php

return array(

  'slug'      => 'pro-footer-1', // Must be unique and singular
  'groups'    => array('footers'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Top Button', 'type'=>'divider'), // Divider - Text
    array('name'=>'text', 'type' => 'text', 'value'=> 'Exclusive Bundle'),

    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'title',
      'value' => 'Download Onepager'
    ),
    array(
      'name'  =>'description',
      'type'  =>'textarea',
      'value' => 'Get all of Onepager Builder—FREE.'
    ),
    //array('name'=>'image', 'type' => 'image', 'value'=> 'http://s3.amazonaws.com/quantum-assets/icons/download.png'),

    array('label'=>'Subscribe Shortcode', 'type'=>'divider'), // Divider - Text

    array(
      'name'  => 'form',
      'label' => 'Form Shortcode',
      'type'  => 'textarea',
      'value' => ''
    ),

    array('name'=>'menu','type'=>'menu'),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(

    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),

    array('label'=>'Top Button', 'type'=>'divider'), // Divider - Text

    array(
      'name'   => 'button_top_font_size',
      'label'  => 'Font Size',
      'append' => 'px',
      'value'  => '18'
    ),

    array(
      'name' => 'button_top_borer_width',
      'label' => 'Border Width',
      'append' => 'px',
      'value' => '2'
    ),
    array(
      'name'     => 'button_top_transformation',
      'label'    => 'Text Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'     => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array('label'=>'Content', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(
        '0'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'content_alignment',
      'label'    => 'Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
      )
    ),

    array(
      'name'     => 'content_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    
    array('label'=>'Form', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'form_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '50'
    ),
    array(
      'name'     => 'form_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'form_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array('label'=>'Menu', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'menu_text_size',
      'label' => 'Text Size',
      'append' => 'px',
      'value' => '16'
    ),

    array(
      'name'     => 'menu_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'menu_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => 'http://try.getonepager.com/wp-content/uploads/2019/02/Bg14.jpg'
    ),

    array(
      'name'     => 'bg_image_size',
      'label'    => 'Size',
      'type'     => 'select',
      'value'    => 'uk-background-contain',
      'options'  => array(
        'uk-background-contain'   => 'Contain',
        'uk-background-cover'     => 'Cover',
      ),
    ),

    array(
      'name'    => 'bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value' => '#0d73ee'
    ),

    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(255,255,255,0)'
    ),

    array('label'=>'Top Button', 'type'=>'divider'), // Divider - Text

        array(
      'name'    => 'button_top_text_color',
      'label'   => 'Text Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'button_top_bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '@color.primary'
    ),

    array(
      'name'    => 'button_top_border_color',
      'label'   => 'Border Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),

    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name'  => 'desc_color',
      'label' => 'Desc Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),

    array('label'=>'Form', 'type'=>'divider'), // Divider - Text

    array(
        'name'    => 'submit_text_color',
        'label'   => 'Submit Text Color',
        'type'    => 'colorpicker',
        'value'   => '@color.primary'
      ),
      array(
        'name'    => 'submit_bg_color',
        'label'   => 'Submit Bg Color',
        'type'    => 'colorpicker',
        'value'   => '#fff'
      ),


    array('label'=>'Menu', 'type'=>'divider'), // Divider - Text

    array(
        'name'    => 'menu_text_color',
        'label'   => 'Text Color',
        'type'    => 'colorpicker',
        'value'   => '#fff'
      ),
      array(
        'name'    => 'menu_hover_color',
        'label'   => 'Hover Color',
        'type'    => 'colorpicker',
        'value'   => '#fff'
      ),
    ),
);
