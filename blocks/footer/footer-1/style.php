#<?php echo $id; ?> {
	background-color:<?php echo $styles['bg_color'];?>
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id; ?> .top-text{
	font-size : <?php echo $settings['button_top_font_size'];?>px;
	background: <?php echo $styles['button_top_bg_color'];?>;
	border:<?php echo $settings['button_top_borer_width'];?>px solid <?php echo $styles['button_top_text_color']?>;
	color : <?php echo $styles['button_top_text_color'];?>;
	text-transform:<?php echo $settings['button_top_transformation'];?>;
}

#<?php echo $id; ?> .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}
#<?php echo $id; ?> .uk-text-lead{
	color : <?php echo $styles['desc_color'];?>;
	font-size : <?php echo $settings['desc_size'];?>px;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

#<?php echo $id; ?> input[type="email"]{
	width: 65%;
    float: left;
    padding: 15px 20px;
    border-radius: <?php echo $settings['form_border_radius'];?>px;
    border:3px solid <?php echo $styles['submit_bg_color'];?>;
    margin-right: 10px;
    background:<?php echo $styles['submit_text_color'];?>;
    color:<?php echo $styles['submit_bg_color'];?>;
}

#<?php echo $id; ?> input[type="email"]:focus{
	outline:none;
}

#<?php echo $id; ?> ::placeholder {
  color:<?php echo $styles['submit_bg_color'];?>;
}

#<?php echo $id; ?> input[type="submit"]{
	padding: 15px 25px;
	color:<?php echo $styles['submit_text_color'];?>;
	background:<?php echo $styles['submit_bg_color'];?>;
    border-radius: <?php echo $settings['form_border_radius'];?>px;
    border:3px solid <?php echo $styles['submit_bg_color'];?>;
    font-size: 14px;
    margin:0;
    text-transform:<?php echo $settings['form_transformation'];?>;
    font-weight: 700;
}
#<?php echo $id; ?> input[type="submit"]:hover{
	color:<?php echo $styles['submit_bg_color'];?>;
	background:<?php echo $styles['submit_text_color'];?>;
	border-color:<?php echo $styles['submit_bg_color'];?>;
}

#<?php echo $id; ?> .op-footer-menu .uk-navbar-nav>li>a{
	color:<?php echo $styles['menu_text_color'];?>;
	font-size : <?php echo $settings['menu_text_size'];?>px;
	text-transform:<?php echo $settings['menu_transformation'];?>;
	min-height:0;
	display:inline;
}

#<?php echo $id; ?> .op-footer-menu .uk-navbar-nav>li>a:hover{
	color:<?php echo $styles['menu_hover_color'];?>;
}

#<?php echo $id; ?> .op-footer-menu .uk-navbar-nav>li:not(:last-child) a {
    border-right: 1px solid <?php echo $styles['menu_text_color'];?>;
}

@media(max-width:768px){
	#<?php echo $id?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}