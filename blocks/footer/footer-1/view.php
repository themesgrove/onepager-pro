<?php
	// animation repeat
	$animation_repeat = '';
	// Content Animation
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].'' : '';
	// Button Animation
	$form_animation = ($settings['form_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['form_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:400"' : '';
	// Menu Animation
	$menu_animation = ($settings['menu_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['menu_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:600"' : '';
	// Content Alignment
	$content_alignment = ($settings['content_alignment']) ? $settings['content_alignment'] : '';
?>

<section id="<?php echo $id;?>" class="uk-section-large uk-position-relative footer pro-footer-1 <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>

	<div class="uk-container">
		<div class="uk-grid-large uk-position-relative" uk-grid>
			<div class="uk-width-1-1">
				<div class="uk-text-<?php echo $content_alignment;?>">
					<?php if ($contents['text']): ?>
						<p class="top-text uk-inline-block uk-padding-small uk-text-bold" 
							<?php echo ($settings['content_animation'] ? $content_animation .';repeat:' . ($animation_repeat ? 'true' : 'false')  . ';delay:' . '100"' : ''); ?>> 			
							<?php echo $contents['text'];?>	
						</p>
					<?php endif; ?>

					<?php if($contents['title']): ?>
						<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary uk-margin-remove uk-text-bold', 
							'uk-text-' . $settings['title_transformation'], 
							$content_animation . '"'
						); 
					?>
					<?php endif; ?>

					<?php if($contents['description']): ?>
						<!-- Description -->
						<div class="uk-text-lead uk-margin-small" 
						<?php echo ($settings['content_animation'] ? $content_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '400"' : ''); ?>> 
						<?php echo $contents['description'];?></div>
					<?php endif; ?>

					<?php if ($contents['form']): ?>	
						<div class="<?php echo ($settings['content_alignment'] == 'center') ? 'uk-margin-auto' : '' ?> uk-margin-medium-top uk-width-1-2" <?php echo $form_animation;?>>
							<?php echo do_shortcode($contents['form']);?>
						</div>
					<?php endif; ?>
				</div> <!-- text-alignment -->
				
			</div> <!-- uk-grid-1 -->
		</div><!-- uk-grid-large -->
	</div><!-- uk-container -->
	<?php if ($contents['menu']): ?>
		<div class="uk-container" <?php echo $menu_animation;?>>
			<div>
				<div class="op-footer-menu uk-position-bottom-center uk-padding-small">
					<?php wp_nav_menu(array(
						'menu' =>$contents['menu'] ,
						'menu_class'=>'uk-navbar-nav uk-visible@m',
						'items_wrap' => '<ul id="%1$s" class="%2$s" uk-scrollspy-nav="closest: li; scroll: true; overflow:false; offset:80">%3$s</ul>',
						'container' =>false,
						//'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
    				//'walker'			=> new WP_Bootstrap_Navwalker()
					)) ?>
			
				</div> <!-- uk-position-bottom-center -->
			</div>
		</div> 
	<?php endif; ?>
</section> <!-- end-section -->
