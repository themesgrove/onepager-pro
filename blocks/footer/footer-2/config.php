<?php

return array(
	'slug'      => 'pro-footer-2', // Must be unique
  	'groups'    => array('footers'), // Blocks group for filter

  	'contents' => array(
  		array('name'=> 'logo', 'type'=>'image', 'value'=> 'http://s3.amazonaws.com/quantum-assets/logo-white.png'),
  		array('name'=> 'description', 'type'=>'textarea', 'value'=> 'Lorem ipsum dolor amet consectetur adipisicing elit sed eiusm tempor incididunt labore dolore magna aliqua enim.'),
  		array('name'=> 'social', 'label' => 'Social Links', 'value' => array('http://facebook.com/ThemeXpert', 'http://twitter.com/themexpert', 'http://linkedin.com/themexpert', 'http://behance.net/ThemeXpert', 'http://dribbble.com/themexpert') ),
  		array('name'=>'menu_service', 'label' => 'Service Menu','type'=>'menu'),
  		array('name'=>'menu_quick_link', 'label' => 'Quick Links', 'type'=>'menu'),

	  	array('label'=>'Subscribe Shortcode', 'type'=>'divider'), // Divider - Text
	    array(
	      'name'  => 'form',
	      'label' => 'Form Shortcode',
	      'type'  => 'textarea',
	      'value' => ''
	    ),

	    array('label'=>'Copyright', 'type'=>'divider'), // Divider - Text
	    array('name'=> 'copyright', 'type'=>'textarea', 'value'=> 'Copyright &copy; 2019 OnePager, All Rights Reserved'),
	    array('name'=>'copyright_menu', 'label' => 'Copyright Menu', 'type'=>'menu'),
	),

'settings' => array(
	
	array(
      'name' => 'text_size',
      'label' => 'Text Size',
      'append' => 'px',
      'value' => '16'
    ),
    array(
      'name'     => 'section_animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

	array('name'=>'animation_repeat', 'label'=>'Animation Repeat', 'type'=>'switch'),
		array(
	      'name'     => 'menu_transformation',
	      'label'    => 'Menu Transformation',
	      'type'     => 'select',
	      'value'    => 'inherit',
	      'options'  => array(
	        'inherit'   => 'Default',
	        'lowercase'   => 'Lowercase',
	        'uppercase'   => 'Uppercase',
	        'capitalize'  => 'Capitalized'
	      ),
	    ),

	),


	'styles' => array(
		array(
	      'name'    => 'bg_color',
	      'label'   => 'Background Color',
	      'type'    => 'colorpicker',
	      'value'   => '#303032'
	    ),

	    array(
	      'name'    => 'text_color',
	      'label'   => 'Color',
	      'type'    => 'colorpicker',
	      'value'   => '#fff'
	    ),

	   	array(
	      'name'    => 'text_hover_color',
	      'label'   => 'Hover Color',
	      'type'    => 'colorpicker',
	      'value'   => '@color.primary'
	    ),

 	array('label'=>'Copyright Colors', 'type'=>'divider'), // Divider - Text
	    array(
	      'name'    => 'copyright_bg_color',
	      'label'   => 'Background Color',
	      'type'    => 'colorpicker',
	      'value'   => '#32bce2'
	    ),

	    array(
	      'name'    => 'copyright_text_color',
	      'label'   => 'Color',
	      'type'    => 'colorpicker',
	      'value'   => '#fff'
	    ),
	),

);
