#<?php echo $id; ?>{
	background : <?php echo $styles['bg_color'];?>;
	color : <?php echo $styles['text_color'];?>;
	font-size: <?php echo $settings['text_size'];?>px;
}
#<?php echo $id; ?> .uk-text-lead{
	color : <?php echo $styles['text_color'];?>;	
}
#<?php echo $id; ?> .quick-link .uk-navbar-nav>li>a,
#<?php echo $id; ?> .service-menu .uk-navbar-nav>li>a,
#<?php echo $id; ?> .uk-navbar-nav>li>a{
	position:relative;
	color : <?php echo $styles['text_color'];?>;
	font-size: <?php echo $settings['text_size'];?>px;
	text-transform:<?php echo $settings['menu_transformation'];?>;

}
#<?php echo $id; ?> .quick-link .uk-navbar-nav>li>a:before,
#<?php echo $id; ?> .service-menu .uk-navbar-nav>li>a:before {
    position: absolute;
    font: normal normal normal 18px/1 FontAwesome;
    content: "\f105";
    top:3px;
    left: -20px;
    color : <?php echo $styles['text_color'];?>;
}

#<?php echo $id; ?> .quick-link .uk-navbar-nav>li>a,
#<?php echo $id; ?> .service-menu .uk-navbar-nav>li>a{
    text-transform:<?php echo $settings['menu_transformation'];?>;
    font-size: <?php echo $settings['text_size'];?>px;
    padding: 0;
    margin: 0 24px;
    line-height: 40px;
    display: inline;
}

#<?php echo $id; ?> #copyright{
	border-top:1px solid #454547;
    background:<?php echo $styles['copyright_bg_color'];?>;
	font-size: <?php echo $settings['text_size'];?>px;
    color:<?php echo $styles['copyright_text_color'];?>;  
}


#<?php echo $id; ?> .copyright-menu .uk-navbar-nav>li>a{
    text-transform:<?php echo $settings['menu_transformation'];?>;
    font-size: <?php echo $settings['text_size'];?>px;
    color:<?php echo $styles['copyright_text_color'];?>; 
    padding: 0;
    margin: 0 14px;
    min-height: 0;
}


#<?php echo $id; ?> .uk-navbar-nav>li>a:hover,
#<?php echo $id; ?> .social-links > a:hover,
#<?php echo $id; ?> #copyright a:hover{
	color : <?php echo $styles['text_hover_color'];?>;
}

#<?php echo $id; ?> .social-links a{
	color : <?php echo $styles['text_color'];?>;
    border: 1px solid <?php echo $styles['text_color'];?>;
    width: 45px;
    height: 45px;
    text-align: center;
    line-height: 45px;
    font-size: 18px;
 }

 #<?php echo $id; ?> .social-links a:hover{
 	border-color : <?php echo $styles['text_hover_color'];?>;
 }