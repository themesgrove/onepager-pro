<?php 
  // animation repeat
  $animation_repeat = $settings['animation_repeat'];
  // section animation
  $section_animation = ($settings['section_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['section_animation'].';' : '';

?>

<footer id="<?php echo $id; ?>" class="footer pro-footer-3">
    <div class="uk-section">
        <div class="uk-container">
            <div class="uk-child-width-expand uk-padding-small" uk-grid>
                <?php if ($contents['logo'] || $contents['description']): ?>
                    <div  <?php echo ($settings['section_animation'])? $section_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
                        <?php if ($contents['logo']): ?>
                             <a href="<?php echo site_url(); ?>">
                                <img src="<?php echo $contents['logo'];?>">
                            </a>
                        <?php endif; ?>
                        <?php if ($contents['description']): ?>
                            <p><?php echo $contents['description'];?></p>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if ($contents['menu_service']): ?>
                    <div class="uk-width-1-5@m" <?php echo ($settings['section_animation'])? $section_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
                        <h3 class="uk-text-lead"><?php echo esc_html('Services', 'onepager-pro');?></h3>
                        <nav class="service-menu" uk-navbar>
                            <div class="uk-navbar-left">
                              <!-- WP Nav -->
                              <?php wp_nav_menu( 
                                    array(
                                        'menu'       => $contents['menu_service'],
                                        'menu_class' => 'uk-navbar-nav uk-inline-block',
                                    ) 
                                  ) 
                              ?>
                            </div><!-- uk-navbar-center -->
                        </nav> <!-- uk-navbar -->
                    </div>
                <?php endif; ?>
                <?php if ($contents['menu_quick_link']): ?>
                    <div class="uk-width-1-5@m" <?php echo ($settings['section_animation'])? $section_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'400"' : '' ;?>>
                        <h3 class="uk-text-lead"><?php echo esc_html('Quick Link', 'onepager-pro');?></h3>
                        <nav class="quick-link" uk-navbar>
                            <div class="uk-navbar-left">
                              <!-- WP Nav -->
                              <?php wp_nav_menu( 
                                    array(
                                        'menu'       => $contents['menu_quick_link'],
                                        'menu_class' => 'uk-navbar-nav uk-inline-block',
                                    ) 
                                  ) 
                              ?>
                            </div><!-- uk-navbar-center -->
                        </nav> <!-- uk-navbar -->
                    </div>
                <?php endif; ?>
                <?php if ($contents['description']): ?>
                    <div <?php echo ($settings['section_animation'])? $section_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'700"' : '' ;?>>
                        <h3 class="uk-text-lead"><?php echo esc_html('Subscribe Us', 'onepager-pro');?></h3>
                        <?php if ($contents['description']): ?>
                             <p><?php echo $contents['description'];?></p>
                        <?php endif; ?>
                        <?php if ($contents['form']): ?>  
                            <div class="<?php echo ($settings['content_alignment'] == 'center') ? 'uk-margin-auto' : '' ?> uk-margin-medium-top">
                              <?php echo do_shortcode($contents['form']);?>
                            </div>
                        <?php endif; ?>
                        <div class="social-links uk-margin-medium-top">
                          <?php foreach ( $contents['social'] as $social ): ?>
                              <?php if ($social): ?>
                                  <a class="icon uk-border-circle" href="<?php echo $social; ?>" target="_blank"></a>
                              <?php endif; ?>
                          <?php endforeach; ?>
                        </div><!-- social-links -->
                    </div>
                <?php endif; ?>

            </div> <!-- uk-grid -->
          </div> <!-- uk-container -->
        </div> <!-- uk-section -->

        <div id="copyright">
            <div class="uk-section-small uk-padding-small">
                <div class="uk-container">
                    <div class="uk-flex-middle" uk-grid>
                        <div class="<?php echo ($contents['copyright_menu']) ? 'uk-width-1-2@m' : 'uk-width-1-1@m uk-text-center'; ?>">
                            <?php if ($contents['copyright']): ?>
                                <p class="copyright"><?php echo $contents['copyright']; ?></p>
                            <?php endif; ?>
                        </div>
                        <?php if ($contents['copyright_menu']): ?>
                            <div class="uk-width-1-2@m">
                                <nav class="copyright-menu" uk-navbar>
                                  <div class="uk-navbar-right">
                                    <!-- WP Nav -->
                                    <?php wp_nav_menu( 
                                          array(
                                              'menu'       => $contents['copyright_menu'],
                                              'menu_class' => 'uk-navbar-nav',
                                          ) 
                                        ) 
                                    ?>
                                  </div><!-- uk-navbar-center -->
                              </nav> <!-- uk-navbar -->
                          </div> <!-- uk-width-1-2 -->
                        <?php endif; ?>
                    </div> <!-- uk-grid -->
                </div> <!-- uk-container -->
            </div> <!-- uk-section -->
       </div> <!-- copyright -->
</footer> <!-- end-footer -->
