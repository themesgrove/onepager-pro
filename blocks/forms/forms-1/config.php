<?php

return array(

  'slug'      => 'pro-forms-1', // Must be unique and singular
  'groups'    => array('Forms'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(

    array(
      'name'  => 'form',
      'label' => 'Form Shortcode',
      'type'  => 'textarea',
      'value' => ''
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array('label'=>'Form', 'type'=>'divider'), // Divider - Text
     array(
      'name'     => 'form_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
      ),
    ),
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => ''
    ),
    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(255,255,255,0.01)'
    ),

    array('label'=>'Form', 'type'=>'divider'), // Divider - Text

    array(
      'name'  => 'heading_color',
      'label' => 'Heading Color',
      'type'  => 'colorpicker',
      'value' => '#d55342'
    ),
    
    array(
      'name'  => 'forms_color',
      'label' => 'Forms Color',
      'type'  => 'colorpicker',
      'value' => '#8ec63d'
    ),
  ),
);
