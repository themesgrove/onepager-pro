
#<?php echo $id;?> .give-form-wrap{
    display: flex;
    justify-content: center;
}
#<?php echo $id;?> form[id*=give-form] select{
    min-width: 190px;
    border: 1px solid <?php echo $styles['forms_color'];?>;
    background-color: #fdfdfd;
    font-size: 16px;
    height: 40px;
}
#<?php echo $id;?> form[id*=give-form] .give-donation-amount #give-amount-text{
	min-width: 190px;
    border: 1px solid <?php echo $styles['forms_color'];?>;
    font-size: 16px;
    height: 40px;
    line-height: 40px;
}
#<?php echo $id;?>  form[id*=give-form] .give-donation-amount #give-amount, 
#<?php echo $id;?>  form[id*=give-form] .give-donation-amount #give-amount-text{
    min-width: 190px;
    border: 1px solid <?php echo $styles['forms_color'];?>;
    font-size: 16px;
    height: 40px;
    line-height: 40px;
}
#<?php echo $id;?> form[id*=give-form] .give-donation-amount .give-currency-symbol{
    border: 1px solid <?php echo $styles['forms_color'];?>;
    font-size: 16px;
    height: 40px;
    background: <?php echo $styles['forms_color'];?>;
    color: #fff;
}
#<?php echo $id;?> .give-btn {
    background: <?php echo $styles['forms_color'];?>;
    border: 1px solid <?php echo $styles['forms_color'];?>;
    color: #fff;
    height: 40px;
    padding: 10px 20px;
}
#<?php echo $id;?> .give-form{
    display: flex;
    align-items: center;
    justify-content: flex-end;
}
#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color'];?>;
}
#<?php echo $id; ?> .give-donation-amount.form-row-wide {
    min-width: 245px;
    width: 245px;
}
#<?php echo $id; ?> form[id*=give-form] select{
    width: 245px;
    margin-right: 20px;
    border-radius: 0!important;
   -webkit-appearance: none;
}
#<?php echo $id; ?> [id*=give-form] .give-form-title{
    margin: 0 30px 0 0;
    color: <?php echo $styles['heading_color'];?>;
    font-size: 24px;
    padding: 5px 0;
}
#<?php echo $id; ?> section.pro-forms-1{
    border-bottom: 1px solid #ddd;
}