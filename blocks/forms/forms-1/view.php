<?php
	// Content Animation
	$form_animation = ($settings['form_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['form_animation'].';' : '';
?>

<section id="<?php echo $id;?>" class=" uk-position-relative uk-background-norepeat uk-background-cover forms pro-forms-1 " tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
		<div class="uk-container">
			<div class="uk-grid-large uk-position-relative uk-padding-small uk-grid-match uk-flex-middle" uk-grid>
				<!-- Link -->
				<div class="uk-width-1-1">
					<?php if ($contents['form']): ?>	
						<div class="uk-margin-medium-top op-form" <?php echo ( $settings['form_animation'] ? $form_animation . 'delay:300"' : '' ); ?>>
							<?php echo do_shortcode($contents['form']);?>
						</div>
					<?php endif; ?>
				</div> <!--uk-width-1-4@m -->
			</div><!-- uk-grid-large -->
		</div><!-- uk-container -->
</section> <!-- end-section -->

