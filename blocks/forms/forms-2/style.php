#<?php echo $id; ?> {
	background-color:<?php echo $styles['bg_color'];?>
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id; ?> .uk-heading-primary{
	font-size : <?php echo $settings['section_title_size'];?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}
#<?php echo $id; ?> .uk-text-lead{
	color : <?php echo $styles['desc_color'];?>;
	font-size : <?php echo $settings['desc_size'];?>px;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

#<?php echo $id; ?> .uk-inline{
	color : #fff;
}

#<?php echo $id; ?> .uk-inline i{
	margin-right: 8px;
	font-size: 16px;
}

#<?php echo $id; ?> input[type="email"]{
	width: 300px;
    float: left;
    height: 45px;
    border-radius: <?php echo $settings['form_border_radius'];?>px;
    border: 3px solid <?php echo $styles['submit_bg_color'];?>;
    margin-right: 8px;
    color:<?php echo $styles['submit_text_color'];?>;
    background:<?php echo $styles['submit_bg_color'];?>;
}

#<?php echo $id; ?> input[type="email"]:focus{
	outline:none;
}

#<?php echo $id; ?> ::placeholder {
  color: #ddd;
}

#<?php echo $id; ?> form.wpforms-form{
	display: flex;
    justify-content: center;
}

#<?php echo $id; ?> div.wpforms-container-full .wpforms-form input[type=submit], 
#<?php echo $id; ?> div.wpforms-container-full .wpforms-form button[type=submit], 
#<?php echo $id; ?> div.wpforms-container-full .wpforms-form .wpforms-page-button {
    background-color: <?php echo $styles['submit_bg_color'];?>;
    border: 2px solid <?php echo $styles['submit_bg_color'];?>;
    color: <?php echo $styles['submit_text_color'];?>;
    border-radius: 5px;
    height: 45px;
    text-transform: uppercase;
    padding: 0 25px;
    transition: all 0.5s ease;
    font-size: 15px;
}

#<?php echo $id; ?> div.wpforms-container-full .wpforms-form input[type=submit]:hover, 
#<?php echo $id; ?> div.wpforms-container-full .wpforms-form button[type=submit]:hover, 
#<?php echo $id; ?> div.wpforms-container-full .wpforms-form .wpforms-page-button:hover {
    background-color: transparent;
    border: 2px solid #fff;
    color: #fff;
    transition: all 0.5s ease;
}

@media(max-width:768px){
	#<?php echo $id?> .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}