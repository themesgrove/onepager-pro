<?php

	// Content Animation
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].'' : '';
	// Button Animation
	$form_animation = ($settings['form_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['form_animation'] . ';delay:400"' : '';
	// title animation
	$title_animation = ( $settings['title_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['title_animation'] . ';"' : '';
	$items_animation = ( $settings['items_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['items_animation'] . ';"' : '';

?>

<section id="<?php echo $id;?>" class="uk-section-large uk-position-relative forms pro-forms-1 <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>

	<div class="uk-container">
		<div class="uk-grid-large uk-position-relative" uk-grid>
			<div class="uk-width-1-1">
				<div class="uk-text-center">

					<?php if ($contents['image']): ?>
						<img class="uk-margin-auto uk-margin-medium-bottom" src="<?php echo $contents['image'];?>">
					<?php endif; ?>

					<?php if ( $contents['title'] ) : ?>
					<!-- Section Title -->
					  	<?php 
							echo op_heading(
								$contents['title'],
								$settings['heading_type'],
								'uk-heading-primary uk-text-bold uk-text-'.$settings['title_transformation'],
								$title_animation
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']): ?>
						<!-- Description -->
						<div class="uk-text-lead" 
						<?php echo ($settings['content_animation'] ? $content_animation . '400"' : ''); ?>> 
						<?php echo $contents['description'];?></div>
					<?php endif; ?>

					<?php if ($contents['form']): ?>	
						<div class="<?php echo ($settings['content_alignment'] == 'center') ? 'uk-margin-auto' : '' ?> uk-margin-medium-top uk-width-1-2" <?php echo $form_animation;?>>
							<?php echo do_shortcode($contents['form']);?>
						</div>
					<?php endif; ?>

					<ul class="uk-list">
						<li class="uk-inline uk-margin-small-right"><?php echo $contents['list_title']; ?></li>
						<?php $i=4; ?>
						<?php foreach($contents['items'] as $feature): ?>
							<?php if ($feature['link']): ?>
								<a href="<?php echo $feature['link']['text'];?>" target="_blank" <?php echo ($settings['items_animation'] ? $items_animation . '400"' : ''); ?>>
									<li class="uk-inline uk-margin-remove-top uk-margin-small-right"><?php if($feature['icon']) : ?><i class="op-icon <?php echo $feature['icon']; ?>"></i><?php endif; ?><?php echo $feature['link']['text'];?></li>
								 </a>
							<?php endif; ?>									    	
						<?php $i++; endforeach; ?>
					</ul>

				</div> <!-- text-alignment -->
				
			</div> <!-- uk-grid-1 -->
		</div><!-- uk-grid-large -->
	</div><!-- uk-container -->

</section> <!-- end-section -->
