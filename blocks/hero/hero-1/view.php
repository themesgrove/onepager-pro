<?php
	// animation repeat
	$animation_repeat = '';
	// Content Animation
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].'' : '';
	// Media Animation
	$media_animation = ($settings['media_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['media_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:300"' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:500"' : '';
	// Content Alignment
	$content_alignment = ($settings['content_alignment']) ? $settings['content_alignment'] : '';
?>


<section id="<?php echo $id;?>" class="fp-section uk-position-relative hero pro-hero-1 uk-padding <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" 	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section-large">
		<div class="uk-container-small uk-margin-auto">
			<div class="uk-grid-large uk-position-relative" uk-grid>
				<div class="uk-width-1-1">
					<div class="uk-text-<?php echo $content_alignment;?>">

						<?php if($contents['title']): ?>
							<!-- Title -->
							<?php
								echo op_heading( 
									$contents['title'],
									$settings['heading_type'], 
									'uk-heading-primary', 
									'uk-text-' . $settings['title_transformation'], 
									$content_animation . '"'
								); 
							?>
						<?php endif; ?>

						<?php if($contents['description']): ?>
							<!-- Description -->
							<div class="uk-text-lead" 
								<?php echo ($settings['content_animation'] ? $content_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>> 
								<?php echo $contents['description'];?></div>
						<?php endif; ?>
						<?php if($contents['image']) :?>
							<!-- Image -->
							<img 
								class="uk-margin-small"
								width="<?php echo $settings['media_size']?>" 
								src="<?php echo $contents['image']?>" 
								alt="<?php echo $contents['title']?>" <?php echo $media_animation;?> uk-image>
						<?php endif; ?>

						<p class="uk-margin-medium-top" <?php echo $button_animation;?>>
							<!-- Link 1 -->
							<?php echo op_link($contents['link_bottom_1'], 'button-1 uk-margin-bottom uk-text-bold uk-button uk-button-primary uk-button-large uk-margin-small-right');?>
							<!-- Link 2-->
							<?php echo op_link($contents['link_bottom_2'], 'button-2 uk-margin-bottom uk-text-bold uk-button uk-button-primary uk-button-large uk-margin-small-left');?>
						</p>
					</div> <!-- text-alignment -->
				</div> <!-- uk-grid-1 -->
			</div><!-- uk-grid-large -->
		</div><!-- uk-container -->
	</div> <!-- uk-seciton-large -->
</section> <!-- end-section -->
