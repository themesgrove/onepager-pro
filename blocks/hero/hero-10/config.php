<?php

return array(

  'slug'      => 'pro-hero-10', // Must be unique and singular
  'groups'    => array('hero'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(

    array('label'=>'Items', 'type'=>'divider'), // Divider - Text

    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
         array('name'=>'video_link', 'label' => 'Video Link', 'value'=>'https://www.youtube.com/watch?v=gJKIwp21dXs'),
          array(
            'name'    => 'video_bg_img',
            'label'   => 'Video Background Image',
            'type'    => 'image',
            'value'   => 'https://demo.wponepager.com/wp-content/uploads/2019/06/carity-video-bg.jpg'
          ),
          array('name'=>'title', 'value' => 'Join Us <br>We Need Your Help'),
          array('name'=>'description', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunt oporteat cu, laboramus definiebas cum et. Duo id omnis persequeris neglegentur, facete commodo ea usu, possit lucilius sed ei. Esse efficiendi scripserit eos ex. Sea utamur iisque salutatus id.'),

          array('name'=>'button', 'placeholder'=> home_url()),
          array('name'=>'button_link', 'text' => 'Join us', 'label' => 'Type Button Text', 'type' => 'link', 'placeholder'=> home_url()),
        ),

        array(
         array('name'=>'video_link', 'label' => 'Video Link', 'value'=>'https://www.youtube.com/watch?v=gJKIwp21dXs'),
          array(
            'name'    => 'video_bg_img',
            'label'   => 'Video Background Image',
            'type'    => 'image',
            'value'   => 'https://demo.wponepager.com/wp-content/uploads/2019/06/carity-video-bg.jpg'
          ),
          array('name'=>'title', 'value' => 'We Invite <br>You to Join our Journey'),
          array('name'=>'description', 'type'=>'textarea','value' => 'Lorem ipsum dolor sit amet, sed dicunt oporteat cu, laboramus definiebas cum et. Duo id omnis persequeris neglegentur, facete commodo ea usu, possit lucilius sed ei. Esse efficiendi scripserit eos ex. Sea utamur iisque salutatus id.'),

          array('name'=>'button', 'placeholder'=> home_url()),
          array('name'=>'button_link', 'text' => 'Join us', 'label' => 'Type Button Text', 'type' => 'link', 'placeholder'=> home_url()),
        ),


      )

    )
  ),


  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(
        '0'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name'     => 'title_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array( 'label' => 'Description', 'type' => 'divider'),
    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),
    array(
      'name'     => 'content_alignment',
      'label'    => 'Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),
    array(
      'name'     => 'content_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
      ),
    ),
    array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'button_font_size',
      'label' => 'Font Size',
      'append' => 'px',
      'value' => '14'
    ),
    array(
      'name'     => 'button_transformation',
      'label'    => 'Text Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name'     => 'button_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
      ),
    ),

  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => 'https://demo.wponepager.com/wp-content/uploads/2019/05/hero-bg-10.jpg'
    ),
    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(248,247,245,0)'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),
    array(
      'name'     => 'bg_image_size',
      'label'    => 'Size',
      'type'     => 'select',
      'value'    => 'uk-background-cover',
      'options'  => array(
        'uk-background-contain'   => 'Contain',
        'uk-background-cover'     => 'Cover',
      ),
    ),
    array(
      'name'    => 'bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value' => ''
    ),

    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),

    array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'button_text_color',
      'label'   => 'Text Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#8ec63d'
    ),
    array(
      'name'    => 'button_border_color',
      'label'   => 'Border Color',
      'type'    => 'colorpicker',
      'value'   => '#8ec63d'
    ),
    array(
      'name' => 'button_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '0'
    ),
  ),

);
