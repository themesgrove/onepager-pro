<?php
	// animation repeat
	$animation_repeat = '';
	// Title Animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// Content Animation
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].'' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:500"' : '';
	// Content Alignment
	$content_alignment = ($settings['content_alignment']) ? $settings['content_alignment'] : '';
?>


<section id="<?php echo $id;?>" class="fp-section uk-position-relative hero pro-hero-10 <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" 	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section-large">
		<div class="uk-container">
			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="center: true">

			    <ul class="uk-slider-items uk-grid uk-grid-match">
				<?php foreach($contents['items'] as $index => $item): ?>
			        <li class="uk-width-1-1">
						<div class="uk-grid-large uk-position-relative" uk-grid>

							<div class="uk-width-1-2 op-video">
								<div class="video-lightbox uk-position-relative" uk-lightbox>
								    <div class="uk-card" <?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>

										<div class="uk-height-large uk-flex uk-flex-middle uk-flex-center uk-background-cover uk-background-norepeat uk-background-center-center" style="background-image: url(<?php echo $item['video_bg_img'];?>);">
										    <div class="uk-text-center">
								            	<a class="uk-button" href="<?php echo $item['video_link'];?>"><i class="fa fa-play-circle"></i></a>
								            </div>
								        </div>
									</div> <!-- uk-card -->
								</div> <!-- uk-lightbox -->
							</div> <!-- uk-grid-1 -->
					
							<div class="uk-width-1-2 op-content uk-height-large">
								<div class="uk-text-<?php echo $content_alignment;?>">
									<?php if($item['title']): ?>
										<!-- Title -->
										<?php
											echo op_heading( 
												$item['title'],
												$settings['heading_type'], 
												'uk-heading-primary uk-margin-small-top', 
												'uk-text-' . $settings['title_transformation'], 
												$title_animation . '"'
											); 
										?>
									<?php endif; ?>
									<?php if($item['description']): ?>
										<!-- Description -->
										<p class="uk-text-lead uk-text-<?php echo $settings['title_transformation'];?>" 
											<?php echo ($settings['content_animation'] ? $content_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '100"' : ''); ?>> 
											<?php echo $item['description'];?></p>
									<?php endif; ?>
									<?php if ($item['button_link']): ?>
										<p class="uk-margin-medium-top" <?php echo $button_animation;?>>
											<!-- Link 1 -->
											<?php echo op_link($item['button_link'], 'uk-button uk-button-default uk-margin-small-right');?>
										</p>
									<?php endif; ?>
								</div> <!-- text-alignment -->
							</div> <!-- uk-grid-1 -->

						</div><!-- uk-grid-large -->
			        </li>
				<?php endforeach;?>

			    </ul>
			    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin-medium-top"></ul>
			</div>

		</div><!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
