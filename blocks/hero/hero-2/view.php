<?php
	// animation repeat
	$animation_repeat = '';
	// Content Animation
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].'' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation']. ';repeat:' .($animation_repeat ? 'true' : 'false') . ';delay:500"' : '';
	// Content Alignment
	$content_alignment = ($settings['content_alignment']) ? $settings['content_alignment'] : '';
?>
<section id="<?php echo $id;?>" class="uk-section-large uk-overflow-hidden uk-position-relative hero pro-hero-2 uk-padding">
  	<video autoplay loop muted playslinline uk-cover>
	    <source src="<?php echo $contents['video_link'];?>" type="video/mp4">
	</video>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-container-small uk-margin-auto">
		<div class="uk-grid-large uk-position-relative" uk-grid>
			<div class="uk-width-1-1">
				<div class="section-heading uk-text-<?php echo $content_alignment;?>">

					<?php if($contents['top_title']): ?>
						<!-- Top Title -->
						<h4 class="uk-text-small uk-margin uk-text-<?php echo $settings['title_transformation'];?>" 
							<?php echo ($settings['content_animation'] ? $content_animation . ';repeat:' .($animation_repeat ? 'true' : 'false') . ';delay:' . '100"' : ''); ?>> 
							<?php echo $contents['top_title'];?>	
						</h4>
					<?php endif; ?>

					<?php if($contents['title']): ?>
						<!-- Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-remove-top uk-text-bold', 
								'uk-text-' . $settings['title_transformation'], 
								$content_animation . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']): ?>
						<!-- Description -->
						<div class="uk-text-lead uk-margin-medium" 
							<?php echo ($settings['content_animation'] ? $content_animation . ';repeat:' .($animation_repeat ? 'true' : 'false') . 'delay:' . '400"' : ''); ?>> 
							<?php echo $contents['description'];?></div>
					<?php endif; ?>
					<?php if ($contents['link']['text']): ?>
						<p class="uk-margin-medium-top" <?php echo $button_animation;?>>
							<!-- Link 1 -->
							<?php echo op_link($contents['link'], 'uk-button uk-button-primary uk-button-large uk-margin-small-right');?>
						</p>
					<?php endif; ?>
				</div> <!-- section-heading -->
			</div> <!-- uk-grid-1 -->
		</div><!-- uk-grid-large -->
	</div><!-- uk-container -->
</section> <!-- end-section -->
