<?php if ($styles['bg_color']): ?>
	#<?php echo $id; ?>{
		background-color : <?php echo $styles['bg_color'];?>;
	}
<?php endif; ?>	

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id; ?> .uk-text-lead{
	color : <?php echo $styles['title_color']?>;
	font-size : <?php echo $settings['top_title_size'];?>px;
}
#<?php echo $id; ?> .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
	font-weight:400;
}

#<?php echo $id; ?> .uk-button-default{
	font-size : <?php echo $settings['button_font_size'];?>px;
	background: <?php echo $styles['button_bg_color'];?>;
	border: 1px solid <?php echo $styles['button_border_color'];?>;
	color : <?php echo $styles['button_text_color'];?>;
	text-transform:<?php echo $settings['button_transformation'];?>;
	border-radius:<?php echo $styles['button_border_radius'];?>px;
}
#<?php echo $id; ?> .uk-button-default:hover{
	background : <?php echo $styles['button_text_color'];?>;
	color : <?php echo $styles['button_bg_color'];?>;
	border-color : <?php echo $styles['button_text_color'];?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

@media(max-width:768px){
	#<?php echo $id?>  .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .uk-text-lead {
		font-size : <?php echo ($settings['top_title_size']/2)+7?>px;
	}
}