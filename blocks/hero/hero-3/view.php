<?php
	// animation repeat
	$animation_repeat = '';
	// Content Animation
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].'' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:500"' : '';
	// Content Alignment
	$content_alignment = ($settings['content_alignment']) ? $settings['content_alignment'] : '';
?>


<section uk-height-viewport="offset-bottom:15" id="<?php echo $id;?>" class="uk-flex uk-flex-center uk-flex-middle uk-section-large uk-position-relative hero pro-hero-3  <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" 	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>

	<div class="uk-container-small uk-margin-auto">
		<div class="uk-grid-large uk-position-relative">
			<div class="uk-width-1-1@m uk-width-1-1@s uk-width-l-1@l">
				<div class="uk-text-<?php echo $content_alignment;?>">
					<?php if($contents['top_title']): ?>
						<!-- Description -->
						<h3 class="uk-text-lead uk-margin-remove uk-text-<?php echo $settings['title_transformation'];?>" 
							<?php echo ($settings['content_animation'] ? $content_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '100"' : ''); ?>> 
							<?php echo $contents['top_title'];?></h3>
					<?php endif; ?>
					<?php if($contents['title']): ?>
						<!-- Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-small-top', 
								'uk-text-' . $settings['title_transformation'], 
								$content_animation . '"'
							); 
						?>
					<?php endif; ?>
					<?php if ($contents['link_bottom']): ?>
						<p class="uk-margin-medium-top" <?php echo $button_animation;?>>
							<!-- Link 1 -->
							<?php echo op_link($contents['link_bottom'], 'uk-button uk-button-default uk-button-large uk-margin-small-right');?>
						</p>
					<?php endif; ?>
				</div> <!-- text-alignment -->
			</div> <!-- uk-grid-1 -->
		</div><!-- uk-grid-large -->
	</div><!-- uk-container -->
</section> <!-- end-section -->
