#<?php echo $id;?> .uk-background-secondary{
	background-color : <?php echo $styles['bg_color']?>;
}


#<?php echo $id;?>  .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +40 ?>px;
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
	line-height : <?php echo ($settings['desc_size']) +10 ?>px;
	font-family: <?php echo Onepager::fonts($settings['desc_font']); ?>;
	font-weight:<?php echo $settings['desc_font_weight'];?>;
}



#<?php echo $id;?> .hero-button{
	margin: -20px 72px;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']?>;
}

#<?php echo $id; ?> .uk-button-default{
	font-size : <?php echo $settings['button_font_size'];?>px;
	background: <?php echo $styles['button_bg_color'];?>;
	border: 1px solid <?php echo $styles['button_bg_color'];?>;
	color : <?php echo $styles['button_text_color'];?>;
	text-transform:<?php echo $settings['button_transformation'];?>;
	border-radius:<?php echo $styles['button_border_radius'];?>px;
	letter-spacing: 2px;
}
#<?php echo $id; ?> .uk-button-default:hover{
	background : <?php echo $styles['button_text_color'];?>;
	color : <?php echo $styles['button_bg_color'];?>;
	border-color : <?php echo $styles['button_bg_color'];?>;
}

@media(max-width:768px){
	#<?php echo $id?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +40 ?>px;
	}

	#<?php echo $id;?> .section-heading .uk-text-lead{
		font-size : <?php echo ($settings['desc_size']/2)+3;?>px;
		line-height : <?php echo ($settings['desc_size']/2) +10 ?>px;
	}
	#<?php echo $id;?> .hero-button {
	    margin: -20px 30px;
	}
	#<?php echo $id; ?> .uk-button-default{
		font-size :14px;
	}
}