<?php
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';': '';
?>


<section id="<?php echo $id;?>" uk-height-viewport="offset-bottom:15" class="uk-position-relative hero pro-hero-5 uk-background-cover uk-background-norepeat"  <?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container uk-position-relative uk-padding">
			<div class="uk-grid-match uk-flex-middle" uk-grid>
				<div class="uk-width-1-2@m ">
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<h1 class="uk-heading-primary uk-margin-remove uk-text-<?php echo $settings['title_transformation'];?>" <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
							<?php echo $contents['title'];?>
						</h1>
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-margin-remove', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>
				</div> <!-- uk-width-1-2 -->

				<div class=" uk-width-1-2@m uk-width-1-1@s">
					<article class="uk-article uk-position-relative uk-margin-auto uk-padding-large uk-background-secondary">
						<div class="section-heading uk-margin-medium-bottom">	
							<?php if($contents['description']):?>
								<!-- Section Sub Title -->
								<h3 class="uk-text-lead uk-margin-medium" <?php echo ($settings['button_animation'])? $button_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
									<?php echo $contents['description'];?>
								</h3>
							<?php endif; ?>
							<?php if ($contents['link']): ?>
								<p class="uk-margin-medium-top uk-position-medium hero-button uk-position-bottom-left" <?php echo ($settings['button_animation'])? $button_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : '' ;?>>
									<!-- Link 1 -->
									<?php echo op_link($contents['link'], 'uk-button uk-button-default uk-button-large uk-margin-small-right');?>
								</p>
							<?php endif; ?>
						</div> <!-- section-heading -->
					</article> <!-- uk-article -->
				</div> <!-- uk-width -->
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- fp-section -->
