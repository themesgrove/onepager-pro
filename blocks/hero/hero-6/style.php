#<?php echo $id;?>{
	background-color : <?php echo $styles['bg_color']?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id;?> .top-text{
	font-size : <?php echo $settings['button_size']?>px;
	color : <?php echo $styles['button_color']?>;
	background-color:<?php echo $styles['buton_bg_color']?>;
	border:<?php echo $settings['item_borer_width'];?>px solid <?php echo $styles['button_color']?>;
	text-transform:<?php echo $settings['button_transformation'];?>;
	letter-spacing: 1px;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']?>px;
	color : <?php echo $styles['desc_color']?>;
}


<?php if ($styles['icon_color']): ?>
	
	#<?php echo $id;?> .op-media{
		color : <?php echo $styles['icon_color']?>;
	}
<?php endif; ?>


#<?php echo $id;?> .item-title,
#<?php echo $id;?> .item-title a{
	font-size : <?php echo $settings['item_title_size']?>px;
	color : <?php echo $styles['item_title_color']?>;
	letter-spacing: 0.8px;
}

#<?php echo $id;?> .intro-item .op-media{
	width: 100px;
}
#<?php echo $id;?> .intro-item .op-media{
	border:<?php echo $settings['item_borer_width'];?>px solid #fff;
}

#<?php echo $id;?> .uk-panel a:hover{
	text-decoration:none;
}

#<?php echo $id;?> .uk-panel a:hover .item-title {
	color:<?php echo $styles['icon_hover_color']?>;
}

@media(max-width:768px){
	#<?php echo $id?>  .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}