<?php
	// animation repeat
	$animation_repeat = '';
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// button animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') . '"' : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// items alignment
	$items_alignment = ($settings['items_alignment']) ? $settings['items_alignment'] : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].'' : '';
?>


<section id="<?php echo $id;?>" class="fp-section hero pro-hero-6">
	<div class="uk-section">
		<div class="uk-container">
			<article class="uk-article">
				<div class="section-heading uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>">
					<?php if ($contents['text']): ?>
						<p class="top-text uk-inline-block uk-margin-medium uk-padding-small uk-text-bold" <?php echo $button_animation;?>>		
							<?php echo $contents['text'];?>	
						</p>
					<?php endif; ?>
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary uk-text-bold uk-margin-remove uk-margin-medium-bottom', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<p class="uk-text-lead uk-width-3-4@m uk-width-1-1@s uk-margin-auto"
							<?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>
							<?php echo $contents['description'];?>
						</p>
					<?php endif; ?>
				</div>

				<div class="uk-grid-medium uk-panel uk-margin-xlarge-bottom" uk-grid >
					<?php $i=4; ?>
					<?php foreach($contents['items'] as $intro): ?>
						<div class="uk-width-1-<?php echo $settings['items_columns'];?>@m">
							<a target="<?php echo $intro['target'] ? '_blank' : ''?>" href="<?php echo $intro['link']; ?>">
								<div class="intro-item uk-text-<?php echo $items_alignment;?>" <?php echo ($settings['items_animation'] ? $items_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . $i . '00"' : ''); ?>>
									<?php if ($intro['media']): ?>
										<!-- Item image -->
										<?php if( op_is_image($intro['media'])):?>
											<img class="op-media uk-padding-small" src="<?php echo $intro['media']; ?>" alt="<?php echo $intro['title'];?>" />
										<?php else :?>
											<span class="op-media uk-padding-small <?php echo $intro['media']; ?>"></span>
										<?php endif;?>
									<?php endif; ?>

									<!-- Item title -->
									<h3 class="item-title uk-margin uk-text-bold uk-text-<?php echo $settings['item_title_transformation'];?>">
											<?php echo $intro['title'];?>
									</h3>
								</div><!-- intro-item -->
							</a>
						</div><!-- uk-columns -->
					<?php $i++; endforeach; ?>
				</div> <!-- uk grid medium -->
			</article> <!-- uk-article -->
		</div> <!-- uk-container -->
	</div><!-- uk-section -->
</section> <!-- op-section -->
