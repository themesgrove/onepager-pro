<?php

return array(

	'slug'      => 'pro-hero-7', // Must be unique and singular
	'groups'    => array('hero'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
	'contents' => array(
		array(
			'name' => 'title',
			'value' => 'October 6 – 14, 2019',
		),
		array('name' => 'logo', 'type' => 'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/05/hajj-loog.png'),
	),

	// Settings - $settings available on view file to access the option
	'settings' => array(
		array(
			'name'     => 'heading_type',
			'label'    => 'Heading Type',
			'type'     => 'select',
			'value'    => 'h1',
			'options'  => array(
			  'h1'   => 'h1',
			  'h2'   => 'h2',
			  'h3'   => 'h3',
			  'h4'   => 'h4',
			  'h5'   => 'h5',
			  'h6'   => 'h6',
			),
		  ),

		array('label' => 'Content', 'type' => 'divider'), // Divider - Text
		array(
			'name'     => 'content_alignment',
			'label'    => 'Alignment',
			'type'     => 'select',
			'value'    => 'center',
			'options'  => array(
				'left'      => 'Left',
				'center'    => 'Center',
				'right'     => 'Right',
				'justify'   => 'Justify',
			),
		),
		array(
			'name'     => 'content_animation',
			'label'    => 'Animation ',
			'type'     => 'select',
			'value'    => '0',
			'options'  => array(
				'0'                     => 'None',
				'fade'                  => 'Fade',
				'scale-up'              => 'Scale Up',
				'scale-down'            => 'Scale Down',
				'slide-top-small'       => 'Slide Top Small',
				'slide-bottom-small'    => 'Slide Bottom Small',
				'slide-left-small'      => 'Slide Left Small',
				'slide-right-small'     => 'Slide Right Small',
				'slide-top-medium'      => 'Slide Top Medium',
				'slide-bottom-medium'   => 'Slide Bottom Medium',
				'slide-left-medium'     => 'Slide Left Medium',
				'slide-right-medium'    => 'Slide Right Medium',
				'slide-top'             => 'Slide Top 100%',
				'slide-bottom'          => 'Slide Bottom 100%',
				'slide-left'            => 'Slide Left 100%',
				'slide-right'           => 'Slide Right 100%',

			),
		),
		array(
			'name' => 'title_size',
			'label' => 'Title Size',
			'append' => 'px',
			'value' => '@section_title_size',
		),
	  	array(
	      'name' => 'title_font',
	      'type' => 'font', 
	      'label' => 'Title Fonts'
	    ),
	    array(
	      'name'     => 'title_font_weight',
	      'label'    => 'Font Weight',
	      'type'     => 'select',
	      'value'    => '700',
	      'options'  => array(
	        '400'  => '400',
	        '500'   => '500',
	        '600'   => '600',
	        '700'   => '700',
	      ),
	    ),
		array(
			'name'     => 'title_transformation',
			'label'    => 'Title Transformation',
			'type'     => 'select',
			'value'    => '0',
			'options'  => array(
				'0'   => 'Default',
				'lowercase'   => 'Lowercase',
				'uppercase'   => 'Uppercase',
				'capitalize'  => 'Capitalized',
			),
		),

	),

	// Fields - $styles available on view file to access the option
	'styles' => array(
		array(
			'name'    => 'bg_image',
			'label'   => 'Background',
			'type'    => 'image',
			'value'   => 'http://try.getonepager.com/wp-content/uploads/2019/05/hajj-hero.jpg',
		),

		array(
			'name'    => 'overlay_color',
			'label'   => 'Overlay Color',
			'type'    => 'colorpicker',
			'value' => 'rgba(188,0,0,0)',
		),

		array(
			'name'  => 'title_color',
			'label' => 'Title Color',
			'type'  => 'colorpicker',
			'value' => '#fff',
		),

		array(
			'name'  => 'shape_color',
			'label' => 'Shape',
			'type'  => 'colorpicker',
			'value' => '#fff',
		),

	),
);
