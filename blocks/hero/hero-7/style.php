#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
    font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
    font-weight:<?php echo $settings['title_font_weight'];?>;
}
#<?php echo $id; ?> .uk-heading-primary{
	font-size : <?php echo $settings['title_size']; ?>px;
	color : <?php echo $styles['title_color']; ?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color']; ?>;
}

#<?php echo $id; ?> #op-shape path {
    fill: <?php echo $styles['shape_color']; ?>;
}
#<?php echo $id; ?> svg#op-shape{
    max-width: 100%;
    width: 100%;
    height: 120px;
    left: 0;
    right:0;
    bottom: 0;
    position: absolute;
    margin: 0 auto;
    text-align: center;
    display: block;
    z-index: 1;
}
@media(max-width:768px){
	#<?php echo $id; ?> .uk-heading-primary{
		font-size : <?php echo ( $settings['title_size'] / 1.5 ); ?>px;
	}
    #<?php echo $id; ?> svg#op-shape{
        height: 73px;
    }
}
@media(max-width:576px){
    #<?php echo $id; ?> svg#op-shape{
        height: 30px;
    }
}

