<?php
	// Content Animation
	$content_animation = ( $settings['content_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['content_animation'] . ';' : '';
	// Content Alignment
	$content_alignment = ( $settings['content_alignment'] ) ? $settings['content_alignment'] : '';
?>
<section id="<?php echo $id; ?>" class="fp-section uk-background-cover uk-position-relative hero pro-hero-7 uk-padding-large" tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>

	<svg id="op-shape" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
		<path d="M0 100 C40 0 60 0 100 100 Z"></path>
	</svg>

	<div class="uk-section">
		<div class="uk-overlay-primary uk-position-cover uk-padding-large"></div>
		<div class="uk-container">
			<div class="uk-grid-large uk-position-relative uk-padding-large" uk-grid>
				<div class="uk-width-1-1 uk-padding-large">
					<div class="uk-text-<?php echo $content_alignment; ?>">
						<div <?php echo ( $settings['content_animation'] ? $content_animation . 'delay:100"' : '' ); ?>>
							<!-- Image -->
							<?php if ( $contents['logo'] ) : ?>
								<img class="op-media" src="<?php echo $contents['logo']; ?>" alt="<?php echo $contents['title']; ?>">
							<?php endif; ?>
						</div>

						<!-- Title -->
						<?php if ( $contents['title'] ) : ?>
							<?php
								echo op_heading( 
									$contents['title'],
									$settings['heading_type'], 
									'uk-heading-primary uk-text-bold uk-margin-remove uk-margin-medium-bottom', 
									'uk-text-' . $settings['title_transformation'], 
									$content_animation . '"'
								); 
							?>
						<?php endif; ?>

					</div> <!-- text-alignment -->

				</div> <!-- uk-grid-1 -->
			</div><!-- uk-grid-large -->
		</div><!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->

