<?php
	
	// Button Animation
	$content_animation = ( $settings['content_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['content_animation'] . '"' : '';
	// Button Animation
	$image_animation = ( $settings['image_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['image_animation'] . '"' : '';
?>
<section id="<?php echo $id; ?>" class="fp-section hero pro-hero-8" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-container">
		<div class="uk-card uk-grid-collapse uk-child-width-1-2@s uk-margin uk-grid" uk-grid="">
		    <div class="uk-first-column uk-grid-item-match uk-flex-middle">
		        <div class="" <?php echo $content_animation; ?>>
					<!-- Title -->
					<?php if ( $contents['title'] ) : ?>
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$content_animation . '"'
							); 
						?>
					<?php endif; ?>
					<?php echo op_link( $contents['link'], 'uk-button uk-button-primary uk-button-large uk-margin-top' ); ?>
		        </div>
		    </div>
		    <div class="uk-flex-last@s uk-card-media-right uk-cover-container">
				<?php if ( $contents['image'] ) : ?>
					<img class="op-media uk-position-bottom-right" src="<?php echo $contents['image']; ?>" alt="<?php echo $contents['title']; ?>">
				<?php endif; ?>
		        <canvas width="600" height="700"></canvas>
		    </div>
		</div>
	</div><!-- uk-container -->
</section> <!-- end-section -->
