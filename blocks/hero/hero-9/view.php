<?php
	// animation repeat
	$animation_repeat = '';
	// Title Animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// Content Animation
	$content_animation = ($settings['content_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['content_animation'].'' : '';
	// Button Animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';repeat:' . ($animation_repeat ? 'true' : 'false') .';delay:500"' : '';
	// Content Alignment
	$content_alignment = ($settings['content_alignment']) ? $settings['content_alignment'] : '';
?>


<section id="<?php echo $id;?>" class="fp-section uk-position-relative hero pro-hero-9 <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" 	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?> tabindex="-1" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section-large">
		<div class="uk-container-small uk-margin-auto uk-padding">
			<div class="uk-grid-large uk-position-relative" uk-grid>
				<div class="uk-width-2-3@m op-content">
					<div class="uk-text-<?php echo $content_alignment;?>">
						<?php if($contents['title']): ?>
							<!-- Title -->
							<?php
								echo op_heading( 
									$contents['title'],
									$settings['heading_type'], 
									'uk-heading-primary uk-margin-small-top', 
									'uk-text-' . $settings['title_transformation'], 
									$title_animation . '"'
								); 
							?>
						<?php endif; ?>
						<?php if($contents['description']): ?>
							<!-- Description -->
							<p class="uk-text-lead uk-text-<?php echo $settings['title_transformation'];?>" 
								<?php echo ($settings['content_animation'] ? $content_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '100"' : ''); ?>> 
								<?php echo $contents['description'];?></p>
						<?php endif; ?>
						<?php if ($contents['link_bottom']): ?>
							<p class="uk-margin-medium-top" <?php echo $button_animation;?>>
								<!-- Link 1 -->
								<?php echo op_link($contents['link_bottom'], 'uk-button uk-button-default uk-margin-small-right');?>
							</p>
						<?php endif; ?>
					</div> <!-- text-alignment -->
				</div> <!-- uk-grid-1 -->
			</div><!-- uk-grid-large -->
		</div><!-- uk-container -->
	</div> <!-- uk-section -->
</section> <!-- end-section -->
