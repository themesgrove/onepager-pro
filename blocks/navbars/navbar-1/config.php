<?php

return array(

	'slug'      => 'pro-navbar-1', // Must be unique
	'groups'    => array('navbars'), // Blocks group for filter

  // Fields - $contents available on view file to access the option
	'contents' => array(
		array('name' => 'logo', 'type' => 'image', 'value' => 'https://demo.wponepager.com/wp-content/uploads/2019/05/charaty-logo.png'),
		array('name' => 'menu', 'type' => 'menu'),
	),

	// Settings
	'settings' => array(
		array(
			'name' => 'sticky_nav',
			'label' => 'Sticky Nav',
			'type' => 'select',
			'value' => 1,
			'options' => array(
				1 => 'Enabled',
				0 => 'Disabled',
			),
		),
	),

	// Styles - $styles available on view file to access the option
	'styles' => array(
		array(
			'name'    => 'bg_color',
			'label'   => 'Background Color',
			'type'    => 'colorpicker',
			'value'   => '#004cfb',
		),
		array(
			'name'  => 'link_color',
			'label' => 'Link Color',
			'type'  => 'colorpicker',
			'value' => '#fff',
		),
		array(
			'name'  => 'link_hover_color',
			'label' => 'Link Hover Color',
			'type'  => 'colorpicker',
			'value' => '#222',
		),
		array(
			'name'  => 'link_active_color',
			'label' => 'Link Active Color',
			'type'  => 'colorpicker',
			'value' => '#004cfb',
		),
	),
);
