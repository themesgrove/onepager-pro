#<?php echo $id; ?>{
	background-color : <?php echo $styles['bg_color']; ?>;
}

#<?php echo $id; ?> .uk-navbar-nav > li > a{
	color : <?php echo $styles['link_color']; ?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s eaase;
	transition: all 0.5s ease;
}

#<?php echo $id; ?> .uk-navbar-toggle svg{
	fill : <?php echo $styles['link_color']; ?>;
}
#<?php echo $id; ?> .uk-navbar-toggle{
	color : <?php echo $styles['link_color']; ?>;
}
#<?php echo $id; ?> .uk-navbar-nav > li:hover > a{
	color : <?php echo $styles['link_hover_color']; ?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s eaase;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-navbar-nav > li.uk-active > a{
	color : <?php echo $styles['link_active_color']; ?>;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s eaase;
	transition: all 0.5s ease;
}

@media only screen and (max-width: 400px) {
#<?php echo $id; ?> .uk-logo img{ width: 140px; }
}
