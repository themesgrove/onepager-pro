<?php
	$sticky = ( $settings['sticky_nav'] ) ? 'uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky"' : '';
?>
<div id="<?php echo $id; ?>" class="navbar pro-navbar-1 fp-section fp-auto-height" <?php echo $sticky; ?>>
	<div class="uk-container">
		<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
				<div class="uk-navbar-left">
						<?php if ( $contents['logo'] ) : ?>
							<a class="uk-navbar-item uk-logo" href="<?php echo site_url(); ?>"><img class="img-responsive" src="<?php echo $contents['logo']; ?>" alt="<?php wp_title(); ?>"/></a>
						<?php else : ?>
						<a class="uk-navbar-item uk-logo" href="<?php echo site_url(); ?>"><?php wp_title(); ?></a>
						<?php endif; ?>
				</div>
				<div class="uk-navbar-right">
					<?php
					wp_nav_menu(
						array(
							'menu' => $contents['menu'],
							'menu_class' => 'uk-navbar-nav uk-visible@m',
							'items_wrap' => '<ul id="%1$s" class="%2$s" uk-scrollspy-nav="closest: li; scroll: true; overflow:false; offset:80">%3$s</ul>',
							'container' => false,
							'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
							'walker'            => new WP_Bootstrap_Navwalker(),
						)
					)
					?>
				
					<a uk-navbar-toggle-icon="" href="#offcanvas-<?php echo $id; ?>" uk-toggle class="uk-navbar-toggle uk-hidden@m uk-navbar-toggle-icon uk-icon">
						<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="navbar-toggle-icon"><rect y="9" width="20" height="2"></rect><rect y="3" width="20" height="2"></rect><rect y="15" width="20" height="2"></rect></svg>
					</a>
				</div>

		        <div class="op-search">
		            <a class="uk-navbar-toggle" href="#" uk-search-icon></a>
		            <div class="uk-navbar-dropdown" uk-drop="mode: click; cls-drop: uk-navbar-dropdown; boundary: !nav">

		                <div class="uk-grid-small uk-flex-middle" uk-grid>
		                    <div class="uk-width-expand">
		                        <form class="uk-search uk-search-navbar uk-width-1-1" action="<?php echo home_url( '/' ); ?>" method="get">
		                            <input class="uk-search-input" type="search" placeholder="Search..." autofocus name="s" id="search" value="<?php the_search_query(); ?>">
		                        </form>
		                    </div>
		                    <div class="uk-width-auto">
		                        <a class="uk-navbar-dropdown-close" href="#" uk-close></a>
		                    </div>
		                </div>

		            </div>
		        </div>
		</nav>
	</div>
</div>


