<?php

return [
    'slug' => 'pro-portfolio-1', // Must be unique
    'groups' => ['portfolios'], // Blocks group for filter

    // Fields - $contents available on view file to access the option
    'contents' => [
        ['name' => 'title', 'value' => 'Premade Layouts'],
        [
            'name' => 'description',
            'type' => 'textarea',
            'value' => 'Pre-built websites with super easy 1 click installation.'
        ],
        ['label' => 'Portfolios', 'type' => 'divider'], // Divider - Text
        [
            'name' => 'portfolios',
            'type' => 'repeater',
            'fields' => [
                [
                    ['name' => 'filter', 'value' => 'Corporate'],
                    ['name' => 'tags', 'value' => 'corporate'],
                    ['name' => 'image', 'type' => 'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/portfolio-1.jpg'],
                    ['name' => 'title', 'value' => 'Corporate'],
                    ['name' => 'label', 'value' => ''],
                    ['name' => 'description', 'type' => 'textarea', 'value' => 'Onepage Corporate Theme'],
                    ['name' => 'link', 'placeholder' => home_url()],
                    ['name' => 'target', 'label' => 'open in new window', 'type' => 'switch'],
                ],
                [
                    ['name' => 'filter', 'value' => 'Business'],
                    ['name' => 'tags', 'value' => 'business'],
                    ['name' => 'image', 'type' => 'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/portfolio-2.jpg'],
                    ['name' => 'title', 'value' => 'Business'],
                    ['name' => 'label', 'value' => ''],
                    ['name' => 'description', 'type' => 'textarea', 'value' => 'Onepage Business Theme'],
                    ['name' => 'link', 'placeholder' => home_url()],
                    ['name' => 'target', 'label' => 'open in new window', 'type' => 'switch'],
                ],
                [
                    ['name' => 'filter', 'value' => 'Fashion'],
                    ['name' => 'tags', 'value' => 'fashion'],
                    ['name' => 'image', 'type' => 'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/portfolio-3.jpg'],
                    ['name' => 'title', 'value' => 'Fashion'],
                    ['name' => 'label', 'value' => ''],
                    ['name' => 'description', 'type' => 'textarea', 'value' => 'Onepage Fashion Theme'],
                    ['name' => 'link', 'placeholder' => home_url()],
                    ['name' => 'target', 'label' => 'open in new window', 'type' => 'switch'],
                ]
            ]
        ]
    ],

    // Settings - $settings available on view file to access the option
    'settings' => [
        array(
            'name'     => 'heading_type',
            'label'    => 'Heading Type',
            'type'     => 'select',
            'value'    => 'h1',
            'options'  => array(
              'h1'   => 'h1',
              'h2'   => 'h2',
              'h3'   => 'h3',
              'h4'   => 'h4',
              'h5'   => 'h5',
              'h6'   => 'h6',
            ),
          ),
          
        ['label' => 'Heading', 'type' => 'divider'], // Divider - Text
        [
            'name' => 'title_size',
            'label' => 'Title Size',
            'append' => 'px',
            'value' => '@section_title_size'
        ],

        array(
          'name' => 'title_font',
          'type' => 'font', 
          'label' => 'Title Fonts'
        ),
        array(
          'name'     => 'title_font_weight',
          'label'    => 'Font Weight',
          'type'     => 'select',
          'value'    => '500',
          'options'  => array(
            '400'  => '400',
            '500'   => '500',
            '600'   => '600',
            '700'   => '700',
          ),
        ),
        [
            'name' => 'title_transformation',
            'label' => 'Title Transformation',
            'type' => 'select',
            'value' => 'inherit',
            'options' => [
                'inherit' => 'Default',
                'lowercase' => 'Lowercase',
                'uppercase' => 'Uppercase',
                'capitalize' => 'Capitalized'
            ]
        ],

        [
            'name' => 'title_alignment',
            'label' => 'Title Alignment',
            'type' => 'select',
            'value' => 'center',
            'options' => [
                'left' => 'Left',
                'center' => 'Center',
                'right' => 'Right',
                'justify' => 'Justify',
            ]
        ],

        [
            'name' => 'desc_size',
            'label' => 'Desc Size',
            'append' => 'px',
            'value' => '18'
        ],

        [
            'name' => 'title_animation',
            'label' => 'Animation',
            'type' => 'select',
            'value' => '0',
            'options' => [
                '0' => 'None',
                'fade' => 'Fade',
                'scale-up' => 'Scale Up',
                'scale-down' => 'Scale Down',
                'slide-top-small' => 'Slide Top Small',
                'slide-bottom-small' => 'Slide Bottom Small',
                'slide-left-small' => 'Slide Left Small',
                'slide-right-small' => 'Slide Right Small',
                'slide-top-medium' => 'Slide Top Medium',
                'slide-bottom-medium' => 'Slide Bottom Medium',
                'slide-left-medium' => 'Slide Left Medium',
                'slide-right-medium' => 'Slide Right Medium',
                'slide-top' => 'Slide Top 100%',
                'slide-bottom' => 'Slide Bottom 100%',
                'slide-left' => 'Slide Left 100%',
                'slide-right' => 'Slide Right 100%'
            ],
        ],

        ['label' => 'Filters', 'type' => 'divider'], // Divider - Text
         ['name' => 'filter_enable', 'value' => 'yes', 'label' => 'Filter Enable', 'type' => 'switch'],

        ['label' => 'Portfolio Items', 'type' => 'divider'], // Divider - Text

        [
            'name' => 'items_columns',
            'label' => 'Columns',
            'type' => 'select',
            'value' => '3',
            'options' => [
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
            ],
        ],

        [
            'name' => 'items_animation',
            'label' => 'Animation',
            'type' => 'select',
            'value' => '0',
            'options' => [
                '0' => 'None',
                'fade' => 'Fade',
                'scale-up' => 'Scale Up',
                'scale-down' => 'Scale Down',
                'slide-top-small' => 'Slide Top Small',
                'slide-bottom-small' => 'Slide Bottom Small',
                'slide-left-small' => 'Slide Left Small',
                'slide-right-small' => 'Slide Right Small',
                'slide-top-medium' => 'Slide Top Medium',
                'slide-bottom-medium' => 'Slide Bottom Medium',
                'slide-left-medium' => 'Slide Left Medium',
                'slide-right-medium' => 'Slide Right Medium',
                'slide-top' => 'Slide Top 100%',
                'slide-bottom' => 'Slide Bottom 100%',
                'slide-left' => 'Slide Left 100%',
                'slide-right' => 'Slide Right 100%'
            ],
        ],

        ['label' => 'Contents', 'type' => 'divider'], // Divider - Text
        [
            'name' => 'item_title_size',
            'label' => 'Title Size',
            'append' => 'px',
            'value' => '20'
        ],

        [
            'name' => 'item_title_transformation',
            'label' => 'Transformation',
            'type' => 'select',
            'value' => 'inherit',
            'options' => [
                'inherit' => 'Default',
                'lowercase' => 'Lowercase',
                'uppercase' => 'Uppercase',
                'capitalize' => 'Capitalized'
            ]
        ],
      [
            'name' => 'item_desc_size',
            'label' => 'Desc Size',
            'append' => 'px',
            'value' => '14'
        ],
    ],

    'styles' => [
        array(
      'name'  => 'bg_image',
      'label' => 'Background Image',
      'type'  => 'image',
      'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/portfolio-bg.png'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(253,253,253,0.07)'
    ),
    [
        'name' => 'bg_color',
        'label' => 'Background Color',
        'type' => 'colorpicker',
        'value' => 'rgba(0,0,0,0)'
    ],

        ['label' => 'Heading', 'type' => 'divider'], // Divider - Text
        [
            'name' => 'title_color',
            'label' => 'Title Color',
            'type' => 'colorpicker',
            'value' => '#323232'
        ],

        [
            'name' => 'desc_color',
            'label' => 'Desc Color',
            'type' => 'colorpicker',
            'value' => '#323232'
        ],

        ['label' => 'Filters', 'type' => 'divider'], // Divider - Text

        [
            'name' => 'button_color',
            'label' => 'Color',
            'type' => 'colorpicker',
            'value' => '#000000'
        ],
        [
            'name' => 'button_active',
            'label' => 'Active color',
            'type' => 'colorpicker',
            'value' => '@color.primary'
        ],

        ['label' => 'Portfolio Items', 'type' => 'divider'], // Divider - Text
        [
            'name' => 'content_color',
            'label' => 'Content Color',
            'type' => 'colorpicker',
            'value' => '#000000'
        ],

        [
            'name' => 'item_bg_color',
            'label' => 'Background Color',
            'type' => 'colorpicker',
            'value' => '#fff'
        ],

    ],

];
