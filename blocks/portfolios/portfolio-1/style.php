#<?php echo $id; ?>{
	background : <?php echo $styles['bg_color'];?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}
#<?php echo $id; ?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}

#<?php echo $id; ?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px;
	color : <?php echo $styles['desc_color'];?>;
}



#<?php echo $id; ?> .uk-card .uk-card-body .uk-text-medium,
#<?php echo $id; ?> .uk-card .uk-card-body a .uk-text-medium{
	font-size : <?php echo $settings['item_title_size'];?>px;
	text-transform: <?php echo $settings['item_title_transformation'];?>;
}

#<?php echo $id; ?> .uk-card .uk-card-body a:hover {
	text-decoration:none;
}

#<?php echo $id; ?> .uk-card .uk-card-body .uk-text-small{
	font-size : <?php echo $settings['item_desc_size'];?>px;
}
#<?php echo $id; ?> .uk-card-default{
	background : <?php echo $styles['item_bg_color'];?>;
}

#<?php echo $id; ?> .uk-text-medium,
#<?php echo $id; ?> a .uk-text-medium,
#<?php echo $id; ?> .uk-text-small{
	color : <?php echo $styles['content_color'];?>;
}

#<?php echo $id; ?> .uk-tab>*>a{
	color : <?php echo $styles['button_color'];?>;
}
#<?php echo $id; ?> .uk-tab::before{
	border-bottom: 1px solid <?php echo $styles['button_color'];?>;
    opacity: 0.1;
}

#<?php echo $id; ?> .uk-tab>.uk-active>a,
#<?php echo $id; ?> .uk-tab>*>a:hover{
	border-color : <?php echo $styles['button_active'];?>;
	color : <?php echo $styles['button_active'];?>;
	outline:none;
}

#<?php echo $id; ?> .uk-label{
	    margin: 0 5px 20px;
	    text-transform:capitalize;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color'];?>;
}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}