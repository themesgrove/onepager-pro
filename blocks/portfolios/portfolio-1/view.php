<?php
    // title alignment
    $title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
    // title animation
    $title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-' . $settings['title_animation'] . ';' : '';
    // Items animation
    $items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-' . $settings['items_animation'] . ';' : '';
    // portfolios items
	$items = $contents['portfolios'];
?>

<section id="<?php echo $id; ?>" 
	class="uk-background-cover uk-background-norepeat uk-position-relative portfolio pro-portfolio-1"   
	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>
	data-src="<?php echo $styles['bg_image'];?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container-expand">
			<div class="section-heading uk-padding uk-margin-large-bottom uk-position-relative uk-text-<?php echo $title_alignment;?>">	
				<?php if ($contents['title']):?>
					<!-- Section Title -->
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary', 
							'uk-text-' . $settings['title_transformation'], 
							$title_animation . '"'
						); 
					?>
				<?php endif; ?>

				<?php if ($contents['description']):?>
					<div class="uk-text-lead"  <?php echo ($settings['title_animation'])? $title_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'200"' : '';?>>
						<?php echo $contents['description']?></div>
				<?php endif; ?>
			</div> <!-- Section heading -->

			<div uk-filter="target: .js-filter">
				<?php if ($settings['filter_enable'] == 'yes'): ?>			
					 <ul class="uk-flex-center uk-margin-medium-bottom"      <?php echo ($settings['title_animation'])? $title_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '';?> uk-tab>
					 	<li class="uk-active" uk-filter-control><a href="#">All</a></li>
						<?php foreach ($contents['portfolios'] as $portfolio_tags) :?>
							<?php if ($portfolio_tags['filter']): ?>	
						        <li uk-filter-control="[data-tags*='<?php echo strtolower($portfolio_tags['filter']. '_'.$id) ;?>']">
						        	<a href="#"><?php echo $portfolio_tags['filter'];?></a>
						        </li>
							<?php endif; ?>
					    <?php endforeach; ?>
					</ul> <!-- filter tags -->
				<?php endif; ?>

			 	<ul class="js-filter uk-padding uk-text-center uk-child-width-1-<?php echo ($settings['items_columns']+2);?>@xl uk-child-width-1-<?php echo $settings['items_columns'];?>@m uk-child-width-1-1@s" uk-grid>
			 		<?php $i=5; ?>
			  		<?php foreach ($contents['portfolios'] as $portfolio) :?>
			  			<?php $tags = explode(',',$portfolio['tags']);?>
				        <li data-tags="<?php foreach($tags as $tag ): echo strtolower($tag.'_'.$id) .' '; endforeach;?>">
				            <div class="uk-card uk-card-default" <?php echo ($settings['items_animation'])? $items_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .$i .'00"' : '';?>>
					            <?php if ($portfolio['link']): ?>
					           		<a href="<?php echo $portfolio['link'] ?>" target="<?php echo $portfolio['target'] ? '_blank' : ''?>">
										<img src="<?php echo $portfolio['image']?>" alt="<?php echo $portfolio['title'];?>" />
									</a>
								<?php else: ?>
					           			<img src="<?php echo $portfolio['image']?>" alt="<?php echo $portfolio['title'];?>" />
					            <?php endif ;?>

					            <div class="uk-card-body uk-padding-small">
					               <?php if ($portfolio['link']): ?>
						           		<a href="<?php echo $portfolio['link'] ?>" target="<?php echo $portfolio['target'] ? '_blank' : ''?>">
						            		<h5 class="uk-text-medium uk-margin-small"> <?php echo $portfolio['title'];?>
						            		<?php if ($portfolio['label']): ?>
							            		<span class="uk-label"><?php echo $portfolio['label'];?></span> 
						            		<?php endif; ?>
						            		</h5>

										</a>
									<?php else: ?>
										<h5 class="uk-text-medium uk-margin-small"> <?php echo $portfolio['title'];?>
											<?php if ($portfolio['label']): ?>
							            		<span class="uk-label"><?php echo $portfolio['label'];?></span> 
						            		<?php endif; ?>
										</h5>
					            	<?php endif ;?>
					            	<?php if ($portfolio['description']): ?>
					            		<p class="uk-text-small uk-margin-remove uk-padding-small uk-padding-remove-top"><?php echo $portfolio['description'];?></p>	
					            	<?php endif; ?>
					            </div> <!-- uk-body -->
				            </div> <!-- uk-card -->
				        </li> <!-- tags -->
			        <?php $i++; endforeach; ?>
			    </ul>  <!-- contents -->
	    	</div> <!-- target -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section>  <!-- end-section -->
