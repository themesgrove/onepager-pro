<?php

return array(

  'slug'      => 'pro-pricing-1', // Must be unique and singular
  'groups'    => array('pricing'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array( 'label' => 'Content', 'type' => 'divider'),
    array(
      'name'=>'title',
      'value' => 'Pricing'
    ),
    array(
      'name'=>'description',
      'type'=>'editor',
      'value'=> ''
    ),
    
    array( 'label' => 'Pricing', 'type' => 'divider'),
    array(
      'name'=>'pricings',
      'type'=>'repeater',
      'fields'=> array(
        array(
          array('name'=>'title', 'value'=>'Personal'),
          array('name'=>'price', 'value'=>'99'),
          array('name'=>'money', 'value'=>'$'),
          array('name'=>'period', 'value'=>'per year'),
          array('name'=>'features', 'value'=> array('1 Personal project', 'Access to all features', 'Billed annually')),
          array('name'=>'link', 'type'=>'link', 'url'=>'#', 'text'=>'Join Now'),
        ),
        array(
          array('name'=>'title', 'value'=>'Agency'),
          array('name'=>'price', 'value'=>'199'),
          array('name'=>'money', 'value'=>'$'),
          array('name'=>'period', 'value'=>'per year'),
          array('name'=>'features', 'value'=> array('Unlimited projects', 'Access to all features', 'Billed annually')),
          array('name'=>'link', 'type'=>'link', 'url'=>'#', 'text'=>'Join Now'),
        ),
      )
    ),


    array( 'label' => 'Testimonials', 'type' => 'divider'),
    array('name'=>'rating_img',  'label' => 'Rating Image','type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/star.png'),
    array('name'=>'rating_text', 'label' => 'Rating Text','type'=>'text', 'value' => '4.42 based on 180+ customer reviews'),
    array(
      'name'=>'testimonilas',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'value' => 'Daven Smith'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'"Worth every penny, if you have basic HTML knowledge. It helped us get an exceptional landing page up and running in no time."'),
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> '')
        ),
        array(
          array('name'=>'title', 'value' => 'Leandro Rocinny'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'"Slides gave us all the tools need to quickly create a marketing site, without the hassle and overhead of a full blown CMS. Fast, secure, and easy to tailer to our needs. We get a lot of compliments on the site, as well. Great work Designmodo!"'),
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> '')
        ),
        array(
          array('name'=>'title', 'value' => 'JP Holecka'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'"This is easily one of the   for constructing a landing page with style. The code is exquisite, customization is fantastic, and the Slides code generator."'),
          array('name'=>'media', 'type'=>'media', 'size'=>'fa-5x', 'value'=> '')
        )
      )

    )
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array( 'label' => 'Heading', 'type' => 'divider'),
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0           => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),
    array(
      'name'     => 'animation_content',
      'label'    => 'Animation Content',
      'type'     => 'select',
      'value'    => 'slide-bottom-medium',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array(
      'name' => 'note_image',
      'label' => 'Pricing',
      'type' => 'divider'
    ),
    array(
      'name'     => 'pricing_alignment',
      'label'    => 'Pricing Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'    => 'Left',
        'right'   => 'Right'
      ),
    ),
    array(
      'name'     => 'media_grid',
      'label'    => 'Pricing Grid',
      'type'     => 'select',
      'value'    => 'width-1-2',
      'options'  => array(
        'width-1-2'   => 'Half',
        'width-1-3'   => 'One Thrids',
        'width-1-4'   => 'One Fourth',
        'width-2-3'   => 'Two Thirds',
      ),
    ),
    array(
      'name' => 'media_size',
      'label' => 'Image Size',
      'append' => 'px',
      'value' => '810'
    ),

    array(
      'name'     => 'pricing_item_animation',
      'label'    => 'Animation Pricing',
      'type'     => 'select',
      'value'    => 'slide-bottom-medium',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array( 'label' => 'testimonial Items', 'type' => 'divider'),

    array(
      'name' => 'testimonial_text_size',
      'label' => 'Description Size',
      'append' => 'px',
      'value' => '14'
    ),
    array(
      'name' => 'testimonial_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'animation_testimonial',
      'label'    => 'Animation Testimonials',
      'type'     => 'select',
      'value'    => 'slide-bottom-medium',
      'options'  => array(
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
        ),
      ),

  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array('label'=>'Background Image', 'type'=>'divider'), // Divider - Background
    array(
      'name'  => 'bg_image',
      'label' => 'Image',
      'type'  => 'image'
    ),
    array(
      'name'     => 'bg_repeat',
      'label'    => 'Repeat',
      'type'     => 'select',
      'options'  => array(
        'no-repeat'     => 'No Repeat',
        'repeat'      => 'Repeat All',
        'repeat-x'      => 'Repeat X',
        'repeat-y'      => 'Repeat Y',
      )
    ),
    array(
      'name'    => 'bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#55d68d'
    ),
    array('label'=>'Heading', 'type'=>'divider'),
    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),
    array(
      'name'  => 'desc_color',
      'label' => 'Desc Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),

    array('label'=>'Pricing', 'type'=>'divider'),
    array(
      'name'    => 'pricing_bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => ' #fff'
    ),
    array(
      'name'    => 'pricing_text_color',
      'label'   => 'Text Color',
      'type'    => 'colorpicker',
      'value'   => ' #3d3d42'
    ),

   
    array(
      'name'    => 'button_text_color',
      'label'   => 'Button Text',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Button Background',
      'type'    => 'colorpicker',
      'value'   => '@color.primary'
    ),
    array('label'=>'Testimonial Item', 'type'=>'divider'),
    array(
      'name'  => 'testimonial_text_color',
      'label' => 'Description Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),
    array(
      'name'  => 'testimonial_title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),
  ),

);
