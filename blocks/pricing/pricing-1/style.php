#<?php echo $id ?>{
	<?php if($styles['bg_image']):?>
	background-image: url(<?php echo $styles['bg_image']?>);
	background-repeat: <?php echo $styles['bg_repeat']?>;
	<?php endif;?>
	background-color : <?php echo $styles['bg_color'] ?>;
	
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id ?> .uk-heading-primary {
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}
#<?php echo $id; ?> .uk-text-lead {
	color : <?php echo $styles['desc_color']?>;
	font-size : <?php echo $settings['desc_size']?>px;
}

#<?php echo $id ?> .uk-slider .uk-text-meta {
	color : <?php echo $styles['desc_color']?>;
}


#<?php echo $id ?> .uk-button{
	background: <?php echo $styles['button_bg_color']?>;
	color : <?php echo $styles['button_text_color']?>;
}

#<?php echo $id ?> .uk-button:hover{
	background: <?php echo $styles['button_text_color'];?>;
	color : <?php echo $styles['button_bg_color'];?>;
	border-color:<?php echo $styles['button_bg_color'];?>;
}

#<?php echo $id ?> .uk-card-title {
	font-size : <?php echo $settings['testimonial_title_size']?>px;
	color : <?php echo $styles['testimonial_title_color']?>;
}

#<?php echo $id ?> .uk-card-desc {
	font-size : <?php echo $settings['testimonial_text_size']?>px;
	color : <?php echo $styles['testimonial_text_color']?>;
}
#<?php echo $id; ?>  .uk-card-default{
	background: <?php echo $styles['pricing_bg_color']?>;
}
#<?php echo $id; ?>  .op-pricing,
#<?php echo $id; ?>  .op-pricing .pricing-title,
#<?php echo $id; ?>  .op-pricing .uk-h1{
	color : <?php echo $styles['pricing_text_color']?>;
}

#<?php echo $id; ?>  .op-pricing ul{
	padding-left:18px;
}

@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
}