<?php
	// animation repeat
	$animation_repeat = '';
	// media grid
	$media_grid = 'uk-'. $settings['media_grid'] . '@m';
	// Animation testimonial
	$animation_testimonial = ($settings['animation_testimonial']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_testimonial'].';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:400"' : '';
	// animation content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';
	// animation pricing items
	$animation_pricing_item = ($settings['pricing_item_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['pricing_item_animation'].'' : '';
	// Alignment
	$content_position = '';
	// media padding
	$media_padding = 'uk-container-item-padding-remove-left';
	
	if($settings['pricing_alignment'] == 'right'){
		$content_position = 'uk-flex-first@m uk-first-column';
		$media_padding = 'uk-container-item-padding-remove-right';
	}
	// Text transformation class
	$heading_class = ($settings['title_transformation']) ? 'uk-text-' . $settings['title_transformation'] : '';

?>
<section id="<?php echo $id; ?>" class="fp-section pricing pro-pricing-1 uk-cover-container">
	<div class="uk-section">
		<div class="uk-container">
			<article class="uk-grid-large">
				<div class="section-heading uk-text-center uk-width-3-4@m uk-width-1-1@s uk-margin-auto">
					<!-- Title -->
					<?php if($contents['title']): ?>
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary', 
							'uk-text-' . $settings['title_transformation'], 
							$animation_content . '"'
						); 
					?>
					<?php endif;?>

					<!-- Description -->
					<?php if($contents['description']): ?>
						<div class="uk-text-lead" 
							<?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : '');?>>
							<?php echo $contents['description']?>
						</div>
					<?php endif; ?>
				</div> <!-- uk-panel -->

				<div class="uk-margin-large-top" uk-grid>
					<!-- Media -->
					<div class="<?php echo $media_grid;?> uk-grid-item-match uk-flex-middle">
						<?php $i=3; ?>
						<?php foreach($contents['pricings'] as $k=>$pricing): ?>
						<div class="uk-width-1-2@m uk-width-1-1@s  uk-margin-bottom uk-flex uk-flex-center">
				            <div class="uk-card uk-card-default uk-padding uk-border-rounded" 
				            	<?php echo ($settings['pricing_item_animation'] ? $animation_pricing_item . ';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . $i . '00"' : ''); ?>>
				                <div class="op-pricing">
				                    <h3 class="pricing-title"><?php echo $pricing['title'];?></h3>
				                    <ul>
				                        <?php foreach($pricing['features'] as $feature): ?>
				                       	 	<li><?php echo $feature;?></li>
				                        <?php endforeach; ?>
				                    </ul>
				                    <div class="value uk-display-inline-block uk-margin">
				                          <span class="uk-text-bold"><?php echo $pricing['money'];?></span>
				                          <span class="uk-h1"><?php echo $pricing['price'];?></span>
				                          <span class="uk-text-bold"><?php echo $pricing['period'];?></span>
				                    </div>
				                    <div class="">
				                     <?php echo op_link($pricing['link'], 'uk-border-rounded uk-button uk-button-primary uk-text-center uk-width-1-1');?>
				                    	
				                    </div>
				                </div>
				            </div>
						</div>
				    	<?php $i++; endforeach; ?>
					</div>
					
					<div class="uk-width-1-2@m uk-grid-item-match uk-flex-middle <?php echo $content_position;?>">				
						<!-- Item slider -->
						<div class="uk-text-center" uk-slider="sets: true;" <?php echo $animation_testimonial;?>>
							<?php if ($contents['rating_img']): ?>
								<img class="op-media" src="<?php echo $contents['rating_img']; ?>" alt="<?php echo $testimonial['title'];?>" />
							<?php endif; ?>
							<?php if ($contents['rating_text']): ?>
								<p class="uk-text-meta uk-text-center"><?php echo $contents['rating_text']; ?></p>
							<?php endif; ?>
							<ul class="uk-slider-items uk-child-width-1-1@s uk-child-width-1-1@m uk-grid">
								<?php foreach($contents['testimonilas'] as $testimonial): ?>
								<li>
									<div class="uk-card uk-text-center">
										<div class="uk-grid-small">
											<div class="">
												<!-- Item image -->
												<?php if( op_is_image($testimonial['media'])):?>
													<img class="op-media uk-padding-small" src="<?php echo $testimonial['media']; ?>" alt="<?php echo $testimonial['title'];?>" />
												<?php else :?>
													<span class="op-media <?php echo $testimonial['media']; ?>"></span>
												<?php endif;?>
											</div>
											<div class="uk-width-expand">
												<p class="uk-card-desc uk-text-bold uk-margin-remove-top"><?php echo $testimonial['description'];?></p>
												<h3 class="uk-card-title uk-margin-remove"><?php echo $testimonial['title'];?></h3>
											</div>
										</div>
									</div>
								</li>
								<?php endforeach; ?>
							</ul>
							<ul class="uk-slider-nav uk-dotnav op-barnav uk-dark uk-flex-center uk-margin"></ul>
						</div><!-- uk-margin-top -->	
					</div> <!-- uk-width-expand -->
				</div> <!-- uk-grid -->
			</article><!-- article -->
		</div> <!-- uk-container -->
	</div> <!-- uk-section -->
</section><!-- section -->