<?php

return array(
  'slug'    => 'pro-pricing-2',
  'groups'    => array('pricing'),

  'contents' => array(
    array(
      'name'=>'title',
      'value'=>'Our most loved dishes'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => ''
    ),


    array('label'=>'Pricing Items  ', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'value' => 'Chicken Schnitzel'),
          array('name'=>'price', 'type'=>'text', 'value'=> '$39.00'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Build hi-fi prototypes with your real design files in Sketch.'),
        ),

        array(
          array('name'=>'title', 'value' => 'Roast Of The Evenin'),
          array('name'=>'price', 'type'=>'text', 'value'=> '$29.00'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Build hi-fi prototypes with your real design files in Sketch.'),
        ),
        array(
          array('name'=>'title', 'value' => 'Fresh Traditional Cod & Chips'),
          array('name'=>'price', 'type'=>'text', 'value'=> '$59.00'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Build hi-fi prototypes with your real design files in Sketch.'),
        ),
        array(
          array('name'=>'title', 'value' => 'Sweet & spicy chicken'),
          array('name'=>'price', 'type'=>'text', 'value'=> '$19.00'),
          array('name'=>'description', 'type'=> 'textarea', 'value'=>'Build hi-fi prototypes with your real design files in Sketch.'),
        )
      )

    ),
    array('label'=>'Center Image  ', 'type'=>'divider'), // Divider - Text
    array('name'=>'image', 'type'=>'image', 'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/02/center-img.png', 'label'=>'Upload Center Image'),
  ),

  'settings' => array(

    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),
    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array('name'=>'animation_repeat', 'label'=>'Animation Repeat', 'type'=>'switch'),

    array('label'=>'Items Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'item_title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'item_title_transformation',
      'label'    => 'Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'item_desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '16'
    ),

    array(
      'name'     => 'items_animation',
      'label'    => 'Items Animation ',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),




  'styles' => array(

    array(
      'name'    => 'bg_image',
      'label'   => 'Background',
      'type'    => 'image',
      'value'   => 'http://try.getonepager.com/wp-content/uploads/2019/02/banner-3-bg.png'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(98,126,117,0.71)'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'     => 'bg_image_size',
      'label'    => 'Size',
      'type'     => 'select',
      'value'    => 'uk-background-cover',
      'options'  => array(
        'uk-background-contain'   => 'Contain',
        'uk-background-cover'     => 'Cover',
      ),
    ),
    array(
      'name' => 'bg_color',
      'label' => 'Background Color',
      'type' => 'colorpicker',
      'value' => 'rgba(98,126,117,0.71)'
    ),


    array('label'=>'Title Styles', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),

    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),


    array('label'=>'Items Styles', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'item_title_color',
      'label' => 'Item Title Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),

    array(
      'name' => 'border_color',
      'label' => 'Border Color',
      'type' => 'colorpicker',
      'value' => '#ddd'
    ),
    array(
      'name' => 'item_desc_color',
      'label' => 'Item Desc Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),

  ),

);
