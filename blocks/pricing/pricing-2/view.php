<?php
	// animation repeat
	$animation_repeat = $settings['animation_repeat'];
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// items animation
	$items_animation = ($settings['items_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['items_animation'].'' : '';
	// array chunk
	$items = array_chunk($contents['items'], 2);

?>

<section id="<?php echo $id;?>" class="uk-section uk-position-relative pricing pro-pricing-2 <?php echo ($styles['bg_image_size'] == 'uk-background-contain') ? 'uk-background-contain' : 'uk-background-cover' ?>" 	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-container">
		<article class="uk-article">
			<div class="section-heading uk-position-relative uk-margin-medium-bottom uk-text-<?php echo $title_alignment;?>">	
				<?php if($contents['title']):?>
					<!-- Section Title -->
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary uk-text-bold', 
							'uk-text-' . $settings['title_transformation'], 
							$title_animation . '"'
						); 
					?>
				<?php endif; ?>

				<?php if($contents['description']):?>
					<!-- Section Sub Title -->
					<p class="uk-text-lead" 
						<?php echo ($settings['title_animation'] ? $title_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . '400"' : ''); ?>>
						<?php echo $contents['description'];?>
					</p>
				<?php endif; ?>
			</div> <!-- section-heading -->

			<div class="item-wrap">
				<?php foreach($items as $item):?>
					<div class="uk-flex uk-flex-wrap uk-margin">
					<?php $i=4; ?>
						<?php foreach($item as $key=>$feature):?>
						<div class="uk-width-1-2@m uk-width-width-1-1@s uk-float-<?= ($key%2) ? 'right' : 'left' ?>">
							<div class="uk-card uk-padding-large uk-padding-remove-bottom">
						   		<div <?php echo ($settings['items_animation'] ? $items_animation .';repeat:' . ($animation_repeat ? 'true' : 'false') . ';delay:' . $i . '00"' : ''); ?>>
						            <header class="uk-margin-small uk-position-relative">
						                <div class="uk-grid-medium uk-flex-middle" uk-grid>
						                    <div class="uk-width-expand">
						                        <h4 class="uk-card-title uk-text-bold uk-margin-remove-bottom uk-text-<?php echo $settings['item_title_transformation'];?>"><?php echo $feature['title'];?></h4>                     
						                    </div>
						                </div>
						                <div class=" uk-position-top-right uk-position">
						                	<h4 class="uk-text-meta uk-text-bold"><?php echo $feature['price']; ?></h4>	
						                </div>
						            </header>
						              <div class="content">
						              	<p class="uk-text-medium uk-margin-small uk-margin-remove-top"><?php echo $feature['description'];?></p>
						            </div>
						        </div>
							</div><!-- uk-card -->
						</div> <!-- uk-width -->
					<?php $i++; endforeach;?>
				</div> <!-- uk-flex -->
				<?php endforeach;?>
				<?php if ($contents['image']): ?>
					<div class="uk-width-1-1@m uk-visible@s uk-text-center uk-width-width-1-1@s uk-position-bottom-center uk-margin-xlarge-top center-image">
						<img class="op-media" width="150" src="<?php echo $contents['image']; ?>" />
					</div>
				<?php endif; ?>
			</div> <!-- uk-position-relative -->
		</article> <!-- uk-article -->
	</div> <!-- uk-container -->
</section> <!-- op-section -->
