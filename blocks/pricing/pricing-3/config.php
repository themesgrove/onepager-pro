<?php

return array(
	'slug' => 'pro-pricing-3',
	'name' => 'Pricing 3',
	'groups' => array('pricing'),
	'contents' => array(
		array('name' => 'title', 'value' => 'Pricing Plan'),
		array('name' => 'description', 'value' => 'By combining documentation with realtime API logs, Onepager makes your API <br> easy to use and helps you understand your developers.', 'type' => 'editor'),
		array(
			'name' => 'pricings',
			'type' => 'repeater',
			'fields' => array(
				array(
					array('name' => 'title', 'value' => 'Standard Plan'),
					array('name' => 'featured', 'type' => 'switch'),
					array('name' => 'price', 'value' => '19'),
					array('name' => 'money', 'value' => '$'),
					array('name' => 'features', 'value' => array('100 GB Disk Space', '1 Sub-Domain', '5 E-mail Account', '24/7 Support', 'Control Panel', 'Free Daily Backup')),
					array('name' => 'link', 'type' => 'link', 'url' => '#', 'text' => 'Get Started'),
				),
				array(
					array('name' => 'title', 'value' => 'Premium Plan'),
					array('name' => 'featured', 'type' => 'switch', 'value' => true),
					array('name' => 'price', 'value' => '39'),
					array('name' => 'money', 'value' => '$'),
					array('name' => 'features', 'value' => array('300 GB Disk Space', '5 Sub-Domain', '10 E-mail Account', '24/7 Support', 'Control Panel', 'Free Daily Backup')),
					array('name' => 'link', 'type' => 'link', 'url' => '#', 'text' => 'Get Started'),
				),
				array(
					array('name' => 'title', 'value' => 'Business Plan'),
					array('name' => 'featured', 'type' => 'switch'),
					array('name' => 'price', 'value' => '99'),
					array('name' => 'money', 'value' => '$'),
					array('name' => 'features', 'value' => array('500 GB Disk Space', '20 Sub-Domain', '50 E-mail Account', '24/7 Support', 'Control Panel', 'Free Daily Backup')),
					array('name' => 'link', 'type' => 'link', 'url' => '#', 'text' => 'Get Started'),
				),
			),
		),
	),


	// Settings - $settings available on view file to access the option
	'settings' => array(
		array(
	      'name'     => 'heading_type',
	      'label'    => 'Heading Type',
	      'type'     => 'select',
	      'value'    => 'h1',
	      'options'  => array(
	        'h1'   => 'h1',
	        'h2'   => 'h2',
	        'h3'   => 'h3',
	        'h4'   => 'h4',
	        'h5'   => 'h5',
	        'h6'   => 'h6',
	      ),
	    ),
    
		array('label' => 'Heading', 'type' => 'divider'), // Divider - Text
	    array(
	      'name'   => 'section_title_size',
	      'label'  => 'Title Size',
	      'append' => 'px',
	      'value'  => '@section_title_size'
	    ),
	    array(
	      'name'     => 'title_font_weight',
	      'label'    => 'Font Weight',
	      'type'     => 'select',
	      'value'    => '700',
	      'options'  => array(
	        '400'  => '400',
	        '500'   => '500',
	        '600'   => '600',
	        '700'   => '700',
	      ),
	    ),
		array(
			'name'     => 'title_transformation',
			'label'    => 'Title Transformation',
			'type'     => 'select',
			'value'    => 'inherit',
			'options'  => array(
				'inherit' => 'Default',
				'lowercase'   => 'Lowercase',
				'uppercase'   => 'Uppercase',
				'capitalize'  => 'Capitalized',
			),
		),

		array(
			'name'     => 'title_alignment',
			'label'    => 'Title Alignment',
			'type'     => 'select',
			'value'    => 'center',
			'options'  => array(
				'left'      => 'Left',
				'center'    => 'Center',
				'right'     => 'Right',
				'justify'   => 'Justify',
			),
		),

		array(
			'name' => 'desc_size',
			'label' => 'Desc Size',
			'append' => 'px',
			'value' => '18',
		),

		array(
			'name'     => 'title_animation',
			'label'    => 'Animation',
			'type'     => 'select',
			'value'    => '0',
			'options'  => array(
				'0'                     => 'None',
				'fade'                  => 'Fade',
				'scale-up'              => 'Scale Up',
				'scale-down'            => 'Scale Down',
				'slide-top-small'       => 'Slide Top Small',
				'slide-bottom-small'    => 'Slide Bottom Small',
				'slide-left-small'      => 'Slide Left Small',
				'slide-right-small'     => 'Slide Right Small',
				'slide-top-medium'      => 'Slide Top Medium',
				'slide-bottom-medium'   => 'Slide Bottom Medium',
				'slide-left-medium'     => 'Slide Left Medium',
				'slide-right-medium'    => 'Slide Right Medium',
				'slide-top'             => 'Slide Top 100%',
				'slide-bottom'          => 'Slide Bottom 100%',
				'slide-left'            => 'Slide Left 100%',
				'slide-right'           => 'Slide Right 100%',

			),
		),

		array('label' => 'Items', 'type' => 'divider'), // Divider - Text

		array(
			'name'     => 'items_columns',
			'label'    => 'Columns',
			'type'     => 'select',
			'value'    => '3',
			'options'  => array(
				'2'   => '2',
				'3'   => '3',
				'4'   => '4',

			),
		),

	),

	'styles' => array(
		array(
			'name'  => 'bg_color',
			'label' => 'Background Color',
			'type'  => 'colorpicker',
			'value' => '#f8f8f8',
		),

		array('label' => 'Heading', 'type' => 'divider'), // Divider - Text
		array(
			'name' => 'title_color',
			'label' => 'Title Color',
			'type'  => 'colorpicker',
			'value' => '#323232',
		),

		array(
			'name' => 'desc_color',
			'label' => 'Desc Color',
			'type'  => 'colorpicker',
			'value' => '#323232',
		),

		array('label' => 'Items', 'type' => 'divider'), // Divider - Text

		array(
			'name' => 'text_color',
			'label' => 'Text Color',
			'type'  => 'colorpicker',
			'value' => '#323232',
		),
		array(
			'name' => 'feature_text_color',
			'label' => 'Featured Text Color',
			'type'  => 'colorpicker',
			'value' => '#fff',
		),


		array(
			'name' => 'feature_bg_color',
			'label' => 'Featured Bg Color',
			'type'  => 'colorpicker',
			'value' => '@color.primary',
		),

		array(
			'name' => 'value_bg_color',
			'label' => 'Value Bg Color',
			'type'  => 'colorpicker',
			'value' => '#ddd',
		),
	),

);
