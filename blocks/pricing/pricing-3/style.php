#<?php echo $id; ?>{
   background : <?php echo $styles['bg_color']; ?>;
   color:<?php echo $styles['text_color']; ?>;
}
#<?php echo $id; ?> h1, #<?php echo $id; ?> h2, #<?php echo $id; ?> h3, #<?php echo $id; ?> h4{
	font-weight:<?php echo $settings['title_font_weight']; ?>;

}
#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['section_title_size']?>px;
	color : <?php echo $styles['title_color']?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}
#<?php echo $id; ?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']; ?>px;
	color : <?php echo $styles['desc_color']; ?>;
}

#<?php echo $id; ?> .price-table {
	background-color: #fff;
}

#<?php echo $id; ?> .price-table .pricing-title{
	color:<?php echo $styles['text_color']; ?>;
}

#<?php echo $id; ?> .price-table.featured .value,
#<?php echo $id; ?> .price-table.featured ul li:last-child a{
	background-color: <?php echo $styles['feature_bg_color']; ?>;
	color: <?php echo $styles['feature_text_color']; ?>;
}

#<?php echo $id; ?> .price-table ul li a{
	color: <?php echo $styles['feature_bg_color']; ?>;
}


#<?php echo $id; ?> .price-table ul li a:hover{
	background-color: <?php echo $styles['feature_bg_color']; ?>;
	color: <?php echo $styles['feature_text_color']; ?>;
	text-decoration: none;
}

#<?php echo $id; ?> .price-table .value{
	color: #fff;
    border-radius: 10px;
	background-color: <?php echo $styles['value_bg_color']; ?>;
	background-image: -moz-linear-gradient( -144deg, rgb(3,161,252) 0%, rgb(60,90,154) 100%);
	background-image: -webkit-linear-gradient( -144deg, rgb(3,161,252) 0%, rgb(60,90,154) 100%);
	background-image: -ms-linear-gradient( -144deg, rgb(3,161,252) 0%, rgb(60,90,154) 100%);
}

#<?php echo $id; ?> .uk-grid > div:nth-child(3n+1) .price-table .value{
	background: #4d82bd; /* Old browsers */
	background: -moz-linear-gradient(left,  #4d82bd 0%, #29aefc 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(left,  #4d82bd 0%,#29aefc 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to right,  #4d82bd 0%,#29aefc 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4d82bd', endColorstr='#29aefc',GradientType=1 ); /* IE6-9 */
}

#<?php echo $id; ?> .uk-grid > div:nth-child(3n+2) .price-table .value{
	background: #845ef5; /* Old browsers */
	background: -moz-linear-gradient(left,  #845ef5 0%, #ce49fc 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(left,  #845ef5 0%,#ce49fc 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to right,  #845ef5 0%,#ce49fc 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#845ef5', endColorstr='#ce49fc',GradientType=1 ); /* IE6-9 */
}

#<?php echo $id; ?> .uk-grid > div:nth-child(3n+3) .price-table .value{
	background: #ff4b67; /* Old browsers */
	background: -moz-linear-gradient(left,  #ff4b67 0%, #fd4fa8 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(left,  #ff4b67 0%,#fd4fa8 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to right,  #ff4b67 0%,#fd4fa8 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff4b67', endColorstr='#fd4fa8',GradientType=1 ); /* IE6-9 */
}

#<?php echo $id; ?> .price-table .pricing-content-wrapper{
	border-radius: 10px;
	position: relative;
    top: -120px;
}

#<?php echo $id; ?> .price-table .value span:nth-child(1){
	font-size: 32px;
	line-height: 32px;
}

#<?php echo $id; ?> .price-table .value span:nth-child(2) {
	font-size: 65px;
	line-height: 65px;
	margin-bottom: 25px;
}

#<?php echo $id; ?> .price-table .value span:last-child {
	font-size: 16px;
}

#<?php echo $id; ?> .price-table {
	box-shadow: 0px 2px 5px 0px rgba(0, 126, 246, 0.35);
	border-radius: 10px;
	padding: 50px 40px;
	height: 82%;
}

#<?php echo $id; ?> .price-table .uk-button{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 4px 5px 0px rgba(77, 117, 249, 0.35);
  padding: 17px 50px;
    -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}

#<?php echo $id; ?> .uk-grid > div:nth-child(3n+1) .price-table .uk-button{
	color: #3498dd;
	box-shadow: 0px 4px 5px 0px rgba(57, 155, 224, 0.35);
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-grid > div:nth-child(3n+1) .price-table .uk-button:hover{
	color: #fff;
	background: #3498dd; 
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}

#<?php echo $id; ?> .uk-grid > div:nth-child(3n+2) .price-table .uk-button{
	color: #af51f9;
	box-shadow: 0px 4px 5px 0px rgba(127, 82, 249, 0.35);
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-grid > div:nth-child(3n+2) .price-table .uk-button:hover{
	color: #fff;
	background: #af51f9;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-grid > div:nth-child(3n+3) .price-table .uk-button{
	color: #f64d8a;
	box-shadow: 0px 4px 5px 0px rgba(254, 69, 226, 0.35);
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-grid > div:nth-child(3n+3) .price-table .uk-button:hover{
	color: #fff;
	background: #f64d8a;
	-webkit-transition: all 0.5s ease;
	-moz-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
}

#<?php echo $id; ?> .uk-grid > div:nth-child(3n+1) .price-table{
	box-shadow: 0px 4px 5px 0px rgba(57, 155, 224, 0.35);
      -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-grid > div:nth-child(3n+1) .price-table:hover{
	box-shadow: 0px 4px 15px 0px rgba(57, 155, 224, 0.35);
	  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-grid > div:nth-child(3n+2) .price-table{
    margin-top: -60px;
	box-shadow: 0px 4px 5px 0px rgba(172, 82, 249, 0.35);
      -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-grid > div:nth-child(3n+2) .price-table:hover{
	box-shadow: 0px 4px 15px 0px rgba(172, 82, 249, 0.35);
      -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-grid > div:nth-child(3n+3) .price-table{
	box-shadow: 0px 4px 5px 0px rgba(254, 69, 126, 0.35);
      -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .uk-grid > div:nth-child(3n+3) .price-table:hover{
	box-shadow: 0px 4px 15px 0px rgba(254, 69, 126, 0.35);
      -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .price-table ul li:last-child{
	padding:0;
}

#<?php echo $id; ?> .price-table ul li:first-child{
	padding-top:20px;
}

@media(max-width:768px){
	#<?php echo $id; ?> .section-heading .uk-heading-primary{
		font-size : <?php echo ( $settings['section_title_size'] / 1.5 ); ?>px;
	}
}
