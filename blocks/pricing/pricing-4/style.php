#<?php echo $id; ?>{
   background : <?php echo $styles['bg_color']; ?>;
   color:<?php echo $styles['text_color']; ?>;
}
#<?php echo $id; ?> h1, #<?php echo $id; ?> h2, #<?php echo $id; ?> h3, #<?php echo $id; ?> h4{
	font-weight:<?php echo $settings['title_font_weight']; ?>;

}
#<?php echo $id; ?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['section_title_size'] ?>px;
	color : <?php echo $styles['title_color'] ?>;
	line-height : <?php echo ($settings['section_title_size']) + 10 ?>px;
}
#<?php echo $id; ?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size']; ?>px;
	color : <?php echo $styles['desc_color']; ?>;
}

#<?php echo $id; ?> .price-table {
	background-color: #fff;
}

#<?php echo $id; ?> .price-table .pricing-title{
	color:<?php echo $styles['text_color']; ?>;
}

#<?php echo $id; ?> .price-table ul li a{
	color: <?php echo $styles['feature_bg_color']; ?>;
}


#<?php echo $id; ?> .price-table ul li a:hover{
	background-color: <?php echo $styles['feature_bg_color']; ?>;
	color: <?php echo $styles['feature_text_color']; ?>;
	text-decoration: none;
}

#<?php echo $id; ?> .price-table .pricing-content-wrapper{
	border-radius: 10px;
	position: relative;
}
#<?php echo $id; ?> .price-table .value {
    color: <?php echo $styles['text_color']; ?>;
    border-bottom: 1px solid <?php echo $styles['plan_bg_color']; ?>;
}
#<?php echo $id; ?> .price-table .value span:nth-child(1){
    font-size: 20px;
    background: <?php echo $styles['plan_bg_color']; ?>;
    padding: 13px 45px;
    border-radius: 50px;
}

#<?php echo $id; ?> .price-table .value span:nth-child(2) {
	font-size: 65px;
	line-height: 65px;
	margin-bottom: 25px;
}

#<?php echo $id; ?> .price-table .value span:nth-child(4) {
	color: #8e8e8e;
}

#<?php echo $id; ?> .price-table .value span:nth-child(6) {
	font-size: 30px;
}

#<?php echo $id; ?> .price-table .value span:nth-child(7) {
    font-size: 68px;
    font-weight: 600;
}

#<?php echo $id; ?> .price-table {
	box-shadow: 0px 4px 5px 0px rgba(251, 136, 159, 0.35);
	border-radius: 10px;
	padding: 50px 40px;
   -webkit-transition: all 0.3s ease;
  -moz-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease;
}

#<?php echo $id; ?> .price-table:hover {
	transform: scale(1.06);
	box-shadow: 0px 4px 25px 0px rgba(251, 136, 159, 0.35);
   -webkit-transition: all 0.3s ease;
  -moz-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease;
}

#<?php echo $id; ?> .price-table .uk-button{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 4px 5px 0px rgba(251, 136, 159, 0.35);
  padding: 15px 50px;
  font-size: 16px;
   -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .price-table:hover .uk-button{
  color: #fff;
  background-color: <?php echo $styles['pricing_hover_color']; ?>;
  box-shadow: 0px 4px 25px 0px rgba(251, 136, 159, 0.35);
   -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
#<?php echo $id; ?> .price-table .uk-button:hover{
  color: <?php echo $styles['pricing_hover_color']; ?>;
  background-color: #fff;
  box-shadow: 0px 4px 25px 0px rgba(251, 136, 159, 0.35);
   -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}

#<?php echo $id; ?>  .price-table:hover .value span:nth-child(1){
	color: #fff;
	background-color: <?php echo $styles['pricing_hover_color']; ?>;
	   -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}

@media(max-width:768px){
	#<?php echo $id; ?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size'] / 1.5); ?>px;
	}
}
