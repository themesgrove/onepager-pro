<?php

  $title_alignment = ( $settings['title_alignment'] ) ? $settings['title_alignment'] : '';
  $title_animation = ( $settings['title_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['title_animation'] . ';"' : '';
  $item_animation = ( $settings['item_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['item_animation'] . ';"' : '';

?>

<section id="<?php echo $id; ?>" class="fp-section pricing pro-pricing-4 uk-padding-small">
	<div class="uk-section uk-padding-large">
		<div class="uk-container">
			<div class="section-heading uk-margin-bottom uk-text-<?php echo $title_alignment; ?>">
				<?php if ( $contents['title'] ) : ?>
				<!-- Section Title -->
				<?php 
					echo op_heading(
						$contents['title'],
						$settings['heading_type'],
						'uk-heading-primary  uk-text-'.$settings['title_transformation'],
						$title_animation
					); 
				?>
				<?php endif; ?>

				<?php if ( $contents['description'] ) : ?>
					<div class="uk-text-lead uk-padding-large uk-padding-remove-top" <?php echo ( $settings['title_animation'] ? $title_animation . 'delay:300"' : '' ); ?>><?php echo $contents['description']; ?></div>
				<?php endif; ?>
			</div> <!-- Section heading -->

			<div class="uk-grid-medium" uk-grid>
				<?php $i = 4; ?>
				<?php foreach ( $contents['pricings'] as $k => $pricing ) : ?>
				<div class="uk-width-1-<?php echo $settings['items_columns']; ?>@m">
					<div class="price-table uk-text-center" <?php echo ( $settings['item_animation'] ? $item_animation . 'delay:' . $i . '00"' : '' ); ?>>
							<div class="pricing-content-wrapper">
								<div class="value">
									<span><?php echo $pricing['title']; ?></span><br><br>
									<span><?php echo $pricing['length']; ?></span><br>
									<span><?php echo $pricing['money']; ?></span>
									<span><?php echo $pricing['price']; ?></span><br>
								</div>
								<ul class="uk-list uk-margin-medium-bottom">
									<?php foreach ( $pricing['features'] as $feature ) : ?>
									<li><?php echo $feature; ?></li>
									<?php endforeach; ?>
								</ul> <!-- uk-list -->
								<?php echo op_link( $pricing['link'], 'uk-button uk-button-link uk-link-heading' ); ?>
							</div>
					</div> <!-- pricing-table -->
				</div> <!-- uk-width -->
					<?php
					$i++;
					endforeach;
				?>
			</div> <!-- uk-grid-medium -->
		</div> <!-- uk-grid -->
	</div>
</section> <!-- end-section -->
