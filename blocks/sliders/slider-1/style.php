#<?php echo $id; ?> {
	background-color : <?php echo $styles['bg_color'];?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id; ?> .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['text_color'];?>;
	line-height : <?php echo ($settings['title_size']+10);?>px;

}
#<?php echo $id; ?> .uk-text-lead{
	font-size : <?php echo $settings['desc_size']; ?>px;
	color : <?php echo $styles['text_color'];?>;
	line-height : <?php echo ($settings['desc_size']) +10;?>px;
}

#<?php echo $id; ?> .uk-button{
	font-size : <?php echo $settings['button_font_size'];?>px;
	background : <?php echo $styles['cta_bg']; ?>;
	color : <?php echo $styles['cta_color']; ?>;
	text-transform:<?php echo $settings['button_transformation'];?>;
	border-radius:<?php echo $settings['button_border_radius'];?>px;
}

#<?php echo $id; ?> .uk-button:hover{
	background : <?php echo $styles['cta_color']; ?>;
	color : <?php echo $styles['cta_bg']; ?>;
}

 #<?php echo $id; ?> .slider-navigation a{
 	color : <?php echo $styles['text_color'];?>;
 }
 #<?php echo $id; ?> .slider-navigation{
	 margin-top:-50px;
 }
 #<?php echo $id; ?> .slider-navigation svg{
	display:none;
 }

 #<?php echo $id; ?> .uk-overlay-primary{
 	background: <?php echo $styles['overlay_color'];?>;
 }


 @media(max-width:768px){
	#<?php echo $id; ?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2)+10;?>px;
		line-height : <?php echo ($settings['title_size']/2)+20;?>px;

	}

	#<?php echo $id; ?> .uk-text-lead{
		font-size : <?php echo ($settings['desc_size']-2); ?>px;
		line-height : <?php echo ($settings['title_size']/2) -3;?>px;
	}
 }