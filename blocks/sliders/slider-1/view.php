<?php 
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	
	$slideshow_options[] = 'autoplay-interval: ' . $settings['slider_interval'] ;
	$slideshow_options[] = ($settings['autoplay']) ? 'autoplay: true' : '';
	// $slideshow_options[] = ($settings['slider_height']) ? 'max-height:' . $settings['slider_height'] : '';
	$slideshow = implode('; ', $slideshow_options);
	
	// title transfrom
	$heading_class = ($settings['title_transformation']) ? 'uk-text-' . $settings['title_transformation'] : '';
?>



	
<div id="<?php echo $id; ?>" class="uk-position-relative uk-visible-toggle uk-light slider pro-slider-1 <?php echo $settings['padding_bottom_off'] ? 'uk-padding-large' : '';?> uk-padding-remove-top" tabindex="-1" uk-slider="<?php echo $slideshow;?>">
	<div class="uk-slider-container uk-position-relative">
		<ul class="uk-slider-items uk-child-width-1-1@l uk-child-width-1-1@s uk-child-width-1-1@m" uk-height-viewport="min-height: 300">
			<?php foreach($contents['sliders'] as $index => $slide): ?>
				<li>
					<img src="<?php echo $slide['image'];?>" uk-img>
					<div class="uk-overlay-primary uk-position-cover"></div>
					<div class="uk-position-large uk-position-center uk-panel uk-padding-large uk-padding-remove-top uk-text-<?php echo $settings['title_alignment'];?>">
						<?php
							echo op_heading( 
								$slide['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$title_animation . '"'
							); 
						?>
						<p class="uk-text-lead uk-margin-medium-bottom"
						<?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'300"' : '' ;?>>
							<?php echo $slide['description'];?>
						</p>
						<p <?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : '' ;?>>
							<?php echo op_link($slide['link'], 'uk-button uk-button-primary uk-button-large');?>
						</p>
					</div> 	<!-- uk-position-large -->
				</li>
			<?php endforeach;?>
		</ul> 	<!-- slider-items-->
		<div class="uk-position-absolute slider-navigation">
			<a class="uk-position-small uk-position-relative" href="#" uk-slidenav-previous uk-slider-item="previous">Previous</a>
			<a class="uk-position-small uk-position-relative" href="#" uk-slidenav-next uk-slider-item="next">Next</a>
		</div> 	<!-- slider-navigation -->
	</div> 	<!-- slider-container -->
 </div>	<!-- section-id -->

