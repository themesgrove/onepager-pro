<?php

return array(

  'slug'      => 'pro-slider-2', // Must be unique
  'groups'    => array('sliders'), // Blocks group for filter

  // Fields - $contents available on view file to access the option
  'contents' => array(
  array('label'=> 'Slider Content' , 'type'=> 'divider'),
    array(
      'name'=>'sliders',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'sub_title', 'label' => 'Sub Title', 'value' => 'We are here to'),
          array('name'=>'title', 'type'=> 'textarea', 'value' => 'Planning Business'),
          array('name'=>'description', 'type'=> 'textarea', 'value' => 'Build website quickly and efficiently with simple easy to use page builder'),
          array('name'=>'link', 'text' => 'More Details', 'type' => 'link'),
          array('name'=>'image','type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/03/banner-2.jpg'),
          array('name'=>'icon', 'label' => 'Navigation Icon' , 'type'=>'icon', 'size'=>'fa-lg', 'value'=> 'fa fa-handshake-o fa-lg'),
          array('name'=>'nav_text', 'label' => 'Navigation Text' , 'value' => 'Consultant')

        ),
        array(
          array('name'=>'sub_title', 'label' => 'Sub Title', 'value' => 'Get your'),
          array('name'=>'title', 'type'=> 'textarea', 'value' => 'Business Consultant'),
          array('name'=>'description', 'type'=> 'textarea', 'value' => 'Ridiculously easy and built for tomorrows internet in mind'),
          array('name'=>'link', 'text' => 'See Demos', 'type' => 'link'),
          array('name'=>'image','type'=>'image', 'value' => 'http://try.getonepager.com/wp-content/uploads/2019/03/banner-1.jpg'),
          array('name'=>'icon', 'label' => 'Navigation Icon' , 'type'=>'icon', 'size'=>'fa-lg', 'value'=> 'fa fa-signal fa-lg'),
          array('name'=>'nav_text', 'label' => 'Navigation Text' ,'value' => 'Marketting')
        )
      )
    )

  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
   array('label'=> 'Slider Settings' , 'type'=> 'divider'),
    array(
      'name' => 'autoplay',
      'label' => 'Autoplay',
      'type' => 'switch',
      'value' => true
    ),
    array(
      'name' => 'slider_interval',
      'label' => 'Slider Interval',
      'append' => 'px',
      'value' => 5000,
    ),
   
    array('label'=> 'Title' , 'type'=> 'divider'),
    array(
      'name' => 'sub_title_size',
      'label' => 'Sub Title Size',
      'append' => 'px',
      'value' => '20'
    ),

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),

    
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'title_animation',
      'label'    => 'Title Animation ',
      'type'     => 'select',
      'value'    => 'slide-top-medium',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    array('name'=>'animation_repeat', 'label'=>'Animation Repeat', 'type'=>'switch'),

    array('label'=> 'Button Settings' , 'type'=> 'divider'),

    array(
      'name' => 'button_font_size',
      'label' => 'Font Size',
      'append' => 'px',
      'value' => '16'
    ),
    array(
      'name'     => 'button_transformation',
      'label'    => 'Text Transformation',
      'type'     => 'select',
      'value'    => 'uppercase',
      'options'  => array(
        'inherit'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'button_border_radius',
      'label' => 'Border Radius',
      'append' => 'px',
      'value' => '50'
    ),
    array(
      'name'     => 'button_animation',
      'label'    => 'Animation ',
      'type'     => 'select',
      'value'    => 'slide-left-medium',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),


  'styles' => array(
    array('label'=> 'Slider Style' , 'type'=> 'divider'),

    array(
        'name'    => 'overlay_color',
        'label'   => 'Overlay Color',
        'type'    => 'colorpicker',
        'value'   => 'rgba(0,0,0,0.25)'
      ),
      array(
        'name'    => 'text_color',
        'label'   => 'Text Color',
        'type'    => 'colorpicker',
        'value'   => '#fff'
      ),


      array('label'=> 'Button Style' , 'type'=> 'divider'),
      array(
        'name'    => 'cta_color',
        'label'   => 'Color',
        'type'    => 'colorpicker',
        'value'   => '#fff'
      ),
      array(
        'name'    => 'cta_bg',
        'label'   => 'Background',
        'type'    => 'colorpicker',
        'value'   => '#000000'
      ),
      array(
        'name'    => 'cta_hover_color',
        'label'   => 'Hover Color',
        'type'    => 'colorpicker',
        'value'   => '#000'
      ),
      array('label'=> 'Navigation Style' , 'type'=> 'divider'),
      array('name'=>'navigation_enable', 'label'=>'Navigation Enable', 'value'=> 'yes', 'type'=>'switch'),
      array(
        'name'    => 'nav_color',
        'label'   => 'Color',
        'type'    => 'colorpicker',
        'value'   => '#fff'
      ),
      array(
        'name'    => 'nav_bg_color',
        'label'   => 'Bg Color',
        'type'    => 'colorpicker',
        'value'   => '#000'
      ),

  ),
);
