#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id; ?> .uk-heading-large{
	font-size : <?php echo $settings['sub_title_size'];?>px;
	color : <?php echo $styles['text_color'];?>;
	line-height : <?php echo ($settings['sub_title_size']+10);?>px;
	letter-spacing:2px;
	text-transform:<?php echo $settings['button_transformation'];?>;

}

#<?php echo $id; ?> .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['text_color'];?>;
	line-height : <?php echo ($settings['title_size']+10);?>px;

}
#<?php echo $id; ?> .uk-text-lead{
	font-size : <?php echo $settings['desc_size']; ?>px;
	color : <?php echo $styles['text_color'];?>;
	line-height : <?php echo ($settings['desc_size']) +10;?>px;
}

#<?php echo $id; ?> .uk-button-default{
	font-size : <?php echo $settings['button_font_size'];?>px;
	<?php if($styles['cta_bg']):?>
		background : <?php echo $styles['cta_bg']; ?>;
	<?php endif;?>
	color : <?php echo $styles['cta_color']; ?>;
	border : 2px solid <?php echo $styles['cta_bg']; ?>;
	text-transform:<?php echo $settings['button_transformation'];?>;
	border-radius:<?php echo $settings['button_border_radius'];?>px;
	font-weight:500;
}

#<?php echo $id; ?> .uk-button-default:hover{
	background : <?php echo $styles['cta_color']; ?>;
	color : <?php echo $styles['cta_hover_color']; ?>;
	border-color:<?php echo $styles['cta_color']; ?>;
}


 #<?php echo $id; ?> .uk-slider-nav .uk-padding{
	 padding:30px;
 }

 #<?php echo $id; ?> .uk-slider-nav a:hover,
 #<?php echo $id; ?> .uk-slider-nav a:focus,
 #<?php echo $id; ?> .uk-slider-nav a:active{
	text-decoration:none;
 }
 #<?php echo $id; ?> .uk-slider-nav .uk-card .uk-card-title{
	 font-size:18px;
 }
 #<?php echo $id; ?> .uk-slider-nav .uk-card .uk-card-title,
 #<?php echo $id; ?> .uk-slider-nav .uk-card .op-icon {
	color:<?php echo $styles['nav_bg_color'];?>;
 }

 #<?php echo $id; ?> .uk-slider-nav .uk-active .uk-card .uk-card-title,
 #<?php echo $id; ?> .uk-slider-nav .uk-active .uk-card .op-icon {
	color: <?php echo $styles['nav_color'];?>;
 }

 #<?php echo $id; ?> .uk-slider-nav .uk-active {
	background: <?php echo $styles['nav_bg_color'];?>;
 }

 #<?php echo $id; ?> .uk-slider-nav .uk-active:before {
    position: absolute;
    content: '';
    left: 50%;
    top: -12px;
    height: 25px;
    width: 25px;
    background: <?php echo $styles['nav_bg_color'];?>;
    transform: translateX(-50%) rotate(-45deg);
}

 #<?php echo $id; ?> .uk-overlay-primary{
 	background: <?php echo $styles['overlay_color'];?>;
 }


 #<?php echo $id; ?>  .uk-slidenav a{
	border-radius: <?php echo $settings['button_border_radius'];?>px;
	padding: 15px 20px;
	border: 1px solid <?php echo $styles['nav_bg_color'];?>;
	background: <?php echo $styles['nav_bg_color'];?>;
	color: <?php echo $styles['nav_color'];?>;
}

 @media(max-width:768px){
	#<?php echo $id; ?> .uk-heading-large{
		font-size : <?php echo ($settings['sub_title_size']-4);?>px;
		line-height : <?php echo ($settings['sub_title_size']+2);?>px;

	}
	#<?php echo $id; ?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2)+10;?>px;
		line-height : <?php echo ($settings['title_size']/2)+20;?>px;

	}

	#<?php echo $id; ?> .uk-text-lead{
		font-size : <?php echo ($settings['desc_size']-2); ?>px;
		line-height : <?php echo ($settings['title_size']/2) -3;?>px;
	}
 }