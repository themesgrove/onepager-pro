<?php 
	// animation repeat
	$animation_repeat = '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';

	// Button animation
	$button_animation = ($settings['button_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['button_animation'].';' : '';
	
	$slideshow_options[] = 'autoplay-interval: ' . $settings['slider_interval'] ;
	$slideshow_options[] = ($settings['autoplay']) ? 'autoplay: true' : '';
	// $slideshow_options[] = ($settings['slider_height']) ? 'max-height:' . $settings['slider_height'] : '';
	$slideshow = implode('; ', $slideshow_options);
	
	// title transfrom
	$heading_class = ($settings['title_transformation']) ? 'uk-text-' . $settings['title_transformation'] : '';
?>


<div id="<?php echo $id; ?>" class="uk-position-relative  slider pro-slider-2" 
tabindex="-1" uk-slider="<?php echo $slideshow;?>">
	<div class="uk-position-relative uk-visible-toggle uk-light">
		<ul class="uk-slider-items uk-child-width-1-1@l uk-child-width-1-1@s uk-child-width-1-1@m">
			<?php foreach($contents['sliders'] as $index => $slide): ?>
				<li>
					<img style="width:100%;" src="<?php echo $slide['image'];?>" uk-img>
					<div class="uk-overlay-primary uk-position-cover "></div>
					<div class="uk-position-large uk-position-center uk-panel uk-padding-large uk-padding-remove-top uk-text-<?php echo $settings['title_alignment'];?>">
						<?php if($slide['sub_title']):?>	
							<h4 class="uk-heading-large uk-margin-remove <?php echo $heading_class; ?>" 
							<?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>> 
								<?php echo $slide['sub_title'];?>
							</h4>
						<?php endif;?>	
						<?php if($slide['title']):?>	
							<?php
								echo op_heading( 
									$slide['title'],
									$settings['heading_type'], 
									'uk-heading-primary', 
									'uk-text-' . $settings['title_transformation'], 
									$title_animation . '"'
								); 
							?>
						<?php endif;?>

						<?php if($slide['description']):?>
							<p class="uk-text-lead uk-margin-medium-bottom"
							<?php echo ($settings['title_animation'])? $title_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'500"' : '' ;?>>
								<?php echo $slide['description'];?>
							</p>
						<?php endif;?>

						<?php if($slide['link']):?>
							<p class="uk-margin-top-small" <?php echo ($settings['button_animation'])? $button_animation . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'700"' : '' ;?>>
								<?php echo op_link($slide['link'], 'uk-button uk-button-default uk-button-large');?>
							</p>
						<?php endif;?>
					</div> 	<!-- uk-position-large -->
				</li>
			<?php endforeach;?>
		</ul> 	<!-- slider-items-->

		<?php if($styles['navigation_enable'] == 'yes'):?>
		<ul class="uk-slider-nav uk-background-default uk-text-center uk-padding-remove uk-margin-remove">
			<?php $i = 0;?>
			<?php foreach($contents['sliders'] as  $dot): ?>
				<li class="uk-inline-block uk-padding uk-position-relative" uk-slider-item="<?php echo $i;?>">
					<a href="#">
						<div class="uk-card uk-grid-match uk-flex-middle" uk-grid>
							<?php if($dot['icon']):?>
								<div class="uk-card-media-left uk-width-auto">
									<span class="op-icon <?php echo $dot['icon']; ?>"></span>
								</div>
							<?php endif;?>
							<?php if($dot['nav_text'] || ['sub_title']):?>
								<div class="uk-width-expand uk-padding-small uk-padding-remove-top uk-padding-remove-bottom">
									<div class="uk-card-body uk-padding-remove">
										<?php if($dot['nav_text']):?>
											<h4 class="uk-card-title uk-margin-remove">
												<?php echo $dot['nav_text'];?>
											</h4>
										<?php else:?>
											<h4 class="uk-card-title uk-margin-remove">
												<?php echo $dot['sub_title'];?>
											</h4>
										<?php endif; ?>
									</div><!-- uk-card-body -->
								</div>
							<?php endif; ?>
						</div>
					</a>
				</li>
			<?php $i++;endforeach;?>
		</ul>
		<hr class="uk-divider-small uk-margin-remove">
		<?php endif;?>

		<div class="uk-slidenav uk-background-default uk-padding-remove">
			<a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
    		<a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
		</div> 	<!-- slider-navigation -->
	</div> 	<!-- slider-container -->
 </div>	<!-- section-id -->

