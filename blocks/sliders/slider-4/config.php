<?php

return array(

    'slug'     => 'pro-slider-4', // Must be unique
    'groups'   => array('sliders'), // Blocks group for filter

    // Fields - $contents available on view file to access the option
    'contents' => array(
        array('label' => 'Slider Content', 'type' => 'divider'),
        array(
            'name'   => 'sliders',
            'type'   => 'repeater',
            'fields' => array(
                array(
                    array('name' => 'title', 'value' => 'Revolutionary Way of Building <br>OnePage Website'),
                    array('name' => 'description', 'type' => 'textarea', 'value' => 'The most easiest and beginner friendly WordPress <br> landing  page builder and Onepage builder plugin to help <br>you make website better and faster.'),
                    array('name' => 'link', 'text' => 'Get Started', 'type' => 'link'),
                ),
                array(
                    array('name' => 'title', 'value' => 'Ultimate Accounting Solution <br> For Ecommerce'),
                    array('name' => 'description', 'type' => 'textarea', 'value' => 'The most easiest and beginner friendly WordPress <br> landing  page builder and Onepage builder plugin to help <br>you make website better and faster.'),
                    array('name' => 'link', 'text' => 'Get Started', 'type' => 'link'),
                ),
            ),
        ),

    ),

    // Settings - $settings available on view file to access the option
    'settings' => array(
        array('label' => 'Slider Settings', 'type' => 'divider'),
        array(
            'name'    => 'heading_type',
            'label'   => 'Heading Type',
            'type'    => 'select',
            'value'   => 'h1',
            'options' => array(
                'h1' => 'h1',
                'h2' => 'h2',
                'h3' => 'h3',
                'h4' => 'h4',
                'h5' => 'h5',
                'h6' => 'h6',
            ),
        ),
        array(
            'name'    => 'animation',
            'label'   => 'Animation',
            'type'    => 'select',
            'value'   => 'scale',
            'options' => array(
                'slide' => 'Slide',
                'fade'  => 'Fade',
                'scale' => 'Scale',
                'pull'  => 'Pull',
                'push'  => 'Push',
            ),
        ),
        array(
            'name'  => 'autoplay',
            'label' => 'Autoplay',
            'type'  => 'switch',
            'value' => true,
        ),
        
        array(
            'name'    => 'bg_image_position',
            'label'   => 'Background Image Position',
            'type'    => 'select',
            'value'   => 'center center',
            'options' => array(
                'left top' => 'left top',
                'left center'  => 'left center',
                'left bottom' => 'left bottom',
                'right top'  => 'right top',
                'right center'  => 'right center',
                'right bottom'  => 'right bottom',
                'center top'  => 'center top',
                'center center'  => 'center center',
                'center bottom'  => 'center bottom',
                'initial'  => 'initial',
            ),
        ),

        array(
            'name'   => 'slider_height',
            'label'  => 'Slider Height',
            'append' => 'px',
        ),

        array('label' => 'Title', 'type' => 'divider'),
        array(
            'name'   => 'title_size',
            'label'  => 'Title Size',
            'append' => 'px',
            'value'  => '58',
        ),

        array(
            'name'    => 'title_font_weight',
            'label'   => 'Font Weight',
            'type'    => 'select',
            'value'   => '700',
            'options' => array(
                '400' => '400',
                '500' => '500',
                '600' => '600',
                '700' => '700',
            ),
        ),

        array(
            'name'    => 'title_transformation',
            'label'   => 'Title Transformation',
            'type'    => 'select',
            'value'   => 'inherit',
            'options' => array(
                'inherit'    => 'Default',
                'lowercase'  => 'Lowercase',
                'uppercase'  => 'Uppercase',
                'capitalize' => 'Capitalized',
            ),
        ),

        array(
            'name'    => 'title_alignment',
            'label'   => 'Title Alignment',
            'type'    => 'select',
            'value'   => 'left',
            'options' => array(
                'left'    => 'Left',
                'center'  => 'Center',
                'right'   => 'Right',
                'justify' => 'Justify',
            ),
        ),

        array(
            'name'   => 'desc_size',
            'label'  => 'Desc Size',
            'append' => 'px',
            'value'  => '20',
        ),

        array(
            'name'    => 'title_animation',
            'label'   => 'Title Animation ',
            'type'    => 'select',
            'value'   => '0',
            'options' => array(
                '0'                   => 'None',
                'fade'                => 'Fade',
                'scale-up'            => 'Scale Up',
                'scale-down'          => 'Scale Down',
                'slide-top-small'     => 'Slide Top Small',
                'slide-bottom-small'  => 'Slide Bottom Small',
                'slide-left-small'    => 'Slide Left Small',
                'slide-right-small'   => 'Slide Right Small',
                'slide-top-medium'    => 'Slide Top Medium',
                'slide-bottom-medium' => 'Slide Bottom Medium',
                'slide-left-medium'   => 'Slide Left Medium',
                'slide-right-medium'  => 'Slide Right Medium',
                'slide-top'           => 'Slide Top 100%',
                'slide-bottom'        => 'Slide Bottom 100%',
                'slide-left'          => 'Slide Left 100%',
                'slide-right'         => 'Slide Right 100%',

            ),
        ),
        array('name' => 'animation_repeat', 'label' => 'Animation Repeat', 'type' => 'switch'),

        array('label' => 'Button Settings', 'type' => 'divider'),

        array(
            'name'   => 'button_font_size',
            'label'  => 'Font Size',
            'append' => 'px',
            'value'  => '16',
        ),
        array(
            'name'    => 'button_transformation',
            'label'   => 'Text Transformation',
            'type'    => 'select',
            'value'   => 'inherit',
            'options' => array(
                'inherit'    => 'Default',
                'lowercase'  => 'Lowercase',
                'uppercase'  => 'Uppercase',
                'capitalize' => 'Capitalized',
            ),
        ),
        array(
            'name'   => 'button_border_radius',
            'label'  => 'Border Radius',
            'append' => 'px',
            'value'  => '10',
        ),

    ),

    'styles'   => array(
        array('label' => 'Slider Style', 'type' => 'divider'),
        array(
            'name'  => 'bg_image',
            'label' => 'Background',
            'type'  => 'image',
            'value' => 'https://demo.wponepager.com/wp-content/uploads/2019/07/slider-90.jpg',
        ),
        array(
            'name'  => 'overlay_color',
            'label' => 'Overlay Color',
            'type'  => 'colorpicker',
            'value' => 'rgba(0,0,0,0)',
        ),

        array(
            'name'  => 'bg_color',
            'label' => 'Bg Color',
            'type'  => 'colorpicker',
            'value' => '#393966',
        ),
        array(
            'name'  => 'text_color',
            'label' => 'Text Color',
            'type'  => 'colorpicker',
            'value' => '#393966',
        ),

        array('label' => 'Button Style', 'type' => 'divider'),
        array(
            'name'  => 'button_color',
            'label' => 'Color',
            'type'  => 'colorpicker',
            'value' => '#1a79fe',
        ),
        array(
            'name'  => 'button_bg',
            'label' => 'Background',
            'type'  => 'colorpicker',
            'value' => '#fff',
        ),

        array(
            'name'  => 'button_shadow_color',
            'label' => 'Button Shadow',
            'type'  => 'colorpicker',
            'value' => 'rgba(0, 126, 246, 0.35)',
        ),

        array('label' => 'Arrow Style', 'type' => 'divider'),
        array(
            'name'  => 'arrow_color',
            'label' => 'Color',
            'type'  => 'colorpicker',
            'value' => '#1a79fe',
        ),

    ),
);
