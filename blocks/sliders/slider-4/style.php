
#<?=$id?> {
	<?php if ($styles['bg_image']): ?>
	background-image: url(<?=$styles['bg_image']?>);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: <?php echo $settings['bg_image_position']; ?>;
	<?php endif;?>
}

#<?php echo $id; ?> h1, #<?php echo $id; ?> h2, #<?php echo $id; ?> h3, #<?php echo $id; ?> h4{
	font-weight:<?php echo $settings['title_font_weight']; ?>;
}

#<?php echo $id; ?> .uk-heading-primary {
	font-size : <?php echo $settings['title_size']; ?>px;
	color : <?php echo $styles['text_color']; ?>;
	line-height : <?php echo ($settings['title_size'] + 10); ?>px;
	font-weight : <?php echo ($settings['title_font_weight']); ?>;

}
#<?php echo $id; ?> .uk-text-lead {
	font-size : <?php echo $settings['desc_size']; ?>px;
	color : <?php echo $styles['text_color']; ?>;
	line-height : <?php echo ($settings['desc_size']) + 10; ?>px;
}

#<?php echo $id; ?> .uk-button{
	font-size : <?php echo $settings['button_font_size']; ?>px;
	background : <?php echo $styles['button_bg']; ?>;
	color : <?php echo $styles['button_color']; ?>;
	text-transform:<?php echo $settings['button_transformation']; ?>;
	border-radius:<?php echo $settings['button_border_radius']; ?>px;
  	box-shadow: 0px 4px 5px 0px <?php echo $styles['button_shadow_color']; ?>;
  	transition: all 0.5s ease;
}

#<?php echo $id; ?> .uk-button:hover{
	background : <?php echo $styles['button_color']; ?>;
	color : <?php echo $styles['button_bg']; ?>;
	transition: all 0.5s ease;
}

 #<?php echo $id; ?> .slider-navigation a{
 	color : <?php echo $styles['text_color']; ?>;
 }
 #<?php echo $id; ?> .uk-icon svg{
 	color : <?php echo $styles['arrow_color']; ?>;
 }
 #<?php echo $id; ?> .slider-navigation{
	 margin-top:-50px;
 }
 #<?php echo $id; ?> .slider-navigation svg{
	display:none;
 }

 #<?php echo $id; ?> .uk-overlay-primary{
 	background: <?php echo $styles['overlay_color']; ?>;
 }


 @media(max-width:768px){
	#<?php echo $id; ?> .uk-heading-primary{
		font-size : <?php echo ($settings['title_size'] / 2) + 10; ?>px;
		line-height : <?php echo ($settings['title_size'] / 2) + 20; ?>px;

	}

	#<?php echo $id; ?> .uk-text-lead{
		font-size : <?php echo ($settings['desc_size'] - 2); ?>px;
		line-height : <?php echo ($settings['title_size'] / 2) - 3; ?>px;
	}
 }