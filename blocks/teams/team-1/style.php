#<?php echo $id; ?>{
	background : <?php echo $styles['bg_color'];?>;
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}

#<?php echo $id; ?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']+10);?>px;
}

#<?php echo $id; ?> .section-heading .uk-text-lead{
	font-size:<?php echo $settings['desc_size']?>px;
	color: <?php echo $styles['desc_color'];?>;
	line-height : <?php echo ($settings['desc_size']+8);?>px;
}


#<?php echo $id; ?> .uk-card .uk-card-body .uk-card-title,
#<?php echo $id; ?> .uk-card .uk-card-body .uk-card-title a{
	font-size : <?php echo $settings['item_name_size']?>px;
	color : <?php echo $styles['item_name_color'];?>;
}

#<?php echo $id; ?> .uk-card .uk-card-body .uk-text-medium{
	font-size : <?php echo $settings['item_designation_size']?>px;
	color : <?php echo $styles['item_designation_color'];?>;
}

#<?php echo $id; ?> .uk-card .uk-card-body .social-links a{
	font-size : <?php echo $settings['item_icon_size']?>px;
	color : <?php echo $styles['item_icon_color'];?>;
}
#<?php echo $id; ?> .uk-card .uk-card-body .social-links a:hover{
	color : <?php echo $styles['item_icon_hover_color'];?>;
}


@media(max-width:768px){
	#<?php echo $id; ?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2)+10?>px;
		line-height : <?php echo ($settings['title_size']/2)+18;?>px;
	}

	#<?php echo $id; ?> .section-heading .uk-text-lead{
		font-size:<?php echo ($settings['desc_size']-2)?>px;
		line-height : <?php echo ($settings['desc_size']+8);?>px;
	}

}