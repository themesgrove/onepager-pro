<?php
	// animation repeat
	$animation_repeat = $settings['animation_repeat'];
	// title alignment
	$title_alignment = ($settings['title_alignment']) ? $settings['title_alignment'] : '';
	// title animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	// item animation
	$item_animation = ($settings['item_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['item_animation'].';' : '';
?>


<section id="<?php echo $id; ?>" class="fp-section teams pro-team-2">
	<div class="uk-section">
		<div class="uk-container uk-padding-small">
			<div class="section-heading uk-width-3-4@m uk-width-1-1@s uk-margin-auto uk-margin-large-bottom uk-text-<?php echo $title_alignment;?>" 
			<?php echo ($settings['title_animation'] ? $title_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' . '100"' : ''); ?>>	
				<?php if($contents['title']):?>
					<!-- Section Title -->
					<?php
						echo op_heading( 
							$contents['title'],
							$settings['heading_type'], 
							'uk-heading-primary', 
							'uk-text-' . $settings['title_transformation'], 
							$title_animation . '"'
						); 
					?>
				<?php endif; ?>

				<?php if($contents['description']):?>
					<!-- Section Sub Title -->
					<p class="uk-text-lead" <?php echo ($settings['title_animation'] ? $title_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' . '300"' : ''); ?>>
						<?php echo $contents['description'];?>
					</p>
				<?php endif; ?>
			</div> <!-- section-heading -->
			
			<div class="uk-padding-small uk-text-center" uk-grid>
				<?php $i = 4;?>
				<?php foreach($contents['items'] as$team):?>
				<div class="uk-width-1-<?php echo $settings['items_columns'];?>@m uk-width-1-1@s">
					<div class="uk-card" <?php echo ($settings['item_animation'] ? $item_animation .'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:'  .$i .'00"' : ''); ?>>
							<img class="uk-border-circle" src="<?php echo $team['image'];?>" alt="<?php echo $team['title'];?>" />
							<div class="uk-card-body uk-text-<?php echo $settings['items_alignment'];?> uk-padding-remove uk-margin-top">
								<h3 class="uk-card-title uk-margin-remove-bottom uk-text-bold">
									<?php if(trim($team['link'])): ?>
										<a href="<?php echo $team['link']; ?>" target="<?php echo $team['target'] ? '_blank' : ''?>"><?php echo $team['title'];?></a>
									<?php else: ?>
										<?php echo $team['title'];?>
									<?php endif; ?>
								</h3>
								<?php if ($team['designation']): ?>
									<h5 class="uk-text-medium uk-margin-small"><?php echo $team['designation'];?></h5>
								<?php endif; ?>
								<?php if ($team['description']): ?>
									<p class="uk-text-small uk-margin-remove-top"><?php echo $team['description'];?></p>
								<?php endif; ?>
								<?php if ($team['social']): ?>										<div class="social-links">
										<?php foreach($team['social'] as $social):?>
											<a class="icon" href="<?php echo $social;?>" target="_blank"></a>
										<?php endforeach;?>
									</div> <!-- social-links -->
								<?php endif; ?>
							</div> <!-- uk-position center -->
						</div> <!-- uk-card -->
					</div> <!-- gird -->
				<?php $i++;endforeach; ?>
			</div> <!-- uk-grid-medium -->
		</div> <!-- uk-container -->
	</div><!-- uk-section -->
</section> <!-- end-section -->
