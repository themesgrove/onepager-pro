<?php

return array(

  'slug'      => 'pro-testimonial-1', // Must be unique
  'groups'    => array('testimonials'), // Blocks group for filter

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'title',
      'value'=>'Tech Master Love Onepager'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => 'Marketers at the world’s fastest-growing companies use Onepager to transform <br>strangers into customers. Here’s what they have to say.'
    ),
    array('label'=>'Testimonials', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'testimonials',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'testimony', 'type'=> 'textarea', 'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis ex risus. Vivamus hendrerit nec ex vitae varius. Aliquam sollicitudin dapibus dapibus. Duis lacus diam, lacinia a fringilla semper, laoreet eget tellus. Vestibulum sed nisi rutrum, efficitur odio et, varius mi.'),
          array('name'=>'name', 'value' => 'John Resig'),
          array('name'=>'designation', 'value' => 'Creator of jQuery'),
          array('name'=>'image', 'label' => 'Image', 'type'=>'image', 'value' => 'http://s3.amazonaws.com/quantum-assets/images/8-thumb.jpg' ),
        ),
        array(
          array('name'=>'testimony', 'type'=> 'textarea', 'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis ex risus. Vivamus hendrerit nec ex vitae varius. Aliquam sollicitudin dapibus dapibus. Duis lacus diam, lacinia a fringilla semper, laoreet eget tellus. Vestibulum sed nisi rutrum, efficitur odio et, varius mi.'),
          array('name'=>'name', 'value' => 'Elon Musk'),
          array('name'=>'designation', 'value' => 'CEO and CTO of SpaceX'),
          array('name'=>'image','type'=>'image', 'value' => 'http://s3.amazonaws.com/quantum-assets/images/7-thumb.jpg'),
        )
      )
    )
  ),

  'styles' => array(

    array(
      'name'  => 'bg_image',
      'label' => 'Background Image',
      'type'  => 'image',
      'value' => 'http://try.getonepager.com/wp-content/uploads/2019/02/testimonial-bg.png'
    ),

    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(34,36,64,0.85)'
    ),
    array('label'=>'Title Styles', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => '#fff'

    ),
    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => '#fff'
    ),

    array('label'=>'Testimoni Styles', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'quote_color',
      'label'   => 'Quote Color',
      'type'    => 'colorpicker',
      'value' => '#7c7c7c'
    ),
    array(
      'name'    => 'testimoni_color',
      'label'   => 'Testimoni Color',
      'type'    => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name'    => 'name_color',
      'label'   => 'Name Color',
      'type'    => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name'    => 'designation_color',
      'label'   => 'Designation Color',
      'type'    => 'colorpicker',
      'value' => '#fff'
    ),

    array(
      'name'    => 'thumb_border_color',
      'label'   => 'Thumb Border Color',
      'type'    => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name' => 'thumb_border_radius',
      'label' => 'Thumb Radius',
      'append' => 'px',
      'value' => 50,
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(
        '0'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'animation_content',
      'label'    => 'Content Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
    
    array('label'=>'Testimonial Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name'     => 'animation',
      'label'    => 'Slide Animation',
      'type'     => 'select',
      'value'    => 'slide',
      'options'  => array(
        'slide'   => 'Slide',
        'fade'   => 'Fade',
        'scale'  => 'Scale',
        'pull'  => 'Pull',
        'push'  => 'Push'
      ),
    ),
    array(
      'name' => 'autoplay',
      'label' => 'Autoplay',
      'type' => 'switch',
      'value' => true
    ),
    array(
      'name' => 'testimonial_height',
      'label' => 'Height',
      'append' => 'px',
      'value' => 300,
    ),

    array(
      'name' => 'text_size',
      'label' => 'Text Size',
      'append' => 'px',
      'value' => '18'
    ),
    array(
      'name' => 'name_size',
      'label' => 'Name Size',
      'append' => 'px',
      'value' => '20'
    ),
    array(
      'name'     => 'name_transformation',
      'label'    => ' Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0 => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),

    ),
      array(
        'name' => 'designation_size',
        'label' => 'Designation Size',
        'append' => 'px',
        'value' => '12'
      ),
    
  ),
);
