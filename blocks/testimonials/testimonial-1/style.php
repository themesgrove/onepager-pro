#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?>  .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px;
	color : <?php echo $styles['desc_color'];?>;
}

#<?php echo $id?> .uk-slideshow-items .testimony{
	font-size : <?php echo $settings['text_size'];?>px;
	color : <?php echo $styles['testimoni_color'];?>;
}
#<?php echo $id;?> .uk-slideshow-items .testimonial-name{
	font-size : <?php echo $settings['name_size'];?>px;
	color : <?php echo $styles['name_color'];?>;
}
#<?php echo $id;?> .uk-slideshow-items .uk-text-lead{
	font-size : <?php echo $settings['designation_size'];?>px;
	color : <?php echo $styles['designation_color'];?>;
	font-weight:500;
}

#<?php echo $id; ?> .uk-thumbnav>*>::after{ 
	background:transparent;
}
#<?php echo $id; ?> .uk-thumbnav> li >a { 
	border:2px solid transparent;
    border-radius: <?php echo $styles['thumb_border_radius'];?>px;
    padding:3px;

}
#<?php echo $id; ?> .uk-thumbnav> li >a img{
	border-radius: <?php echo $styles['thumb_border_radius'];?>px;
}

#<?php echo $id; ?> .uk-thumbnav li.uk-active a { 
	border:2px solid <?php echo $styles['thumb_border_color']; ?> !important;

}

#<?php echo $id; ?> .uk-light .uk-slidenav{
	color:<?php echo $styles['thumb_border_color']; ?>;
}

#<?php echo $id; ?> .uk-overlay-primary{
	background: transparent;
	background-image: linear-gradient(#157b72, <?php echo $styles['overlay_color'];?>);
}

#<?php echo $id; ?> .uk-slideshow-items .fa-quote-right {
    color: <?php echo $styles['quote_color'];?>;
    font-size: 40px;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
	#<?php echo $id; ?> .testimony{
	 	font-size:16px;
	 }

	 #<?php echo $id; ?> .uk-slideshow-items{
	 min-height:450px !important;
	 }

	 #<?php echo $id; ?> .uk-slideshow-items .testimony{
	 	font-size:16px !important;
	 }
}