<?php
	// animation repeat
	$animation_repeat = '';
	// animation content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';
	// slideshow options
	$slideshow_options[] = 'animation: ' . $settings['animation'] ;
	$slideshow_options[] = ($settings['autoplay']) ? 'autoplay: true' : '';
	$slideshow_options[] = ($settings['testimonial_height']) ? 'max-height:' . $settings['testimonial_height'] : '';
	$slideshow = implode('; ', $slideshow_options);
	// heading transform
	$heading_class = ($settings['name_transformation']) ? 'uk-text-' . $settings['name_transformation'] : '';
?>

<div id="<?php echo $id; ?>" 
	class="uk-background-cover uk-background-norepeat uk-position-relative testimonials pro-testimonial-1 fp-section" 
	tabindex="-1" 
	uk-slideshow="<?php echo $slideshow; ?>"  
	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>
	data-src="<?php echo $styles['bg_image'];?>" uk-img>

	<div class="uk-overlay-primary uk-position-cover"></div>
		<div class="uk-section-large">
			<div class="uk-container">
				<div class="section-heading uk-width-1-1 uk-margin-auto uk-text-center uk-position-relative">
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['name_transformation'], 
								$animation_content . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<div class="uk-text-lead uk-padding-small"
							<?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' .($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>
							<?php echo $contents['description'];?>
						</div>
					<?php endif; ?>
				</div> <!-- section-heading -->
			    <ul class="uk-slideshow-items uk-margin-top">
					<?php foreach($contents['testimonials'] as $index => $testimonial): ?>
				        <li>
				            <div class="uk-position-center uk-position-small uk-text-center uk-light">
					            <blockquote class="uk-padding-remove">
								 	<span class="fa fa-quote-right"></span>
								</blockquote>
								<?php if($testimonial['testimony']):?>
									<p class="testimony uk-width-3-4@m uk-width-1-1@s uk-text-bold uk-margin-auto" 
										uk-slideshow-parallax="x: 200,-200">
										<?php echo $testimonial['testimony'];?>
									</p>
								<?php endif; ?>

								<?php if($testimonial['name']):?>
									<h3 class="testimonial-name uk-text-bold <?php echo $heading_class; ?>" 
										uk-slideshow-parallax="x: 200,0,-100">
										<?php echo $testimonial['name'];?>
											<?php if($testimonial['designation']):?>
												<span class="uk-text-lead">
													-- <?php echo $testimonial['designation'];?>
												</span>
											<?php endif; ?>
									</h3>
								<?php endif; ?>
				            </div>
				        </li>
					<?php endforeach; ?>
			    </ul>

			  <div class="uk-position-bottom-center uk-position-small">
		        <ul class="uk-thumbnav uk-margin-large-bottom">
		        	<?php $i=0; ?>
		            <?php foreach($contents['testimonials'] as $index => $testimonial): ?>
			            <li uk-slideshow-item="<?php echo $i;?>">
				            <a href="#">
				            	<img width="50" src="<?php echo $testimonial['image']?>" alt="<?php echo $testimonial['name']?>">
				            </a>
			            </li>
		            <?php $i++; endforeach; ?>
		        </ul>
		    </div>
		</div>
	</div>

	<!--     <div class="uk-light">
        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
    </div> -->
</div>