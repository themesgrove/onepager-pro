<?php

return array(

  'slug'      => 'pro-testimonial-2', // Must be unique
  'groups'    => array('testimonials'), // Blocks group for filter

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Title', 'type'=>'divider'), // Divider - Text
    array(
      'name'=>'title',
      'value'=>''
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value' => ''
    ),
    array('label'=>'Testimonial', 'type'=>'divider'), // Divider - Text
    array('name'=>'name', 'value' => 'Christian Chicoine'),
    array('name'=>'testimony', 'type'=> 'textarea', 'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis ex risus. Vivamus hendrerit nec ex vitae varius. Aliquam sollicitudin dapibus dapibus. Duis lacus diam, lacinia a fringilla semper, laoreet eget tellus. Vestibulum sed nisi rutrum, efficitur odio et, varius mi.'),
    array('name'=>'designation', 'value' => 'Creator of jQuery'),
  ),

  'styles' => array(

    array(
      'name'  => 'bg_image',
      'label' => 'Background Image',
      'type'  => 'image',
      'value' => 'http://try.getonepager.com/wp-content/uploads/2019/03/testimonial-2-bg.png'
    ),

    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),

    array(
      'name'    => 'overlay_color',
      'label'   => 'Overlay Color',
      'type'    => 'colorpicker',
      'value' => 'rgba(255,135,119,0.85)'
    ),
    array('label'=>'Title Styles', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_color',
      'label' => 'Title Color',
      'type' => 'colorpicker',
      'value' => ''

    ),
    array(
      'name' => 'desc_color',
      'label' => 'Desc Color',
      'type' => 'colorpicker',
      'value' => ''
    ),

    array('label'=>'Testimoni Styles', 'type'=>'divider'), // Divider - Text

    array(
      'name'    => 'testimoni_color',
      'label'   => 'Testimoni Color',
      'type'    => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name'    => 'name_color',
      'label'   => 'Name Color',
      'type'    => 'colorpicker',
      'value' => '#252c3a'
    ),
    array(
      'name'    => 'designation_color',
      'label'   => 'Designation Color',
      'type'    => 'colorpicker',
      'value' => '#252c3a'
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title Settings', 'type'=>'divider'), // Divider - Text
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(
        '0'   => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'animation_content',
      'label'    => 'Content Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

    array('name'=>'animation_repeat', 'label'=>'Animation Repeat', 'type'=>'switch'),
    
    array('label'=>'Testimonial Settings', 'type'=>'divider'), // Divider - Text

    array(
      'name' => 'text_size',
      'label' => 'Text Size',
      'append' => 'px',
      'value' => '20'
    ),
    array(
      'name' => 'name_size',
      'label' => 'Name Size',
      'append' => 'px',
      'value' => '20'
    ),
    array(
      'name'     => 'name_transformation',
      'label'    => ' Transformation',
      'type'     => 'select',
      'value'    => 0,
      'options'  => array(
        0 => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),

    ),
      array(
        'name' => 'designation_size',
        'label' => 'Designation Size',
        'append' => 'px',
        'value' => '12'
      ),
    
  ),

);
