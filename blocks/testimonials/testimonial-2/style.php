#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?>  .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px;
	color : <?php echo $styles['desc_color'];?>;
}

#<?php echo $id?> .testimony{
	font-size : <?php echo $settings['text_size'];?>px;
	color : <?php echo $styles['testimoni_color'];?>;
}
#<?php echo $id;?> .testimonial-name{
	font-size : <?php echo $settings['name_size'];?>px;
	color : <?php echo $styles['name_color'];?>;
	letter-spacing: 2px;
}
#<?php echo $id;?> .uk-text-lead{
	font-size : <?php echo $settings['designation_size'];?>px;
	color : <?php echo $styles['designation_color'];?>;
	font-weight:500;
}


#<?php echo $id; ?> .uk-overlay-primary{
	background:<?php echo $styles['overlay_color'];?>;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
	#<?php echo $id; ?> .testimony{
	 	font-size:16px;
	 }
}