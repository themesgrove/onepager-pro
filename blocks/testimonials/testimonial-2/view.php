<?php
	// animation repeat
	$animation_repeat = '';
	// animation content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';

	// heading transform
	$heading_class = ($settings['name_transformation']) ? 'uk-text-' . $settings['name_transformation'] : '';
?>

<div id="<?php echo $id; ?>" 
	class="uk-background-contain uk-background-norepeat uk-position-relative testimonials pro-testimonial-2 fp-section" 
	  
	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>
	data-src="<?php echo $styles['bg_image'];?>" uk-img>

	<div class="uk-overlay-primary uk-position-cover"></div>
		<div class="uk-section-large">
			<div class="uk-container">
				<div class="section-heading uk-width-1-1 uk-margin-auto uk-text-center uk-position-relative">
					<?php if($contents['title']):  ?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['name_transformation'], 
								$animation_content . '"'
							); 
						?>
					<?php endif; ?>

					<?php if($contents['description']):?>
						<!-- Section Sub Title -->
						<div class="uk-text-lead uk-padding-small"
							<?php echo ($settings['animation_content'] ? $animation_content . ';repeat:' .($animation_repeat ? 'true' : 'false') . ';delay:' . '300"' : ''); ?>>
							<?php echo $contents['description'];?>
						</div>
					<?php endif; ?>
				</div> <!-- section-heading -->
				<div class="uk-text-center uk-position-relative uk-margin-medium-top">
					<?php if($contents['testimony']):?>
						<p class="testimony uk-width-1-2@m uk-width-1-1@s uk-text-bold uk-margin-auto" 
							>
							<?php echo $contents['testimony'];?>
						</p>
					<?php endif; ?>

					<?php if($contents['name']):?>
						<h3 class="testimonial-name <?php echo $heading_class; ?>" 
							>
							<?php echo $contents['name'];?>
								<?php if($contents['designation']):?>
									<span class="uk-text-lead">
										-- <?php echo $contents['designation'];?>
									</span>
							<?php endif; ?>
						</h3>
					<?php endif; ?>
				</div> <!-- uk-text-center -->
			</div> <!-- uk-container -->
		</div> <!-- section-large -->
	 </div> <!-- section-id -->