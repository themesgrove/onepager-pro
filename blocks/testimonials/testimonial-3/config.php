<?php

return array(

  'slug'      => 'pro-testimonial-3', // Must be unique and singular
  'groups'    => array('testimonials'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array(
      'name'=>'image',
      'type'=>'image',
      'value'=> 'http://try.getonepager.com/wp-content/uploads/2019/03/testimonial-3.jpg'
    ),
    array(
      'name' => 'note_content',
      'label' => 'Content',
      'type' => 'divider'
    ),
    array('name'=>'testimony', 'type'=> 'textarea', 'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis ex risus. Vivamus hendrerit nec ex vitae varius. Aliquam sollicitudin dapibus dapibus. Duis lacus diam, lacinia a fringilla semper, laoreet eget tellus. Vestibulum sed nisi rutrum, efficitur odio et, varius mi.'),
    array('name'=>'name', 'value' => 'Christian Chicoine'),
    array('name'=>'designation', 'value' => 'Creator of jQuery'),

  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name' => 'note_image',
      'label' => 'Image',
      'type' => 'divider'
    ),
    array(
      'name'     => 'media_alignment',
      'label'    => 'Image Alignment',
      'type'     => 'select',
      'value'    => 'left',
      'options'  => array(
        'left'    => 'Left',
        'right'   => 'Right'
      ),
    ),
    array(
      'name'     => 'media_grid',
      'label'    => 'Image Grid',
      'type'     => 'select',
      'value'    => 'width-1-3',
      'options'  => array(
        'width-1-2'   => 'Half',
        'width-1-3'   => 'One Thrids',
        'width-1-4'   => 'One Fourth',
        'width-2-3'   => 'Two Thirds',
      ),
    ),
    array(
      'name' => 'media_size',
      'label' => 'Image Size',
      'append' => 'px',
      'value' => '450'
    ),
    array(
      'name'     => 'animation_media',
      'label'    => 'Animation Media',
      'type'     => 'select',
      'value'    => 'slide-left-medium',
      'options'  => array(
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'
        ),
      ),
    array( 'label' => 'Content Settings', 'type' => 'divider'),

    array(
      'name'  => 'testimoni_size',
      'label' => 'Testimoni Size',
      'value' => 20
    ),

    array(
      'name' => 'name_size', 
      'label' => 'name Size',
      'value' => 22
    ),

    array(
      'name' => 'name_font',
      'type' => 'font', 
      'label' => 'Name Fonts'
    ),
    array(
      'name'     => 'name_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name' => 'designation_size', 
      'label' => 'Designation Size',
      'value' => 14
    ),


    array(
      'name'     => 'animation_content',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => 'slide-right-medium',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array(
      'name'    => 'bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array('name'=>'padding_bottom_off', 'label'=>'Padding Off', 'type'=>'switch'),
    
    array('label'=>'Content', 'type'=>'divider'),
    array(
      'name'  => 'testimoni_color',
      'label' => 'Testimoni Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),
    array(
      'name'  => 'name_color',
      'label' => 'Name Color',
      'type'  => 'colorpicker',
      'value' => '#ff8777'
    ),
    array(
      'name'  => 'designation_color',
      'label' => 'Designation Color',
      'type'  => 'colorpicker',
      'value' => '#ff8777'
    ),
  ),
);
