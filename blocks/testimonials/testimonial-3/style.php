#<?php echo $id ?>{
	<?php if($styles['bg_color']):?>
		background-color : <?php echo $styles['bg_color'] ?>;
	<?php endif;?>
	
}
#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['name_font']); ?>;
	font-weight:<?php echo $settings['name_font_weight'];?>;
}

#<?php echo $id?> .testimony{
	font-size : <?php echo $settings['testimoni_size'];?>px;
	color : <?php echo $styles['testimoni_color'];?>;
}
#<?php echo $id;?> .testimonial-name{
	font-size : <?php echo $settings['name_size'];?>px;
	color : <?php echo $styles['name_color'];?>;
	letter-spacing: 2px;
}
#<?php echo $id;?> .uk-text-lead{
	font-size : <?php echo $settings['designation_size'];?>px;
	color : <?php echo $styles['designation_color'];?>;
	font-weight:500;
}


#<?php echo $id;?> .uk-panel blockquote{
	margin-bottom:-100px;
	font-size: 70px;
	color : <?php echo $styles['testimoni_color'];?>;
	opacity:0.1;
	border: none;
}

@media(max-width:768px){
}