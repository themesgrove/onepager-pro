<?php
	// animation repeat
	$animation_repeat = '';
	// media grid
	$media_grid = 'uk-'. $settings['media_grid'] . '@m';
	// Animation media
	$animation_media = ($settings['animation_media']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_media'] .';repeat:' . ($animation_repeat ? 'true' : 'false') . '"' : '';
	// animation contant
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'] .';repeat:' . ($animation_repeat ? 'true' : 'false') . '"' : '';

	// Alignment
	$content_position = '';
	// media padding
	$media_padding = 'uk-container-item-padding-remove-left';
	
	if($settings['media_alignment'] == 'right'){
		$content_position = 'uk-flex-first@m uk-first-column';
		$media_padding = 'uk-container-item-padding-remove-right';
	}

?>
<section id="<?php echo $id; ?>" class="fp-section testimonials pro-testimonial-3">
	<div class="uk-section <?php echo $styles['padding_bottom_off'] ? 'uk-padding-remove': '';?>">
		<div class="uk-container">
			<article uk-grid>
				<!-- Media -->
				<?php if($contents['image']):?>
					<div class="<?php echo $media_grid?> uk-width-1-1@s uk-grid-item-match uk-flex-middle">
						<div class="uk-panel <?php echo $media_padding?>" <?php echo $animation_media;?>>
							<img 
								width="<?php echo $settings['media_size']?>" 
								src="<?php echo $contents['image']?>" 
								alt="<?php echo $contents['title']?>" 
								class="op-max-width-none <?php echo ($settings['media_alignment'] == 'left') ? 'uk-float-right' :''; ?>" uk-image>
						</div>
					</div>
				<?php endif;?>
				<div class="uk-width-expand@m uk-grid-item-match uk-flex-middle <?php echo $content_position;?>">
					<div class="uk-panel uk-padding uk-text-center" <?php echo $animation_content;?>>
						
						<?php if($contents['testimony']):?>
							<blockquote class="uk-padding-remove">
								<span class="fa fa-quote-right"></span>
							</blockquote>
							<p class="testimony uk-width-1-1@s uk-text-bold" 
								>
								<?php echo $contents['testimony'];?>
							</p>
						<?php endif; ?>

						<?php if($contents['name']):?>
							<h3 class="testimonial-name" 
								>
								<?php echo $contents['name'];?>
									<?php if($contents['designation']):?>
										<span class="uk-text-lead">
											-- <?php echo $contents['designation'];?>
										</span>
								<?php endif; ?>
							</h3>
						<?php endif; ?>
					</div>	<!-- uk-panel -->
				</div> <!-- uk-grid-match -->
			</article> <!-- uk-article --> 
		</div> <!-- uk-container -->
	</div> <!-- uk-section --> 
</section> <!-- end-section --> 