#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4,{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}


#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id;?>  .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px;
	color : <?php echo $styles['desc_color'];?>;
}

#<?php echo $id?> .uk-slideshow-items .testimony{
	font-size : <?php echo $settings['text_size'];?>px;
	color : <?php echo $styles['testimoni_color'];?>;
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	line-height : <?php echo ($settings['text_size']) +10 ?>px;
}


#<?php echo $id; ?> .uk-light .uk-slidenav{
	color:<?php echo $styles['nav_border_color']; ?>;
	border: 1px solid <?php echo $styles['nav_border_color']; ?>;
	border-radius: <?php echo $styles['nav_border_radius'];?>px;
	padding: 8px 13px;
}

#<?php echo $id; ?>  .uk-icon>*{
	width: 10px;
    height: 20px;
}
#<?php echo $id; ?> .uk-overlay-primary{
	background: <?php echo $styles['overlay_color'];?>;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}
	#<?php echo $id ?> .section-heading .uk-text-lead {
		font-size : <?php echo ($settings['desc_size']/2)+7?>px;
	}
	

	 #<?php echo $id; ?> .uk-slideshow-items .testimony{
		font-size : <?php echo ($settings['text_size']/2);?>px;
		line-height : <?php echo ($settings['text_size']/2)+12;?>px;
	 }
}