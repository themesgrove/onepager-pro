<?php
	// animation repeat
	$animation_repeat = $settings['animation_repeat'];
	// animation content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';
	// slideshow options
	$slideshow_options[] = 'animation: ' . $settings['animation'] ;
	$slideshow_options[] = ($settings['autoplay']) ? 'autoplay: true' : '';
	$slideshow_options[] = ($settings['testimonial_height']) ? 'max-height:' . $settings['testimonial_height'] : '';
	$slideshow = implode('; ', $slideshow_options);
?>

<div id="<?php echo $id; ?>" 
	class="uk-background-cover uk-background-norepeat uk-position-relative testimonials pro-testimonial-4 fp-section" 
	tabindex="-1" 
	uk-slideshow="<?php echo $slideshow; ?>"  
	<?php echo ($styles['bg_parallax']) ? 'uk-parallax="bgy: -200"' : '' ?>
	data-src="<?php echo $styles['bg_image'];?>" uk-img>

	<div class="uk-overlay-primary uk-position-cover"></div>
	<div class="uk-section">
		<div class="uk-container">
			<ul class="uk-slideshow-items uk-margin-top">
				<?php foreach($contents['testimonials'] as $index => $testimonial): ?>
					<li>
						<div class="uk-position-center uk-position-small uk-text-center uk-light">
							<?php if($testimonial['testimony']):?>
								<p class="testimony uk-width-3-4@m uk-width-1-1@s uk-margin-auto" 
									uk-slideshow-parallax="x: 200,-200">
									<?php echo $testimonial['testimony'];?>
								</p>
							<?php endif; ?>
						</div>
					</li>
				<?php endforeach; ?>
			</ul> <!-- uk-slideshow-items -->
			<div class="uk-light">
				<a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
				<a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
			</div>
		</div><!-- uk-container-->
	</div> <!-- seciton-large -->
</div> <!-- seciton-id -->