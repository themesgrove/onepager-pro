#<?php echo $id;?>{
	background-color:<?php echo $styles['bg_color'];?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;
}
#<?php echo $id;?> .section-heading .uk-text-large{
	font-size : <?php echo $settings['sub_title_size']?>px;
	color : <?php echo $styles['sub_title_color']?>;
	line-height : <?php echo ($settings['sub_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-heading-primary{
	font-size : <?php echo $settings['title_size'];?>px;
	color : <?php echo $styles['title_color'];?>;
	line-height : <?php echo ($settings['title_size']) +10 ?>px;
}


#<?php echo $id?> .uk-slideshow-items .testimony{
	font-size : <?php echo $settings['text_size'];?>px;
	color : <?php echo $styles['testimoni_color'];?>;
}
#<?php echo $id;?> .uk-slideshow-items .testimonial-name{
	font-size : <?php echo $settings['name_size'];?>px;
	color : <?php echo $styles['name_color'];?>;
}
#<?php echo $id;?> .uk-slideshow-items .uk-text-lead{
	font-size : <?php echo $settings['designation_size'];?>px;
	color : <?php echo $styles['designation_color'];?>;
}


@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['title_size']/2) +15 ?>px;
	}

	 #<?php echo $id; ?> .uk-slideshow-items{
		 min-height:450px !important;
	 }

	 #<?php echo $id; ?> .uk-slideshow-items .testimony{
	 	font-size : <?php echo ($settings['text_size']-2);?>px;
	 }
}