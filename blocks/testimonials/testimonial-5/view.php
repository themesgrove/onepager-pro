<?php
	// animation repeat
	$animation_repeat = '';
	// animation content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';
	// slideshow options
	$slideshow_options[] = 'animation: ' . $settings['animation'] ;
	$slideshow_options[] = ($settings['autoplay']) ? 'autoplay: true' : '';
	$slideshow_options[] = ($settings['testimonial_height']) ? 'max-height:' . $settings['testimonial_height'] : '';
	$slideshow = implode('; ', $slideshow_options);
	// heading transform
	$heading_class = ($settings['name_transformation']) ? 'uk-text-' . $settings['name_transformation'] : '';
?>

<section id="<?php echo $id; ?>" class="testimonials pro-testimonial-5 fp-section" 
	tabindex="-1" 
	uk-slideshow="<?php echo $slideshow; ?>">
		<div class="uk-section ">
			<div class="uk-container">
				<div class="section-heading uk-width-1-1 uk-margin-large-bottom uk-text-center uk-position-relative">
					<?php if($contents['sub_title']):?>
						<!-- Section Sub Title -->
						<h4 class="uk-text-large uk-margin-small" <?php echo ($settings['animation_content'])? $animation_content . 'repeat:' .($animation_repeat ? 'true' : 'false') .';delay:' .'100"' : '' ;?>>
							<?php echo $contents['sub_title'];?>
						</h4>
					<?php endif; ?>
					<?php if($contents['title']):?>
						<!-- Section Title -->
						<?php
							echo op_heading( 
								$contents['title'],
								$settings['heading_type'], 
								'uk-heading-primary', 
								'uk-text-' . $settings['title_transformation'], 
								$animation_content . '"'
							); 
						?>
					<?php endif; ?>
				</div> <!-- section-heading -->

				<div class="" uk-grid>
					<div class="uk-width-1-3@m uk-position-relative ">
						<?php if ($contents['image']): ?>
							<img src="<?php echo $contents['image'];?>">
						<?php endif; ?>
					</div> <!-- uk-width-1-3@m -->

				<div class="uk-width-1-2@m uk-position-relative uk-background-default uk-card-default uk-padding-remove-left uk-margin-medium-left">
			    	<ul class="uk-slideshow-items uk-margin-medium-top uk-position-relative">
					<?php foreach($contents['testimonials'] as $index => $testimonial): ?>
				        <li>
				            <div class="uk-position-center uk-position-large uk-text-left uk-light">
								<?php if($testimonial['testimony']):?>
									<p class="testimony uk-margin-auto" 
										uk-slideshow-parallax="x: 200,-200">
										<?php echo $testimonial['testimony'];?>
									</p>
								<?php endif; ?>

								<?php if($testimonial['name']):?>
									<h3 class="testimonial-name uk-margin-remove uk-text-bold <?php echo $heading_class; ?>" 
										uk-slideshow-parallax="x: 200,0,-100">
										<?php echo $testimonial['name'];?>
									</h3>
								<?php endif; ?>
								<?php if($testimonial['designation']):?>
									<span class="uk-text-lead">
										<?php echo $testimonial['designation'];?>
									</span>
								<?php endif; ?>
				            </div>
				        </li>
					<?php endforeach; ?>
				    </ul>
					<div class="uk-position-top-right uk-margin-medium-right uk-margin-medium-top">
						<a class="uk-position-center-left uk-position-relative" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
					    <a class="uk-position-center-right uk-position-relative" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
					</div>
			    </div> <!-- k-width-1-2@m -->
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->
	</div><!-- uk-section -->
</section> <!-- end-section -->