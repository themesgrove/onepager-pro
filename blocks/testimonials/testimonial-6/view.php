<?php
	// Image content
	$img_animation = ($settings['img_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['img_animation'].'' : '';
	// animation content
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].'' : '';
	// slideshow options
	$slideshow_options[] = 'animation: ' . $settings['animation'] ;
	$slideshow_options[] = ($settings['autoplay']) ? 'autoplay: true' : '';
	$slideshow_options[] = ($settings['testimonial_height']) ? 'max-height:' . $settings['testimonial_height'] : '';
	$slideshow = implode('; ', $slideshow_options);
	// heading transform
	$heading_class = ($settings['name_transformation']) ? 'uk-text-' . $settings['name_transformation'] : '';
?>


<section id="<?php echo $id; ?>" uk-slideshow="<?php echo $slideshow; ?>" class="fp-section testimonial pro-testimonial-6" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-container">

		<div class="uk-card uk-grid-collapse uk-child-width-1-2@s uk-margin uk-grid" uk-grid="">


		    <div class="uk-card-media-left uk-cover-container">
				<?php if ( $contents['image'] ) : ?>
					<img class="op-media uk-position-bottom-left" src="<?php echo $contents['image']; ?>" alt="man">
				<?php endif; ?>
				 <canvas width="300" height="700"></canvas>
		    </div>

		    <div class="uk-flex-last@s uk-flex uk-flex-middle">

		    	<ul class="uk-slideshow-items uk-position-relative">

				<?php foreach($contents['testimonials'] as $index => $testimonial): ?>
			        <li class="uk-position-relative">
						<div class="">

							<?php if($contents['title']):?>
								<!-- Section Title -->
								<?php
									echo op_heading( 
										$contents['title'],
										$settings['heading_type'], 
										'uk-heading-primary', 
										'uk-text-' . $settings['title_transformation'], 
										$title_animation . '"'
									); 
								?>
							<?php endif; ?>
							<?php if($testimonial['testimony']):?>
								<p class="testimony uk-margin-auto" 
									uk-slideshow-parallax="x: 200,-200">
									<?php echo $testimonial['testimony'];?>
								</p>
							<?php endif; ?>

							<?php if($testimonial['name']):?>
								<h3 class="testimonial-name uk-margin-remove uk-text-bold <?php echo $heading_class; ?>" 
									uk-slideshow-parallax="x: 200,0,-100">
									<?php echo $testimonial['name'];?>
								</h3>
							<?php endif; ?>
							<?php if($testimonial['designation']):?>
								<span class="uk-text-lead">
									<?php echo $testimonial['designation'];?>
								</span>
							<?php endif; ?>
						</div>
			        </li>
				<?php endforeach; ?>
			    </ul>
			     <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
		    </div>

		</div>
	</div><!-- uk-container -->
</section> <!-- end-section -->