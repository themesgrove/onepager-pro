<?php

	// animation content
	$animation_content = ($settings['animation_content']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['animation_content'].'' : '';
	// slideshow options
	$slideshow_options[] = 'animation: ' . $settings['animation'] ;
	$slideshow_options[] = ($settings['autoplay']) ? 'autoplay: true' : '';
	$slideshow_options[] = ($settings['testimonial_height']) ? 'max-height:' . $settings['testimonial_height'] : '';
	$slideshow = implode('; ', $slideshow_options);
	// heading transform
	$heading_class = ($settings['name_transformation']) ? 'uk-text-' . $settings['name_transformation'] : '';
?>

<section id="<?php echo $id; ?>" class="testimonials pro-testimonial-7 fp-section" 
	tabindex="-1" 
	uk-slideshow="<?php echo $slideshow; ?>" data-src="<?php echo $styles['bg_image'];?>" uk-img>

			<div class="uk-container uk-container-center">
				<div class="uk-grid" uk-grid>
				<div class="uk-width-1-1@m uk-position-relative">
			    	<ul class="uk-slideshow-items uk-position-relative">
					<?php foreach($contents['testimonials'] as $index => $testimonial): ?>
				        <li>
				            <div class="uk-position-center uk-position-large uk-text-center uk-light uk-padding-large uk-padding-remove-vertical">
								<?php if($testimonial['testimony']):?>
									<p class="testimony uk-margin-auto" 
										uk-slideshow-parallax="x: 200,-200">
										<?php echo $testimonial['testimony'];?>
									</p>
								<?php endif; ?>

								<?php if($testimonial['name']):?>
									<h3 class="testimonial-name uk-margin-remove uk-text-bold <?php echo $heading_class; ?>" 
										uk-slideshow-parallax="x: 200,0,-100">
										<?php echo $testimonial['name'];?>
										<?php if($testimonial['designation']):?>
											<span class="uk-text-lead">
												<?php echo $testimonial['designation'];?>
											</span>
										<?php endif; ?>
									</h3>
								<?php endif; ?>
				            </div>
				        </li>
					<?php endforeach; ?>
				    </ul>
				  <div class="uk-position-bottom-center uk-position-small">
			        <ul class="uk-slideshow-nav uk-dotnav uk-padding uk-padding-remove-top uk-position-relative"></ul>
			    </div>
			    </div> <!-- k-width-1-2@m -->
			</div> <!-- uk-grid -->
		</div> <!-- uk-container -->

</section> <!-- end-section -->