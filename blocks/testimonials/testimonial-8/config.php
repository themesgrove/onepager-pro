<?php

return array(

    'slug'     => 'pro-testimonial-8', // Must be unique
    'groups'   => array('testimonials'), // Blocks group for filter

    // Fields - $contents available on view file to access the option
    'contents' => array(
        array('label' => 'Title', 'type' => 'divider'),
        array(
            'name'  => 'title',
            'value' => 'Trusted by owners at fast-growing organizations',
        ),
        array(
            'name'  => 'desc',
            'type'  => 'textarea',
            'value' => "We're powering growth and retention efforts for some exceptional customers.",
        ),
        array('label' => 'Testimonial', 'type' => 'divider'),
        array(
            'name'   => 'testimonials',
            'type'   => 'repeater',
            'fields' => array(
                array(
                    array('name' => 'image', 'label' => 'Image', 'type' => 'image', 'value' => 'http://s3.amazonaws.com/quantum-assets/images/8-thumb.jpg'),
                    array('name' => 'testimony', 'type' => 'textarea', 'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis ex risus. Vivamus hendrerit nec ex vitae varius. Aliquam sollicitudin dapibus dapibus. Duis lacus diam, lacinia a fringilla semper, laoreet eget tellus. Vestibulum sed nisi rutrum, efficitur odio et, varius mi.'),
                    array('name' => 'name', 'value' => 'John Resig'),
                    array('name' => 'designation', 'value' => 'Creator of jQuery'),
                ),
                array(
                    array('name' => 'image', 'type' => 'image', 'value' => 'http://s3.amazonaws.com/quantum-assets/images/7-thumb.jpg'),
                    array('name' => 'testimony', 'type' => 'textarea', 'value' => "Wow! I have the exact same personality, the only thing that has changed is my mindset and a few behaviors. I gained so much confidence in my ability to connect and deepen my relationships with people. It’s amazing how much easier meet new people and create connections"),
                    array('name' => 'name', 'value' => 'Elon Musk'),
                    array('name' => 'designation', 'value' => 'CEO and CTO of SpaceX'),
                ),
                array(
                    array('name' => 'image', 'type' => 'image', 'value' => 'https://demo.wponepager.com/wp-content/uploads/2019/03/img2.jpg'),
                    array('name' => 'testimony', 'type' => 'textarea', 'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis ex risus. Vivamus hendrerit nec ex vitae varius. Aliquam sollicitudin dapibus dapibus. Duis lacus diam, lacinia a fringilla semper, laoreet eget tellus. Vestibulum sed nisi rutrum, efficitur odio et, varius mi.'),
                    array('name' => 'name', 'value' => 'John Doe'),
                    array('name' => 'designation', 'value' => 'Member of Apple Inc'),
                ),
                array(
                    array('name' => 'image', 'type' => 'image', 'value' => 'https://demo.wponepager.com/wp-content/uploads/2019/03/img1.jpg'),
                    array('name' => 'testimony', 'type' => 'textarea', 'value' => "Wow! I have the exact same personality, the only thing that has changed is my mindset and a few behaviors. I gained so much confidence in my ability to connect and deepen my relationships with people. It’s amazing how much easier meet new people and create connections"),
                    array('name' => 'name', 'value' => 'Emma Watson'),
                    array('name' => 'designation', 'value' => 'Member of Fashion BD'),
                ),
                array(
                    array('name' => 'image', 'type' => 'image', 'value' => 'https://demo.wponepager.com/wp-content/uploads/2019/03/team-1.png'),
                    array('name' => 'testimony', 'type' => 'textarea', 'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis ex risus. Vivamus hendrerit nec ex vitae varius. Aliquam sollicitudin dapibus dapibus. Duis lacus diam, lacinia a fringilla semper, laoreet eget tellus. Vestibulum sed nisi rutrum, efficitur odio et, varius mi.'),
                    array('name' => 'name', 'value' => 'Kevin McKeon'),
                    array('name' => 'designation', 'value' => 'CTO of KevinX'),
                ),
                array(
                    array('name' => 'image', 'type' => 'image', 'value' => 'https://demo.wponepager.com/wp-content/uploads/2019/03/client.png'),
                    array('name' => 'testimony', 'type' => 'textarea', 'value' => "Wow! I have the exact same personality, the only thing that has changed is my mindset and a few behaviors. I gained so much confidence in my ability to connect and deepen my relationships with people. It’s amazing how much easier meet new people and create connections"),
                    array('name' => 'name', 'value' => 'Peter Pants'),
                    array('name' => 'designation', 'value' => 'CEO of PantsX'),
                ),
            ),
        ),
    ),
    // Settings - $settings available on view file to access the option
    'settings' => array(
        array(
            'name'  => 'autoplay',
            'label' => 'Autoplay',
            'type'  => 'switch',
            'value' => true,
        ),
        array('label' => 'Heading', 'type' => 'divider'), // Divider - Text
        array(
            'name'    => 'heading_type',
            'label'   => 'Heading Type',
            'type'    => 'select',
            'value'   => 'h1',
            'options' => array(
                'h1' => 'h1',
                'h2' => 'h2',
                'h3' => 'h3',
                'h4' => 'h4',
                'h5' => 'h5',
                'h6' => 'h6',
            ),
        ),
        array(
            'name'   => 'section_title_size',
            'label'  => 'Title Size',
            'append' => 'px',
            'value'  => '@section_title_size',
        ),
        array(
            'name'    => 'title_font_weight',
            'label'   => 'Font Weight',
            'type'    => 'select',
            'value'   => '700',
            'options' => array(
                '400' => '400',
                '500' => '500',
                '600' => '600',
                '700' => '700',
            ),
        ),
        array(
            'name'    => 'title_transformation',
            'label'   => 'Title Transformation',
            'type'    => 'select',
            'value'   => 'inherit',
            'options' => array(
                'inherit'    => 'Default',
                'lowercase'  => 'Lowercase',
                'uppercase'  => 'Uppercase',
                'capitalize' => 'Capitalized',
            ),
        ),
        array(
            'name'   => 'desc_size',
            'label'  => 'Desc Size',
            'append' => 'px',
            'value'  => '16',
        ),

        array(
            'name'    => 'title_animation',
            'label'   => 'Animation',
            'type'    => 'select',
            'value'   => '0',
            'options' => array(
                '0'                   => 'None',
                'fade'                => 'Fade',
                'scale-up'            => 'Scale Up',
                'scale-down'          => 'Scale Down',
                'slide-top-small'     => 'Slide Top Small',
                'slide-bottom-small'  => 'Slide Bottom Small',
                'slide-left-small'    => 'Slide Left Small',
                'slide-right-small'   => 'Slide Right Small',
                'slide-top-medium'    => 'Slide Top Medium',
                'slide-bottom-medium' => 'Slide Bottom Medium',
                'slide-left-medium'   => 'Slide Left Medium',
                'slide-right-medium'  => 'Slide Right Medium',
                'slide-top'           => 'Slide Top 100%',
                'slide-bottom'        => 'Slide Bottom 100%',
                'slide-left'          => 'Slide Left 100%',
                'slide-right'         => 'Slide Right 100%',
            ),
        ),

        array('label' => 'Items Settings', 'type' => 'divider'), // Divider - Text
        array(
            'name'   => 'text_size',
            'label'  => 'Text Size',
            'append' => 'px',
            'value'  => '18',
        ),
        array(
            'name'   => 'name_size',
            'label'  => 'Name Size',
            'append' => 'px',
            'value'  => '32',
        ),
        array(
            'name'    => 'name_transformation',
            'label'   => ' Transformation',
            'type'    => 'select',
            'value'   => 0,
            'options' => array(
                0            => 'Default',
                'lowercase'  => 'Lowercase',
                'uppercase'  => 'Uppercase',
                'capitalize' => 'Capitalized',
            ),

        ),
        array(
            'name'   => 'designation_size',
            'label'  => 'Designation Size',
            'append' => 'px',
            'value'  => '15',
        ),
        array(
            'name'    => 'items_animation',
            'label'   => 'Items Animation ',
            'type'    => 'select',
            'value'   => '0',
            'options' => array(
                '0'                   => 'None',
                'fade'                => 'Fade',
                'scale-up'            => 'Scale Up',
                'scale-down'          => 'Scale Down',
                'slide-top-small'     => 'Slide Top Small',
                'slide-bottom-small'  => 'Slide Bottom Small',
                'slide-left-small'    => 'Slide Left Small',
                'slide-right-small'   => 'Slide Right Small',
                'slide-top-medium'    => 'Slide Top Medium',
                'slide-bottom-medium' => 'Slide Bottom Medium',
                'slide-left-medium'   => 'Slide Left Medium',
                'slide-right-medium'  => 'Slide Right Medium',
                'slide-top'           => 'Slide Top 100%',
                'slide-bottom'        => 'Slide Bottom 100%',
                'slide-left'          => 'Slide Left 100%',
                'slide-right'         => 'Slide Right 100%',

            ),
        ),
    ),
    'styles'   => array(
        array(
            'name'  => 'bg_image',
            'label' => 'Background Image',
            'type'  => 'image',
        ),

        array(
            'name'  => 'bg_color',
            'label' => 'Overlay Color',
            'type'  => 'colorpicker',
            'value' => '#fff6f8',
        ),
        array('label' => 'Title Style', 'type' => 'divider'), // Divider - Text
        array(
            'name'  => 'title_color',
            'label' => 'Title Color',
            'type'  => 'colorpicker',
            'value' => '#393966',

        ),

        array(
            'name'  => 'desc_color',
            'label' => 'Desc Color',
            'type'  => 'colorpicker',
            'value' => '#393966',

        ),
        array('label' => 'Testimonial Style', 'type' => 'divider'), // Divider - Text

        array(
            'name'  => 'testimoni_color',
            'label' => 'Testimoni Color',
            'type'  => 'colorpicker',
            'value' => '#142b45',
        ),

        array(
            'name'  => 'name_color',
            'label' => 'Name Color',
            'type'  => 'colorpicker',
            'value' => '#142b45',
        ),

        array(
            'name'  => 'designation_color',
            'label' => 'Designation Color',
            'type'  => 'colorpicker',
            'value' => '#8a8a8a',
        ),
        array(
            'name'  => 'shadow_color',
            'label' => 'Shadow Color',
            'type'  => 'colorpicker',
            'value' => 'rgba(251, 136, 159, 0.35)',
        ),
        array('label' => 'Nav Style', 'type' => 'divider'), // Divider - Text
        array(
            'name'  => 'nav_color',
            'label' => 'Nav Color',
            'type'  => 'colorpicker',
            'value' => '#142b45',
        ),
        array(
            'name'  => 'nav_hover_color',
            'label' => 'Nav Hover Color',
            'type'  => 'colorpicker',
            'value' => '#fff',
        ),
        array(
            'name'  => 'nav_bg_color',
            'label' => 'Nav Background Color',
            'type'  => 'colorpicker',
            'value' => 'rgb(255, 255, 255)',
        ),
        array(
            'name'  => 'nav_bg_hover_color',
            'label' => 'Nav Background Hover Color',
            'type'  => 'colorpicker',
            'value' => '#ff5e49',
        ),

    ),

);
