#<?php echo $id; ?>{
    background-color : <?php echo $styles['bg_color'] ?>;
}
#<?php echo $id; ?> h1, #<?php echo $id; ?> h2, #<?php echo $id; ?> h3, #<?php echo $id; ?> h4{
    font-weight:<?php echo $settings['title_font_weight']; ?>;
}

#<?php echo $id; ?> .section-heading .uk-heading-primary{
    font-size : <?php echo $settings['section_title_size'] ?>px;
    color : <?php echo $styles['title_color'] ?>;
    line-height : <?php echo ($settings['section_title_size']) + 10 ?>px;
}
#<?php echo $id; ?> .uk-text-lead{
    font-size : <?php echo $settings['desc_size'] ?>px;
    color : <?php echo $styles['desc_color'] ?>;
}
#<?php echo $id; ?> .uk-card{
  border-radius: 10px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 4px 5px 0px rgba(251, 136, 159, 0.35);
}
#<?php echo $id; ?> .testionial-img img {
    max-height: 80px;
    max-width: 80px;
    margin: 0 auto;
    height: 80px;
    width: 80px;
    object-fit: cover;
}
#<?php echo $id; ?> .testimony {
    font-size: <?php echo $settings['text_size'] ?>;
    color: <?php echo $styles['testimoni_color'] ?>;
}
#<?php echo $id; ?> .name {
    font-weight: 700;
    color: <?php echo $styles['name_color'] ?>;
    font-size: <?php echo $settings['name_size'] ?>;
    text-transform: <?php echo $settings['name_transformation'] ?>;
}
#<?php echo $id; ?> .designation {
    font-size: <?php echo $settings['designation_size'] ?>;
    color: <?php echo $styles['designation_color'] ?>;
    margin-bottom: 30px;
}
#<?php echo $id; ?> .uk-slidenav {
    border-radius: 100px;
    color:<?php echo $styles['nav_color'] ?>;
    background-color: <?php echo $styles['nav_bg_color'] ?>;
    box-shadow: 0px 4px 5px 0px <?php echo $styles['shadow_color'] ?>;
    text-align: center;
    line-height: 38px;
    display: block;
    padding: 8px 22px;
    margin: 0 18px;
    width: 8px;
}

#<?php echo $id; ?> .uk-slidenav-previous.uk-slidenav:hover{
    color:  <?php echo $styles['nav_hover_color'] ?>;
     background-color:<?php echo $styles['nav_bg_hover_color'] ?>;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}

#<?php echo $id; ?> .uk-slidenav-next.uk-slidenav:hover{
    color:  <?php echo $styles['nav_hover_color'] ?>;
    background-color:<?php echo $styles['nav_bg_hover_color'] ?>;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}

#<?php echo $id; ?> li .uk-card {
    box-shadow: 0px 4px 5px 0px <?php echo $styles['shadow_color'] ?>;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
#<?php echo $id; ?> li:hover .uk-card {
    transform: scale(1.05);
    box-shadow: 0px 4px 25px 0px <?php echo $styles['shadow_color'] ?>;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}


