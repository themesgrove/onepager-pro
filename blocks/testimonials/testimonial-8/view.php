<?php

$title_animation = ( $settings['title_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['title_animation'] . ';"' : '';
$items_animation = ( $settings['items_animation'] ) ? 'uk-scrollspy="cls:uk-animation-' . $settings['items_animation'] . ';"' : '';

?>


<div  id="<?php echo $id; ?>" class="fp-section uk-position-relative testimonials testimonial-1  uk-padding-large" data-src="<?php echo $styles['bg_image']; ?>" uk-img>
	<div class="uk-container uk-padding-large uk-padding-remove-horizontal">
		<div class="section-heading uk-text-center">	
			<?php if ( $contents['title'] ) : ?>
			<!-- Section Title -->
			  	<?php 
					echo op_heading(
						$contents['title'],
						$settings['heading_type'],
						'uk-heading-primary uk-text-'.$settings['title_transformation'],
						$title_animation
					); 
				?>
			<?php endif; ?>
			<?php if($contents['desc']):?>
				<!-- Section Sub Title -->
				<p class="uk-text-lead" <?php echo ($settings['title_animation'])? $title_animation . ';delay:' .'300"' : '' ;?>>
					<?php echo $contents['desc'];?>
				</p>
			<?php endif; ?>
		</div>
		<div uk-slider=" <?php if($settings['autoplay'] == 'true'): echo 'autoplay:true'; endif;?>">
		    <div class="uk-position-relative">

		        <div class="uk-slider-container uk-light uk-padding-small">
		            <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-3@m uk-grid">
					    <?php foreach ( $contents['testimonials'] as $index => $testimonial ) : ?>
					        <li>
					            <div class="uk-panel">
					             	<div class="uk-card uk-card-default uk-card-body uk-margin-large-top uk-margin-large-bottom ">

										<?php if ( $testimonial['testimony'] ) : ?>
											<p class="testimony uk-width-3-4@m uk-width-1-1@s  uk-align-center uk-text-center">
												<?php echo $testimonial['testimony']; ?>
											</p>
										<?php endif; ?>

										<?php if ( $testimonial['name'] ) : ?>
											<div class="name uk-margin-remove-bottom uk-text-center">
												<?php echo $testimonial['name']; ?>
											</div>
										<?php endif; ?>

										<?php if ( $testimonial['designation'] ) : ?>
											<p class="designation uk-margin-small-top uk-text-center">
												<?php echo $testimonial['designation']; ?>
											</p>
										<?php endif; ?>

										<?php if ( $testimonial['image'] ) : ?>
											<div class="testionial-img">
												<img class="uk-border-circle uk-align-center" src="<?php echo $testimonial['image']; ?>" alt="<?php echo $testimonial['name']; ?>">
											</div>
										<?php endif; ?>

					            	</div>
					            </div>
					        </li>
						<?php endforeach; ?>
		               
		            </ul>
		        </div>

		        <div class="uk-visible@s">
		            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
		            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
		        </div>

		    </div>
		</div>
	</div>
</div>



