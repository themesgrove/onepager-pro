<?php

return array(

  'slug'      => 'pro-videos-1', // Must be unique and singular
  'groups'    => array('videos'), // Blocks group for filter and plural

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('label'=>'Title', 'type'=>'divider'),
    array(
      'name'=>'title',
      'value' => 'Videos Lecture'
    ),
    array(
      'name'=>'description',
      'type'=>'textarea',
      'value'=> 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum<br> has been the industrys standard dummy text ever since the 1500s'
    ),
    array(
      'name'=>'items',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'value' => 'Video'),
          array('name'=>'video_link', 'label' => 'Video Link', 'value'=>'https://www.youtube.com/watch?v=gJKIwp21dXs'),
          array(
            'name'    => 'video_bg_img',
            'label'   => 'Video Background Image',
            'type'    => 'image',
            'value'   => 'https://demo.wponepager.com/wp-content/uploads/2019/05/v-bg.jpg'
          ),
        ),
        array(
          array('name'=>'title', 'value' => 'Video'),
          array('name'=>'video_link', 'label' => 'Video Link', 'value'=>'https://www.youtube.com/watch?v=gJKIwp21dXs'),
          array(
            'name'    => 'video_bg_img',
            'label'   => 'Video Background Image',
            'type'    => 'image',
            'value'   => 'https://images.unsplash.com/photo-1552974936-5f13ce934b79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=180'
          ),
        ),
        array(
          array('name'=>'title', 'value' => 'Video'),
          array('name'=>'video_link', 'label' => 'Video Link', 'value'=>'https://www.youtube.com/watch?v=gJKIwp21dXs'),
          array(
            'name'    => 'video_bg_img',
            'label'   => 'Video Background Image',
            'type'    => 'image',
            'value'   => 'https://images.unsplash.com/photo-1544685584-9d18c3420eb4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=170'
          ),
        ),
        array(
          array('name'=>'title', 'value' => 'Video'),
          array('name'=>'video_link', 'label' => 'Video Link', 'value'=>'https://www.youtube.com/watch?v=gJKIwp21dXs'),
          array(
            'name'    => 'video_bg_img',
            'label'   => 'Video Background Image',
            'type'    => 'image',
            'value'   => 'https://images.unsplash.com/photo-1519834089823-08a494ba5a12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=180'
          ),
        ),
        array(
          array('name'=>'title', 'value' => 'Video'),
          array('name'=>'video_link', 'label' => 'Video Link', 'value'=>'https://www.youtube.com/watch?v=gJKIwp21dXs'),
          array(
            'name'    => 'video_bg_img',
            'label'   => 'Video Background Image',
            'type'    => 'image',
            'value'   => 'https://images.unsplash.com/photo-1523131108800-e6b0e492bad4?ixlib=rb-1.2.1&auto=format&fit=crop&w=300&q=180'
          ),
        ),
        
      )
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name'     => 'heading_type',
      'label'    => 'Heading Type',
      'type'     => 'select',
      'value'    => 'h1',
      'options'  => array(
        'h1'   => 'h1',
        'h2'   => 'h2',
        'h3'   => 'h3',
        'h4'   => 'h4',
        'h5'   => 'h5',
        'h6'   => 'h6',
      ),
    ),
    array('label'=>'Title', 'type'=>'divider'),
    
    array(
      'name'   => 'section_title_size',
      'label'  => 'Title Size',
      'append' => 'px',
      'value'  => '@section_title_size'
    ),

    array(
      'name' => 'title_font',
      'type' => 'font', 
      'label' => 'Title Fonts'
    ),
    array(
      'name'     => 'title_font_weight',
      'label'    => 'Font Weight',
      'type'     => 'select',
      'value'    => '700',
      'options'  => array(
        '400'  => '400',
        '500'   => '500',
        '600'   => '600',
        '700'   => '700',
      ),
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'inherit',
      'options'  => array(
        'inherit' => 'Default',
        'lowercase'   => 'Lowercase',
        'uppercase'   => 'Uppercase',
        'capitalize'  => 'Capitalized'
      ),
    ),


    array(
      'name'     => 'title_alignment',
      'label'    => 'Title Alignment',
      'type'     => 'select',
      'value'    => 'center',
      'options'  => array(
        'left'      => 'Left',
        'center'    => 'Center',
        'right'     => 'Right',
        'justify'   => 'Justify',
      )
    ),

    array(
      'name' => 'desc_size',
      'label' => 'Desc Size',
      'append' => 'px',
      'value' => '18'
    ),

    array(
      'name'     => 'title_animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => '0',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  array('label'=>'Items', 'type'=>'divider'),
   array(
    'name'     => 'item_animation',
    'label'    => 'Animation Item',
    'type'     => 'select',
    'value'    => 'fadeInUp',
      'options'  => array(        
        '0'                     =>  'None',
        'fade'                  =>  'Fade',
        'scale-up'              =>  'Scale Up',
        'scale-down'            =>  'Scale Down',
        'slide-top-small'       =>  'Slide Top Small',
        'slide-bottom-small'    =>  'Slide Bottom Small',
        'slide-left-small'      =>  'Slide Left Small',
        'slide-right-small'     =>  'Slide Right Small',
        'slide-top-medium'      =>  'Slide Top Medium',
        'slide-bottom-medium'   =>  'Slide Bottom Medium',
        'slide-left-medium'     =>  'Slide Left Medium',
        'slide-right-medium'    =>  'Slide Right Medium',
        'slide-top'             =>  'Slide Top 100%',
        'slide-bottom'          =>  'Slide Bottom 100%',
        'slide-left'            =>  'Slide Left 100%',
        'slide-right'           =>  'Slide Right 100%'

      ),
    ),

  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(

    array(
      'name'    => 'bg_color',
      'label'   => 'Background Color',
      'type'    => 'colorpicker',
      'value'   => '#f5f5f5'
    ),

    array('label'=>'Title', 'type'=>'divider'),
    array(
      'name'  => 'section_title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#222'
    ),

    array(
      'name'  => 'desc_color',
      'label' => 'Desc Color',
      'type'  => 'colorpicker',
      'value' => '#666'
    ),

   array('label'=>'Button', 'type'=>'divider'), // Divider - Text
    array(
      'name'    => 'button_bg_color',
      'label'   => 'Bg Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array(
      'name'    => 'play_button_bg',
      'label'   => 'Play Button Background',
      'type'    => 'colorpicker',
      'value'   => '#e74c3c'
    ),
  ),

);
