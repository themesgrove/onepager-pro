#<?php echo $id; ?>{
	background-color : <?php echo $styles['bg_color'];?>;
}

#<?php echo $id;?> h1, #<?php echo $id;?> h2, #<?php echo $id;?> h3, #<?php echo $id;?> h4{
	font-family: <?php echo Onepager::fonts($settings['title_font']); ?>;
	font-weight:<?php echo $settings['title_font_weight'];?>;

}

#<?php echo $id;?> .section-heading .uk-heading-primary{ 
	font-size : <?php echo $settings['section_title_size'];?>px; 
	color : <?php echo $styles['section_title_color'];?>;
	line-height : <?php echo ($settings['section_title_size']) +10 ?>px;
}

#<?php echo $id;?> .section-heading .uk-text-lead{
	font-size : <?php echo $settings['desc_size'];?>px; 
	color : <?php echo $styles['desc_color'];?>;
}

#<?php echo $id;?> .op-video-big{
    height: 625px;
    border-radius: 10px;
}

#<?php echo $id;?> .op-video-small{
    border-radius: 10px;
    height: 152px;
}

#<?php echo $id; ?> .video-lightbox .op-video-big a{
    border-radius: 50px;
    width: 90px;
    height: 90px;
    line-height: 90px;
    font-size: 30px;
    text-align: center;
    background: <?php echo $styles['play_button_bg'];?>;
    position: relative;
}
#<?php echo $id; ?> .video-lightbox .op-video-big a:before{
	border: 3px solid #fff;
    border-radius: 100px;
    position: absolute;
    content: "";
    width: 105px;
    height: 105px;
    line-height: 105px;
    left: -8px;
    top: -8px;
    bottom: 0;
    right: 0;
}
#<?php echo $id; ?> .video-lightbox .op-video-small a{
    border-radius: 100px;
    width: 60px;
    height: 60px;
    line-height: 60px;
    font-size: 20px;
    text-align: center;
    color: #333;
    background: #fff;
    padding: 0;
    text-align: center;
    position:relative;
}
#<?php echo $id; ?> .video-lightbox .op-video-small a:before{
    border: 3px solid #fff;
    border-radius: 50px;
    position: absolute;
    content: "";
    border-radius: 50px;
    width: 72px;
    height: 72px;
    line-height: 72px;
    left: -6px;
    top: -6px;
    bottom: 0;
    right: 0;
}
#<?php echo $id; ?> .video-lightbox .op-video-small i{
    color: #333;
}
#<?php echo $id; ?> .video-lightbox a i{
	color: <?php echo $styles['button_bg_color'];?>;
}
@media(max-width:768px){
	#<?php echo $id?> .section-heading .uk-heading-primary{
		font-size : <?php echo ($settings['section_title_size']/2) +10 ?>px;
		line-height : <?php echo ($settings['section_title_size']/2) +15 ?>px;
	}
}
