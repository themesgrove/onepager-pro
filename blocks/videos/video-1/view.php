<?php
	// items animation
	$title_animation = ($settings['title_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['title_animation'].';' : '';
	$item_animation = ($settings['item_animation']) ? 'uk-scrollspy="cls:uk-animation-'.$settings['item_animation'].';' : '';
?>
<section id="<?php echo $id;?>" class="uk-section uk-position-relative videos pro-videos-1 uk-background-cover uk-background-norepeat">
	<div class="uk-container">

	    <div class="section-heading uk-padding-large uk-text-center uk-padding-remove-top">
	        <?php if($contents['title']):?>
              <!-- Section Title -->
				<?php
					echo op_heading( 
						$contents['title'],
						$settings['heading_type'], 
						'uk-heading-primary', 
						'uk-text-' . $settings['title_transformation'], 
						$title_animation . '"'
					); 
				?>
            <?php endif; ?>

            <?php if($contents['description']):?>
                <div class="uk-text-lead" <?php echo ($settings['title_animation']) ? $title_animation .';delay:' .'300"' : ''?>><?php echo $contents['description']?></div>
	        <?php endif; ?>
	    </div> <!-- Section heading -->

		<div class="uk-grid-match uk-flex-middle" uk-grid>
			<?php $i = 0; ?>
			<?php foreach($contents['items'] as $feature): ?>
				<div class="uk-width-1-1@m">
					<div class="video-lightbox uk-position-relative" uk-lightbox>
					    <div class="uk-card" <?php echo ($settings['item_animation'] ? $item_animation . ';delay:' . '300"' : ''); ?>>
							<div class="op-video-big uk-flex uk-flex-middle uk-flex-center uk-background-cover uk-background-norepeat uk-background-center-center" style="background-image: url(<?php echo $feature['video_bg_img'];?>);">
							    <div class="uk-text-center">
					            	<a class="uk-button" href="<?php echo $feature['video_link'];?>"><i class="fa fa-play"></i></a>
					            </div>
					        </div>
						</div> <!-- uk-card -->
					</div> <!-- uk-lightbox -->
				</div>
			<?php  if ($i == 0) { break; } ?>
			<?php $i++; endforeach; ?>
	
		</div> <!-- uk-grid -->

		<div class="uk-child-width-expand@s uk-text-center" uk-grid uk-height-match="target: > div > .uk-card">
			<?php $count = 0; ?>
			<?php foreach($contents['items'] as $feature): ?>
			<?php
			    $count++; // Note that first iteration is $count = 1 not 0 here.
			    if($count <= 1) continue; 
			?>
		    <div class="op-video" <?php echo ( $settings['item_animation'] ? $item_animation . 'delay:300"' : '' ); ?>>
					<div class="video-lightbox uk-position-relative" uk-lightbox>
					    <div class="uk-card" <?php echo ($settings['item_animation'] ? $item_animation . ';delay:' . '300"' : ''); ?>>

							<div class="op-video-small uk-flex uk-flex-middle uk-flex-center uk-background-cover uk-background-norepeat uk-background-center-center" style="background-image: url(<?php echo $feature['video_bg_img'];?>);">
							    <div class="uk-text-center">
					            	<a class="uk-button" href="<?php echo $feature['video_link'];?>"><i class="fa fa-play"></i></a>
					            </div>
					        </div>
						</div> <!-- uk-card -->
					</div> <!-- uk-lightbox -->
		    </div>

		    <?php $i++; endforeach; ?>
		</div>

	</div> <!-- uk-container -->
</section> <!-- fp-section -->