const { series, src, dest } = require('gulp');
const fs = require('fs');
const del = require('del');

// Utility
const plumber = require('gulp-plumber'); // Prevent pipe breaking caused by errors from gulp plugins.
const gulpZip = require('gulp-zip');

// The `clean` function is not exported so it can be considered a private task.
function clean(cb) {
  return del('temp/*')
  cb();
}
// Copy files to /temp dir
function copy(cb){
  return src([
      './**',
      './*/**',
      '!./node_modules/**',
      '!./node_modules',
      '!gulpfile.js',
      '!package.json',
      '!./assets',
      '!*.json',
      '!*.config.js',
      '!*.lock',
      '!*.gitignore',
  ]).pipe(dest('temp/onepager-pro'));
  cb();
}
// Create release
function package(cb){
  const version = getVersion();
  const fileName = 'onepager-pro-' + version + '.zip' 

  return src(['./temp/**', './temp/*/**',])
        .pipe(plumber())
        .pipe(gulpZip(fileName))
        .pipe(dest('temp'))
  cb();
}

// get the version number of plugin
function getVersion(){
  const p = fs.readFileSync('onepager-pro.php', 'utf-8');
  return p.split("\n")[6].split(":")[1].trim();
}

exports.release = series(clean, copy, package);
// exports.default = series(clean, copy, release);
