<?php
// Include settings panel
require ONEPAGER_PRO_PATH . 'includes/settings.php';
// Include licence checker
include ONEPAGER_PRO_PATH . 'license/admin.php';
$groups = ['Pro Blocks'];
  
// Reg blocks : $path , $url, $groups
onepager()->blockManager()->loadAllFromPath( ONEPAGER_PRO_BLOCKS_PATH, ONEPAGER_PRO_URL . '/blocks', $groups);
// Reg presets : $path, $url, $groups
onepager()->presetManager()->loadAllFromPath( ONEPAGER_PRO_PRESET_PATH, ONEPAGER_PRO_URL . '/presets', $groups);

// Remove GoPro button
add_action( 'admin_init', 'remove_go_pro_menu' );
function remove_go_pro_menu() {
  remove_submenu_page( 'onepager', 'onepager-gopro' );
}