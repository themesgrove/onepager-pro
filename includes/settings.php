<?php
// this file should be in the theme
Onepager::getOptionPanel()
        ->tab( 'general', 'Generals' )
        ->add(
          array(
            'name'   => 'full_screen',
            'label'  => 'Full Screen',
            'type'   => 'switch'
          )
        )
        ->tab( 'styles', 'Styles' )
        ->add()
        ->tab( 'advanced' )
        ->add();

// Onepager::basePreset( array(
//   'primary'   => "#1565c0",
//   'secondary' => '#0a3d78',
//   'accent'    => '#E64A19',
// ) );
//
// Onepager::addPresets( "default", array(
//   [ 'primary' => 'red', 'secondary' => 'yellow' ],
//   [ 'primary' => 'green', 'secondary' => 'yellow' ],
// ) );

add_action( 'wp_head', function () { ?>
  
  <!--  Full pager-->
  <?php if ( Onepager::getOption( 'full_screen' ) ): ?>
  <script>
    jQuery(document).ready(function ($) {
      // UIkit.container = '.uk-section *';
      $(".op-sections").fullpage({
        sectionSelector: ".fp-section",
        css3: true,
        navigation: true,
        scrollOverflow: true,
        lazyLoading: false,

        afterLoad: function(origin, destination, direction){
          UIkit.scrollspy('#' + destination.item.id + ' [uk-scrollspy]', {inview: true, outview: true});
        }
      });
    });
  </script>
  <?php endif; ?>
<?php }, 100 );

// Enabled Fullpage slide
if ( Onepager::getOption( 'full_screen' ) ) {
  add_action( 'wp_enqueue_scripts', function () {
    $q = onepager()->asset();
    // $q->script( 'op-slimscroll', op_asset( 'assets/js/jquery.slimscroll.min.js' ) );
    
    $q->script( 'op-fullpage-ext-overflow', op_asset( 'assets/js/scrolloverflow.js' ) );
    $q->script( 'op-fullpage', op_asset( 'assets/js/fullpage.js' ) );

    $q->style( 'op-fullpage', op_asset( 'assets/css/fullpage.css' ) );
  } );
}
