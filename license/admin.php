<?php

// this is the URL our updater / license checker pings.
define( 'ONEPAGER_PRO_STORE_URL', 'https://themesgrove.com' ); 

// the download ID 
define( 'PRODUCT_NAME', 'WPOnepager Pro' ); 
define( 'ONEPAGER_PRO_ITEM_ID', 22128 ); 

// the name of the settings page for the license input to be displayed
define( 'ONEPAGER_PRO_LICENSE_PAGE', 'onepager-license' );

if( !class_exists( 'Onepager_Updater' ) ) {
	// load our custom updater
	include( dirname( __FILE__ ) . '/updater.php' );
}

// All error message
function get_errors_msg(){
  $license_page_link = admin_url( 'admin.php?page=' . ONEPAGER_PRO_LICENSE_PAGE );
  $renew_url = 'https://themesgrove.com/account/';

  $errors = [
    'expired' => [
      'title' => __( 'Your License Has Expired', 'elementor-pro' ),
      'description' => sprintf( __( '<a href="%s" target="_blank">Renew your license today</a>, to keep getting feature updates, premium support and unlimited access to the template library.', 'onepager-pro' ), $renew_url ),
      'button_text' => __( 'Renew License', 'onepager-pro' ),
      'button_url' => $renew_url,
    ],
    'disabled' => [
      'title' => __( 'Your License Is Inactive', 'onepager-pro' ),
      'description' => __( '<strong>Your license key has been cancelled</strong> (most likely due to a refund request). Please consider acquiring a new license.', 'onepager-pro' ),
      'button_text' => __( 'Activate License', 'onepager-pro' ),
      'button_url' => $license_page_link,
    ],
    'invalid' => [
      'title' => __( 'License Invalid', 'onepager-pro' ),
      'description' => __( '<strong>Your license key doesn\'t match your current domain</strong>. This is most likely due to a change in the domain URL of your site (including HTTPS/SSL migration). Please deactivate the license and then reactivate it again.', 'onepager-pro' ),
      'button_text' => __( 'Reactivate License', 'onepager-pro' ),
      'button_url' => $license_page_link,
    ],
    'site_inactive' => [
      'title' => __( 'License Mismatch', 'onepager-pro' ),
      'description' => __( '<strong>Your license key doesn\'t match your current domain</strong>. This is most likely due to a change in the domain URL. Please deactivate the license and then reactivate it again.', 'onepager-pro' ),
      'button_text' => __( 'Reactivate License', 'onepager-pro' ),
      'button_url' => $license_page_link,
    ],
    'inactive' => [
      'title' => __( 'License Mismatch', 'onepager-pro' ),
      'description' => __( '<strong>Your license key doesn\'t match your current domain</strong>. This is most likely due to a change in the domain URL. Please deactivate the license and then reactivate it again.', 'onepager-pro' ),
      'button_text' => __( 'Reactivate License', 'onepager-pro' ),
      'button_url' => $license_page_link,
    ],
  ];
  return $errors;
}


function onepager_pro_updater() {

	// retrieve our license key from the DB
	$license_key = get_license_key();

	// setup the updater
	$edd_updater = new Onepager_Updater( ONEPAGER_PRO_STORE_URL, ONEPAGER_PRO_PLUGIN_BASE,
		array(
			'version' => '1.0.0',                    // current version number
			'license' => $license_key,             // license key (used get_option above to retrieve from DB)
			'item_id' => ONEPAGER_PRO_ITEM_ID,       // ID of the product
			'author'  => 'Themesgrove', // author of this plugin
			'beta'    => false,
		)
	);

}
add_action( 'admin_init', 'onepager_pro_updater', 0 );

// Get license key
function get_license_key() {
  return trim( get_option( 'onepager_pro_license_key' ) );
}
// set license key
function set_license_key( $license_key ) {
  return update_option( 'onepager_pro_license_key', $license_key );
}


/************************************
* Add license menu
*************************************/

function onepager_pro_register_license_page() {
  add_submenu_page(
    'onepager', 
    __( 'License', 'onepager' ),
    __( 'License', 'onepager' ),
    'manage_options', 
    ONEPAGER_PRO_LICENSE_PAGE,
    'onepager_pro_license_page'
  );
}
add_action('admin_menu', 'onepager_pro_register_license_page', 800);

/************************************
* Add license page
*************************************/

function onepager_pro_license_page() {
	$license_key = get_license_key();
  $status  = get_option( 'onepager_pro_license_status' );

	?>
	<div class="wrap">
		<form method="post" action="options.php">
      <?php wp_nonce_field( 'onepager-pro-license' ); ?>

			<?php //settings_fields('onepager_pro_license'); ?>

      <div class="uk-card uk-card-default uk-width-2-3@m"">
        <div class="uk-card-header">  
          <?php if( $status !== 'valid'): ?>
            <h3 class="uk-card-title uk-margin-remove-bottom"><?php _e('Activate Your License', 'onepager-pro'); ?></h3>
          <?php else:?>
            <h3 class="uk-card-title uk-margin-remove-bottom">
              <?php _e('Status', 'onepager-pro'); ?>: 
              <?php if ( 'expired' === $status ) : ?>
                <span style="color: #ff0000; font-style: italic;"><?php _e( 'Expired', 'onepager-pro' ); ?></span>
              <?php elseif ( 'site_inactive' === $status ) : ?>
                <span style="color: #ff0000; font-style: italic;"><?php _e( 'Mismatch', 'onepager-pro' ); ?></span>
              <?php elseif ( 'invalid' === $status ) : ?>
                <span style="color: #ff0000; font-style: italic;"><?php _e( 'Invalid', 'onepager-pro' ); ?></span>
              <?php elseif ( 'disabled' === $status ) : ?>
                <span style="color: #ff0000; font-style: italic;"><?php _e( 'Disabled', 'onepager-pro' ); ?></span>
              <?php else : ?>
                <span style="color: #008000; font-style: italic;"><?php _e( 'Active', 'elementor-pro' ); ?></span>
              <?php endif; ?>
            </h3>
          <?php endif;?>
        </div>

        <div class="uk-card-body">
          <?php if ( empty($license_key) ) : ?>
            <p><?php _e( 'Enter your license key here, to activate WPOnepager Pro, and get feature updates, premium support and unlimited access to the template library.', 'onepager-pro' ); ?></p>
            <ol>
              <li><?php printf( __( 'Log in to <a href="%s" target="_blank">your account</a> to get your license key.', 'onepager-pro' ), 'https://themesgrove.com/' ); ?></li>
              <li><?php printf( __( 'If you don\'t yet have a license key, <a href="%s" target="_blank">get WPOnepager Pro now</a>.', 'onepager-pro' ), 'https://themesgrove.com/wp-onepager/' ); ?></li>
              <li><?php _e( 'Copy the license key from your account and paste it below.', 'onepager-pro' ); ?></li>
            </ol>
  
            <h4><label class="description" for="onepager_pro_license_key"><?php _e('Your license key'); ?></label></h4>

            <div class="uk-grid-small uk-grid">
              <div class="uk-width-3-4@s">
                <input id="onepager_pro_license_key" name="onepager_pro_license_key" type="text" class="uk-input" value="<?php esc_attr_e( $license_key ); ?>" placeholder="<?php _e('Place your license key here', 'onepager-pro'); ?>" />
              </div>
              <div class="uk-width-1-4@s">
                <input type="submit" class="uk-button uk-button-primary" name="onepager_pro_license_activate" value="<?php _e('Activate License', 'onepager-pro'); ?>"/>
              </div>
            </div>
            <p class="uk-text-meta"><?php _e( 'Your license key should look something like this: <code>fb351f05958872E193feb37a505a84be</code>', 'onepager-pro' ); ?></p>
          <?php else:?>

            <?php if ( 'expired' === $status ) : ?>
              <p class="uk-alert uk-alert-danger uk-margin-medium-bottom"><?php printf( __( '<strong>Your License Has Expired.</strong> <a href="%s" target="_blank">Renew your license today</a> to keep getting feature updates, premium support and unlimited access to the template library.', 'onepager-pro' ), 'https://themesgrove.com/account/' ); ?></p>
            <?php endif; ?>

              <?php if ( 'site_inactive' === $status ) : ?>
              <p class="uk-alert uk-alert-danger uk-margin-medium-bottom"><?php echo __( '<strong>Your license key doesn\'t match your current domain</strong>. This is most likely due to a change in the domain URL of your site (including HTTPS/SSL migration). Please deactivate the license and then reactivate it again.', 'onepager-pro' ); ?></p>
            <?php endif; ?>

              <?php if ( 'invalid' === $status ) : ?>
              <p class="uk-alert uk-alert-primary uk-margin-medium-bottom"><?php echo __( '<strong>Your license key doesn\'t match your current domain</strong>. This is most likely due to a change in the domain URL of your site (including HTTPS/SSL migration). Please deactivate the license and then reactivate it again.', 'onepager-pro' ); ?></p>
            <?php endif; ?>

            <div class="uk-grid uk-flex-middle">
              <div class="uk-width-2-3@m">
                <p>Want to deactivate the license for any reason?</p>
              </div>
              <div class="uk-width-1-3@m uk-text-right">
                <input type="submit" class="uk-button uk-button-danger uk-button-small" name="onepager_pro_license_deactivate" value="<?php _e('Deactivate License'); ?>"/>
              </div>
            </div>
          <?php endif;?>
        </div>
      </div>
		</form>
	<?php
}

// function edd_sample_register_option() {
// 	// creates our settings in the options table
// 	register_setting('onepager_pro_license', 'onepager_pro_license_key', 'edd_sanitize_license' );
// }
// add_action('admin_init', 'edd_sample_register_option');

// function edd_sanitize_license( $new ) {
// 	$old = get_option( 'onepager_pro_license_key' );
// 	if( $old && $old != $new ) {
// 		delete_option( 'onepager_pro_license_status' ); // new license has been entered, so must reactivate
// 	}
// 	return $new;
// }



/************************************
* Activate license
*************************************/

function onepager_action_activate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['onepager_pro_license_activate'] ) ) {

		// run a quick security check
    check_admin_referer( 'onepager-pro-license' );

    if ( empty( $_POST['onepager_pro_license_key'] ) ) {
      wp_die( __( 'Please enter your license key.', 'onepager-pro' ), __( 'WPOnepager Pro', 'onepager-pro' ), [
        'back_link' => true,
      ] );
    }

    $license_key = $_POST['onepager_pro_license_key'];


		// data to send in our API request
		$api_params = array(
			'edd_action' => 'activate_license',
			'license'    => $license_key,
			'item_name'  => urlencode( PRODUCT_NAME ), // the name of our product in EDD
			'url'        => home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( ONEPAGER_PRO_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
    
		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = __( 'An error occurred, please try again.' );
			}

		} else {

      $license_data = json_decode( wp_remote_retrieve_body( $response ) );

			if ( false === $license_data->success ) {

				switch( $license_data->error ) {

					case 'expired' :

						$message = sprintf(
							__( 'Your license key expired on %s.' ),
							date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
						);
						break;

					case 'disabled' :
					case 'revoked' :

						$message = __( 'Your license key has been disabled.' );
						break;

					case 'missing' :

						$message = __( 'Invalid license.' );
						break;

					case 'invalid' :
          case 'site_inactive' :

						$message = __( 'Your license is not active for this URL.' );
						break;

					case 'item_name_mismatch' :

						$message = sprintf( __( 'This appears to be an invalid license key for %s.' ), PRODUCT_NAME );
						break;

					case 'no_activations_left':

						$message = __( 'Your license key has reached its activation limit.' );
						break;

					default :

						$message = __( 'An error occurred, please try again.' );
						break;
				}

			}

    }

		// Check if anything passed on a message constituting a failure
		if ( ! empty( $message ) ) {
			$base_url = admin_url( 'admin.php?page=' . ONEPAGER_PRO_LICENSE_PAGE );
			$redirect = add_query_arg( array( 'onepager_pro_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );

			wp_redirect( $redirect );
			exit();
		}

    // $license_data->license will be either "valid" or "invalid"
    
    set_license_key($license_key);
		update_option( 'onepager_pro_license_status', $license_data->license );
		wp_redirect( admin_url( 'admin.php?page=' . ONEPAGER_PRO_LICENSE_PAGE ) );
		exit();
	}
}
add_action('admin_init', 'onepager_action_activate_license');


/***********************************************
* deactivate license key.
***********************************************/

function onepager_action_deactivate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['onepager_pro_license_deactivate'] ) ) {
    
		// run a quick security check
    check_admin_referer( 'onepager-pro-license');

		// retrieve the license from the database
		$license = get_license_key();

		// data to send in our API request
		$api_params = array(
			'edd_action' => 'deactivate_license',
			'license'    => $license,
			'item_name'  => urlencode( PRODUCT_NAME ), // the name of our product in EDD
			'url'        => home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( ONEPAGER_PRO_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
    
		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = __( 'An error occurred, please try again.' );
			}

			$base_url = admin_url( 'plugins.php?page=' . ONEPAGER_PRO_LICENSE_PAGE );
			$redirect = add_query_arg( array( 'onepager_pro_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );

			wp_redirect( $redirect );
			exit();
		}

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// $license_data->license will be either "deactivated" or "failed"
		if( $license_data->license == 'deactivated' or $license_data->license == 'failed' ) {
			delete_option( 'onepager_pro_license_key' );
			delete_option( 'onepager_pro_license_status' );
		}

		wp_redirect( admin_url( 'admin.php?page=' . ONEPAGER_PRO_LICENSE_PAGE ) );
		exit();

	}
}
add_action('admin_init', 'onepager_action_deactivate_license');


/************************************
* this illustrates how to check if
* a license key is still valid
* the updater does this for you,
* so this is only needed if you
* want to do something custom
*************************************/

function onepager_pro_check_license() {

	global $wp_version;

	$license = get_license_key();

	$api_params = array(
		'edd_action' => 'check_license',
		'license' => $license,
		'item_name' => urlencode( PRODUCT_NAME ),
		'url'       => home_url()
	);

	// Call the custom API.
	$response = wp_remote_post( ONEPAGER_PRO_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

	if ( is_wp_error( $response ) )
		return false;

	$license_data = json_decode( wp_remote_retrieve_body( $response ) );

	// if( $license_data->license == 'valid' ) {
	// 	echo 'valid'; exit;
	// 	// this license is still valid
	// } else {
	// 	echo 'invalid'; exit;
  //   // this license is no longer valid

  // }
  update_option('onepager_pro_license_status', $license_data->license);
}
add_action('admin_init', 'onepager_pro_check_license');

// Function to print admin notices
function onepager_pro_print_admin_msg($title, $description, $button_text='', $button_url='#', $button_class=''){
  ?>
  <div class="wrap">
    <div class="uk-card uk-card-small uk-card-default uk-card-body uk-margin uk-margin-medium-top">
      <div class="uk-grid uk-grid-small uk-flex-middle">
        <div class="uk-width-2-3">
          <div class="uk-grid uk-grid-small">
            <div class="uk-width-1-6 uk-first-column">
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                width="60px" height="40px" viewBox="251 356 110 80" enable-background="new 251 356 110 80" xml:space="preserve" fill="#007bff">
                <g>
                  <path d="M291.9,433.6l-25.1-67.7c0.3-2.5,2.4-3,4.6-3.1c2.2-0.2,49-1.3,49-1.3c5,0,6.2,1.3,7.5,3.3c1.3,2,17.8,30.6,17.8,30.6
                    c1.5,3.1-1.4,3.6-3.5,3.8c-3.1,0.4-48.8,4.6-48.8,4.6l4.6,12.1l54.9-5.6c8.8-2.6,8-9,6.2-12.3c-1.8-3.2-25-37.8-25-37.8
                    c-2.7-3.6-7.7-4.3-11.7-4.3c-4-0.1-57.5-0.3-57.5-0.3c-12.8,0.1-14.5,4.6-14.6,8.3c-0.1,3.6,21.1,71.7,21.1,71.7L291.9,433.6z"/>
                  <path d="M316.5,368.7L316.5,368.7c-0.2,0-25.3,1.3-26.2,1.3c-1,0.1-1.1,0.3-1.1,0.3s-0.1,0.2,0.1,0.7l6.6,15.9
                    c0,0.1,0.7,1.6,2.5,1.5c7.2-0.3,27.5-1.9,28.6-2.1c0.8-0.1,0.9-0.3,0.9-0.3s0-0.2-0.3-0.7l0,0c-2.3-4.1-8.2-14.9-8.7-15.5
                    C318.6,369.1,318.3,368.7,316.5,368.7z"/>
                  <path d="M325.8,389.3c0,0-0.2-0.4-0.5-1c-5.1,0.5-21,1.7-27.2,2c-0.1,0-0.1,0-0.2,0c-2.7,0-3.7-2.3-3.7-2.4l-6-14.2
                    c-0.2,0-0.4,0-0.4,0c-2,0.1-2.1,1-1.7,2l6.3,15.2c0,0.1,1.1,2.6,4,2.6c0.1,0,0.1,0,0.2,0l28-2.1C325.4,391.4,326.9,391,325.8,389.3
                    z"/>
                </g>
              </svg>
            </div>
            <div class="uk-width-5-6">
              <p class="uk-margin-remove-bottom"><strong><?php echo $title; ?></strong></p>
              <p class="uk-margin-remove-top"><?php echo $description; ?></p>
            </div>
          </div>

        </div>
        <div class="uk-width-1-3">
          <a href="<?php echo $button_url?>" class="uk-button uk-button-primary"><?php echo $button_text?></a>
        </div>
      </div>
    </div>
  </div>
  <?php
}
/**
 * This is a means of catching errors from the activation method above and displaying it to the customer
 */
function onepager_pro_admin_notices() {
  $license_key = get_license_key();
  $status  = get_option( 'onepager_pro_license_status' );
  $errors = get_errors_msg();
  
  if( $license_key ){
    if('valid' !== $status){
      onepager_pro_print_admin_msg(
        '<span class="uk-text-danger">'. $errors[$status]['title'] . '</span>', 
        $errors[$status]['description'],
        $errors[$status]['button_text'],
        $errors[$status]['button_url']
      );
    }
  }else{
    $license_page = self_admin_url('admin.php?page=onepager-license');
    onepager_pro_print_admin_msg(
      __('Welcome to WPOnepager Pro!'),
      __('Please activate your license to get feature updates, premium support and unlimited access to the template library.'),
      __('Activate License'),
      $license_page
    );
  }
  

	if ( isset( $_GET['onepager_pro_activation'] ) && ! empty( $_GET['message'] ) ) {

		switch( $_GET['onepager_pro_activation'] ) {

			case 'false':
				$message = urldecode( $_GET['message'] );
				?>
				<div class="uk-alert uk-alert-danger">
          <a class="uk-alert-close" uk-close></a>
					<p><?php echo $message; ?></p>
				</div>
				<?php
				break;

      case 'true':
      ?>
				<div class="uk-alert uk-alert-success">
					<p><?php echo $message; ?></p>
				</div>
      <?php
			default:
				// Developers can put a custom success message here for when activation is successful if they way.
				break;

		}
	}
}
add_action( 'admin_notices', 'onepager_pro_admin_notices' );
