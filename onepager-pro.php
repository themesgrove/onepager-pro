<?php
/**
 * Plugin Name: WPOnepager Pro
 * Description: Build outstanding one page website with WPOnepager Pro.
 * Plugin URI: https://themesgrove.com/wp-onepager
 * Author: Themesgrove
 * Version: 1.1.4
 * Author URI: https://themesgrove.com/
 *
 * Text Domain: onepager-pro
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
// Let's define some const
define( 'ONEPAGER_PRO_VERSION', '1.1.4' );

define('ONEPAGER_PRO__FILE__', __FILE__);
define('ONEPAGER_PRO_PLUGIN_BASE', plugin_basename(ONEPAGER_PRO__FILE__));
define('ONEPAGER_PRO_PATH', plugin_dir_path(ONEPAGER_PRO__FILE__));
define('ONEPAGER_PRO_BLOCKS_PATH', ONEPAGER_PRO_PATH . 'blocks/');
define('ONEPAGER_PRO_PRESET_PATH', ONEPAGER_PRO_PATH . 'presets/');
define('ONEPAGER_PRO_URL', plugins_url('/', ONEPAGER_PRO__FILE__));

// Lets load the plugin
function onepager_pro_load()
{
    load_plugin_textdomain('onepager-pro');

    if (!did_action('onepager_loaded')) {
        add_action('admin_notices', 'onepager_pro_fail');

        return;
    }

    $onepager_version_required = '2.2.0';
    if (!version_compare(ONEPAGER_VERSION, $onepager_version_required, '>=')) {
        add_action('admin_notices', 'onepager_pro_fail_load_out_of_date');

        return;
    }

    $onepager_version_recommendation = '2.2.0';
    if (!version_compare(ONEPAGER_VERSION, $onepager_version_recommendation, '>=')) {
        add_action('admin_notices', 'onepager_pro_admin_notice_upgrade_recommendation');
    }

    require ONEPAGER_PRO_PATH . 'includes/functions.php';
}
add_action('plugins_loaded', 'onepager_pro_load');

// Function to check why plugin loading failed
function onepager_pro_fail()
{
    $screen = get_current_screen();
    if (isset($screen->parent_file) && 'plugins.php' === $screen->parent_file && 'update' === $screen->id) {
        return;
    }

    $plugin = 'tx-onepager/tx-onepager.php';
    /**
     * 1) We'll check OnePager activation status
     * 2) Then we'll ask to install onepager
     */
    if (check_onepager_installation()) {
        if (!current_user_can('activate_plugins')) {
            return;
        }

		add_option('onepager_pro_coming', true);

        $activation_url = wp_nonce_url('plugins.php?action=activate&amp;plugin=' . $plugin . '&amp;plugin_status=all&amp;paged=1&amp;s', 'activate-plugin_' . $plugin);

        $message = '<p>' . __('OnePager Pro is not working because you need to activate the Onepager plugin.', 'onepager-pro') . '</p>';
        $message .= '<p>' . sprintf('<a href="%s" class="button-primary">%s</a>', $activation_url, __('Activate Onepager Now', 'onepager-pro')) . '</p>';
    } else {
        if (!current_user_can('install_plugins')) {
            return;
        }

        // $install_url = wp_nonce_url( self_admin_url( 'update.php?action=upgrade-plugin&plugin=tx-onepager' ), 'install-plugin_onepager' );
        $install_url = wp_nonce_url(self_admin_url('plugin-install.php?tab=plugin-information&plugin=tx-onepager&TB_iframe=true&width=600&height=500'), 'install-plugin_onepager');

        $message = '<p>' . __('OnePager Pro is not working because you need to install the OnePager plugin.', 'onepager-pro') . '</p>';
        $message .= '<p>' . sprintf('<a href="%s" class="button-primary thickbox open-plugin-details-modal">%s</a>', $install_url, __('Install OnePager Now', 'onepager-pro')) . '</p>';
    }
    // deactivate plugin to avoid further collison
    deactivate_plugins(ONEPAGER_PRO_PLUGIN_BASE);
    echo '<div class="error"><p>' . $message . '</p></div>';
}

// Onepager version compatibility check
function onepager_pro_fail_load_out_of_date()
{
    if (!current_user_can('update_plugins')) {
        return;
    }

    $file_path = 'tx-onepager/tx-onepager.php';

    $upgrade_link = wp_nonce_url(self_admin_url('update.php?action=upgrade-plugin&plugin=') . $file_path, 'upgrade-plugin_' . $file_path);
    $message = '<p>' . __('OnePager Pro is not working because you are using an old version of OnePager.', 'onepager-pro') . '</p>';
    $message .= '<p>' . sprintf('<a href="%s" class="button-primary">%s</a>', $upgrade_link, __('Update OnePager Now', 'onepager-pro')) . '</p>';

    echo '<div class="error">' . $message . '</div>';
}

// Onepager new version recommendation

function onepager_pro_admin_notice_upgrade_recommendation()
{
    if (!current_user_can('update_plugins')) {
        return;
    }

    $file_path = 'tx-onepager/tx-onepager.php';

    $upgrade_link = wp_nonce_url(self_admin_url('update.php?action=upgrade-plugin&plugin=') . $file_path, 'upgrade-plugin_' . $file_path);
    $message = '<p>' . __('A new version of OnePager is available. For better performance and compatibility of OnePager Pro, we recommend updating to the latest version.', 'onepager-pro') . '</p>';
    $message .= '<p>' . sprintf('<a href="%s" class="button-primary">%s</a>', $upgrade_link, __('Update OnePager Now', 'onepager-pro')) . '</p>';

    echo '<div class="error">' . $message . '</div>';
}

// Check onepager core exists or not
if (!function_exists('check_onepager_installation')) {
    function check_onepager_installation()
    {
        $file_path = 'tx-onepager/tx-onepager.php';
        $installed_plugins = get_plugins();

        return isset($installed_plugins[$file_path]);
    }
}

// add_action( 'template_redirect', 'wpse_inspect_page_id' );
// function wpse_inspect_page_id() {
//     $page_object = get_queried_object();
//     // var_dump($page_object);

//     $page_id = get_queried_object_id();
//     echo $page_id;
// }

do_action('onepager_pro_loaded');

// Activation hook
register_activation_hook(__FILE__, 'onepager_pro_activation_hook');
function onepager_pro_activation_hook()
{
    add_option('onepagerpro_activated', true);
}

/**
* redirect to the installation page
* after active the plugin
*/
add_action('admin_init', 'onepagerpro_redirect');
function onepagerpro_redirect()
{
    if (get_option('onepagerpro_activated', false) && get_option('onepager_pro_coming', false)) {
        delete_option('onepagerpro_activated');
        delete_option('onepager_pro_coming');
        wp_redirect(admin_url('admin.php?page=onepager-getting-started'));
    }
}
